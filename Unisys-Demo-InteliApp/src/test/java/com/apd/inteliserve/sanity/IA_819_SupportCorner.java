package com.apd.inteliserve.sanity;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */

public class IA_819_SupportCorner extends BaseTest{
	@Test
	@Parameters({"platform"})
	public void supportCornerTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 240);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-819, Favrouite Module-Support Corner Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-819, Favrouite Module-Support Corner Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			click(fm.clickCoreSupportButton());
			extentTest.log(LogStatus.PASS, "Clicked  on Core Support Buton");
			if(elementIsDisplayed(fm.verifyHeaderText())){
				extentTest.log(LogStatus.PASS, "Core Support Header Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Core Support Header Text is Not Displayed");
			}
			if(elementIsDisplayed(fm.verifyItText())){
				extentTest.log(LogStatus.PASS, "It text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "It text is Not Displayed");
			}
			if(elementIsDisplayed(fm.verifyItContentText())){
				extentTest.log(LogStatus.PASS, "IT Context Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "IT Context Text is Not Displayed");
			}
			if(elementIsDisplayed(fm.emailButton())){
				extentTest.log(LogStatus.PASS, "Email Button is Present");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Email Button is Not Present");
			}
			if(elementIsDisplayed(fm.chatButton())){
				extentTest.log(LogStatus.PASS, "Chat Button is Present");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Chat Button is Not Present");
			}
			if(elementIsDisplayed(fm.callButton())){
				extentTest.log(LogStatus.PASS, "Call Button is Present");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Call Button is Not Present");
			}
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Favrouite Module-Support Corner Test Failed");
		}
	}
}
