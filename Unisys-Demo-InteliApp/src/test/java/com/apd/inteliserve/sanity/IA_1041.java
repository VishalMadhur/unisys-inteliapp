package com.apd.inteliserve.sanity;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.ProfileScreenLegalPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */

public class IA_1041 extends BaseTest {
	@Test
	@Parameters({"platform"})
	public void signOutTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 240);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-1041, LogOut Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-1041, LogOut Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			ProfileScreenLegalPage pl= new ProfileScreenLegalPage(driver);
			click(pl.profileIcon());
			extentTest.log(LogStatus.PASS, "User is Able to Navigate to Profile Icon");
			scroll(platform, "Sign Out");
			if(elementIsDisplayed(pl.signOutLink())){
				extentTest.log(LogStatus.PASS, "Sign Out Link is Present in Profile Page");
				click(pl.signOutLink());
				extentTest.log(LogStatus.PASS, "Clicked on Sign Out Link");
			}
			if(elementIsDisplayed(pl.confirmButton()) && elementIsDisplayed(pl.cancelButton())){
				extentTest.log(LogStatus.PASS, "Pop-up Appears After Clicking Sign Out Link");
			}
			else{
				extentTest.log(LogStatus.FAIL, "No Pop-up Appears After Clicking Sign Out Link");
			}
			click(pl.cancelButton());
			extentTest.log(LogStatus.PASS, "Clicked on Cancel Button");
			if(elementIsDisplayed(pl.profileHeaderText())){
				extentTest.log(LogStatus.PASS, "Clicking On Confirm Button Closes The Pop-up And Returns to Profile Page");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Profile Header Text is Not Displayed");
			}
			click(pl.signOutLink());
			extentTest.log(LogStatus.PASS, "Clicked on Sign Out Link");
			click(pl.confirmButton());
			extentTest.log(LogStatus.PASS, "Clicked on Confirm Button");
			//if(elementIsDisplayed(pl.emailTextBox()) && elementIsDisplayed(pl.firstNameTextBox()) && elementIsDisplayed(pl.lastNameTextBox())){
			if(elementIsDisplayed(pl.emailTextBox())){
				extentTest.log(LogStatus.PASS, "Log Out Happend SuccessFully And Login Screen Contains No Pre-Filled Data");
			}
			else{
				extentTest.log(LogStatus.PASS, "Fail To Log Out  SuccessFully");
			}
			//loginWithOutEnablingLocation();
			login(platform);
			if(platform.equalsIgnoreCase("android")){
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
			}
			else{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
			}
			if(elementIsDisplayed(pl.verifyHelloText())){
				extentTest.log(LogStatus.PASS, "Logged In Happend Successfully");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Failed to Logged In");
			}
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("SignOut Test Failed");
		}
	}
}

