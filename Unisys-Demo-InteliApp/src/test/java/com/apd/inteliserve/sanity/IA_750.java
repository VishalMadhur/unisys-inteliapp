package com.apd.inteliserve.sanity;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.general.Lib;
import com.apd.inteliserve.pompages.LandingPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */


public class IA_750 extends BaseTest{
	@Test
	@Parameters({"platform"})
	public void landingPageTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-750, Landing Page Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-750, Landing Page Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			LandingPage ld = new LandingPage(driver);
			if(elementIsDisplayed(ld.verifyHelloText())){
				String actualGrettingText =" ";
				if(platform.equalsIgnoreCase("ios")){
					actualGrettingText=ld.verifyHelloText().getAttribute("name");
				}else {
					actualGrettingText=ld.verifyHelloText().getText();
				}

				extentTest.log(LogStatus.PASS, "Hello Greetings Text is Displayed -- " +actualGrettingText);
				String expectedGrettingText=Lib.getPropertyValue("ExpectedGreetingName");
				if(actualGrettingText.contains(expectedGrettingText)){
					extentTest.log(LogStatus.PASS, "Correct Name is Displayed With Greetings Text");
				}
				else{
					extentTest.log(LogStatus.FAIL, "Wrong Name is Displayed With Greetings Text");
				}
				s.assertTrue(actualGrettingText.contains(expectedGrettingText));
			}
			if(elementDisplayed(ld.verifyNoOutagesText())) {
				extentTest.log(LogStatus.PASS, "Outages is Not Displayed as There is No Outages For User");
				extentTest.log(LogStatus.INFO, "Instead of Outage Got Message --- " + getText(ld.verifyNoOutagesText(), platform));
			}
			else if(elementDisplayed(ld.verifyOutageErrorMessage())){
				extentTest.log(LogStatus.PASS, "Outages is Not Displayed Due to Internet or API Issues");
				extentTest.log(LogStatus.INFO, "Instead of Outage Got Message --- " + getText(ld.verifyOutageErrorMessage()));
			}
			else{
				extentTest.log(LogStatus.PASS, "Outages is Displayed");
			}
			if(elementIsDisplayed(ld.verifyIvaText())){
				extentTest.log(LogStatus.PASS, "Amelia Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Amelia Text is Not Displayed");
			}
			if(elementIsDisplayed(ld.verifyEpaText())){
				extentTest.log(LogStatus.PASS, "EPA Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "EPA Text is Not Displayed");
			}
			if(elementIsDisplayed(ld.verifyLetsChatButton())){
				extentTest.log(LogStatus.PASS, "Lets Chat Button is Present");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Lets Chat Button is Not Present");
			}

			//s.assertTrue(ld.verifyLetsTalkButton().isDisplayed());
			//extentTest.log(LogStatus.PASS, "Lets Talk Button is Present");

			// if(elementIsDisplayed(ld.verifyFavoritesText())){
			// 	extentTest.log(LogStatus.PASS, "Favorites Text is Displayed");
			// }
			// else{
			// 	extentTest.log(LogStatus.FAIL, "Favorites Text is Not Displayed");
			// }

			if(elementIsDisplayed(ld.verifyKnowledgeBaseButton())){
				extentTest.log(LogStatus.PASS, "Knowledge Base Button is Present");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Knowledge Base Button is Not Present");
			}
			if(elementIsDisplayed(ld.verifyCompanyNewsButton())){
				extentTest.log(LogStatus.PASS, "Company News Button is Present");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Company News Button is Not Present");
			}
			if(elementIsDisplayed(ld.verifyCoreSupportButton())){
				extentTest.log(LogStatus.PASS, "Core Support Corner Button is Present");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Core Support Corner Button is Not Present");
			}
			if(elementDisplayed(ld.verifyNoTicketText())){
				extentTest.log(LogStatus.PASS, "Ticket is Not Present For user"); 
				extentTest.log(LogStatus.INFO, "Instead of Ticket Got Message --- " + getText(ld.verifyNoTicketText()));
			}
			else if (elementDisplayed(ld.verifyTicketErrorMessage())) {
				extentTest.log(LogStatus.PASS, "Ticket is Present For user But Not Displayed Due to API or Internet Issue");
				extentTest.log(LogStatus.INFO, "Instead of Ticket Got Message --- " + getText(ld.verifyTicketErrorMessage()));
			}else{
				extentTest.log(LogStatus.PASS, "Ticket is Present For User");
			}
			//click(ld.chatButton());
			click(ld.verifyLetsChatButton());
			extentTest.log(LogStatus.PASS, "Clicked on Let's Chat Button");
			try {
				if(platform.equalsIgnoreCase("ios")){
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//XCUIElementTypeOther[@name='toggle menu button']"))).click();
					extentTest.log(LogStatus.PASS, "Navigated to IVA Screen");
				}
				if(platform.equalsIgnoreCase("android")){
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.View[contains(@text,'menu toggle button.')]"))).click();
					extentTest.log(LogStatus.PASS, "Navigated to IVA Screen");

				}
			}catch(Exception e){
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL, "Failed to  Navigate to IVA Screen");
			}

			//XCUIElementTypeButton[@name="Send message"]
			// try {
			// 	if(platform.equalsIgnoreCase("ios")){
			// 		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//XCUIElementTypeButton[@name='Send message']")));
			// 		extentTest.log(LogStatus.PASS, "Navigated to IVA Screen");
			// 	}
			// 	if(platform.equalsIgnoreCase("android")){
			// 		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.View[contains(@text,'menu toggle button.')]")));
			// 		extentTest.log(LogStatus.PASS, "Navigated to IVA Screen");
			// 	}
			// }catch(Exception e){
			// 	e.printStackTrace();
			// 	extentTest.log(LogStatus.FAIL, "Failed to  Navigate to IVA Screen");
			// }
			s.assertAll();
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Landing Page Test Failed");
		}
	}
}

