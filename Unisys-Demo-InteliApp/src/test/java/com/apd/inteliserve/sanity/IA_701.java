package com.apd.inteliserve.sanity;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.pompages.ProfileScreenPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */



public class IA_701 extends ProfileScreenPage{
	@Test
	@Parameters({"platform"})
	public void profilePageTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 240);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("android")){
			extentTest = extent.startTest("IA-701, Profile Page Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
			try {
				extentTest.log(LogStatus.PASS, "Test Started");
				click(clickProfileIconAnd());
				extentTest.log(LogStatus.PASS, "Clicked  on Profile Icon");
				try{
					elementIsDisplayed(verifyProfileHeaderAnd());
					extentTest.log(LogStatus.PASS, "Profile Header Text is Displayed");
				}catch(Exception e){
					extentTest.log(LogStatus.FAIL, "Profile Header Text is Not Displayed");	
				}
				try{
					elementIsDisplayed(verifyInitialsAnd());
					extentTest.log(LogStatus.PASS, "Initials  is Displayed -- " + verifyInitialsAnd().getText());	
				}catch(Exception e){
					extentTest.log(LogStatus.FAIL, "Initials  is Not Displayed");	
				}
				try{
					elementIsDisplayed(verifyFullNameAnd());
					extentTest.log(LogStatus.PASS, "Full Name is Displayed -- " + verifyFullNameAnd().getText());
				}catch(Exception e){
					extentTest.log(LogStatus.PASS, "Full Name is Not Displayed");
				}
				try{
					elementIsDisplayed(verifyEmailAnd());
					extentTest.log(LogStatus.PASS, "Email is Displayed -- " + verifyEmailAnd().getText());
				}catch(Exception e){
					extentTest.log(LogStatus.FAIL, "Email is Not Displayed");	
				}

				try {
					extentTest.log(LogStatus.INFO, "Checking if Labels are Displayed");
					try {
						if(verifyPhoneLabelAnd().isDisplayed()) {
							extentTest.log(LogStatus.PASS, "Phone Label is Displayed");
							try {
								//s.assertTrue(verifyPhoneAnd().isDisplayed());
								extentTest.log(LogStatus.PASS, "Phone Number is Displayed -- " + verifyPhoneLabelAnd().getText());
							}catch(Exception e) {
								extentTest.log(LogStatus.FAIL, "Phone Number is Not Displayed");
								Assert.fail("Phone Number Not Dispalyed");
							}
						}
					}catch(Exception e) {
						extentTest.log(LogStatus.INFO, "Phone Label  is Not Displayed as Phone Number is Not Available For User");
					}
					try {
						if (verifyLocationLabelAnd().isDisplayed()) {
							extentTest.log(LogStatus.PASS, "Location Label is Displayed");
							try {
								//s.assertTrue(verifylocationAnd().isDisplayed());
								extentTest.log(LogStatus.PASS, "Location is Displayed -- " + verifyLocationLabelAnd().getText());
							}catch(Exception e) {
								extentTest.log(LogStatus.FAIL, "Location is Not Displayed");
								Assert.fail("Location Not Dispalyed");
							}
						}
					}catch(Exception e) {
						extentTest.log(LogStatus.INFO, "Location Label is Not Displayed as Location Data is Not Available For User");
					}
					try{
						if (verifyLanguageLabelAnd().isDisplayed()) {
							extentTest.log(LogStatus.PASS, "Language Label is Displayed");
							try {
								//s.assertTrue(verifyLanguageAnd().isDisplayed());
								extentTest.log(LogStatus.PASS, "Language is Displayed -- " + verifyLanguageLabelAnd().getText());
							}catch(Exception e){
								extentTest.log(LogStatus.FAIL, "Language is Not Displayed");
								Assert.fail("Language Not Dispalyed");
							}
						}
					}catch (Exception e) {
						extentTest.log(LogStatus.INFO, "Language Label is Not Displayed as Language  Data is Not Available For User");
					}
				}catch (Exception e) {
					e.printStackTrace();
					extentTest.log(LogStatus.FAIL, e.getMessage());
					extentTest.log(LogStatus.FAIL, "No Label is Displayed");
				}
				s.assertAll();
			}catch(Exception e) {
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL, e.getMessage());
				Assert.fail("Profile Page Test Failed");
			}
		}
		else {
			extentTest = extent.startTest("IA-701, Profile Page Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
			try {
				extentTest.log(LogStatus.PASS, "Test Started");
				click(clickProfileIconIos());
				extentTest.log(LogStatus.PASS, "Clicked  on Profile Icon");
				try{
					elementIsDisplayed(verifyProfileHeaderIos());
					extentTest.log(LogStatus.PASS, "Profile Header Text is Displayed");
				}catch(Exception e){
					extentTest.log(LogStatus.FAIL, "Profile Header Text is not Displayed");
				}
				try {
					elementDisplayed(verifyInitialsIos());
					extentTest.log(LogStatus.PASS, "Initials  is Displayed -- " + verifyInitialsIos().getAttribute("name"));
				}catch(Exception e) {
					extentTest.log(LogStatus.FAIL, "Initials not Displayed");
				}
				try{
					elementIsDisplayed(verifyFullNameIos());
					extentTest.log(LogStatus.PASS, "Full Name is Displayed -- " +verifyFullNameIos().getAttribute("name"));;
				}catch(Exception e) {
					extentTest.log(LogStatus.FAIL, "Full Name  is Not Displayed");
				}
				try{
					elementIsDisplayed(verifyEmailIos());
					extentTest.log(LogStatus.PASS, "Email is Displayed --"+ verifyEmailIos().getAttribute("name"));
				}catch(Exception e) {
					extentTest.log(LogStatus.FAIL, "Email is Not Dispalyed");
				}
				try {
					extentTest.log(LogStatus.INFO, "Checking if Labels are Displayed");
					try {
						if(verifyPhoneLabelIos().isDisplayed()) {
							extentTest.log(LogStatus.PASS, "Phone Label is Displayed");
							try {
								//s.assertTrue(verifyPhoneAnd().isDisplayed());
								extentTest.log(LogStatus.PASS, "Phone Number is Displayed -- " + verifyPhoneLabelIos().getAttribute("name"));
							}catch(Exception e) {
								extentTest.log(LogStatus.FAIL, "Phone Number is Not Displayed");
								Assert.fail("Phone Number Not Dispalyed");
							}
						}
					}catch(Exception e) {
						extentTest.log(LogStatus.INFO, "Phone Label  is Not Displayed as Phone Number is Not Available For User");
					}
					try {
						if (verifyLocationLabelIos().isDisplayed()) {
							extentTest.log(LogStatus.PASS, "Location Label is Displayed");
							try {
								//s.assertTrue(verifylocationAnd().isDisplayed());
								extentTest.log(LogStatus.PASS, "Location is Displayed -- " + verifyLocationLabelIos().getAttribute("name"));
							}catch(Exception e) {
								extentTest.log(LogStatus.FAIL, "Location is Not Displayed");
								Assert.fail("Location Not Dispalyed");
							}
						}
					}catch(Exception e) {
						extentTest.log(LogStatus.INFO, "Location Label is Not Displayed as Location Data is Not Available For User");
					}
					try{
						if (verifyLanguageLabelIos().isDisplayed()) {
							extentTest.log(LogStatus.PASS, "Language Label is Displayed");
							try {
								//s.assertTrue(verifyLanguageAnd().isDisplayed());
								extentTest.log(LogStatus.PASS, "Language is Displayed -- " + verifyLanguageLabelIos().getAttribute("name"));;
							}catch(Exception e){
								extentTest.log(LogStatus.FAIL, "Language is Not Displayed");
								Assert.fail("Language Not Dispalyed");
							}
						}
					}catch (Exception e) {
						extentTest.log(LogStatus.INFO, "Language Label is Not Displayed as Language  Data is Not Available For User");
					}
				}catch (Exception e) {
					e.printStackTrace();
					extentTest.log(LogStatus.FAIL, e.getMessage());
					extentTest.log(LogStatus.FAIL, "No Label is Displayed");
				}
				s.assertAll();
			}catch(Exception e) {
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL, e.getMessage());
				Assert.fail("Profile Page Test Failed");
			}
		}
	}
}
