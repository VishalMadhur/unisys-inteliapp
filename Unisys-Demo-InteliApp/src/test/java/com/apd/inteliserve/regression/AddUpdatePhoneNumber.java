package com.apd.inteliserve.regression;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.EditProfilePage;
import com.apd.inteliserve.pompages.LandingPage;
import com.relevantcodes.extentreports.LogStatus;

public class AddUpdatePhoneNumber extends BaseTest {
	@Test
	@Parameters({"platform","language","transalationSheet"})
	public void addUpdatePhoneNumberTest(@Optional String platform,@Optional String language,@Optional String transalationSheet) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2955, Add/Update Phone Number Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-2952, Add/Update Phone Number Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");

			String expectedEditProfileHeaderLabel =getCellValue(transalationSheet, 97, 1);
			String expectedPhoneTitleLabel=getCellValue(transalationSheet, 98, 1);
			String expectedPhoneSubTitleLabel=getCellValue(transalationSheet, 99, 1);
			String expectedUnsavedChangesLabel =getCellValue(transalationSheet, 99, 1);
			String expectedDiscardLabel =getCellValue(transalationSheet, 100, 1);
			String expectedDiscardButtonLabel="";
			String expectedCancelButtonLabel="";
			String expectedInvalidNumberLabel=getCellValue(transalationSheet, 103, 1);
			String expectedValidPhoneNumber=getCellValue(transalationSheet, 104, 1);
			String expectedOkButtonLabel=getCellValue(transalationSheet, 105, 1);
			String expectedphoneNumberInfoLabel=getCellValue(transalationSheet, 108, 1);
			if(platform.equalsIgnoreCase("ios")){
				expectedDiscardButtonLabel=getCellValue(transalationSheet, 101, 1);
				expectedCancelButtonLabel=getCellValue(transalationSheet, 102, 1);
			}else {
				expectedDiscardButtonLabel=getCellValue(transalationSheet, 106, 1);
				expectedCancelButtonLabel=getCellValue(transalationSheet, 107, 1);
			}
			LandingPage ld = new LandingPage(driver);
			EditProfilePage ep = new EditProfilePage(driver);
			clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
			String actualProfileNumber="";
			String profileNumber="";
			try {
				profileNumber=getText(ep.verifyPhoneNumber(), platform);
				String profilePhoneNumber[]=profileNumber.split(" ");
				actualProfileNumber=profilePhoneNumber[1];
				System.out.println(actualProfileNumber);
				extentTest.log(LogStatus.PASS, "Phone number is dispalyed on Profile Page -- " +actualProfileNumber);
			}catch(Exception e){
				e.printStackTrace();
				extentTest.log(LogStatus.PASS, "No Phone Number is dispalyed on Profile screen as Number Was  Not Entered Earlier");
			}

			clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
			String actualEditProfileHeaderLabel=getText(ep.verifyEditProfileHeader(),platform);
			String actualPhoneTitleLabel=getText(ep.verifyPhoneTitle(),platform);
			String actualPhoneNumberInfoLabel=getText(ep.verifyPhoneNumberInfo(),platform);
			verifyLabel("Edit Profile Header", matchData(expectedEditProfileHeaderLabel,actualEditProfileHeaderLabel));
			verifyLabel("Phone Title", matchData(expectedPhoneTitleLabel,actualPhoneTitleLabel));
			verifyLabel("Phone Number Information ", matchData(expectedphoneNumberInfoLabel,actualPhoneNumberInfoLabel));
			System.out.println(expectedPhoneTitleLabel);
			String actualPhoneSubTitleLabel=getText(ep.verifyPhoneSubTitle(),platform);
			System.out.println(actualPhoneSubTitleLabel);
			if(actualPhoneSubTitleLabel.equals(expectedPhoneSubTitleLabel)) {
				extentTest.log(LogStatus.PASS, "Enter Phone Number Label is Displayed-- " +actualPhoneSubTitleLabel);
			}
			else if (actualPhoneSubTitleLabel.equals(actualProfileNumber)) {
				extentTest.log(LogStatus.PASS, "Profile Phone Number is Displayed-- " +actualPhoneSubTitleLabel);
			}else {
				extentTest.log(LogStatus.FAIL, "Nither Profile Phone Number Nor 'Enter Phone Number Label' is Displayed-- " +actualPhoneSubTitleLabel);
			}
			//clearData(ep.verifyPhoneSubTitle());
			clickOnElement("Phone Number Edit Box", clickElement(ep.verifyPhoneSubTitle()));
			clickOnElement("Clear Icon", clickElement(ep.verifyClearIcon()));
			sendExcelTestData(ep.verifyPhoneSubTitle(), "TestData",18,1);
			String actaulMobileNumberEntered=getText(ep.verifyPhoneSubTitle(), platform);
			clickOnElement("Cancel Button", clickElement(ep.verifyCancelButton()));
			verifyElement("Warning Pop up", elementDisplayed(ep.verifyUnsavedChangesText()));
			String actualUnsavedChangesLabel=getText(ep.verifyUnsavedChangesText(),platform);
			String actualDiscardLabel=getText(ep.verifyDiscardText(),platform);
			String actualDiscardButtonLabel=getText(ep.verifyDiscardButton(),platform);
			String actualCancelButtonLabel=getText(ep.verifyPopCancelButton(),platform);
			verifyLabel("You Have Unsaved Changes", matchData(expectedUnsavedChangesLabel,actualUnsavedChangesLabel));
			verifyLabel("Do You Want to Disacrd Them", matchData(expectedDiscardLabel,actualDiscardLabel));
			verifyLabel("Discard Button", matchData(expectedDiscardButtonLabel,actualDiscardButtonLabel));
			verifyLabel("Cancel Button", matchData(expectedCancelButtonLabel,actualCancelButtonLabel));
			clickOnElement("Cancel Button", clickElement(ep.verifyPopCancelButton()));
			String actualMobileNumberFound=	getText(ep.verifyPhoneSubTitle(), platform);
			verifyData(actualMobileNumberFound, "Entered Number", matchData(actaulMobileNumberEntered, actualMobileNumberFound));
			clickOnElement("Cancel Button", clickElement(ep.verifyCancelButton()));
			clickOnElement("Discard Button", clickElement(ep.verifyDiscardButton()));
			String profilePhoneNumberAfterClickingDiscardButton = getText(ep.verifyPhoneNumber(), platform);
			verifyData(profilePhoneNumberAfterClickingDiscardButton, "After Clicking Discard Button Previous Mobile Number ", matchData(profilePhoneNumberAfterClickingDiscardButton, profileNumber));
			clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
			//clearData(ep.verifyPhoneSubTitle());
			clickOnElement("Phone Number Edit Box", clickElement(ep.verifyPhoneSubTitle()));
			clickOnElement("Clear Icon", clickElement(ep.verifyClearIcon()));
			sendExcelTestData(ep.verifyPhoneSubTitle(), "TestData",19,1);
			clickOnElement("Save Button",clickElement(ep.verifyEnableSaveButton()));
			verifyElement("Invalid Number Warning Pop up", elementDisplayed(ep.verifyInvalidText()));
			String actualInvalidNumberLabel=getText(ep.verifyInvalidText(), platform);
			String actualValidPhoneNumber=getText(ep.verifyValidPhoneNumberText(), platform);
			String actualOkButtonLabel=getText(ep.verifyOkButton(), platform);
			verifyLabel("Invalid Phone Number", matchData(expectedInvalidNumberLabel,actualInvalidNumberLabel));
			verifyLabel("Enter a Valid Phone Number", matchData(expectedValidPhoneNumber,actualValidPhoneNumber));
			verifyLabel("Ok Button", matchData(expectedOkButtonLabel,actualOkButtonLabel));  
			clickOnElement("Ok Button", clickElement(ep.verifyOkButton()));
			for(int i=20;i<=33;i++){
				clickOnElement("Phone Number Edit Box", clickElement(ep.verifyPhoneSubTitle()));
				clickOnElement("Clear Icon", clickElement(ep.verifyClearIcon()));
				//clearData(ep.verifyPhoneSubTitle());
				sendExcelTestData(ep.verifyPhoneSubTitle(), "TestData",i,1);
				verifyElement("Clear Icon", elementDisplayed(ep.verifyClearIcon()));
				clickOnElement("Save Button",clickElement(ep.verifyEnableSaveButton()));
				verifyElement("Invalid Number Warning Pop up", elementDisplayed(ep.verifyInvalidText()));
				clickOnElement("Ok Button", clickElement(ep.verifyOkButton()));
				if(platform.equalsIgnoreCase("android")){
					verifyElement("Alert Icon", elementDisplayed(ep.verifyAlertIcon()));
				}
			}	
			clickOnElement("Cancel Button", clickElement(ep.verifyCancelButton()));
			clickOnElement("Discard Button", clickElement(ep.verifyDiscardButton()));
			for(int i=34;i<=39;i++){
				waitForVisibityOfSpecificElement("Edit Profile Icon", ep.verifyEditProfileIcon());
				clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
				//clearData(ep.verifyPhoneSubTitle());
				clickOnElement("Phone Number Edit Box", clickElement(ep.verifyPhoneSubTitle()));
				clickOnElement("Clear Icon", clickElement(ep.verifyClearIcon()));
				sendExcelTestData(ep.verifyPhoneSubTitle(), "TestData",i,1);
				String enteredPhoneNumber=getText(ep.verifyPhoneSubTitle(), platform);
				clickOnElement("Save Button",clickElement(ep.verifyEnableSaveButton()));
				try {
					waitForVisibityOfSpecificElement("Profile Phone Number", ep.verifyPhoneNumber());
					Thread.sleep(2000);
					profileNumber=getText(ep.verifyPhoneNumber(), platform);
					String profilePhoneNumber[]=profileNumber.split(" ");
					actualProfileNumber=profilePhoneNumber[1];
					verifyData(actualProfileNumber, "Mobile Number is Saved successfully And Displayed in Profile Page as Entered Mobile Number ", matchData(enteredPhoneNumber,actualProfileNumber));
				}catch(Exception e) {
					e.printStackTrace();
					extentTest.log(LogStatus.FAIL, "Failed to Save Mobile Number or Wrong Number is Displayed as Expected " + enteredPhoneNumber + " But Found " + actualProfileNumber);
				}
			}
			s.assertAll();
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Add/Update Phone Number Test Failed");
		}
	}	
}
