package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.LandingPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */


public class IA_700 extends BaseTest{
	@Test
	@Parameters({"platform"})
	public void navigationBarTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-700, Navigation Bar Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-700, Navigation Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			LandingPage ld = new LandingPage(driver);
			if(elementIsDisplayed(ld.homeIcon())){
				extentTest.log(LogStatus.PASS, "Home Icon is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Home Icon is Not Displayed");
			}
			if(elementIsDisplayed(ld.profileIcon())){
				extentTest.log(LogStatus.PASS, "Profile Icon is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Profile Icon is Not Displayed");
			}
			if(elementIsDisplayed(ld.ivaIcon())){
				extentTest.log(LogStatus.PASS, "IVA Icon is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "IVA Icon is Not Displayed");
			}
			try {
				ld.profileIcon().click();
				extentTest.log(LogStatus.PASS, "User is Able to Navigate to Profile Icon");
			}catch(Exception e) {
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL, e.getMessage());
				extentTest.log(LogStatus.FAIL, "Failed to Navigate to Profile Icon");
			}
			try {
				ld.ivaIcon().click();
				extentTest.log(LogStatus.PASS, "User is Able to Navigate to Iva Icon");
			}catch(Exception e) {
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL, e.getMessage());
				extentTest.log(LogStatus.FAIL, "Failed to Navigate to Iva Icon");
			}
			click(ld.homeIcon());
			extentTest.log(LogStatus.PASS, "User is Able to Navigate to Home Icon");
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Navigation Bar Test Failed");
		}
	}
}

