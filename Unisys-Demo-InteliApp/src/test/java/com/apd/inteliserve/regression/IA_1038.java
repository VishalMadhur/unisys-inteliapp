package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.ProfileScreenLegalPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */


public class IA_1038 extends BaseTest{
	@Test
	@Parameters({"platform"})
	public void thirdPartyNoticesTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-1038, Third Party Notices Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-1038, Third Party Notices Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			ProfileScreenLegalPage pl= new ProfileScreenLegalPage(driver);
			click(pl.profileIcon());
			extentTest.log(LogStatus.PASS, "User is Able to Navigate to Profile Icon");
			click(pl.aboutText());
			extentTest.log(LogStatus.PASS, "Clink on About Link");
			if(platform.equalsIgnoreCase("ios")){
				click(pl.thirdPartyPolicyText());
			}
			else {
				click(pl.thirdPartyPloicyLink());
			}
			//				click(pl.companyText());
			//				pl.privacyPolicyText().click();
			//				pl.termsOfUseText().click();
			//click(pl.thirdPartyPolicyText());
			extentTest.log(LogStatus.PASS, "Clicked on Third Party Notices Link");
			if(elementIsDisplayed(pl.verifyThirdPartyNoticesHeader())){
				extentTest.log(LogStatus.PASS, "Third Party Notices Header is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Third Party Notices Header is Not Displayed");
			}
			if(elementIsDisplayed(pl.homeIcon())){
				extentTest.log(LogStatus.PASS, "Home Icon is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Home Icon is Not Displayed");
			}
			if(elementIsDisplayed(pl.ivaIcon())){
				extentTest.log(LogStatus.PASS, "Profile Icon is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Profile Icon is Not Displayed");
			}
			if(elementIsDisplayed(pl.ivaIcon())){
				extentTest.log(LogStatus.PASS, "IVA Icon is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "IVA Icon is Not Displayed");
			}
			click(pl.clickBackButton());
			extentTest.log(LogStatus.PASS, "User is Able Click Back Button");
			if(elementIsDisplayed(pl.verifyAboutHeader())){
				extentTest.log(LogStatus.PASS, "User is Navigated Back to About Screen");
			}
			else{
				extentTest.log(LogStatus.FAIL, "User is Failed to Navigate Back to About Screen");
			}
			if(platform.equalsIgnoreCase("ios")){
				click(pl.thirdPartyPolicyText());
			}
			else {
				click(pl.thirdPartyPloicyLink());
			}
			extentTest.log(LogStatus.PASS, "Clicked on Third Party Notices Link");
			try {
				Thread.sleep(2000);
				pl.ivaIcon().click();
				extentTest.log(LogStatus.PASS, "User is Able to Navigate to Iva Icon");
			}catch(Exception e) {
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL, e.getMessage());
				extentTest.log(LogStatus.FAIL, "Failed to Navigate to Iva Icon");
			}
			Thread.sleep(2000);
			click(pl.profileIcon());
			extentTest.log(LogStatus.PASS, "User is Able to Navigate to Profile Icon");
			click(pl.aboutText());
			extentTest.log(LogStatus.PASS, "Clink on About Link");
			if(platform.equalsIgnoreCase("ios")){
				click(pl.thirdPartyPolicyText());
			}
			else {
				click(pl.thirdPartyPloicyLink());
			}
			extentTest.log(LogStatus.PASS, "Clicked on Third Party Notices Link");
			try {
				Thread.sleep(2000);
				pl.homeIcon().click();
				extentTest.log(LogStatus.PASS, "User is Able to Navigate to Home Icon");
			}catch(Exception e) {
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL, e.getMessage());
				extentTest.log(LogStatus.FAIL, "Failed to Navigate to Home Icon");
			}
			Thread.sleep(2000);
			click(pl.profileIcon());
			extentTest.log(LogStatus.PASS, "User is Able to Navigate to Profile Icon");
			click(pl.aboutText());
			extentTest.log(LogStatus.PASS, "Clink on About Link");
			if(platform.equalsIgnoreCase("ios")){
				click(pl.thirdPartyPolicyText());
			}
			else {
				click(pl.thirdPartyPloicyLink());
			}
			extentTest.log(LogStatus.PASS, "Clicked on Third Party Notices Link");
			Thread.sleep(2000);
			click(pl.profileIcon());
			extentTest.log(LogStatus.PASS, "User is Able to Navigate to Profile Icon");
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Third Party Notices Test Failed");
		}
	}
}
