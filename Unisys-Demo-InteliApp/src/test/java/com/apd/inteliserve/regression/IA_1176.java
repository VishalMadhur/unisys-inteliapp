package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.EPAPage;
import com.apd.inteliserve.pompages.OpenTicketListPage;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;



public class IA_1176 extends BaseTest {
	@Test
	@Parameters({"platform"})
	public void resolveTicketFromAmelia(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		String createdTicket = null;
		String createdNewTicketNumber =null;
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-1176, Resolve Ticket From Amelia Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-1176, Resolve Ticket From Amelia Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			EPAPage epa = new EPAPage(driver);
			click(epa.ivaIcon());
			extentTest.log(LogStatus.PASS, "User is Able to Navigate to Amelia Page");
			waitForVisibityOfSpecificElement("Send Message Button", epa.sendMessage());
			Thread.sleep(5000);
			click(epa.sendMessage());
			//click(epa.sendMessage());
			sendExcelTestData(epa.editBox(), "TestData", 13, 1);
			extentTest.log(LogStatus.INFO, "Fetched Query From Excel Sheet");
			Thread.sleep(3000);
			click(epa.sendMessage());
			click(epa.sendMessage());
			//click(epa.sendMessage());
			Thread.sleep(10000);
			extentTest.log(LogStatus.PASS, "Query Entered Successfully");
			sendExcelTestData(epa.titleEditBox(), "TestData", 14, 1);
			extentTest.log(LogStatus.PASS, "Fetched Ticket Title From Excel Sheet");
			sendExcelTestData(epa.descriptionEditBox(), "TestData", 15, 1);
			click(epa.sendMessage());
			//click(epa.sendMessage());
			extentTest.log(LogStatus.PASS, "Clicked on Send Message Button Successfully");
			Thread.sleep(5000);
			click(epa.createTicketButton());
			extentTest.log(LogStatus.PASS, "Clicked on Create Ticket Button");
			Thread.sleep(10000);
			click(epa.noRadioButton());
			extentTest.log(LogStatus.PASS, "Clicked on No Button");
			click(epa.chatToggleButton());
			extentTest.log(LogStatus.PASS, "Clicked on Ticket Managemnet Button");
			if(elementIsDisplayed(epa.getCreatedTicketNumber())){
				extentTest.log(LogStatus.PASS, "Ticket is Dispalyed in Ticket Management");
				createdTicket=getText(epa.getCreatedTicketNumber(),platform);
				click(epa.closeChatIcon());
				String[]strr= createdTicket.split(" ");
				createdNewTicketNumber = strr[0];
				extentTest.log(LogStatus.PASS, "Got Created Ticket from Management ----- " + createdNewTicketNumber);
			}
			else{
				extentTest.log(LogStatus.FAIL, "Ticket is Not Dispalyed in Ticket Managemnet");
			}
			//Thread.sleep(10000);
			//click(epa.closeChatIcon());
			extentTest.log(LogStatus.PASS, "Closed Ticket Mangement Menu Successfully");
			Thread.sleep(3000);
			click(epa.toggleButton());
			extentTest.log(LogStatus.PASS, "Clicked Toggle Button Successfully");
			Thread.sleep(2000);
			click(epa.resetConversationButton());
			extentTest.log(LogStatus.PASS, "clicked Reset Conversation Button Successfully");
			Thread.sleep(20000);
			click(epa.sendMessage());
			click(epa.sendMessage());
			sendExcelTestData(epa.editBox(), "TestData",16,1);
			extentTest.log(LogStatus.PASS, "Query Entered Successfully ---- Close Ticket");
			click(epa.sendMessage());
			extentTest.log(LogStatus.PASS, "Query Posted Successfully");
			Thread.sleep(10000);
			String[]strr= createdTicket.split(" ");
			String createdTicketNumber = strr[0];
			
			if(platform.equalsIgnoreCase("ios")){
				TouchAction action = new TouchAction(driver);
				action.press(PointOption.point(16,767)).release().perform();
				Thread.sleep(5000);
				sendTestData(epa.messageBox(), createdTicketNumber);
				extentTest.log(LogStatus.PASS, "Entered Ticket to Resolve---- " +createdTicketNumber);
				clickElement(epa.clickReturnButton());
				clickElement(epa.clickDeleteButton());
				clickElement(epa.clickReturnButton());
				clickElement(epa.clickReturnButton());
				extentTest.log(LogStatus.PASS, "Clicked on Ticket Successfully");
			}
			else {
				sendTestData(epa.editBox(), createdTicketNumber);
				extentTest.log(LogStatus.PASS, "Entered Ticket to Resolve---- " +createdTicketNumber);
				Thread.sleep(3000);
				click(epa.verifyTicketNumEPA());
				extentTest.log(LogStatus.PASS, "Entered Ticket to Resolve---- " +createdTicketNumber);
				clickElement(epa.toggleButton());
				Thread.sleep(2000);
				clickElement(epa.toggleButton());
				extentTest.log(LogStatus.PASS, "Clicked on Selected Ticket Successfully");
			}
			
			
			click(epa.sendMessage());
			click(epa.sendMessage());	
//		epa.editBox().sendKeys(createdTicketNumber);			
//			extentTest.log(LogStatus.PASS, "Entered Ticket Number to Resolve ----- " + createdTicketNumber);
//			click(epa.sendMessage());
//			extentTest.log(LogStatus.PASS, "Ticket Number to Resolve Posted Successfully")	;
			Thread.sleep(10000);
			sendExcelTestData(epa.editBox(), "TestData", 17, 1);
			extentTest.log(LogStatus.PASS, "Entered Brief Description to Resolve Ticket");
			Thread.sleep(10000);
			click(epa.sendMessage());
			click(epa.sendMessage());
			extentTest.log(LogStatus.PASS, "Brief Description to Resolve Ticket Posted Successfully");
			Thread.sleep(15000);
			click(epa.noRadioButton());
			extentTest.log(LogStatus.PASS, "Clicked No Button to Resolve the Conversation with Amelia");
			click(epa.chatToggleButton());
			extentTest.log(LogStatus.PASS, "Clicked Ticket Mangement Menu Successfully");
			if(elementIsDisplayed(epa.getCreatedTicketNumber())){
				extentTest.log(LogStatus.PASS, "Resolved Ticket is Displayed  in Ticket Management Menu");
				createdTicket=getText(epa.getCreatedTicketNumber());
			}
			else{
				extentTest.log(LogStatus.FAIL, "Resolved Ticket is Not Displayed  in Ticket Management Menu");
			}
			String expectedStatus =getCellValue("ExpectedText", 3, 1);
			String actualStatus =createdTicket;
			if(actualStatus.contains(expectedStatus)){
				extentTest.log(LogStatus.PASS, "Status of Ticket is Set to Resolved Successfully ----- " + actualStatus);
			}
			else{
				extentTest.log(LogStatus.FAIL, "Failed to Set Status of Ticket to Resolved  ----- " + actualStatus);
			}
			Thread.sleep(10000);
			logOut(platform);
			//loginWithOutEnablingLocation();
			login(platform);
			if(platform.equalsIgnoreCase("ios")){
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
			}
			else{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
			}
			String resolvedTicketNumber = createdTicketNumber;

			OpenTicketListPage ot = new OpenTicketListPage(driver);
			swipe(ot.upSwipeBar(), ot.swipeTillHelloText());
			String scrollToTicket = resolvedTicketNumber;
			/*if(platform.equalsIgnoreCase("ios")){
					iOSScroll("INC0023483");
				}*/
			try{
				if(platform.equalsIgnoreCase("android")){
					driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+scrollToTicket+"\").instance(0))"));
					extentTest.log(LogStatus.FAIL, "Resolved Ticket is  Displayed in Open Ticket List");
				}
			}
			catch(Exception e){
				e.printStackTrace();
				extentTest.log(LogStatus.PASS, "Resolved Ticket is Not Displayed in Open Ticket List");
			}

			//				String ticketNumber=getText(epa.verifyTicketNumText());
			//				System.out.println(ticketNumber);
			//				extentTest.log(LogStatus.INFO, "Checking if Resolved Ticket is Not Displayed in Open Ticket List");
			//				if(!(createdTicketNumber).contains(ticketNumber)){
			//					extentTest.log(LogStatus.PASS, "Resolved Ticket is Not Displayed in Open Ticket List");
			//				}
			//				else{
			//					extentTest.log(LogStatus.FAIL, "Resolved Ticket is  Displayed in Open Ticket List  ----- " + ticketNumber);
			//				}
			//				s.assertFalse(createdTicketNumber.contains(ticketNumber));
			//		}catch(Exception e){
			//			e.printStackTrace();
			//				extentTest.log(LogStatus.FAIL, "No Tickets is Displayed For The User");
			//			}
			s.assertAll();
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Resolve Ticket From Amelia Test Failed");
		}
	}
}


