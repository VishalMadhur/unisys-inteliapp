package com.apd.inteliserve.regression;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.EditProfileLocationPage;
import com.apd.inteliserve.pompages.EditProfilePage;
import com.apd.inteliserve.pompages.LandingPage;
import com.relevantcodes.extentreports.LogStatus;


public class AddUpdateLocation extends BaseTest {
	@Test
	@Parameters({"platform","language","transalationSheet"})
	public void updateProfileLocation(@Optional String platform,@Optional String language,@Optional String transalationSheet) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2949, Add/Update Profile Location Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-3287, Add/Update Profile Location-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");

			String expectedCountryHeaderLabel =getCellValue(transalationSheet, 181, 1);
			String expectedAllCountriesLabel =getCellValue(transalationSheet, 182, 1);
			String expectedFirstCountry =getCellValue(transalationSheet, 183, 1);
			String expectedSecondCountry =getCellValue(transalationSheet, 184, 1);
			String expectedThirdCountry =getCellValue(transalationSheet, 185, 1);
			String expectedFourthCountry =getCellValue(transalationSheet, 186, 1);
			String expectedFifthCountry =getCellValue(transalationSheet, 187, 1);
			String expectedSixthCountry =getCellValue(transalationSheet, 188, 1);
			String expectedSeventhCountry =getCellValue(transalationSheet, 189, 1);
			String expectedEighthCountry =getCellValue(transalationSheet, 190, 1);
			String expectedNinthCountry =getCellValue(transalationSheet, 191, 1);
			String expectedTenthCountry =getCellValue(transalationSheet, 192, 1);
			String expectedEleventhCountry =getCellValue(transalationSheet, 193, 1);
			String expectedTwelvethCountry =getCellValue(transalationSheet, 194, 1);
			String expectedThirteenthCountry =getCellValue(transalationSheet, 195, 1);
			String expectedFourtteenthCountry =getCellValue(transalationSheet, 196, 1);
			String expectedFiftteenthCountry =getCellValue(transalationSheet, 197, 1);
			String expectedSixteenthCountry =getCellValue(transalationSheet, 198, 1);
			String expectedSeventeenthCountry =getCellValue(transalationSheet, 199, 1);
			String expectedEighteenthCountry =getCellValue(transalationSheet, 200, 1);
			String expectedNineteenthCountry =getCellValue(transalationSheet, 201, 1);
			String expectedTwentiethCountry =getCellValue(transalationSheet, 202, 1);
			String expectedTwentyFirstCountry =getCellValue(transalationSheet, 203, 1);
			String expectedTwentySecondCountry =getCellValue(transalationSheet, 204, 1);
			String expectedTwentyThirdCountry =getCellValue(transalationSheet, 205, 1);
			String expectedTwentyFourthCountry =getCellValue(transalationSheet, 206, 1);
			String expectedTwentyFifthCountry =getCellValue(transalationSheet, 207, 1);
			String expectedTwentySixthCountry =getCellValue(transalationSheet, 208, 1);
			String expectedTwentySeventhCountry =getCellValue(transalationSheet, 209, 1);
			String expectedTwentyEighthCountry =getCellValue(transalationSheet, 210, 1);
			String expectedlLocationTitleLabel=getCellValue(transalationSheet, 265, 1);
			String expectedCountryLabel=getCellValue(transalationSheet, 266, 1);
			String aexpectedAllLocationsLabel=getCellValue(transalationSheet, 267, 1);
			String expectedArgentinaFirstOffice=getCellValue(transalationSheet, 211, 1);
			String expectedArgentinaSecondOffice=getCellValue(transalationSheet, 212, 1);
			String expectedArgentinaThirdOffice=getCellValue(transalationSheet, 213, 1);
			String expectedArgentinaFirstCity=getCellValue(transalationSheet, 214, 1);
			String expectedArgentinaSecondCity=getCellValue(transalationSheet, 215, 1);
			String expectedArgentinaThirdCity=getCellValue(transalationSheet, 216, 1);
			String expectedAustraliaFirstOffice=getCellValue(transalationSheet, 217, 1);
			String expectedAustraliaSecondOffice=getCellValue(transalationSheet, 218, 1);
			String expectedAustraliaFirstCity=getCellValue(transalationSheet, 219, 1);
			String expectedAustraliaSecondCity=getCellValue(transalationSheet, 220, 1);
			String expectedAustriaFirstOffice=getCellValue(transalationSheet, 221, 1);
			String expectedAustriaFirstCity=getCellValue(transalationSheet, 222, 1);
			String expectedBelgiumFirstOffice=getCellValue(transalationSheet, 223, 1);
			String expectedBelgiumFirstCity=getCellValue(transalationSheet, 224, 1);
			String expectedBrazilFirstOffice=getCellValue(transalationSheet, 225, 1);
			String expectedBrazilSecondOffice=getCellValue(transalationSheet, 226, 1);
			String expectedBrazilThirdOffice=getCellValue(transalationSheet, 227, 1);
			String expectedBrazilFourthOffice=getCellValue(transalationSheet, 228, 1);
			String expectedBrazilFifthOffice=getCellValue(transalationSheet, 229, 1);
			String expectedBrazilSixthOffice=getCellValue(transalationSheet, 230, 1);
			String expectedBrazilSeventhOffice=getCellValue(transalationSheet, 231, 1);
			String expectedBrazilEighthOffice=getCellValue(transalationSheet, 232, 1);
			String expectedBrazilFirstCity=getCellValue(transalationSheet, 233, 1);
			String expectedBrazilSecondCity=getCellValue(transalationSheet, 234, 1);
			String expectedBrazilThirdCity=getCellValue(transalationSheet, 235, 1);
			String expectedBrazilFourthCity=getCellValue(transalationSheet, 236, 1);
			String expectedBrazilFifthCity=getCellValue(transalationSheet, 237, 1);
			String expectedBrazilSixthCity=getCellValue(transalationSheet, 238, 1);
			String expectedBrazilSeventhCity=getCellValue(transalationSheet, 239, 1);
			String expectedBrazilEighthCity=getCellValue(transalationSheet, 240, 1);
			String expectedCanadaFirstOffice=getCellValue(transalationSheet, 241, 1);
			String expectedCanadaSecondOffice=getCellValue(transalationSheet, 242, 1);
			String expectedCanadaFirstCity=getCellValue(transalationSheet, 243, 1);
			String expectedCanadaSecondCity=getCellValue(transalationSheet, 244, 1);
			String expectedChinaFirstOffice=getCellValue(transalationSheet, 245, 1);
			String expectedChinaSecondOffice=getCellValue(transalationSheet, 246, 1);
			String expectedChinaThirdOffice=getCellValue(transalationSheet, 247, 1);
			String expectedChinaFourthOffice=getCellValue(transalationSheet, 248, 1);
			String expectedChinaFifthOffice=getCellValue(transalationSheet, 249, 1);
			String expectedChinaFirstCity=getCellValue(transalationSheet, 250, 1);
			String expectedChinaSecondCity=getCellValue(transalationSheet, 251, 1);
			String expectedChinaThirdCity=getCellValue(transalationSheet, 252, 1);
			String expectedChinaFourthCity=getCellValue(transalationSheet, 253, 1);
			String expectedChinaFifthCity=getCellValue(transalationSheet, 254, 1);
			String expectedColombiaFirstOffice=getCellValue(transalationSheet, 255, 1);
			String expectedColombiaSecondOffice=getCellValue(transalationSheet, 256, 1);
			String expectedColombiaThirdOffice=getCellValue(transalationSheet, 257, 1);
			String expectedColombiaFirstCity=getCellValue(transalationSheet, 258, 1);
			String expectedColombiaSecondCity=getCellValue(transalationSheet, 259, 1);
			String expectedColombiaThirdCity=getCellValue(transalationSheet, 260, 1);
			String expectedCostaRicaFirstOffice=getCellValue(transalationSheet, 261, 1);
			String expectedCostaRicaFirstCity=getCellValue(transalationSheet, 262, 1);
			String expectedFranceFirstOffice=getCellValue(transalationSheet, 263, 1);
			String expectedFranceFirstCity=getCellValue(transalationSheet, 264, 1);
			String expectedGermanyFirstOffice=getCellValue(transalationSheet, 268, 1);
			String expectedGermanySecondOffice=getCellValue(transalationSheet, 269, 1);
			String expectedGermanyThirdOffice=getCellValue(transalationSheet, 270, 1);
			String expectedGermanyFirstCity=getCellValue(transalationSheet, 271, 1);
			String expectedGermanySecondCity=getCellValue(transalationSheet, 272, 1);
			String expectedGermanyThirdCity=getCellValue(transalationSheet, 273, 1);
			String expectedHongKongFirstOffice=getCellValue(transalationSheet, 274, 1);
			String expectedHongKongFirstCity=getCellValue(transalationSheet, 275, 1);
			String expectedHungaryFirstOffice=getCellValue(transalationSheet, 276, 1);
			String expectedHungarySecondOffice=getCellValue(transalationSheet, 277, 1);
			String expectedHungaryThirdOffice=getCellValue(transalationSheet, 278, 1);
			String expectedHungaryFirstCity=getCellValue(transalationSheet, 279, 1);
			String expectedHungarySecondCity=getCellValue(transalationSheet, 280, 1);
			String expectedHungaryThirdCity=getCellValue(transalationSheet, 281, 1);
			String expectedIndiaFirstOffice=getCellValue(transalationSheet, 282, 1);
			String expectedIndiaSecondOffice=getCellValue(transalationSheet, 283, 1);
			String expectedIndiaThirdOffice=getCellValue(transalationSheet, 284, 1);
			String expectedIndiaFourthOffice=getCellValue(transalationSheet, 285, 1);
			String expectedIndiaFirstCity=getCellValue(transalationSheet, 286, 1);
			String expectedIndiaSecondCity=getCellValue(transalationSheet, 287, 1);
			String expectedIndiaThirdCity=getCellValue(transalationSheet, 288, 1);
			String expectedIndiaFourthCity=getCellValue(transalationSheet, 289, 1);
			String expectedJapanFirstOffice=getCellValue(transalationSheet, 290, 1);
			String expectedJapanFirstCity=getCellValue(transalationSheet, 291, 1);
			String expectedLuxembourgFirstOffice=getCellValue(transalationSheet, 292, 1);
			String expectedLuxembourgFirstCity=getCellValue(transalationSheet, 293, 1);
			String expectedMalaysiaFirstOffice=getCellValue(transalationSheet, 294, 1);
			String expectedMalaysiaSecondOffice=getCellValue(transalationSheet, 295, 1);
			String expectedMalaysiaThirdOffice=getCellValue(transalationSheet, 296, 1);
			String expectedMalaysiaFourthOffice=getCellValue(transalationSheet, 297, 1);
			String expectedMalaysiaFifthOffice=getCellValue(transalationSheet, 298, 1);
			String expectedMalaysiaSixthOffice=getCellValue(transalationSheet, 299, 1);
			String expectedMalaysiaSeventhOffice=getCellValue(transalationSheet, 300, 1);
			String expectedMalaysiaEighthOffice=getCellValue(transalationSheet, 301, 1);
			String expectedMalaysiaFirstCity=getCellValue(transalationSheet, 302, 1);
			String expectedMalaysiaSecondCity=getCellValue(transalationSheet, 303, 1);
			String expectedMalaysiaThirdCity=getCellValue(transalationSheet, 304, 1);
			String expectedMalaysiaFourthCity=getCellValue(transalationSheet, 305, 1);
			String expectedMalaysiaFifthCity=getCellValue(transalationSheet, 306, 1);
			String expectedMalaysiaSixthCity=getCellValue(transalationSheet, 307, 1);
			String expectedMalaysiaSeventhCity=getCellValue(transalationSheet, 308, 1);
			String expectedMalaysiaEighthCity=getCellValue(transalationSheet, 309, 1);
			String expectedMexicoFirstOffice=getCellValue(transalationSheet, 310, 1);
			String expectedMexicoFirstCity=getCellValue(transalationSheet, 311, 1);
			String expectedNetherlandsFirstOffice=getCellValue(transalationSheet, 312, 1);
			String expectedNetherlandsSecondOffice=getCellValue(transalationSheet, 313, 1);
			String expectedNetherlandsFirstCity=getCellValue(transalationSheet, 314, 1);
			String expectedNetherlandsSecondCity=getCellValue(transalationSheet, 315, 1);
			String expectedNewZealandFirstOffice=getCellValue(transalationSheet, 316, 1);
			String expectedNewZealandSecondOffice=getCellValue(transalationSheet, 317, 1);
			String expectedNewZealandThirdOffice=getCellValue(transalationSheet, 318, 1);
			String expectedNewZealandFirstCity=getCellValue(transalationSheet, 319, 1);
			String expectedNewZealandSecondCity=getCellValue(transalationSheet, 320, 1);
			String expectedNewZealandThirdCity=getCellValue(transalationSheet, 321, 1);
			String expectedPeruFirstOffice=getCellValue(transalationSheet, 322, 1);
			String expectedPeruFirstCity=getCellValue(transalationSheet, 323, 1);
			String expectedPhilippinesFirstOffice=getCellValue(transalationSheet, 324, 1);
			String expectedPhilippinesSecondOffice=getCellValue(transalationSheet, 325, 1);
			String expectedPhilippinesThirdOffice=getCellValue(transalationSheet, 326, 1);
			String expectedPhilippinesFourthOffice=getCellValue(transalationSheet, 327, 1);
			String expectedPhilippinesFifthOffice=getCellValue(transalationSheet, 328, 1);
			String expectedPhilippinesFirstCity=getCellValue(transalationSheet, 329, 1);
			String expectedPhilippinesSecondCity=getCellValue(transalationSheet, 330, 1);
			String expectedPhilippinesThirdCity=getCellValue(transalationSheet, 331, 1);
			String expectedPhilippinesFourthCity=getCellValue(transalationSheet, 332, 1);
			String expectedPhilippinesFifthCity=getCellValue(transalationSheet, 333, 1);
			String expectedSingaporeFirstOffice=getCellValue(transalationSheet, 334, 1);
			String expectedSingaporeFirstCity=getCellValue(transalationSheet, 335, 1);
			String expectedSpainFirstOffice=getCellValue(transalationSheet, 336, 1);
			String expectedSpainSecondOffice=getCellValue(transalationSheet, 337, 1);
			String expectedSpainFirstCity=getCellValue(transalationSheet, 338, 1);
			String expectedSpainSecondCity=getCellValue(transalationSheet, 339, 1);
			String expectedSwitzerlandFirstOffice=getCellValue(transalationSheet, 340, 1);
			String expectedSwitzerlandSecondOffice=getCellValue(transalationSheet, 341, 1);
			String expectedSwitzerlandThirdOffice=getCellValue(transalationSheet, 342, 1);
			String expectedSwitzerlandFirstCity=getCellValue(transalationSheet, 343, 1);
			String expectedSwitzerlandSecondCity=getCellValue(transalationSheet, 344, 1);
			String expectedSwitzerlandThirdCity=getCellValue(transalationSheet, 345, 1);
			String expectedTaiwanFirstOffice=getCellValue(transalationSheet, 346, 1);
			String expectedTaiwanSecondOffice=getCellValue(transalationSheet, 347, 1);
			String expectedTaiwanThirdOffice=getCellValue(transalationSheet, 348, 1);
			String expectedTaiwanFirstCity=getCellValue(transalationSheet, 349, 1);
			String expectedTaiwanSecondCity=getCellValue(transalationSheet, 350, 1);
			String expectedTaiwanThirdCity=getCellValue(transalationSheet, 351, 1);
			String expectedUnitedKingdomFirstOffice=getCellValue(transalationSheet, 352, 1);
			String expectedUnitedKingdomSecondOffice=getCellValue(transalationSheet, 353, 1);
			String expectedUnitedKingdomFirstCity=getCellValue(transalationSheet, 354, 1);
			String expectedUnitedKingdomSecondCity=getCellValue(transalationSheet, 355, 1);
			String expectedUSAFirstOffice=getCellValue(transalationSheet, 356, 1);
			String expectedUSASecondOffice=getCellValue(transalationSheet, 357, 1);
			String expectedUSAThirdOffice=getCellValue(transalationSheet, 358, 1);
			String expectedUSAFourthOffice=getCellValue(transalationSheet, 359, 1);
			String expectedUSAFifthOffice=getCellValue(transalationSheet, 360, 1);
			String expectedUSASixthOffice=getCellValue(transalationSheet, 361, 1);
			String expectedUSASeventhOffice=getCellValue(transalationSheet, 362, 1);
			String expectedUSAEighthOffice=getCellValue(transalationSheet, 363, 1);
			String expectedUSANinthOffice=getCellValue(transalationSheet, 364, 1);
			String expectedUSATenthOffice=getCellValue(transalationSheet, 365, 1);
			String expectedUSAEleventhOffice=getCellValue(transalationSheet, 366, 1);
			String expectedUSATwevelthOffice=getCellValue(transalationSheet, 367, 1);
			String expectedUSAThirteenthOffice=getCellValue(transalationSheet, 368, 1);
			String expectedUSAFourteenthOffice=getCellValue(transalationSheet, 369, 1);
			String expectedUSAFirstCity=getCellValue(transalationSheet,370 , 1);
			String expectedUSASecondCity=getCellValue(transalationSheet, 371, 1);
			String expectedUSAThirdCity=getCellValue(transalationSheet, 372, 1);
			String expectedUSAFourthCity=getCellValue(transalationSheet, 373, 1);
			String expectedUSAFifthCity=getCellValue(transalationSheet, 374, 1);
			String expectedUSASixthCity=getCellValue(transalationSheet, 375, 1);
			String expectedUSASeventhCity=getCellValue(transalationSheet, 376, 1);
			String expectedUSAEighthCity=getCellValue(transalationSheet, 377, 1);
			String expectedUSANinthCity=getCellValue(transalationSheet, 378, 1);
			String expectedUSATenthCity=getCellValue(transalationSheet, 379, 1);
			String expectedUSAEleventhCity=getCellValue(transalationSheet, 380, 1);
			String expectedUSATwevelthCity=getCellValue(transalationSheet, 381, 1);
			String expectedUSAThirteenthCity=getCellValue(transalationSheet, 382, 1);
			String expectedUSAFourteenthCity=getCellValue(transalationSheet, 383, 1);


			LandingPage ld = new LandingPage(driver);
			EditProfilePage ep = new EditProfilePage(driver);
			EditProfileLocationPage el = new EditProfileLocationPage(driver);

			clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
			String profilePageLocation=getText(el.verifyProfilePageLocation(), platform);
			String actualProfileLocation[]=profilePageLocation.split(" ", 2);
			profilePageLocation=actualProfileLocation[1];
			clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
			clickOnElement("Location", clickElement(ep.verifyProfileLocation()));
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));

			String actualCountryHeaderLabel =getText(el.verifyLocationHeader(),platform);
			String actualAllCountriesLabel =getText(el.verifyLocationSubHeader(),platform);
			String actualFirstCountry =getText(el.verifyFirstCountry(),platform);
			String actualSecondCountry =getText(el.verifySecondCountry(),platform);
			String actualThirdCountry =getText(el.verifyThirdCountry(),platform);
			String actualFourthCountry =getText(el.verifyFourthCountry(),platform);
			String actualFifthCountry =getText(el.verifyFifthCountry(),platform);
			String actualSixthCountry =getText(el.verifySixthCountry(),platform);
			String actualSeventhCountry =getText(el.verifySeventhCountry(),platform);
			String actualEighthCountry =getText(el.verifyEighthCountry(),platform);;
			String actualNinthCountry =getText(el.verifyNinethCountry(),platform);
			String actualTenthCountry =getText(el.verifyTenthCountry(),platform);
			String actualEleventhCountry =getText(el.verifyEleventhCountry(),platform);
			String actualTwelvethCountry =getText(el.verifyTwelevethCountry(),platform);
			String actualThirteenthCountry =getText(el.verifyThirteenthCountry(),platform);
			scroll(platform, "Singapore");
			scroll(platform, "Spain");
			Thread.sleep(3000);
			String actualFourtteenthCountry =getText(el.verifyFourteenthCountry(),platform);
			String actualFiftteenthCountry =getText(el.verifyFifteenthCountry(),platform);
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			String actualSixteenthCountry =getText(el.verifySixteenthCountry(),platform);
			String actualSeventeenthCountry =getText(el.verifySeventeenthCountry(),platform);
			String actualEighteenthCountry =getText(el.verifyEighteenthCountry(),platform);;
			String actualNineteenthCountry =getText(el.verifyNineteenthCountry(),platform);
			String actualTwentiethCountry =getText(el.verifyTwenteithCountry(),platform);
			String actualTwentyFirstCountry =getText(el.verifyTwentyFirstCountry(),platform);
			String actualTwentySecondCountry =getText(el.verifyTwentySecondCountry(),platform);
			String actualTwentyThirdCountry =getText(el.verifyTwentyThirdCountry(),platform);
			String actualTwentyFourthCountry =getText(el.verifyTwentyFourthCountry(),platform);
			String actualTwentyFifthCountry =getText(el.verifyTwentyFifthCountry(),platform);
			String actualTwentySixthCountry =getText(el.verifyTwentySixthCountry(),platform);
			String actualTwentySeventhCountry =getText(el.verifyTwentySeventhCountry(),platform);
			String actualTwentyEighthCountry =getText(el.verifyTwentyEighthCountry(),platform);

			verifyLabel("Location Header", matchData(expectedCountryHeaderLabel,actualCountryHeaderLabel));
			verifyLabel("COUNTRIES", matchData(expectedAllCountriesLabel,actualAllCountriesLabel));
			verifyLabel("Argentina", matchData(expectedFirstCountry,actualFirstCountry));
			verifyLabel("Australia", matchData(expectedSecondCountry,actualSecondCountry));
			verifyLabel("Austria", matchData(expectedThirdCountry,actualThirdCountry));
			verifyLabel("Belgium", matchData(expectedFourthCountry,actualFourthCountry));
			verifyLabel("Brazil", matchData(expectedFifthCountry,actualFifthCountry));
			verifyLabel("Canada", matchData(expectedSixthCountry,actualSixthCountry));
			verifyLabel("China", matchData(expectedSeventhCountry,actualSeventhCountry));
			verifyLabel("Colombia", matchData(expectedEighthCountry,actualEighthCountry));
			verifyLabel("Costa Rica", matchData(expectedNinthCountry,actualNinthCountry));
			verifyLabel("France", matchData(expectedTenthCountry,actualTenthCountry));
			verifyLabel("Germany", matchData(expectedEleventhCountry,actualEleventhCountry));
			verifyLabel("Hong Kong", matchData(expectedTwelvethCountry,actualTwelvethCountry));
			verifyLabel("Hungary", matchData(expectedThirteenthCountry,actualThirteenthCountry));
			verifyLabel("India", matchData(expectedFourtteenthCountry,actualFourtteenthCountry));
			verifyLabel("Japan", matchData(expectedFiftteenthCountry,actualFiftteenthCountry));
			verifyLabel("Luxembourg", matchData(expectedSixteenthCountry,actualSixteenthCountry));
			verifyLabel("Malaysia", matchData(expectedSeventeenthCountry,actualSeventeenthCountry));
			verifyLabel("Mexico", matchData(expectedEighteenthCountry,actualEighteenthCountry));
			verifyLabel("Netherlands", matchData(expectedNineteenthCountry,actualNineteenthCountry));
			verifyLabel("New Zealand", matchData(expectedTwentiethCountry,actualTwentiethCountry));
			verifyLabel("Peru", matchData(expectedTwentyFirstCountry,actualTwentyFirstCountry));
			verifyLabel("Philippines", matchData(expectedTwentySecondCountry,actualTwentySecondCountry));
			verifyLabel("Singapore", matchData(expectedTwentyThirdCountry,actualTwentyThirdCountry));
			verifyLabel("Spain", matchData(expectedTwentyFourthCountry,actualTwentyFourthCountry));
			verifyLabel("Switzerland", matchData(expectedTwentyFifthCountry,actualTwentyFifthCountry));
			verifyLabel("Taiwan", matchData(expectedTwentySixthCountry,actualTwentySixthCountry));
			verifyLabel("United Kingdom", matchData(expectedTwentySeventhCountry,actualTwentySeventhCountry));
			verifyLabel("United states of America", matchData(expectedTwentyEighthCountry,actualTwentyEighthCountry));
			verifyElement("Back Icon", elementDisplayed(el.verifyCountryPageBackIcon()));
			clickOnElement("Back Icon", clickElement(el.verifyCountryPageBackIcon()));
			verifyElement("Office Selection Screen", elementDisplayed(el.verifyAllLocationsLabel()));
			String actualLocationTitleLabel=getText(el.verifyLocationTitle(),platform);
			String actualCountryLabel=getText(el.verifyCountryLabel(),platform);
			String actualAllLocationsLabel=getText(el.verifyAllLocationsLabel(),platform);
			verifyLabel("Location", matchData(expectedlLocationTitleLabel,actualLocationTitleLabel));
			verifyLabel("COUNTRY", matchData(expectedCountryLabel,actualCountryLabel));
			verifyLabel("All LOCATIONS", matchData(aexpectedAllLocationsLabel,actualAllLocationsLabel));
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("First Country", clickElement(el.verifyFirstCountry()));
			Thread.sleep(2000);
			String actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedFirstCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualArgentinaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualArgentinaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualArgentinaThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualArgentinaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualArgentinaSecondCity=getText(el.verifySecondCity(),platform);
				String actualArgentinaThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("Av. Alicia Moreau de Justo ", matchData(expectedArgentinaFirstOffice,actualArgentinaFirstOffice));
				verifyLabel("Primitivo de la Reta ", matchData(expectedArgentinaSecondOffice,actualArgentinaSecondOffice));
				verifyLabel("Calle CÃ³rdoba", matchData(expectedArgentinaThirdOffice,actualArgentinaThirdOffice));
				verifyLabel("Buenos Aires", matchData(expectedArgentinaFirstCity,actualArgentinaFirstCity));
				verifyLabel("Mendoza", matchData(expectedArgentinaSecondCity,actualArgentinaSecondCity));
				verifyLabel("Parana", matchData(expectedArgentinaThirdCity,actualArgentinaThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedFirstCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Second Country", clickElement(el.verifySecondCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			System.out.println(actualSelectedCountry);
			System.out.println(expectedSecondCountry);
			if(expectedSecondCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualAustraliaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualAustraliaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualAustraliaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualAustraliaSecondCity=getText(el.verifySecondCity(),platform);
				verifyLabel("Moore Street", matchData(expectedAustraliaFirstOffice,actualAustraliaFirstOffice));
				verifyLabel("Homebush Bay Drive", matchData(expectedAustraliaSecondOffice,actualAustraliaSecondOffice));
				verifyLabel("Canberra", matchData(expectedAustraliaFirstCity,actualAustraliaFirstCity));
				verifyLabel("Sydney", matchData(expectedAustraliaSecondCity,actualAustraliaSecondCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedSecondCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Third Country", clickElement(el.verifyThirdCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			System.out.println(actualSelectedCountry);
			System.out.println(expectedThirdCountry);
			if(expectedThirdCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualAustriaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualAustriaFirstCity=getText(el.verifyFirstCity(),platform);
				verifyLabel("Andromeda Tower", matchData(expectedAustriaFirstOffice,actualAustriaFirstOffice));
				verifyLabel("Vienna", matchData(expectedAustriaFirstCity,actualAustriaFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedThirdCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Fourth Country", clickElement(el.verifyFourthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			System.out.println(actualSelectedCountry);
			System.out.println(expectedFourthCountry);
			if(expectedFourthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualBelgiumFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualBelgiumFirstCity=getText(el.verifyFirstCity(),platform);
				verifyLabel("Telecomlaan", matchData(expectedBelgiumFirstOffice,actualBelgiumFirstOffice));
				verifyLabel("Diegem", matchData(expectedBelgiumFirstCity,actualBelgiumFirstCity));
			}else{
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedFourthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Fifth Country", clickElement(el.verifyFifthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			System.out.println(actualSelectedCountry);
			System.out.println(expectedFifthCountry);
			if(expectedFifthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualBrazilFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualBrazilSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualBrazilThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualBrazilFourthOffice=getText(el.verifyFourthOffice(),platform);
				String actualBrazilFifthOffice=getText(el.verifyFifthOffice(),platform);
				String actualBrazilSixthOffice=getText(el.verifySixthOffice(),platform);
				String actualBrazilSeventhOffice=getText(el.verifySeventhOffice(),platform);
				String actualBrazilEighthOffice=getText(el.verifyEighthOffice(),platform);
				String actualBrazilFirstCity=getText(el.verifyFirstCity(),platform);
				String actualBrazilSecondCity=getText(el.verifySecondCity(),platform);
				String actualBrazilThirdCity=getText(el.verifyThirdCity(),platform);
				String actualBrazilFourthCity=getText(el.verifyFourthCity(),platform);
				String actualBrazilFifthCity=getText(el.verifyFifthCity(),platform);
				String actualBrazilSixthCity=getText(el.verifySixthCity(),platform);
				String actualBrazilSeventhCity=getText(el.verifySeventhCity(),platform);
				String actualBrazilEighthCity=getText(el.verifyEighthCity(),platform);
				verifyLabel("SCN - Quadra 4", matchData(expectedBrazilFirstOffice,actualBrazilFirstOffice));
				verifyLabel("DH3 - condominio Techno Park", matchData(expectedBrazilSecondOffice,actualBrazilSecondOffice));
				verifyLabel("Rua Antonio Maria Coelho", matchData(expectedBrazilThirdOffice,actualBrazilThirdOffice));
				verifyLabel("Alameda Oscar Niemeyer", matchData(expectedBrazilFourthOffice,actualBrazilFourthOffice));
				verifyLabel("Rua da Gloria", matchData(expectedBrazilFifthOffice,actualBrazilFifthOffice));
				verifyLabel("Morumbi - Centro Corporativo", matchData(expectedBrazilSixthOffice,actualBrazilSixthOffice));
				verifyLabel("Sao Paulo - Birmann - Centro Operacional", matchData(expectedBrazilSeventhOffice,actualBrazilSeventhOffice));
				verifyLabel("Embu - Logistic Center", matchData(expectedBrazilEighthOffice,actualBrazilEighthOffice));
				verifyLabel("Brasilia", matchData(expectedBrazilFirstCity,actualBrazilFirstCity));
				verifyLabel("Campinas", matchData(expectedBrazilSecondCity,actualBrazilSecondCity));
				verifyLabel("Campo Grande", matchData(expectedBrazilThirdCity,actualBrazilThirdCity));
				verifyLabel("Nova Lima", matchData(expectedBrazilFourthCity,actualBrazilFourthCity));
				verifyLabel("Rio de Janeiro", matchData(expectedBrazilFifthCity,actualBrazilFifthCity));
				verifyLabel("Sao Paulo", matchData(expectedBrazilSixthCity,actualBrazilSixthCity));
				verifyLabel("Sao Paulo", matchData(expectedBrazilSeventhCity,actualBrazilSeventhCity));
				verifyLabel("Sao Paulo", matchData(expectedBrazilEighthCity,actualBrazilEighthCity));
			}else{
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedFifthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Sixth Country", clickElement(el.verifySixthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedSixthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualCanadaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualCanadaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualCanadaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualCanadaSecondCity=getText(el.verifySecondCity(),platform);
				verifyLabel("CIBC Building", matchData(expectedCanadaFirstOffice,actualCanadaFirstOffice));
				verifyLabel("270 Albert St.", matchData(expectedCanadaSecondOffice,actualCanadaSecondOffice));
				verifyLabel("Halifax", matchData(expectedCanadaFirstCity,actualCanadaFirstCity));
				verifyLabel("Ottawa", matchData(expectedCanadaSecondCity,actualCanadaSecondCity));
			}else{
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedSixthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Seventh Country", clickElement(el.verifySeventhCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedSeventhCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualChinaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualChinaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualChinaThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualChinaFourthOffice=getText(el.verifyFourthOffice(),platform);
				String actualChinaFifthOffice=getText(el.verifyFifthOffice(),platform);
				String actualChinaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualChinaSecondCity=getText(el.verifySecondCity(),platform);
				String actualChinaThirdCity=getText(el.verifyThirdCity(),platform);
				String actualChinaFourthCity=getText(el.verifyFourthCity(),platform);
				String actualChinaFifthCity=getText(el.verifyFifthCity(),platform);
				verifyLabel("Oriental Plaza", matchData(expectedChinaFirstOffice,actualChinaFirstOffice));
				verifyLabel("Silver Court", matchData(expectedChinaSecondOffice,actualChinaSecondOffice));
				verifyLabel("Zhangjiang Hi-Tech Area", matchData(expectedChinaThirdOffice,actualChinaThirdOffice));
				verifyLabel("Anlian Centre", matchData(expectedChinaFourthOffice,actualChinaFourthOffice));
				verifyLabel("Ginza Mansion", matchData(expectedChinaFifthOffice,actualChinaFifthOffice));
				verifyLabel("Beijing", matchData(expectedChinaFirstCity,actualChinaFirstCity));
				verifyLabel("Shanghai", matchData(expectedChinaSecondCity,actualChinaSecondCity));
				verifyLabel("Shanghai", matchData(expectedChinaThirdCity,actualChinaThirdCity));
				verifyLabel("Shenzhen", matchData(expectedChinaFourthCity,actualChinaFourthCity));
				verifyLabel("Tianjin", matchData(expectedChinaFifthCity,actualChinaFifthCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedSeventhCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Eighth Country", clickElement(el.verifyEighthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedEighthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualColombiaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualColombiaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualColombiaThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualColombiaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualColombiaSecondCity=getText(el.verifySecondCity(),platform);
				String actualColombiaThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("Bogota Distrito Capital de BogotÃ¡ 0", matchData(expectedColombiaFirstOffice,actualColombiaFirstOffice));
				verifyLabel("Medellin Antioquia", matchData(expectedColombiaSecondOffice,actualColombiaSecondOffice));
				verifyLabel("Vereda Chachafruto Zona Franca", matchData(expectedColombiaThirdOffice,actualColombiaThirdOffice));
				verifyLabel("Bogota", matchData(expectedColombiaFirstCity,actualColombiaFirstCity));
				verifyLabel("Medellin", matchData(expectedColombiaSecondCity,actualColombiaSecondCity));
				verifyLabel("Rionegro", matchData(expectedColombiaThirdCity,actualColombiaThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedEighthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Nineth Country", clickElement(el.verifyNinethCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedNinthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualCostaRicaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualCostaRicaFirstCity=getText(el.verifyFirstCity(),platform);
				verifyLabel("Av. Alicia Moreau de Justo ", matchData(expectedCostaRicaFirstOffice,actualCostaRicaFirstOffice));
				verifyLabel("Mendoza", matchData(expectedCostaRicaFirstCity,actualCostaRicaFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedNinthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Tenth Country", clickElement(el.verifyTenthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualFranceFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualFranceFirstCity=getText(el.verifyFirstCity(),platform);
				verifyLabel("Tour Nova", matchData(expectedFranceFirstOffice,actualFranceFirstOffice));
				verifyLabel("Colombes", matchData(expectedFranceFirstCity,actualFranceFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Eleventh Country", clickElement(el.verifyEleventhCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedEleventhCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualGermanyFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualGermanySecondOffice=getText(el.verifySecondOffice(),platform);
				String actualGermanyThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualGermanyFirstCity=getText(el.verifyFirstCity(),platform);
				String actualGermanySecondCity=getText(el.verifySecondCity(),platform);
				String actualGermanyThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("Henkelstrasse", matchData(expectedGermanyFirstOffice,actualGermanyFirstOffice));
				verifyLabel("Philipp-Reis-Str. 2", matchData(expectedGermanySecondOffice,actualGermanySecondOffice));
				verifyLabel("Pelkovenstrasse", matchData(expectedGermanyThirdOffice,actualGermanyThirdOffice));
				verifyLabel("DÃ¼sseldorf", matchData(expectedGermanyFirstCity,actualGermanyFirstCity));
				verifyLabel("Hattersheim", matchData(expectedGermanySecondCity,actualGermanySecondCity));
				verifyLabel("Munich", matchData(expectedGermanyThirdCity,actualGermanyThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedEleventhCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Twelveth Country", clickElement(el.verifyTwelevethCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwelvethCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualHongKongFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualHongKongFirstCity=getText(el.verifyFirstCity(),platform);
				verifyLabel("Manulife Financial Centre", matchData(expectedHongKongFirstOffice,actualHongKongFirstOffice));
				verifyLabel("Hong Kong", matchData(expectedHongKongFirstCity,actualHongKongFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwelvethCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Thirteenth Country", clickElement(el.verifyThirteenthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedThirteenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualHungaryFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualHungarySecondOffice=getText(el.verifySecondOffice(),platform);
				String actualHungaryThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualHungaryFirstCity=getText(el.verifyFirstCity(),platform);
				String actualHungarySecondCity=getText(el.verifySecondCity(),platform);
				String actualHungaryThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("Westend City Centre ", matchData(expectedHungaryFirstOffice,actualHungaryFirstOffice));
				verifyLabel("Budapest One", matchData(expectedHungarySecondOffice,actualHungarySecondOffice));
				verifyLabel("Vince", matchData(expectedHungaryThirdOffice,actualHungaryThirdOffice));
				verifyLabel("Budapest", matchData(expectedHungaryFirstCity,actualHungaryFirstCity));
				verifyLabel("Budapest", matchData(expectedHungarySecondCity,actualHungarySecondCity));
				verifyLabel("Pecs", matchData(expectedHungaryThirdCity,actualHungaryThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedThirteenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "Singapore");
			scroll(platform, "Spain");
			Thread.sleep(4000);
			clickOnElement("Fourteenth Country", clickElement(el.verifyFourteenthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedFourtteenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualIndiaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualIndiaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualIndiaThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualIndiaFourthOffice=getText(el.verifyFourthOffice(),platform);
				String actualIndiaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualIndiaSecondCity=getText(el.verifySecondCity(),platform);
				String actualIndiaThirdCity=getText(el.verifyThirdCity(),platform);
				String actualIndiaFourthCity=getText(el.verifyFourthCity(),platform);
				verifyLabel("Gopalan Global Axis", matchData(expectedIndiaFirstOffice,actualIndiaFirstOffice));
				verifyLabel("Rajajinagar IT Park", matchData(expectedIndiaSecondOffice,actualIndiaSecondOffice));
				verifyLabel("RGA Tech Park", matchData(expectedIndiaThirdOffice,actualIndiaThirdOffice));
				verifyLabel("DLF Cyber City", matchData(expectedIndiaFourthOffice,actualIndiaFourthOffice));
				verifyLabel("Bangalore", matchData(expectedIndiaFirstCity,actualIndiaFirstCity));
				verifyLabel("Bangalore", matchData(expectedIndiaSecondCity,actualIndiaSecondCity));
				verifyLabel("Bangalore", matchData(expectedIndiaThirdCity,actualIndiaThirdCity));
				verifyLabel("Hyderabad", matchData(expectedIndiaFourthCity,actualIndiaFourthCity));
				s.assertEquals(expectedFourtteenthCountry, actualSelectedCountry);
//			}else if(expectedFiftteenthCountry.equals(actualSelectedCountry)) {
//				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
//				String actualJapanFirstOffice=getText(el.verifyFirstOffice(),platform);
//				String actualJapanFirstCity=getText(el.verifyFirstCity(),platform);
//				verifyLabel("Marunouchi 2 chome Bldg", matchData(expectedJapanFirstOffice,actualJapanFirstOffice));
//				verifyLabel("Tokyo", matchData(expectedJapanFirstCity,actualJapanFirstCity));
//				s.assertEquals(expectedFiftteenthCountry, actualSelectedCountry);
			}else{
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}

			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "Singapore");
			scroll(platform, "Spain");
			Thread.sleep(3000);
			clickOnElement("Fifteen Country", clickElement(el.verifyFifteenthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedFiftteenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualJapanFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualJapanFirstCity=getText(el.verifyFirstCity(),platform);
				verifyLabel("Marunouchi 2 chome Bldg", matchData(expectedJapanFirstOffice,actualJapanFirstOffice));
				verifyLabel("Tokyo", matchData(expectedJapanFirstCity,actualJapanFirstCity));
//			}else if(expectedFourtteenthCountry.equals(actualSelectedCountry)) {
//				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
//				String actualIndiaFirstOffice=getText(el.verifyFirstOffice(),platform);
//				String actualIndiaSecondOffice=getText(el.verifySecondOffice(),platform);
//				String actualIndiaThirdOffice=getText(el.verifyThirdOffice(),platform);
//				String actualIndiaFourthOffice=getText(el.verifyFourthOffice(),platform);
//				String actualIndiaFirstCity=getText(el.verifyFirstCity(),platform);
//				String actualIndiaSecondCity=getText(el.verifySecondCity(),platform);
//				String actualIndiaThirdCity=getText(el.verifyThirdCity(),platform);
//				String actualIndiaFourthCity=getText(el.verifyFourthCity(),platform);
//				verifyLabel("Gopalan Global Axis", matchData(expectedIndiaFirstOffice,actualIndiaFirstOffice));
//				verifyLabel("Rajajinagar IT Park", matchData(expectedIndiaSecondOffice,actualIndiaSecondOffice));
//				verifyLabel("RGA Tech Park", matchData(expectedIndiaThirdOffice,actualIndiaThirdOffice));
//				verifyLabel("DLF Cyber City", matchData(expectedIndiaFourthOffice,actualIndiaFourthOffice));
//				verifyLabel("Bangalore", matchData(expectedIndiaFirstCity,actualIndiaFirstCity));
//				verifyLabel("Bangalore", matchData(expectedIndiaSecondCity,actualIndiaSecondCity));
//				verifyLabel("Bangalore", matchData(expectedIndiaThirdCity,actualIndiaThirdCity));
//				verifyLabel("Hyderabad", matchData(expectedIndiaFourthCity,actualIndiaFourthCity));
//				s.assertEquals(expectedFourtteenthCountry, actualSelectedCountry);
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}

			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Sixteenth Country", clickElement(el.verifySixteenthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedSixteenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualLuxembourgFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualLuxembourgFirstCity=getText(el.verifyFirstCity(),platform);
				verifyLabel("Route des Trois Cantons", matchData(expectedLuxembourgFirstOffice,actualLuxembourgFirstOffice));
				verifyLabel("Luxembourg", matchData(expectedLuxembourgFirstCity,actualLuxembourgFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedSixteenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Seventeenth Country", clickElement(el.verifySeventeenthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedSeventeenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualMalaysiaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualMalaysiaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualMalaysiaThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualMalaysiaFourthOffice=getText(el.verifyFourthOffice(),platform);
				String actualMalaysiaFifthOffice=getText(el.verifyFifthOffice(),platform);
				String actualMalaysiaSixthOffice=getText(el.verifySixthOffice(),platform);
				String actualMalaysiaSeventhOffice=getText(el.verifySeventhOffice(),platform);
				String actualMalaysiaEighthOffice=getText(el.verifyEighthOffice(),platform);
				String actualMalaysiaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualMalaysiaSecondCity=getText(el.verifySecondCity(),platform);
				String actualMalaysiaThirdCity=getText(el.verifyThirdCity(),platform);
				String actualMalaysiaFourthCity=getText(el.verifyFourthCity(),platform);
				String actualMalaysiaFifthCity=getText(el.verifyFifthCity(),platform);
				String actualMalaysiaSixthCity=getText(el.verifySixthCity(),platform);
				String actualMalaysiaSeventhCity=getText(el.verifySeventhCity(),platform);
				String actualMalaysiaEighthCity=getText(el.verifyEighthCity(),platform);
				verifyLabel("Komplek Sultan Abdul Hamid", matchData(expectedMalaysiaFirstOffice,actualMalaysiaFirstOffice));
				verifyLabel("JALAN LEONG SIN NAM", matchData(expectedMalaysiaSecondOffice,actualMalaysiaSecondOffice));
				verifyLabel("65 Jalan Trus", matchData(expectedMalaysiaThirdOffice,actualMalaysiaThirdOffice));
				verifyLabel("HP TOWERS", matchData(expectedMalaysiaFourthOffice,actualMalaysiaFourthOffice));
				verifyLabel("Sri Dagangan Business Centre", matchData(expectedMalaysiaFifthOffice,actualMalaysiaFifthOffice));
				verifyLabel("Jalan Melaka Raya", matchData(expectedMalaysiaSixthOffice,actualMalaysiaSixthOffice));
				verifyLabel("Bangunan KWSP", matchData(expectedMalaysiaSeventhOffice,actualMalaysiaSeventhOffice));
				verifyLabel("Damansara Uptown", matchData(expectedMalaysiaEighthOffice,actualMalaysiaEighthOffice));
				verifyLabel("ALOR SETAR", matchData(expectedMalaysiaFirstCity,actualMalaysiaFirstCity));
				verifyLabel("IPOH", matchData(expectedMalaysiaSecondCity,actualMalaysiaSecondCity));
				verifyLabel("Johor Bahru", matchData(expectedMalaysiaThirdCity,actualMalaysiaThirdCity));
				verifyLabel("KUALA LUMPUR", matchData(expectedMalaysiaFourthCity,actualMalaysiaFourthCity));
				verifyLabel("KUANTAN", matchData(expectedMalaysiaFifthCity,actualMalaysiaFifthCity));
				verifyLabel("Melaka", matchData(expectedMalaysiaSixthCity,actualMalaysiaSixthCity));
				verifyLabel("Penang", matchData(expectedMalaysiaSeventhCity,actualMalaysiaSeventhCity));
				verifyLabel("Petaling Jaya", matchData(expectedMalaysiaEighthCity,actualMalaysiaEighthCity));
			}else{
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedSeventeenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Eighteenth Country", clickElement(el.verifyEighteenthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedEighteenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualMexicoFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualMexicoFirstCity=getText(el.verifyFirstCity(),platform);
				verifyLabel("Av. Paseo de la Reforma", matchData(expectedMexicoFirstOffice,actualMexicoFirstOffice));
				verifyLabel("Mexico City", matchData(expectedMexicoFirstCity,actualMexicoFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedEighteenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Nineteenth Country", clickElement(el.verifyNineteenthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedNineteenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualNetherlandsFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualNetherlandsSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualNetherlandsFirstCity=getText(el.verifyFirstCity(),platform);
				String actualNetherlandsSecondCity=getText(el.verifySecondCity(),platform);
				verifyLabel("Spicalaan", matchData(expectedNetherlandsFirstOffice,actualNetherlandsFirstOffice));
				verifyLabel("UPSS Leusden", matchData(expectedNetherlandsSecondOffice,actualNetherlandsSecondOffice));
				verifyLabel("Hoofddorp", matchData(expectedNetherlandsFirstCity,actualNetherlandsFirstCity));
				verifyLabel("Leusden", matchData(expectedNetherlandsSecondCity,actualNetherlandsSecondCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedNineteenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Twentieth Country", clickElement(el.verifyTwenteithCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentiethCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualNewZealandFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualNewZealandSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualNewZealandThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualNewZealandFirstCity=getText(el.verifyFirstCity(),platform);
				String actualNewZealandSecondCity=getText(el.verifySecondCity(),platform);
				String actualNewZealandThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("Great South Road", matchData(expectedNewZealandFirstOffice,actualNewZealandFirstOffice));
				verifyLabel("Te Roto Drive", matchData(expectedNewZealandSecondOffice,actualNewZealandSecondOffice));
				verifyLabel("Radio New Zealand House", matchData(expectedNewZealandThirdOffice,actualNewZealandThirdOffice));
				verifyLabel("Auckland", matchData(expectedNewZealandFirstCity,actualNewZealandFirstCity));
				verifyLabel("Kapiti", matchData(expectedNewZealandSecondCity,actualNewZealandSecondCity));
				verifyLabel("Wellington", matchData(expectedNewZealandThirdCity,actualNewZealandThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentiethCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Twenty First Country", clickElement(el.verifyTwentyFirstCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentyFirstCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualPeruFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualPeruFirstCity=getText(el.verifyFirstCity(),platform);
				verifyLabel("Calle Las Camelias", matchData(expectedPeruFirstOffice,actualPeruFirstOffice));
				verifyLabel("Lima", matchData(expectedPeruFirstCity,actualPeruFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentyFirstCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Twenty Second", clickElement(el.verifyTwentySecondCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentySecondCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualPhilippinesFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualPhilippinesSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualPhilippinesThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualPhilippinesFourthOffice=getText(el.verifyFourthOffice(),platform);
				String actualPhilippinesFifthOffice=getText(el.verifyFifthOffice(),platform);
				String actualPhilippinesFirstCity=getText(el.verifyFirstCity(),platform);
				String actualPhilippinesSecondCity=getText(el.verifySecondCity(),platform);
				String actualPhilippinesThirdCity=getText(el.verifyThirdCity(),platform);
				String actualPhilippinesFourthCity=getText(el.verifyFourthCity(),platform);
				String actualPhilippinesFifthCity=getText(el.verifyFifthCity(),platform);
				verifyLabel("Nepo Center", matchData(expectedPhilippinesFirstOffice,actualPhilippinesFirstOffice));
				verifyLabel("Barangay Highway Hills", matchData(expectedPhilippinesSecondOffice,actualPhilippinesSecondOffice));
				verifyLabel("One Cyberpod Centris", matchData(expectedPhilippinesThirdOffice,actualPhilippinesThirdOffice));
				verifyLabel("Diosdado Macapagal Ave", matchData(expectedPhilippinesFourthOffice,actualPhilippinesFourthOffice));
				verifyLabel("EDSA corner Cornel Street", matchData(expectedPhilippinesFifthOffice,actualPhilippinesFifthOffice));
				verifyLabel("Angeles", matchData(expectedPhilippinesFirstCity,actualPhilippinesFirstCity));
				verifyLabel("CityNet", matchData(expectedPhilippinesSecondCity,actualPhilippinesSecondCity));
				verifyLabel("Eton Centris", matchData(expectedPhilippinesThirdCity,actualPhilippinesThirdCity));
				verifyLabel("Macapagal", matchData(expectedPhilippinesFourthCity,actualPhilippinesFourthCity));
				verifyLabel("Polar Center", matchData(expectedPhilippinesFifthCity,actualPhilippinesFifthCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentySecondCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Twenty Third Country", clickElement(el.verifyTwentyThirdCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentyThirdCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualSingaporeFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualSingaporeFirstCity=getText(el.verifyFirstCity(),platform);
				verifyLabel("Capital Tower", matchData(expectedSingaporeFirstOffice,actualSingaporeFirstOffice));
				verifyLabel("Singapore", matchData(expectedSingaporeFirstCity,actualSingaporeFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentyThirdCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Twenty Fourth", clickElement(el.verifyTwentyFourthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentyFourthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualSpainFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualSpainSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualSpainFirstCity=getText(el.verifyFirstCity(),platform);
				String actualSpainSecondCity=getText(el.verifySecondCity(),platform);
				verifyLabel("Edificio MERRIMACK-II", matchData(expectedSpainFirstOffice,actualSpainFirstOffice));
				verifyLabel("Cidade da Cultura de Galicia", matchData(expectedSpainSecondOffice,actualSpainSecondOffice));
				verifyLabel("Madrid", matchData(expectedSpainFirstCity,actualSpainFirstCity));
				verifyLabel("Santiago", matchData(expectedSpainSecondCity,actualSpainSecondCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentyFourthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Twenty Fifth Country", clickElement(el.verifyTwentyFifthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentyFifthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualSwitzerlandFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualSwitzerlandSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualSwitzerlandThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualSwitzerlandFirstCity=getText(el.verifyFirstCity(),platform);
				String actualSwitzerlandSecondCity=getText(el.verifySecondCity(),platform);
				String actualSwitzerlandThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("St. Jakobs-Strasse", matchData(expectedSwitzerlandFirstOffice,actualSwitzerlandFirstOffice));
				verifyLabel("Chutzenstrasse", matchData(expectedSwitzerlandSecondOffice,actualSwitzerlandSecondOffice));
				verifyLabel("Zuercherstrasse", matchData(expectedSwitzerlandThirdOffice,actualSwitzerlandThirdOffice));
				verifyLabel("Basel", matchData(expectedSwitzerlandFirstCity,actualSwitzerlandFirstCity));
				verifyLabel("Bern", matchData(expectedSwitzerlandSecondCity,actualSwitzerlandSecondCity));
				verifyLabel("Thalwil", matchData(expectedSwitzerlandThirdCity,actualSwitzerlandThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentyFifthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Twenty Sixth Country", clickElement(el.verifyTwentySixthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentySixthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualTaiwanFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualTaiwanSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualTaiwanThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualTaiwanFirstCity=getText(el.verifyFirstCity(),platform);
				String actualTaiwanSecondCity=getText(el.verifySecondCity(),platform);
				String actualTaiwanThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("13/F No. 21 Yixin 2nd Road", matchData(expectedTaiwanFirstOffice,actualTaiwanFirstOffice));
				verifyLabel("3/F Gongyi Road", matchData(expectedTaiwanSecondOffice,actualTaiwanSecondOffice));
				verifyLabel("DunHua North Rd", matchData(expectedTaiwanThirdOffice,actualTaiwanThirdOffice));
				verifyLabel("Kaohsiung", matchData(expectedTaiwanFirstCity,actualTaiwanFirstCity));
				verifyLabel("Taichung", matchData(expectedTaiwanSecondCity,actualTaiwanSecondCity));
				verifyLabel("Taipei", matchData(expectedTaiwanThirdCity,actualTaiwanThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentySixthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Twenty Seventh", clickElement(el.verifyTwentySeventhCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentySeventhCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualUnitedKingdomFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualUnitedKingdomSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualUnitedKingdomFirstCity=getText(el.verifyFirstCity(),platform);
				String actualUnitedKingdomSecondCity=getText(el.verifySecondCity(),platform);
				verifyLabel("Wavendon Business Park", matchData(expectedUnitedKingdomFirstOffice,actualUnitedKingdomFirstOffice));
				verifyLabel("Central Park", matchData(expectedUnitedKingdomSecondOffice,actualUnitedKingdomSecondOffice));
				verifyLabel("Enigma", matchData(expectedUnitedKingdomFirstCity,actualUnitedKingdomFirstCity));
				verifyLabel("Leeds", matchData(expectedUnitedKingdomSecondCity,actualUnitedKingdomSecondCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentySeventhCountry, actualSelectedCountry);
			try{
				el.verifyFirstOffice().click();
				Thread.sleep(3000);
				el.verifyEnabledDoneButton().isDisplayed();
				extentTest.log(LogStatus.PASS, "Clicked on United Kingdom First Office");
			}catch(Exception e){
				clickOnElement("United Kingdom Second Office", clickElement(el.verifySecondOffice()));
			}
			waitForVisibityOfSpecificElement("Enabled Done Button", el.verifyEnabledDoneButton());
			clickOnElement("Done Button", clickElement(el.verifyEnabledDoneButton()));
			waitForVisibityOfSpecificElement("Enabled Save Button", ep.verifyEnableSaveButton());
			clickOnElement("Save Button", clickElement(ep.verifyEnableSaveButton()));
			Thread.sleep(5000);
			clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
			clickOnElement("Location", clickElement(ep.verifyProfileLocation()));
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			scroll(platform, "United Kingdom");
			scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Twenty Eighth", clickElement(el.verifyTwentyEighthCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentyEighthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualUSAFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualUSASecondOffice=getText(el.verifySecondOffice(),platform);
				String actualUSAThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualUSAFourthOffice=getText(el.verifyFourthOffice(),platform);
				String actualUSAFifthOffice=getText(el.verifyFifthOffice(),platform);
				String actualUSASixthOffice=getText(el.verifySixthOffice(),platform);
				String actualUSASeventhOffice=getText(el.verifySeventhOffice(),platform);
				String actualUSAEighthOffice=getText(el.verifyEighthOffice(),platform);
				String actualUSAFirstCity=getText(el.verifyFirstCity(),platform);
				String actualUSASecondCity=getText(el.verifySecondCity(),platform);
				String actualUSAThirdCity=getText(el.verifyThirdCity(),platform);
				String actualUSAFourthCity=getText(el.verifyFourthCity(),platform);
				String actualUSAFifthCity=getText(el.verifyFifthCity(),platform);
				String actualUSASixthCity=getText(el.verifySixthCity(),platform);
				String actualUSASeventhCity=getText(el.verifySeventhCity(),platform);
				String actualUSAEighthCity=getText(el.verifyEighthCity(),platform);
				scroll(platform,"Wilmington");
				String actualUSANinethOffice=getText(el.verifyNinethOffice(),platform);
				String actualUSATenthOffice=getText(el.verifyTenthOffice(),platform);
				String actualUSAEleventhOffice=getText(el.verifyEleventhOffice(),platform);
				String actualUSATwelevethOffice=getText(el.verifyTwelvethOffice(),platform);
				String actualUSAThirteenthOffice=getText(el.verifyThirteenthOffice(),platform);
				String actualUSAFourteenthOffice=getText(el.verifyFourthteenthOffice(),platform);
				String actualUSANinethCity=getText(el.verifyNinethCity(),platform);
				String actualUSATenthCity=getText(el.verifyTenthCity(),platform);
				String actualUSAEleventhCity=getText(el.verifyEleventhCity(),platform);
				String actualUSATwelevethCity=getText(el.verifyTwelvethCity(),platform);
				String actualUSAThirteenthCity=getText(el.verifyThirteenthCity(),platform);
				String actualUSAFourteenthCity=getText(el.verifyFourthteenthCity(),platform);
				verifyLabel("Madison Ave Extension", matchData(expectedUSAFirstOffice,actualUSAFirstOffice));
				verifyLabel("44664 Guilford Drive", matchData(expectedUSASecondOffice,actualUSASecondOffice));
				verifyLabel("Discovery Plaza", matchData(expectedUSAThirdOffice,actualUSAThirdOffice));
				verifyLabel("Lakeview Drive", matchData(expectedUSAFourthOffice,actualUSAFourthOffice));
				verifyLabel("North Harwood St", matchData(expectedUSAFifthOffice,actualUSAFifthOffice));
				verifyLabel("Pilot Knob Rd", matchData(expectedUSASixthOffice,actualUSASixthOffice));
				verifyLabel("Lindle Road", matchData(expectedUSASeventhOffice,actualUSASeventhOffice));
				verifyLabel("Bishop Street Tower", matchData(expectedUSAEighthOffice,actualUSAEighthOffice));
				verifyLabel("Cromwell", matchData(expectedUSANinthOffice,actualUSANinethOffice));
				verifyLabel("Broad Street", matchData(expectedUSATenthOffice,actualUSATenthOffice));
				verifyLabel("Carry Street", matchData(expectedUSAEleventhOffice,actualUSAEleventhOffice));
				verifyLabel("480 North 2200 West", matchData(expectedUSATwevelthOffice,actualUSATwelevethOffice));
				verifyLabel("Great America Parkway", matchData(expectedUSAThirteenthOffice,actualUSAThirteenthOffice));
				verifyLabel("Silverside Road", matchData(expectedUSAFourteenthOffice,actualUSAFourteenthOffice));
				verifyLabel("Albany", matchData(expectedUSAFirstCity,actualUSAFirstCity));
				verifyLabel("Ashburn", matchData(expectedUSASecondCity,actualUSASecondCity));
				verifyLabel("Augutsa", matchData(expectedUSAThirdCity,actualUSAThirdCity));
				verifyLabel("Blue Bell", matchData(expectedUSAFourthCity,actualUSAFourthCity));
				verifyLabel("Dallas", matchData(expectedUSAFifthCity,actualUSAFifthCity));
				verifyLabel("Eagan", matchData(expectedUSASixthCity,actualUSASixthCity));
				verifyLabel("Harrisburg", matchData(expectedUSASeventhCity,actualUSASeventhCity));
				verifyLabel("Honolulu", matchData(expectedUSAEighthCity,actualUSAEighthCity));
				verifyLabel("Irvine", matchData(expectedUSANinthCity,actualUSANinethCity));
				verifyLabel("New York", matchData(expectedUSATenthCity,actualUSATenthCity));
				verifyLabel("Richmond", matchData(expectedUSAEleventhCity,actualUSAEleventhCity));
				verifyLabel("Salt Lake City", matchData(expectedUSATwevelthCity,actualUSATwelevethCity));
				verifyLabel("Santa Clara", matchData(expectedUSAThirteenthCity,actualUSAThirteenthCity));
				verifyLabel("Wilmington", matchData(expectedUSAFourteenthCity,actualUSAFourteenthCity));
			}else{
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentyEighthCountry, actualSelectedCountry);
			if(profilePageLocation.contains(expectedUSATenthCity)) {
				clickOnElement("USA Tenth Office", clickElement(el.verifyEleventhOffice()));
				waitForVisibityOfSpecificElement("Enabled Done Button", el.verifyEnabledDoneButton());
				clickOnElement("Done Button", clickElement(el.verifyEnabledDoneButton()));
				Thread.sleep(2000);
				String getEditProfileLocation=getText(ep.verifyProfileLocation(), platform);
				verifyData(getEditProfileLocation, "Selected City & Country at Edit Profile Page", containsData(getEditProfileLocation,expectedUSAEleventhCity));
				waitForVisibityOfSpecificElement("Enabled Save Button", ep.verifyEnableSaveButton());
				clickOnElement("Save Button", clickElement(ep.verifyEnableSaveButton()));
				Thread.sleep(5000);
				String getProfileLocation=getText(el.verifyProfilePageLocation(), platform);
				verifyData(getProfileLocation, "Selected City & Country at  Profile Page", containsData(getProfileLocation,expectedUSAEleventhCity));
				logOut(platform);
				login(platform);
				clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
				profilePageLocation=getText(el.verifyProfilePageLocation(), platform);
				String actualLocation[]=profilePageLocation.split(" ", 2);
				profilePageLocation=actualLocation[1];
				verifyData(profilePageLocation, "Profile Page Location", containsData(profilePageLocation,expectedUSAEleventhCity));
			}else {
				clickOnElement("USA Tenth Office", clickElement(el.verifyTenthOffice()));
				waitForVisibityOfSpecificElement("Enabled Done Button", el.verifyEnabledDoneButton());
				clickOnElement("Done Button", clickElement(el.verifyEnabledDoneButton()));
				Thread.sleep(2000);
				String getEditProfileLocation=getText(ep.verifyProfileLocation(), platform);
				verifyData(getEditProfileLocation, "Selected City & Country at Edit Profile Page",containsData(getEditProfileLocation,expectedUSATenthCity));
				waitForVisibityOfSpecificElement("Enabled Save Button", ep.verifyEnableSaveButton());
				clickOnElement("Save Button", clickElement(ep.verifyEnableSaveButton()));
				Thread.sleep(5000);
				String getProfileLocation=getText(el.verifyProfilePageLocation(), platform);
				verifyData(getProfileLocation, "Selected City & Country at  Profile Page", containsData(getProfileLocation,expectedUSATenthCity));
				logOut(platform);
				login(platform);
				clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
				profilePageLocation=getText(el.verifyProfilePageLocation(), platform);
				String actualLocation[]=profilePageLocation.split(" ", 2);
				profilePageLocation=actualLocation[1];
				verifyData(profilePageLocation, "Profile Page Location", containsData(profilePageLocation,expectedUSATenthCity) );
			}
			s.assertAll();
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Add/Update Profile Location Test Failed");
		}
	}	
}