package com.apd.inteliserve.regression;


import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.EditProfilePage;
import com.apd.inteliserve.pompages.KnowledgeBasePage;
import com.apd.inteliserve.pompages.LandingPage;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.MobileElement;

public class AddUpdateTitle extends BaseTest {
	@Test
	@Parameters({"platform","Title","transalationSheet"})
	public void updateProfileTitle(@Optional String platform,@Optional String Title,@Optional String transalationSheet) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2595, Add/Update Profile Title Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-2624, Add/Update Profile Title Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");

			String expectedTitleHeaderLabel =getCellValue(transalationSheet, 160, 1);
			String expectedAllTitlesLabel =getCellValue(transalationSheet, 161, 1);
			String expectedFirstTitle =getCellValue(transalationSheet, 162, 1);
			String expectedSecondTitle =getCellValue(transalationSheet, 163, 1);
			String expectedThirdTitle =getCellValue(transalationSheet, 164, 1);
			String expectedFourthTitle =getCellValue(transalationSheet, 165, 1);
			String expectedFifthTitle =getCellValue(transalationSheet, 166, 1);
			String expectedSixthTitle =getCellValue(transalationSheet, 167, 1);
			String expectedSeventhTitle =getCellValue(transalationSheet,168, 1);
			String expectedEighthTitle =getCellValue(transalationSheet, 169, 1);
			String expectedNinthTitle =getCellValue(transalationSheet, 170, 1);
			String expectedTenthTitle =getCellValue(transalationSheet, 171, 1);
			String expectedEleventhTitle =getCellValue(transalationSheet, 172, 1);
			String expectedTwelevethTitle =getCellValue(transalationSheet, 173, 1);

			LandingPage ld = new LandingPage(driver);
			EditProfilePage ep = new EditProfilePage(driver);
			KnowledgeBasePage kb = new KnowledgeBasePage(driver);

			clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
			String profileTitle=getText(ep.verifyProfilePageTitle(),platform);
			clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
			clickOnElement("Title", clickElement(ep.verifyProfileTitle()));
			List<MobileElement> allTitle=ep.getAllTitle();
			for(MobileElement element :allTitle) {
				String getTitles= getText(element, platform);
				if(getTitles.equals(expectedFirstTitle)) {
					clickOnElement("Administrative Assistant Title", clickElement(element));
				}
			}
			verifyElement("Back Icon", elementDisplayed(kb.clickBackIcon()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			verifyElement("Edit Profile Page", elementDisplayed(ep.verifyEditProfileHeader()));
			try {
				Thread.sleep(5000);
				ep.verifyEnableSaveButton().click();
				Thread.sleep(5000);
				waitForVisibityOfSpecificElement("Edit Profile Icon", ep.verifyEditProfileIcon());
				clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
			}catch(Exception e) {
				e.printStackTrace();
				String getChangedTitle=getText(ep.verifyProfileTitle(), platform);
				if(platform.equalsIgnoreCase("ios")){
					String actualTitle[]=getChangedTitle.split(" ", 2);
					getChangedTitle=actualTitle[1];
				}
				if(profileTitle.equals(getChangedTitle)) {
					extentTest.log(LogStatus.PASS, "Save Button is Disabled because Profile Title And Selected Title is Same");
				}else {
					extentTest.log(LogStatus.FAIL, "Save Button is Disabled Even though Profile Title And Selected Title is Not Same");
				}
				s.assertEquals(getChangedTitle, profileTitle);
			}
			clickOnElement("Title", clickElement(ep.verifyProfileTitle()));

			String actualTitleHeaderLabel =getText(ep.verifyTitleHeader(),platform);
			String actualAllTitlesLabel =getText(ep.verifyTitleSubHeader(),platform);
			String actualFirstTitle =getText(ep.verifyFirstTitle(),platform);
			String actualSecondTitle =getText(ep.verifySecondTitle(),platform);
			String actualThirdTitle =getText(ep.verifyThirdTitle(),platform);
			String actualFourthTitle =getText(ep.verifyFourthTitle(),platform);
			String actualFifthTitle =getText(ep.verifyFifthTitle(),platform);
			String actualSixthTitle =getText(ep.verifySixthTitle(),platform);
			String actualSeventhTitle =getText(ep.verifySeventhTitle(),platform);
			String actualEighthTitle =getText(ep.verifyEighthTitle(),platform);;
			String actualNinthTitle =getText(ep.verifyNinthTitle(),platform);
			String actualTenthTitle =getText(ep.verifyTenthTitle(),platform);
			String actualEleventhTitle =getText(ep.verifyEleventhTitle(),platform);
			String actualTwlevethTitle =getText(ep.verifyTwelvethTitle(),platform);

			verifyLabel("Title", matchData(expectedTitleHeaderLabel,actualTitleHeaderLabel));
			verifyLabel("ALL TITLES", matchData(expectedAllTitlesLabel,actualAllTitlesLabel));
			verifyLabel("Administrative Assistant", matchData(expectedFirstTitle,actualFirstTitle));
			verifyLabel("Chief Executive Officer", matchData(expectedSecondTitle,actualSecondTitle));
			verifyLabel("Chief Financial Officer", matchData(expectedThirdTitle,actualThirdTitle));
			verifyLabel("Chief Technology Officer", matchData(expectedFourthTitle,actualFourthTitle));
			verifyLabel("Director", matchData(expectedFifthTitle,actualFifthTitle));
			verifyLabel("IT Technician", matchData(expectedSixthTitle,actualSixthTitle));
			verifyLabel("Junior Developer", matchData(expectedSeventhTitle,actualSeventhTitle));
			verifyLabel("Sales Executive", matchData(expectedEighthTitle,actualEighthTitle));
			verifyLabel("Senior Developer", matchData(expectedNinthTitle,actualNinthTitle));
			verifyLabel("Solution Consultant", matchData(expectedTenthTitle,actualTenthTitle));
			verifyLabel("System Administrator", matchData(expectedEleventhTitle,actualEleventhTitle));
			verifyLabel("Vice President", matchData(expectedTwelevethTitle,actualTwlevethTitle));

			clickOnElement("First Title", clickElement(ep.verifyFirstTitle()));
			verifyElement("Tick Icon Along with Selected Title", elementDisplayed(ep.verifyFirstTitleTickItem()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			String actualTitleSelected ="";
			Thread.sleep(1000);
			actualTitleSelected =getText(ep.verifyProfileTitle(), platform);
			if(platform.equalsIgnoreCase("ios")){
				String actualTitle[]=actualTitleSelected.split(" ", 2);
				actualTitleSelected=actualTitle[1];
			}
			verifyData(actualTitleSelected, "Selected Title at Edit Profile Page", matchData(expectedFirstTitle, actualTitleSelected));
			clickOnElement("Profile Title", clickElement(ep.verifyProfileTitle()));
			clickOnElement("Second Title", clickElement(ep.verifySecondTitle()));
			if(platform.equals("android")) {
				verifyElementNotDisplayed("Tick Icon With Previous Selected Title", elementDisplayed(ep.verifyFirstTitleTickItem()));
			}
			verifyElement("Tick Icon Along with Selected Title", elementDisplayed(ep.verifySecondTitleTickItem()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(1000);
			actualTitleSelected =getText(ep.verifyProfileTitle(), platform);
			if(platform.equalsIgnoreCase("ios")){
				String actualTitle[]=actualTitleSelected.split(" ", 2);
				actualTitleSelected=actualTitle[1];
			}
			verifyData(actualTitleSelected, "Selected Title at Edit Profile Page", matchData(expectedSecondTitle, actualTitleSelected));
			clickOnElement("Profile Title", clickElement(ep.verifyProfileTitle()));
			clickOnElement("Third Title", clickElement(ep.verifyThirdTitle()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(1000);
			actualTitleSelected =getText(ep.verifyProfileTitle(), platform);
			if(platform.equalsIgnoreCase("ios")){
				String actualTitle[]=actualTitleSelected.split(" ", 2);
				actualTitleSelected=actualTitle[1];
			}
			verifyData(actualTitleSelected, "Selected Title at Edit Profile Page", matchData(expectedThirdTitle, actualTitleSelected));
			clickOnElement("Profile Title", clickElement(ep.verifyProfileTitle()));
			clickOnElement("Fourth Title", clickElement(ep.verifyFourthTitle()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(1000);
			actualTitleSelected =getText(ep.verifyProfileTitle(), platform);
			if(platform.equalsIgnoreCase("ios")){
				String actualTitle[]=actualTitleSelected.split(" ", 2);
				actualTitleSelected=actualTitle[1];
			}
			verifyData(actualTitleSelected, "Selected Title at Edit Profile Page", matchData(expectedFourthTitle, actualTitleSelected));
			clickOnElement("Profile Title", clickElement(ep.verifyProfileTitle()));
			clickOnElement("Fifth Title", clickElement(ep.verifyFifthTitle()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(1000);
			actualTitleSelected =getText(ep.verifyProfileTitle(), platform);
			if(platform.equalsIgnoreCase("ios")){
				String actualTitle[]=actualTitleSelected.split(" ", 2);
				actualTitleSelected=actualTitle[1];
			}
			verifyData(actualTitleSelected, "Selected Title at Edit Profile Page", matchData(expectedFifthTitle, actualTitleSelected));
			clickOnElement("Profile Title", clickElement(ep.verifyProfileTitle()));
			clickOnElement("Sixth Title", clickElement(ep.verifySixthTitle()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(1000);
			actualTitleSelected =getText(ep.verifyProfileTitle(), platform);
			if(platform.equalsIgnoreCase("ios")){
				String actualTitle[]=actualTitleSelected.split(" ", 2);
				actualTitleSelected=actualTitle[1];
			}
			verifyData(actualTitleSelected, "Selected Title at Edit Profile Page", matchData(expectedSixthTitle, actualTitleSelected));
			clickOnElement("Profile Title", clickElement(ep.verifyProfileTitle()));
			clickOnElement("Seventh Title", clickElement(ep.verifySeventhTitle()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(1000);
			actualTitleSelected =getText(ep.verifyProfileTitle(), platform);
			if(platform.equalsIgnoreCase("ios")){
				String actualTitle[]=actualTitleSelected.split(" ", 2);
				actualTitleSelected=actualTitle[1];
			}
			verifyData(actualTitleSelected, "Selected Title at Edit Profile Page", matchData(expectedSeventhTitle, actualTitleSelected));
			clickOnElement("Profile Title", clickElement(ep.verifyProfileTitle()));
			clickOnElement("Eighth Title", clickElement(ep.verifyEighthTitle()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(1000);
			actualTitleSelected =getText(ep.verifyProfileTitle(), platform);
			if(platform.equalsIgnoreCase("ios")){
				String actualTitle[]=actualTitleSelected.split(" ", 2);
				actualTitleSelected=actualTitle[1];
			}
			verifyData(actualTitleSelected, "Selected Title at Edit Profile Page", matchData(expectedEighthTitle, actualTitleSelected));
			clickOnElement("Profile Title", clickElement(ep.verifyProfileTitle()));
			clickOnElement("Nineth Title", clickElement(ep.verifyNinthTitle()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(1000);
			actualTitleSelected =getText(ep.verifyProfileTitle(), platform);
			if(platform.equalsIgnoreCase("ios")){
				String actualTitle[]=actualTitleSelected.split(" ", 2);
				actualTitleSelected=actualTitle[1];
			}
			verifyData(actualTitleSelected, "Selected Title at Edit Profile Page", matchData(expectedNinthTitle, actualTitleSelected));
			clickOnElement("Profile Title", clickElement(ep.verifyProfileTitle()));
			clickOnElement("Tenth Title", clickElement(ep.verifyTenthTitle()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(1000);
			actualTitleSelected =getText(ep.verifyProfileTitle(), platform);
			if(platform.equalsIgnoreCase("ios")){
				String actualTitle[]=actualTitleSelected.split(" ", 2);
				actualTitleSelected=actualTitle[1];
			}
			verifyData(actualTitleSelected, "Selected Title at Edit Profile Page", matchData(expectedTenthTitle, actualTitleSelected));
			clickOnElement("Profile Title", clickElement(ep.verifyProfileTitle()));
			scroll(platform,"Española (México)");
			clickOnElement("Eleventh Title", clickElement(ep.verifyEleventhTitle()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(1000);
			actualTitleSelected =getText(ep.verifyProfileTitle(), platform);
			if(platform.equalsIgnoreCase("ios")){
				String actualTitle[]=actualTitleSelected.split(" ", 2);
				actualTitleSelected=actualTitle[1];
			}
			verifyData(actualTitleSelected, "Selected Title at Edit Profile Page", matchData(expectedEleventhTitle, actualTitleSelected));
			clickOnElement("Profile Title", clickElement(ep.verifyProfileTitle()));
			scroll(platform,"Española (México)");
			clickOnElement("Twelveth Title", clickElement(ep.verifyTwelvethTitle()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(1000);
			actualTitleSelected =getText(ep.verifyProfileTitle(), platform);
			if(platform.equalsIgnoreCase("ios")){
				String actualTitle[]=actualTitleSelected.split(" ", 2);
				actualTitleSelected=actualTitle[1];
			}
			verifyData(actualTitleSelected, "Selected Title at Edit Profile Page", matchData(expectedTwelevethTitle, actualTitleSelected));
			clickOnElement("Save Button",clickElement(ep.verifyEnableSaveButton()));
			logOut(platform);
			login(platform);
			clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
			profileTitle=getText(ep.verifyProfilePageTitle(), platform);
			verifyData(profileTitle, "Profile Page Title", matchData(expectedTwelevethTitle, profileTitle));
			s.assertAll();
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Add/Update Profile Title Test Failed");
		}
	}	
}
