package com.apd.inteliserve.regression;


import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.apd.inteliserve.pompages.PhonePage;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.MobileElement;

/**
 * @author  VishalMadhur
 *
 */



public class IA_1681_ServiceDeskNumberAndroid extends BaseTest{
	@Test
	@Parameters({"platform","language","transalationSheet"})
	public void supportCornerPhoneTest(@Optional String platform,@Optional String language,@Optional String transalationSheet) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-1681, Support Corner Service Desk Numbers Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-1681, Support Corner Service Desk Numbers Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			click(fm.clickCoreSupportButton());
			extentTest.log(LogStatus.PASS, "Clicked  on Core Support Buton");
			click(fm.callButton());
			extentTest.log(LogStatus.PASS, "Clicked  on Call Buton");
			String expectedPhoneHeaderLabel =getCellValue(transalationSheet, 2, 1);
			String expectedLocationLabel =getCellValue(transalationSheet, 4, 1);
			String expectedSelectCountriesLabel =getCellValue(transalationSheet, 3, 1);
			String expectedSelectLocationHelpText =getCellValue(transalationSheet, 6, 1);
			String expectedCallAnAgentLabel =getCellValue(transalationSheet, 5, 1);
			String expectedDefaultFirstPhoneLangugae =getCellValue(transalationSheet, 10, 1);
			String expectedDefaultSecondPhoneLangugae=getCellValue(transalationSheet, 11, 1);
			String expectedDefaultFirstPhoneCoverage =getCellValue(transalationSheet, 7, 1);
			String expectedDefaultSecondPhoneCoverage=getCellValue(transalationSheet, 8, 1);
			String expectedLocationHeaderLabel=getCellValue(transalationSheet, 13, 1);
			String expectedCountriesHeaderLabel=getCellValue(transalationSheet, 21, 1);
			String expectedfirstCountry=getCellValue(transalationSheet, 14, 1);
			String expectedSecondCountry =getCellValue(transalationSheet, 15, 1);
			String expectedThirdCountry =getCellValue(transalationSheet, 16, 1);
			String expectedFourthCountry =getCellValue(transalationSheet, 17, 1);
			String expectedFifthCountry =getCellValue(transalationSheet, 18, 1);
			String expectedSixthCountry =getCellValue(transalationSheet, 19, 1);
			String expectedSeventhCountry =getCellValue(transalationSheet, 20, 1);
			String expectedDefaultFirstPhoneNumber =getPropertyValue("DefaultFirstPhoneNumber");
			String expectedDefaultSecondPhoneNumber=getPropertyValue("DefaultSecondPhoneNumber");
			String expectedUSFirstPhoneNumber=getPropertyValue("USFirstPhoneNumber");
			String expectedUSSecondPhoneNumber=getPropertyValue("USSecondPhoneNumber");
			String expectedUSThirdPhoneNumber=getPropertyValue("USThirdPhoneNumber");
			String expectedAustraliaFirstPhoneNumber=getPropertyValue("AustraliaFirstPhoneNumber");
			String expectedColombiaFirstPhoneNumber=getPropertyValue("ClombiaFirstPhoneNumber");
			String expectedMexicoFirstPhoneNumber=getPropertyValue("MexicoFirstPhoneNumber"); 
			String expectedPanamaFirstPhoneNumber=getPropertyValue("PanamaFirstPhoneNumber");
			String expectedGuatemalaFirstPhoneNumber=getPropertyValue("GuatemalaFirstPhoneNumber");
			String expectedAOCFirstPhoneNumber=getPropertyValue("AOCFirstPhoneNumber");
			String expectedAOCSecondPhoneNumber=getPropertyValue("AOCSecondPhoneNumber");

			PhonePage pp = new PhonePage(driver);
			String actualPhoneHeaderLabel=getText(pp.verifyPhoneHeaderText(),platform);
			String actualLocationLabel=getText(pp.verifylocationText(),platform);
			String actualSelectCountriesLabel=getText(pp.verifySelectCountryText(),platform);
			String actualSelectLocationHelpText=getText(pp.verifyLocationHelpText(),platform);
			String actualCallAnAgentLabel =getText(pp.verifyCallAnAgentText(),platform);
			verifyLabel("Phone Header", matchData(expectedPhoneHeaderLabel,actualPhoneHeaderLabel));
			verifyLabel("Location", matchData(expectedLocationLabel,actualLocationLabel));
			verifyLabel("Location Help Text", matchData(expectedSelectLocationHelpText,actualSelectLocationHelpText));
			verifyLabel("CALL AN AGENT", matchData(expectedCallAnAgentLabel,actualCallAnAgentLabel));
			verifyElement("Select Countries Flag", elementIsDisplayed(pp.verifyCountryIcon()));
			verifyElement("Phone Icon with number", elementIsDisplayed(pp.verifyPhoneCallIcon()));
			try {
				if(expectedSelectCountriesLabel.equals(actualSelectCountriesLabel)) {
					String actualFirstPhoneNumber=getText(pp.verifyPhoneNumber(),platform);
					String actualFirstPhoneLangugae=getText(pp.verifyPhoneNumberLanguage(),platform);
					String actualFirstPhoneCoverage=getText(pp.verifyPhoneNumberCoverage(),platform);
					String actualSecondPhoneNumber=getText(pp.verifySecondPhoneNumber(),platform);
					String actualSecondPhoneLangugae=getText(pp.verifySecondPhoneNumberLanguage(),platform);
					String actualSecondPhoneCoverage=getText(pp.verifySecondPhoneNumberCoverage(),platform);
					verifyData(actualFirstPhoneNumber, "Default First Phone Number", matchData(actualFirstPhoneNumber, expectedDefaultFirstPhoneNumber));
					verifyData( actualFirstPhoneLangugae, "Default First Phone Langauge", matchData(actualFirstPhoneLangugae, expectedDefaultFirstPhoneLangugae));
					verifyData(actualFirstPhoneCoverage, "Default First Phone Coverage ", matchData(actualFirstPhoneCoverage, expectedDefaultFirstPhoneCoverage));
					verifyData(actualSecondPhoneNumber, "Default Second Phone Number ", matchData(actualSecondPhoneNumber, expectedDefaultSecondPhoneNumber));
					verifyData(actualSecondPhoneLangugae, "Default Second Phone Langauge ", matchData(actualSecondPhoneLangugae, expectedDefaultFirstPhoneLangugae));
					verifyData(actualSecondPhoneCoverage, "Default Second Phone Coverage ", matchData(actualSecondPhoneCoverage, expectedDefaultFirstPhoneCoverage));

				}
			}catch(Exception e) {
				e.printStackTrace();
			}

			clickElement(pp.verifySelectCountryText());
			String actualLocationHeaderLabel=getText(pp.verifylocationHeader(), platform);
			String actualCountriesHeaderLabel=getText(pp.verifycountriesHeader(),platform);
			verifyLabel("Location", matchData(expectedLocationHeaderLabel,actualLocationHeaderLabel));
			verifyLabel("COUNTRIES", matchData(expectedCountriesHeaderLabel,actualCountriesHeaderLabel));
			try {
				List<MobileElement> countries= pp.getcountryNamesCount();
				int expectedCountriesCount=8;
				int actualCountriesCount=countries.size();
				if(expectedCountriesCount==actualCountriesCount) {
					extentTest.log(LogStatus.PASS, "ALL Seven Countries  Are Displayed");
					extentTest.log(LogStatus.INFO, "List of Countries Are ---- ");
					for(MobileElement element : countries) {
						String countriesName = getText(element, platform);
						extentTest.log(LogStatus.INFO, countriesName);
					}
				}
				else {
					extentTest.log(LogStatus.FAIL, "ALL Seven Countries  Are Not Dispalyed");
				}
				s.assertEquals(actualCountriesCount, expectedCountriesCount);
				List<MobileElement> countryiesIcons= pp.getcountryIconsCount();
				int expectedIconsCount=8;
				int actualIconsCount=countryiesIcons.size();
				if(expectedIconsCount==actualIconsCount) {
					extentTest.log(LogStatus.PASS, "ALL Seven Countries Flag Icons Are Displayed");
				}
				else {
					extentTest.log(LogStatus.FAIL, "ALL Seven Countries Flag Icons Are Displayed Not Dispalyed");
				}
				s.assertEquals(actualIconsCount, expectedIconsCount);
			}catch(Exception e) {
				e.printStackTrace();
			}
			String actualFirstCountryText=getText(pp.verifyFirstCountry(),platform);
			String actualSecondCountryText=getText(pp.verifySecondCountry(),platform);
			String actualThirdCountryText=getText(pp.verifyThirdCountry(),platform);
			String actualFourthCountryText=getText(pp.verifyFourthCountry(),platform);
			String actualFifthCountryText=getText(pp.verifyFifthCountry(),platform);
			String actualSixthCountryText=getText(pp.verifySixthCountry(),platform);
			String actualSeventhCountryText=getText(pp.verifySeventhCountry(),platform);
			verifyLabel("United States", matchData(expectedfirstCountry,actualFirstCountryText));
			verifyLabel("Australia", matchData(expectedSecondCountry,actualSecondCountryText));
			verifyLabel("Colombia", matchData(expectedThirdCountry,actualThirdCountryText));
			verifyLabel("Mexico", matchData(expectedFourthCountry,actualFourthCountryText));
			verifyLabel("Panama", matchData(expectedFifthCountry,actualFifthCountryText));
			verifyLabel("Guatemala", matchData(expectedSixthCountry,actualSixthCountryText));
			verifyLabel("All Other Countries", matchData(expectedSeventhCountry,actualSeventhCountryText));
			clickElement(pp.verifyFirstCountry());
			verifyElement("Tick Icon is Displayed For the Selected Country", elementIsDisplayed(pp.verifyTickIcon()));
			clickElement(pp.clickBackIcon());
			verifyElement("United States Country Flag Icon", elementIsDisplayed(pp.verifyCountryIcon()));
			verifyElement("Phone Icon With Phone Number is Displayed", elementIsDisplayed(pp.verifyPhoneCallIcon()));
			String actualFirstCountyDisplayed=getText(pp.verifySelectCountryText(),platform);
			try {
				if(expectedfirstCountry.equals(actualFirstCountyDisplayed)) {
					String actualFirstPhoneNumber=getText(pp.verifyPhoneNumber(),platform);
					String actualFirstPhoneLangugae=getText(pp.verifyPhoneNumberLanguage(),platform);
					String actualFirstPhoneCoverage=getText(pp.verifyPhoneNumberCoverage(),platform);
					String actualSecondPhoneNumber=getText(pp.verifySecondPhoneNumber(),platform);
					String actualSecondPhoneLangugae=getText(pp.verifySecondPhoneNumberLanguage(),platform);
					String actualSecondPhoneCoverage=getText(pp.verifySecondPhoneNumberCoverage(),platform);
					String actualThirdPhoneNumber=getText(pp.verifyThirdPhoneNumber(),platform);
					String actualThirdPhoneLangugae=getText(pp.verifyThirdPhoneNumberLanguage(),platform);
					String actualThirdPhoneCoverage=getText(pp.verifyThirdPhoneNumberCoverage(),platform);
					verifyData(actualFirstPhoneNumber, "United States First Phone Number", matchData(actualFirstPhoneNumber, expectedUSFirstPhoneNumber));
					verifyData( actualFirstPhoneLangugae, "United States First Phone Langauge", matchData(actualFirstPhoneLangugae, expectedDefaultFirstPhoneLangugae));
					verifyData(actualFirstPhoneCoverage, "United States First Phone Coverage ", matchData(actualFirstPhoneCoverage, expectedDefaultFirstPhoneCoverage));
					verifyData(actualSecondPhoneNumber, "United States Second Phone Number ", matchData(actualSecondPhoneNumber, expectedUSSecondPhoneNumber));
					verifyData(actualSecondPhoneLangugae, "United States Second Phone Langauge ", matchData(actualSecondPhoneLangugae, expectedDefaultFirstPhoneLangugae));
					verifyData(actualSecondPhoneCoverage, "United States Second Phone Coverage ", matchData(actualSecondPhoneCoverage, expectedDefaultFirstPhoneCoverage));
					verifyData(actualThirdPhoneNumber, "United States Third Phone Number ", matchData(actualThirdPhoneNumber, expectedUSThirdPhoneNumber));
					verifyData(actualThirdPhoneLangugae, "United States Third Phone Language ", matchData(actualThirdPhoneLangugae, expectedDefaultSecondPhoneLangugae));
					verifyData(actualThirdPhoneCoverage, "United States Third Phone Coverage", matchData(actualThirdPhoneCoverage, expectedDefaultSecondPhoneCoverage));
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			clickElement(pp.clickChangeCountryButton());
			clickElement(pp.verifySecondCountry());
			verifyElement("Tick Icon is Displayed For the Selected Country", elementIsDisplayed(pp.verifyTickIcon()));
			clickElement(pp.clickBackIcon());
			verifyElement("Australia Country Flag Icon", elementIsDisplayed(pp.verifyCountryIcon()));
			verifyElement("Phone Icon With Phone Number is Displayed", elementIsDisplayed(pp.verifyPhoneCallIcon()));
			String actualSecondCountyDisplayed=getText(pp.verifySelectCountryText(),platform);
			try {
				if(expectedSecondCountry.equals(actualSecondCountyDisplayed)) {
					String actualFirstPhoneNumber=getText(pp.verifyPhoneNumber(),platform);
					String actualFirstPhoneLangugae=getText(pp.verifyPhoneNumberLanguage(),platform);
					String actualFirstPhoneCoverage=getText(pp.verifyPhoneNumberCoverage(),platform);
					verifyData(actualFirstPhoneNumber, "Australia First Phone Number", matchData(actualFirstPhoneNumber, expectedAustraliaFirstPhoneNumber));
					verifyData( actualFirstPhoneLangugae, "Australia  First Phone Langauge", matchData(actualFirstPhoneLangugae, expectedDefaultFirstPhoneLangugae));
					verifyData(actualFirstPhoneCoverage, "Australia First Phone Coverage ", matchData(actualFirstPhoneCoverage, expectedDefaultFirstPhoneCoverage));
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			clickElement(pp.clickChangeCountryButton());
			clickElement(pp.verifyThirdCountry());
			verifyElement("Tick Icon is Displayed For the Selected Country", elementIsDisplayed(pp.verifyTickIcon()));
			clickElement(pp.clickBackIcon());
			verifyElement("Colombia Country Flag Icon", elementIsDisplayed(pp.verifyCountryIcon()));
			verifyElement("Phone Icon With Phone Number is Displayed", elementIsDisplayed(pp.verifyPhoneCallIcon()));
			String actualThirdCountyDisplayed=getText(pp.verifySelectCountryText(),platform);
			try {
				if(expectedThirdCountry.equals(actualThirdCountyDisplayed)) {
					String actualFirstPhoneNumber=getText(pp.verifyPhoneNumber(),platform);
					String actualFirstPhoneLangugae=getText(pp.verifyPhoneNumberLanguage(),platform);
					String actualFirstPhoneCoverage=getText(pp.verifyPhoneNumberCoverage(),platform);
					verifyData(actualFirstPhoneNumber, "Colombia First Phone Number", matchData(actualFirstPhoneNumber, expectedColombiaFirstPhoneNumber));
					verifyData( actualFirstPhoneLangugae, "Colombia First Phone Langauge", matchData(actualFirstPhoneLangugae, expectedDefaultSecondPhoneLangugae));
					verifyData(actualFirstPhoneCoverage, "Colombia  First Phone Coverage ", matchData(actualFirstPhoneCoverage, expectedDefaultSecondPhoneCoverage));
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			clickElement(pp.clickChangeCountryButton());
			clickElement(pp.verifyFourthCountry());
			verifyElement("Tick Icon is Displayed For the Selected Country", elementIsDisplayed(pp.verifyTickIcon()));
			clickElement(pp.clickBackIcon());
			verifyElement("Mexico Country Flag Icon", elementIsDisplayed(pp.verifyCountryIcon()));
			verifyElement("Phone Icon With Phone Number is Displayed", elementIsDisplayed(pp.verifyPhoneCallIcon()));
			String actualFourthCountyDisplayed=getText(pp.verifySelectCountryText(),platform);
			try {
				if(expectedFourthCountry.equals(actualFourthCountyDisplayed)) {
					String actualFirstPhoneNumber=getText(pp.verifyPhoneNumber(),platform);
					String actualFirstPhoneLangugae=getText(pp.verifyPhoneNumberLanguage(),platform);
					String actualFirstPhoneCoverage=getText(pp.verifyPhoneNumberCoverage(),platform);
					verifyData(actualFirstPhoneNumber, "Mexico First Phone Number", matchData(actualFirstPhoneNumber, expectedMexicoFirstPhoneNumber));
					verifyData( actualFirstPhoneLangugae, "Mexico  First Phone Langauge", matchData(actualFirstPhoneLangugae, expectedDefaultSecondPhoneLangugae));
					verifyData(actualFirstPhoneCoverage, "Mexico  First Phone Coverage ", matchData(actualFirstPhoneCoverage, expectedDefaultSecondPhoneCoverage));
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			clickElement(pp.clickChangeCountryButton());
			clickElement(pp.verifyFifthCountry());
			verifyElement("Tick Icon is Displayed For the Selected Country", elementIsDisplayed(pp.verifyTickIcon()));
			clickElement(pp.clickBackIcon());
			verifyElement("Panama Country Flag Icon", elementIsDisplayed(pp.verifyCountryIcon()));
			verifyElement("Phone Icon", elementIsDisplayed(pp.verifyPhoneCallIcon()));
			String actualFifthCountyDisplayed=getText(pp.verifySelectCountryText(),platform);
			try {
				if(expectedFifthCountry.equals(actualFifthCountyDisplayed)) {
					String actualFirstPhoneNumber=getText(pp.verifyPhoneNumber(),platform);
					String actualFirstPhoneLangugae=getText(pp.verifyPhoneNumberLanguage(),platform);
					String actualFirstPhoneCoverage=getText(pp.verifyPhoneNumberCoverage(),platform);
					verifyData(actualFirstPhoneNumber, "Panama First Phone Number", matchData(actualFirstPhoneNumber, expectedPanamaFirstPhoneNumber));
					verifyData( actualFirstPhoneLangugae, "Panama First Phone Langauge", matchData(actualFirstPhoneLangugae, expectedDefaultSecondPhoneLangugae));
					verifyData(actualFirstPhoneCoverage, "Panama First Phone Coverage ", matchData(actualFirstPhoneCoverage, expectedDefaultSecondPhoneCoverage));
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			clickElement(pp.clickChangeCountryButton());
			clickElement(pp.verifySixthCountry());
			verifyElement("Tick Icon is Displayed For the Selected Country", elementIsDisplayed(pp.verifyTickIcon()));
			clickElement(pp.clickBackIcon());
			verifyElement("Guatemala Country Flag Icon", elementIsDisplayed(pp.verifyCountryIcon()));
			verifyElement("Phone Icon With Phone Number is Displayed", elementIsDisplayed(pp.verifyPhoneCallIcon()));
			String actualSixthCountyDisplayed=getText(pp.verifySelectCountryText(),platform);
			try {
				if(expectedSixthCountry.equals(actualSixthCountyDisplayed)) {
					String actualFirstPhoneNumber=getText(pp.verifyPhoneNumber(),platform);
					String actualFirstPhoneLangugae=getText(pp.verifyPhoneNumberLanguage(),platform);
					String actualFirstPhoneCoverage=getText(pp.verifyPhoneNumberCoverage(),platform);
					verifyData(actualFirstPhoneNumber, "Guatemala First Phone Number", matchData(actualFirstPhoneNumber, expectedGuatemalaFirstPhoneNumber));
					verifyData( actualFirstPhoneLangugae, "Guatemala First Phone Langauge", matchData(actualFirstPhoneLangugae, expectedDefaultSecondPhoneLangugae));
					verifyData(actualFirstPhoneCoverage, "Guatemala First Phone Coverage ", matchData(actualFirstPhoneCoverage, expectedDefaultSecondPhoneCoverage));
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			clickElement(pp.clickChangeCountryButton());
			clickElement(pp.verifySeventhCountry());
			verifyElement("Tick Icon is Displayed For the Selected Country", elementIsDisplayed(pp.verifyTickIcon()));
			clickElement(pp.clickBackIcon());
			verifyElement("All other Countries Country Flag Icon", elementIsDisplayed(pp.verifyCountryIcon()));
			verifyElement("Phone Icon With Phone Number is Displayed", elementIsDisplayed(pp.verifyPhoneCallIcon()));
			String actualSeventhCountyDisplayed=getText(pp.verifySelectCountryText(),platform);
			try {
				if(expectedSeventhCountry.equals(actualSeventhCountyDisplayed)) {
					String actualFirstPhoneNumber=getText(pp.verifyPhoneNumber(),platform);
					String actualFirstPhoneLangugae=getText(pp.verifyPhoneNumberLanguage(),platform);
					String actualFirstPhoneCoverage=getText(pp.verifyPhoneNumberCoverage(),platform);
					String actualSecondPhoneNumber=getText(pp.verifySecondPhoneNumber(),platform);
					String actualSecondPhoneLangugae=getText(pp.verifySecondPhoneNumberLanguage(),platform);
					String actualSecondPhoneCoverage=getText(pp.verifySecondPhoneNumberCoverage(),platform);
					verifyData(actualFirstPhoneNumber, "All Other Countries First Phone Number", matchData(actualFirstPhoneNumber, expectedAOCFirstPhoneNumber));
					verifyData( actualFirstPhoneLangugae, "All Other Countries First Phone Langauge", matchData(actualFirstPhoneLangugae, expectedDefaultFirstPhoneLangugae));
					verifyData(actualFirstPhoneCoverage, "All Other Countries First Phone Coverage ", matchData(actualFirstPhoneCoverage, expectedDefaultFirstPhoneCoverage));
					verifyData(actualSecondPhoneNumber, "All Other Countries Second Phone Number ", matchData(actualSecondPhoneNumber, expectedAOCSecondPhoneNumber));
					verifyData(actualSecondPhoneLangugae, "All Other Countries Second Phone Langauge ", matchData(actualSecondPhoneLangugae, expectedDefaultFirstPhoneLangugae));
					verifyData(actualSecondPhoneCoverage, "All Other Countries Second Phone Coverage ", matchData(actualSecondPhoneCoverage, expectedDefaultFirstPhoneCoverage));
					clickElement(pp.verifyPhoneCallIcon());
					extentTest.log(LogStatus.PASS, "Clicked on Phone Icon");
					String expectedSupportNumberAndroid =getPropertyValue("SupportNumber1");
					String actualSupportNumber = getText(fm.getSupportNumber(), platform);
					Reporter.log(actualSupportNumber,true);
					if(actualSupportNumber.equals(expectedSupportNumberAndroid)) {
						extentTest.log(LogStatus.PASS, "Support Number is Verified in Phone Dailer -- " +actualSupportNumber);
					}
					else {
						extentTest.log(LogStatus.FAIL, "Support Number is Not Verified in Phone Dailer -- " +actualSupportNumber);
					}
					s.assertEquals(actualSupportNumber, expectedSupportNumberAndroid);
				}
				
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			s.assertAll();
			//



			//			if(platform.equalsIgnoreCase("android")){
			//				String expectedSupportNumberAndroid =getPropertyValue("SupportNumber");
			//				String actualSupportNumber = fm.getSupportNumber().getText();
			//				Reporter.log(actualSupportNumber,true);
			//				if(actualSupportNumber.equals(expectedSupportNumberAndroid)) {
			//					extentTest.log(LogStatus.PASS, "Support Number is Verified -- " +actualSupportNumber);
			//				}
			//				else {
			//					extentTest.log(LogStatus.FAIL, "Support Number is Not Verified -- " +actualSupportNumber);
			//				}
			//				s.assertEquals(actualSupportNumber, expectedSupportNumberAndroid);
			//
			//			}
			//			else {
			//String expectedSupportNumberIos =getPropertyValue("SupportNumberIos");
			//			String actualSupportNumber = fm.getSupportNumber().getText();
			//			Reporter.log(actualSupportNumber,true);
			//				if(actualSupportNumber.contains(expectedSupportNumberIos)) {
			//					extentTest.log(LogStatus.PASS, "Support Number is Verified");
			//				}
			//				else {
			//					extentTest.log(LogStatus.FAIL, "Support Number is Not Verified");
			//				}
			//			}

		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("IA-1681, Support Corner Service Desk Numbers Test Failed");
		}
	}
}
