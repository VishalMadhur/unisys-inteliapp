package com.apd.inteliserve.regression;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.apd.inteliserve.pompages.KnowledgeBasePage;
import com.apd.inteliserve.pompages.LandingPage;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class KnowledgeBaseArticleDetails extends BaseTest {
	@SuppressWarnings("rawtypes")
	@Test
	@Parameters({"platform","language","transalationSheet","device"})
	public void KnowledgeBaseArticleDeatilsTest(@Optional String platform,@Optional String language,@Optional String transalationSheet,@Optional String device) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2254, Knowledge Base Articles Deatils Page Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-2194, Knowledge Base Articles Deatils Page Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			
			String expectedKnowledgeBaseHeaderLabel =getCellValue(transalationSheet, 23, 1);
			String expectedArticleTitle=getCellValue("ExpectedText", 8, 1);
			String expectedArticlDate=getCellValue("ExpectedText", 9, 1);
			String expectedCanNotFindAswerText =getCellValue(transalationSheet, 28, 1);
			String expectedChatWithAmeliaLabel =getCellValue(transalationSheet, 30, 1);
			String expectedWriteToUsLabel=getCellValue(transalationSheet, 29, 1);
			String expectedCancelLabel=getCellValue(transalationSheet, 31, 1);
			String expectedAttachmentsTitle=getCellValue(transalationSheet, 36, 1);
			
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			KnowledgeBasePage kb = new KnowledgeBasePage(driver);
			clickOnElement("Knowledge Base Button", clickElement(fm.clickKnowledgeBaseButton()));
			String actualKnowledgeBaseHeaderLabel=getText(kb.verifyKnowledgeHeaderText(),platform);
			verifyLabel("Knowledge Base Header", matchData(expectedKnowledgeBaseHeaderLabel,actualKnowledgeBaseHeaderLabel));
			clickOnElement("Search Text Box", clickElement(kb.verifySearchTextBox()));
			kb.verifySearchTextBox().sendKeys("attachments\n");
			if(platform.equalsIgnoreCase("android")){
				//kb.verifyArticlePageSearchTextBox().sendKeys("attachments");
				((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
			}
			clickOnElement("First Seatch Result", clickElement(kb.clickFirstSearchItem()));
			String actualArticleTitle=getText(kb.verifyArticleTitle(), platform);
			String actualArticleDate=getText(kb.verifyArticleDate(), platform);
			verifyLabel("Knowledge Base Header", matchData(expectedKnowledgeBaseHeaderLabel,actualKnowledgeBaseHeaderLabel));
			verifyLabel("Article Title", matchData(expectedArticleTitle,actualArticleTitle));
			verifyLabel("Article Date", matchData(expectedArticlDate,actualArticleDate));
			verifyElement("Contextual Menu", elementDisplayed(kb.verifyContextualMenu()));
			verifyElement("Back Icon", elementDisplayed(kb.clickBackIcon()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			verifyElement("Searh Item List", elementDisplayed(kb.clickFirstSearchItem()));
			clickOnElement("First Seatch Result", clickElement(kb.clickFirstSearchItem()));
			String actualAttachmentsTitle=getText(kb.verifyAttachmentsTitle(), platform);
			verifyLabel("Attachments Title", matchData(expectedAttachmentsTitle,actualAttachmentsTitle));
			verifyElement("Attachments", elementDisplayed(kb.verifyAttachments()));
			clickOnElement("First Attachment", clickElement(kb.verifyAttachments()));
			//			System.out.println(actualArticleDate);
			//			System.out.println(actualArticleTitle);
			//			System.out.println(actualAttachmentsTitle);
			if(elementDisplayed(kb.verifyFileNotSupportedText())) {
				extentTest.log(LogStatus.PASS, "Failed to Open Excel Attachment because File is Not Supported by Device");
				clickOnElement("Dismiss Button", clickElement(kb.clickDismissButton()));
			}else if (elementDisplayed(kb.verifyAttachmentsErrorMessage())){
				try {
					wait.until(ExpectedConditions.visibilityOf(kb.verifyAttachmentsErrorMessage()));
				}catch(Exception e) {
					e.printStackTrace();
				}
				extentTest.log(LogStatus.FAIL, "Failed to Open Excel Attachment");
				clickOnElement("Dismiss Button", clickElement(kb.clickDismissButton()));
			}else {
				extentTest.log(LogStatus.PASS, "Excel Attachment Opened Successfully");
				clickOnElement("Close Attachment Button", clickElement(kb.clickCloseAttachmentButton()));
			}
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			clickOnElement("Cancel Button", clickElement(kb.clickCancelButton()));
			clickOnElement("Search Text Box", clickElement(kb.verifySearchTextBox()));
			//				//kb.verifySearchTextBox().clear();
			kb.verifySearchTextBox().sendKeys("Printer\n");
			//			}catch(Exception e) {
			//				e.printStackTrace();
			//				extentTest.log(LogStatus.FAIL, "Failed to Send Data For Search");
			//
			//			}
			if(platform.equalsIgnoreCase("android")){
//				try {
//					kb.verifyArticlePageSearchTextBox().sendKeys("Printer");
//				}catch(Exception e) {
//					e.printStackTrace();
//				}
				((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
			}
			clickOnElement("First Seatch Result", clickElement(kb.clickFirstSearchItem()));
			if(platform.equalsIgnoreCase("ios")){
				wait.until(ExpectedConditions.visibilityOf(kb.clickDismissEscapeButton()));
				clickOnElement("Dismiss Button", clickElement(kb.clickDismissEscapeButton()));
			}
			verifyElement("Embeded Image", elementDisplayed(kb.verifyEmbededImage()));
			verifyElement("Article Content ", elementDisplayed(kb.verifyArticleContent()));
			scroll(platform, "Attachments");
			clickOnElement("First Attachment", clickElement(kb.verifyAttachments()));
			if(elementDisplayed(kb.verifyFileNotSupportedText())) {
				extentTest.log(LogStatus.PASS, "Failed to Open Image Attachment because File is Not Supported by Device");
				clickOnElement("Dismiss Button", clickElement(kb.clickDismissButton()));
			}else if (elementDisplayed(kb.verifyAttachmentsErrorMessage())){
				try {
					wait.until(ExpectedConditions.visibilityOf(kb.verifyAttachmentsErrorMessage()));
				}catch(Exception e) {
					e.printStackTrace();
				}
				extentTest.log(LogStatus.FAIL, "Failed to Open Image Attachment");
				clickOnElement("Dismiss Button", clickElement(kb.clickDismissButton()));
			}else {
				extentTest.log(LogStatus.PASS, "Image Attachment Opened Successfully");
				clickOnElement("Close Attachment Button", clickElement(kb.clickCloseAttachmentButton()));
			}
			clickOnElement("Contextual Menu", clickElement(kb.verifyContextualMenu()));
			String actualCanNotFindAswerLabel=getText(kb.verifyCanNotFindAnswerText(), platform);
			String actualChatWithAmeliaLabel=getText(kb.verifyChatWithAmeliaText(), platform);;
			String actualWriteToUsLabel=getText(kb.verifyWriteToUsText(), platform);;
			verifyLabel("Can't Find Answer Text", matchData(expectedCanNotFindAswerText,actualCanNotFindAswerLabel));
			verifyLabel("Chat With Amelia Text", matchData(expectedChatWithAmeliaLabel,actualChatWithAmeliaLabel));
			verifyLabel("Write To Us Text", matchData(expectedWriteToUsLabel,actualWriteToUsLabel));
			if(!device.equalsIgnoreCase("iPad")){
			String actualCancelLabel=getText(kb.verifyCancelText(), platform);
			verifyLabel("Cancel Text", matchData(expectedCancelLabel,actualCancelLabel));
			clickOnElement("Cancel Text on Contextual Menu", clickElement(kb.verifyCancelText()));
			verifyElement("Article Details Page", elementDisplayed(kb.verifyKnowledgeHeaderText()));
			clickOnElement("Contextual Menu", clickElement(kb.verifyContextualMenu()));
			Thread.sleep(3000);
			}
			clickOnElement("Chat With Amelia Text on Contextual Menu", clickElement(kb.verifyChatWithAmeliaText()));
			wait.until(ExpectedConditions.visibilityOf(fm.toggleButton()));
			verifyElement("IVA Screen",elementDisplayed(fm.toggleButton())) ;
			LandingPage lp = new LandingPage(driver);
			clickOnElement("Home Naviagation Bar", clickElement(lp.homeIcon()));
			clickOnElement("Knowledge Base Button", clickElement(fm.clickKnowledgeBaseButton()));
			clickOnElement("Search Text Box", clickElement(kb.verifySearchTextBox()));
			kb.verifySearchTextBox().sendKeys("attachments\n");
			if(platform.equalsIgnoreCase("android")){
				//kb.verifyArticlePageSearchTextBox().sendKeys("attachments");
				((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
			}
			clickOnElement("First Seatch Result", clickElement(kb.clickFirstSearchItem()));
			clickOnElement("Contextual Menu", clickElement(kb.verifyContextualMenu()));
			clickOnElement("Write To Us Text on Contextual Menu", clickElement(kb.verifyWriteToUsText()));
			String expectedToEmail =getPropertyValue("ToEmail");
			if(platform.equalsIgnoreCase("android")){
				Thread.sleep(3000);
				clickElement(fm.getToEmailText());
				String actualToEmail =getText(fm.getToEmailText());
				Reporter.log(actualToEmail,true);
				if(actualToEmail.contains(expectedToEmail)) {
					extentTest.log(LogStatus.PASS, "To Email is Verified -- " +actualToEmail);
				}
				else {
					extentTest.log(LogStatus.FAIL, "To Email is Not Verified -- " +actualToEmail);
				}
				s.assertTrue(actualToEmail.contains(expectedToEmail));
			}
			s.assertAll();
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Knowledge Base Articles Deatils Page Test Failed");
		}
	}	
}
