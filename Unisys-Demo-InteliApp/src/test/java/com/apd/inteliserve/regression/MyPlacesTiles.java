package com.apd.inteliserve.regression;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.apd.inteliserve.pompages.KnowledgeBasePage;
import com.apd.inteliserve.pompages.LandingPage;
import com.apd.inteliserve.pompages.MyPlacesPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */


public class MyPlacesTiles extends BaseTest{
	@Test
	@Parameters({"platform","language","transalationSheet"})
	public void myPlacesTilesTest(@Optional String platform,@Optional String language,@Optional String transalationSheet) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2618 My Places Tiles Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-2613, My Places Tiles Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			String expectedFirstSupportTileLabel ="";
			String expectedSecondSupportTileLabel ="";
			String expectedThirdSupportTileLabel ="";
			String expectedFourthSupportTileLabel ="";
			String expectedFifthSupportTileLabel ="";
			String expectedSixthSupportTileLabel ="";
			String expectedFirstLearningTileLabel ="";
			String expectedSecondLearningTileLabel ="";
			String expectedThirdLearningTileLabel ="";
			String expectedFourthLearningTileLabel ="";
			String expectedFirstCompanyTileLabel ="";
			String expectedSecondCompanyTileLabel ="";
			String expectedThirdCompanyTileLabel ="";
			String expectedFourthCompanyTileLabel ="";
			String expectedFifthCompanyTileLabel ="";
			String expectedSixthCompanyTileLabel ="";
			String expectedFirstServicesTileLabel ="";
			String expectedSecondServicesTileLabel ="";
			String expectedThirdServicesTileLabel ="";
			String expectedFourthServicesTileLabel ="";
			String expectedFifthServicesTileLabel ="";
			String expectedSixthServicesTileLabel ="";
			String expectedMyPlacesHeaderLabel  ="";
			String expectedSupportTitleLabel  ="";
			String expectedSupportDescriptionLabel  ="";
			String expectedLearningTitleLabel  ="";
			String expectedLearningDiscriptionLabel  ="";
			String expectedCompanyTitleLabel  ="";
			String expectedCompanyDiscriptionLabel  ="";
			String expectedServicesTitleLabel  ="";
			String expectedServicesDiscriptionLabel  ="";
			if(platform.equalsIgnoreCase("ios")){
				expectedFirstSupportTileLabel =getCellValue(transalationSheet, 75, 1);
				expectedSecondSupportTileLabel =getCellValue(transalationSheet, 76, 1);
				expectedThirdSupportTileLabel =getCellValue(transalationSheet, 77, 1);
				expectedFourthSupportTileLabel =getCellValue(transalationSheet, 78, 1);
				expectedFifthSupportTileLabel =getCellValue(transalationSheet, 57, 1);
				expectedSixthSupportTileLabel =getCellValue(transalationSheet, 58, 1);
				expectedFirstLearningTileLabel =getCellValue(transalationSheet, 59, 1);
				expectedSecondLearningTileLabel =getCellValue(transalationSheet, 60, 1);
				expectedThirdLearningTileLabel =getCellValue(transalationSheet, 61, 1);
				expectedFourthLearningTileLabel =getCellValue(transalationSheet, 79, 1);
				expectedFirstCompanyTileLabel =getCellValue(transalationSheet, 80, 1);
				expectedSecondCompanyTileLabel =getCellValue(transalationSheet, 81, 1);
				expectedThirdCompanyTileLabel =getCellValue(transalationSheet, 82, 1);
				expectedFourthCompanyTileLabel =getCellValue(transalationSheet, 66, 1);
				expectedFifthCompanyTileLabel =getCellValue(transalationSheet, 83, 1);
				expectedSixthCompanyTileLabel =getCellValue(transalationSheet, 84, 1);
				expectedFirstServicesTileLabel =getCellValue(transalationSheet, 69, 1);
				expectedSecondServicesTileLabel =getCellValue(transalationSheet, 85, 1);
				expectedThirdServicesTileLabel =getCellValue(transalationSheet, 71, 1);
				expectedFourthServicesTileLabel =getCellValue(transalationSheet, 72, 1);
				expectedFifthServicesTileLabel =getCellValue(transalationSheet, 86, 1);
				expectedSixthServicesTileLabel =getCellValue(transalationSheet, 74, 1);
				expectedMyPlacesHeaderLabel =getCellValue(transalationSheet, 87, 1);
				expectedSupportTitleLabel  =getCellValue(transalationSheet, 88, 1);
				expectedSupportDescriptionLabel  =getCellValue(transalationSheet, 89, 1);
				expectedLearningTitleLabel  =getCellValue(transalationSheet, 90, 1);
				expectedLearningDiscriptionLabel  =getCellValue(transalationSheet, 91, 1);
				expectedCompanyTitleLabel  =getCellValue(transalationSheet, 92, 1);
				expectedCompanyDiscriptionLabel  =getCellValue(transalationSheet, 93, 1);
				expectedServicesTitleLabel  =getCellValue(transalationSheet, 94, 1);
				expectedServicesDiscriptionLabel  =getCellValue(transalationSheet, 95, 1);
			}else {
				expectedFirstSupportTileLabel =getCellValue(transalationSheet, 53, 1);
				expectedSecondSupportTileLabel =getCellValue(transalationSheet, 54, 1);
				expectedThirdSupportTileLabel =getCellValue(transalationSheet, 55, 1);
				expectedFourthSupportTileLabel =getCellValue(transalationSheet, 56, 1);
				expectedFifthSupportTileLabel =getCellValue(transalationSheet, 57, 1);
				expectedSixthSupportTileLabel =getCellValue(transalationSheet, 58, 1);
				expectedFirstLearningTileLabel =getCellValue(transalationSheet, 59, 1);
				expectedSecondLearningTileLabel =getCellValue(transalationSheet, 60, 1);
				expectedThirdLearningTileLabel =getCellValue(transalationSheet, 61, 1);
				expectedFourthLearningTileLabel =getCellValue(transalationSheet, 62, 1);
				expectedFirstCompanyTileLabel =getCellValue(transalationSheet, 63, 1);
				expectedSecondCompanyTileLabel =getCellValue(transalationSheet, 64, 1);
				expectedThirdCompanyTileLabel =getCellValue(transalationSheet, 65, 1);
				expectedFourthCompanyTileLabel =getCellValue(transalationSheet, 66, 1);
				expectedFifthCompanyTileLabel =getCellValue(transalationSheet, 67, 1);
				expectedSixthCompanyTileLabel =getCellValue(transalationSheet, 68, 1);
				expectedFirstServicesTileLabel =getCellValue(transalationSheet, 69, 1);
				expectedSecondServicesTileLabel =getCellValue(transalationSheet, 70, 1);
				expectedThirdServicesTileLabel =getCellValue(transalationSheet, 71, 1);
				expectedFourthServicesTileLabel =getCellValue(transalationSheet, 72, 1);
				expectedFifthServicesTileLabel =getCellValue(transalationSheet, 73, 1);
				expectedSixthServicesTileLabel =getCellValue(transalationSheet, 74, 1);
				expectedMyPlacesHeaderLabel =getCellValue(transalationSheet, 87, 1);
				expectedSupportTitleLabel  =getCellValue(transalationSheet, 88, 1);
				expectedSupportDescriptionLabel  =getCellValue(transalationSheet, 89, 1);
				expectedLearningTitleLabel  =getCellValue(transalationSheet, 90, 1);
				expectedLearningDiscriptionLabel  =getCellValue(transalationSheet, 91, 1);
				expectedCompanyTitleLabel  =getCellValue(transalationSheet, 92, 1);
				expectedCompanyDiscriptionLabel  =getCellValue(transalationSheet, 93, 1);
				expectedServicesTitleLabel  =getCellValue(transalationSheet, 94, 1);
				expectedServicesDiscriptionLabel  =getCellValue(transalationSheet, 95, 1);
			}
			LandingPage ld = new LandingPage(driver);
			MyPlacesPage mp= new MyPlacesPage(driver);
			clickOnElement("Nine Dot Launcher", clickElement(ld.launcherIcon()));
			String actualMyPlacesHeaderLabel=getText(mp.verifyMyPlacesHeader(), platform);
			String actualSupportTitleLabel=getText(mp.verifySupportHeader(), platform);
			String actualSupportDescriptionLabel=getText(mp.verifySupportDescription(), platform);
			String actualFirstSupportTileLabel=getText(mp.verifyFirstSupportTile(), platform);
			String actualSecondSupportTileLabel=getText(mp.verifySecondSupportTile(), platform);
			String actualThirdSupportTileLabel=getText(mp.verifyThirdSupportTile(), platform);
			String actualSupportTitle=actualSupportTitleLabel.toUpperCase();
			verifyLabel("My Places Header", matchData(expectedMyPlacesHeaderLabel,actualMyPlacesHeaderLabel));
			verifyLabel("Support Title", matchData(expectedSupportTitleLabel,actualSupportTitle));
			verifyLabel("Support Description", matchData(expectedSupportDescriptionLabel,actualSupportDescriptionLabel));
			verifyLabel("Support Corner Tile", matchData(expectedFirstSupportTileLabel,actualFirstSupportTileLabel));
			verifyLabel("Knowledge Base Tile", matchData(expectedSecondSupportTileLabel,actualSecondSupportTileLabel));
			verifyLabel("Crisis Managemnet Tile", matchData(expectedThirdSupportTileLabel,actualThirdSupportTileLabel));
			KnowledgeBasePage kb = new KnowledgeBasePage(driver);
			FavouriteModulesPage fm =  new FavouriteModulesPage(driver);
			clickOnElement("Knowledge Base Button", clickElement(mp.clickKknowledgeBaseButton()));
			verifyElement("Knowledge Base Landing Page", elementDisplayed(kb.verifyCategoriesText()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			clickOnElement("Support Corner", clickElement(mp.clickSupportCornerButton()));
			verifyElement("Support Corner Page", elementDisplayed(kb.verifyAskITText()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			clickOnElement("Crisis Managment",clickElement(mp.clickCrisisManagemnetButton()));
			verifyElement("Your Company Content Text", elementDisplayed(ld.verifyCompanyContentLabel()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			extentTest.log(LogStatus.INFO, "Performing Left Swipe");
			swipe(mp.verifyThirdSupportTile(), mp.verifyFirstSupportTile());
			Thread.sleep(2000);
			String actualFourthSupportTileLabel=getText(mp.verifyFourthSupportTile(), platform);
			String actualFifthSupportTileLabel=getText(mp.verifyFifthSupportTile(), platform);
			String actualSixthSupportTileLabel=getText(mp.verifySixthSupportTile(), platform);
			verifyLabel("Emergency Contacts", matchData(expectedFourthSupportTileLabel,actualFourthSupportTileLabel));
			verifyLabel("CSR", matchData(expectedFifthSupportTileLabel,actualFifthSupportTileLabel));
			verifyLabel("Feedback", matchData(expectedSixthSupportTileLabel,actualSixthSupportTileLabel));
			String actualLearningTitleLabel=getText(mp.verifyLearningHeader(), platform);
			String actualLearningDescriptionLabel=getText(mp.verifyLearningDescription(), platform);
			String actualLearningTitle=actualLearningTitleLabel.toUpperCase();
			verifyLabel("Learning Title", matchData(expectedLearningTitleLabel,actualLearningTitle));
			verifyLabel("Learning Description", matchData(expectedLearningDiscriptionLabel,actualLearningDescriptionLabel));
			extentTest.log(LogStatus.INFO, "Performing Left Swipe Second Time");
			swipe(mp.verifyFifthSupportTile(), mp.verifyThirdSupportTile());
			String actualFirstLearningTileLabel=getText(mp.verifyFirstLearningTile(), platform);
			String actualSecondLearningTileLabel=getText(mp.verifySecondLearningTile(), platform);
			String actualThirdLearningTileLabel=getText(mp.verifyThirdLearningTile(), platform);
			String actualFourthLearningTileLabel=getText(mp.verifyFourthLearningTile(), platform);
			verifyLabel("University Tile", matchData(expectedFirstLearningTileLabel,actualFirstLearningTileLabel));
			verifyLabel("Blog Tile", matchData(expectedSecondLearningTileLabel,actualSecondLearningTileLabel));
			verifyLabel("Tech Talk Tile", matchData(expectedThirdLearningTileLabel,actualThirdLearningTileLabel));
			verifyLabel("Online Tile", matchData(expectedFourthLearningTileLabel,actualFourthLearningTileLabel));
			extentTest.log(LogStatus.INFO, "Performing Left Swipe");
			swipe(mp.verifyThirdLearningTile(), mp.verifyFirstLearningTile());
			//Thread.sleep(2000);
			//swipe(mp.verifyFifthSupportTile(), mp.verifyThirdSupportTile());
			scroll(platform, "SERVICES");
			String actualCompanyTitleLabel=getText(mp.verifyCompanyHeader(), platform);
			String actualCompanyDescriptionLabel=getText(mp.verifyCompanyDescription(), platform);
			String actualCompanyTitle=actualCompanyTitleLabel.toUpperCase();
			verifyLabel("Company Title", matchData(expectedCompanyTitleLabel,actualCompanyTitle));
			verifyLabel("Company Description", matchData(expectedCompanyDiscriptionLabel,actualCompanyDescriptionLabel));
			String actualFirstCompanyTileLabel=getText(mp.verifyFirstCompanyTile(), platform);
			String actualSecondCompanyTileLabel=getText(mp.verifySecondCompanyTile(), platform);
			String actualThirdCompanyTileLabel=getText(mp.verifyThirdCompanyTile(), platform);
			String actualFourthCompanyTileLabel=getText(mp.verifyFourthCompanyTile(), platform);
			verifyLabel("Company News Tile", matchData(expectedFirstCompanyTileLabel,actualFirstCompanyTileLabel));
			verifyLabel("Human Resources Tile", matchData(expectedSecondCompanyTileLabel,actualSecondCompanyTileLabel));
			verifyLabel("Organizational Chart Tile", matchData(expectedThirdCompanyTileLabel,actualThirdCompanyTileLabel));
			verifyLabel("Payroll Tile", matchData(expectedFourthCompanyTileLabel,actualFourthCompanyTileLabel));
			extentTest.log(LogStatus.INFO, "Performing Left Swipe");
			swipe(mp.verifyThirdCompanyTile(), mp.verifyFirstCompanyTile());
			String actualFifthCompanyTileLabel=getText(mp.verifyFifthCompanyTile(), platform);
			String actualSixthCompanyTileLabel=getText(mp.verifySixthCompanyTile(), platform);
			//Thread.sleep(2000);
			verifyLabel("Holiday List Tile", matchData(expectedFifthCompanyTileLabel,actualFifthCompanyTileLabel));
			verifyLabel("Medical Insurance Tile", matchData(expectedSixthCompanyTileLabel,actualSixthCompanyTileLabel));
			String actualServicesTitleLabel=getText(mp.verifyServicesHeader(), platform);
			String actualServicesDescriptionLabel=getText(mp.verifyServicesDescription(), platform);
			String actualServicesTitle=actualServicesTitleLabel.toUpperCase();
			verifyLabel("Services Title", matchData(expectedServicesTitleLabel,actualServicesTitle));
			verifyLabel("Services Title", matchData(expectedServicesDiscriptionLabel,actualServicesDescriptionLabel));
			String actualFirstServicesTileLabel=getText(mp.verifyFirstServicesTile(), platform);
			String actualSecondServicesTileLabel=getText(mp.verifySecondServicesTile(), platform);
			String actualThirdServicesTileLabel=getText(mp.verifyThirdServicesTile(), platform);
			String actualFourthServicesTileLabel=getText(mp.verifyFourthServicesTile(), platform);
			verifyLabel("Timesheet Tile", matchData(expectedFirstServicesTileLabel,actualFirstServicesTileLabel));
			verifyLabel("Expense Reporting Tile", matchData(expectedSecondServicesTileLabel,actualSecondServicesTileLabel));
			verifyLabel("Travel Desk Tile", matchData(expectedThirdServicesTileLabel,actualThirdServicesTileLabel));
			verifyLabel("Meetings Tile", matchData(expectedFourthServicesTileLabel,actualFourthServicesTileLabel));
			extentTest.log(LogStatus.INFO, "Performing Left Swipe");
			swipe(mp.verifyThirdServicesTile(), mp.verifyFirstServicesTile());
			String actualFifthServicesTileLabel=getText(mp.verifyFifthServicesTile(), platform);
			String actualSixthServicesTileLabel=getText(mp.verifySixthServicesTile(), platform);
			//Thread.sleep(2000);
			verifyLabel("Performance Review Tile", matchData(expectedFifthServicesTileLabel,actualFifthServicesTileLabel));
			verifyLabel("Attendance Tile", matchData(expectedSixthServicesTileLabel,actualSixthServicesTileLabel));
			swipe(mp.verifyThirdCompanyTile(), mp.verifySixthCompanyTile());
			clickOnElement("Company News Button", clickElement(mp.clickKCompanyNewsButtonButton()));
			String expectedUrl =getPropertyValue("CompanyNewsUrl");
			if(platform.equalsIgnoreCase("ios")){
				clickElement(fm.getURL());
			}
			String actualUrl= getText(fm.getCurrentURL());
			Reporter.log(actualUrl,true);
			if(actualUrl.contains(expectedUrl)) {
				extentTest.log(LogStatus.PASS, "Navigated to Corerect URL -- " + actualUrl);
			}
			else {
				extentTest.log(LogStatus.FAIL, "Navigated to Wrong URL -- " + actualUrl);
			}
			s.assertTrue(actualUrl.contains(expectedUrl));
			s.assertAll();
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("My Places Tiles Test Failed");
		}	
	}

}
