package com.apd.inteliserve.regression;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.general.Lib;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.apd.inteliserve.pompages.KnowledgeBasePage;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class KnowledgeBaseSearch extends BaseTest {
	@Test
	@Parameters({"platform","language","transalationSheet"})
	public void KnowledgeBaseLandingScreenTest(@Optional String platform,@Optional String language,@Optional String transalationSheet) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2071, Knowledge Base Search Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-2084, Knowledge Base Search Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			KnowledgeBasePage kb = new KnowledgeBasePage(driver);
			clickOnElement("Knowledge Base Button", clickElement(fm.clickKnowledgeBaseButton()));
			extentTest.log(LogStatus.INFO, "Verifying Search Functionality on Categories List Screen");	
			searchTest(platform, language, transalationSheet);
			verifyElement("Knowledge Base Landing Screen", elementIsDisplayed(kb.verifyCategoriesText()));
			clickOnElement("Knowledge Base Button", clickElement(kb.clickFirstCategory()));
			extentTest.log(LogStatus.INFO, "Verifying Search Functionality on Article List Screen");		
			searchTest(platform, language, transalationSheet);
			verifyElement("Article List Page Screen", elementIsDisplayed(kb.clickFirstArticle()));

		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Knowledge Base Search Test Failed");
		}
	}

	@SuppressWarnings("rawtypes")
	@Parameters({"platform","language","transalationSheet"})
	public void searchTest(@Optional String platform,@Optional String language,@Optional String transalationSheet) {

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();


		String expectedKnowledgeBaseHeaderLabel =getCellValue(transalationSheet, 23, 1);
		String expectedSearchKeywordLabel =getCellValue(transalationSheet, 24, 1);
		String expectedCancelButtonLabel=getCellValue(transalationSheet, 31, 1);
		String expectedNoResultsLabel=getCellValue(transalationSheet, 32, 1);
		String expectedNoResultsHelpLabel=getCellValue(transalationSheet, 33, 1);
		String expectedSearchHelpLabel=getCellValue(transalationSheet, 34, 1);
		String expectedSearchResultsLabel=getCellValue(transalationSheet, 35, 1);
		String expectedSearchItemText=getCellValue("ExpectedText", 7, 1);
       
		KnowledgeBasePage kb = new KnowledgeBasePage(driver);
		
		String actualKnowledgeBaseHeaderLabel=getText(kb.verifyKnowledgeHeaderText(),platform);
		String actualSearchKeywordLabel=getText(kb.verifySearchTextBox(),platform);
		verifyLabel("Knowledge Base Header", matchData(expectedKnowledgeBaseHeaderLabel,actualKnowledgeBaseHeaderLabel));
		verifyElement("Search Text Box",elementIsDisplayed(kb.verifySearchTextBox())) ;
		verifyLabel("Search Any Keyword", containsData(actualSearchKeywordLabel, expectedSearchKeywordLabel));
		verifyElement("Search Icon in Search Text Box",elementIsDisplayed(kb.verifySearchIconInSearchBox())) ;
		
		try {
			clickOnElement("Search Text Box", clickElement(kb.verifySearchTextBox()));
			Thread.sleep(3000);
			clickOnElement("Second Time Search Text Box", clickElement(kb.verifyArticlePageSearchTextBox()));
			String actualSearchHelpLabel=getText(kb.verifySearchHelpText(), platform);
			String actualCancelButtonLabel=getText(kb.clickCancelButton(), platform);
			System.out.println(actualSearchHelpLabel);
			System.out.println(actualCancelButtonLabel);
			verifyLabel("Cancel Button Text", matchData(expectedCancelButtonLabel,actualCancelButtonLabel));
			//verifyData(actualCancelButtonLabel,"Cancel Button Text", matchData(expectedCancelButtonLabel,actualCancelButtonLabel));
			verifyLabel("Search Help Text",matchData(actualSearchHelpLabel,expectedSearchHelpLabel));
			kb.verifyArticlePageSearchTextBox().sendKeys("VPN\n");
			Thread.sleep(3000);
			if(platform.equalsIgnoreCase("android")){
				((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
			}
			String actualSearchResultsLabel=getText(kb.verifySearchResultsText(), platform);
			verifyLabel("Search Results Label", matchData(expectedSearchResultsLabel,actualSearchResultsLabel));
			List<MobileElement> searchItems=kb.getsearchItemsCount();
			List<MobileElement> searchTexts=kb.getsearchTextsCount();
			int actualSearchItemsCount=searchItems.size();
			int actualSearchTextsCount=searchTexts.size();
//			System.out.println(actualSearchItemsCount+"-----------------------------------");
//			System.out.println(actualSearchTextsCount+"-----------------------------------");
			if(platform.equalsIgnoreCase("ios")){
				if(actualSearchTextsCount==actualSearchItemsCount/2) {
					extentTest.log(LogStatus.PASS, "VPN Releated Search Items Are Shown Correctly");
					extentTest.log(LogStatus.INFO,"Articles Displayed Are --------");
					for(MobileElement element : searchTexts) {
						String getSerachedArticle= getText(element, platform);
						extentTest.log(LogStatus.INFO,getSerachedArticle);
					}
				}
				else {
					extentTest.log(LogStatus.FAIL, "VPN Releated Search Items Are Not Shown Correctly");
				}
				kb.verifySearchTextBox().clear();
				kb.verifySearchTextBox().sendKeys("Hello\n");
			}else {
				if(actualSearchTextsCount==actualSearchItemsCount) {
					extentTest.log(LogStatus.PASS, "VPN Releated Search Items Are Shown Correctly");
					extentTest.log(LogStatus.INFO,"Articles Displayed Are --------");
					for(MobileElement element : searchTexts) {
						String getSerachedArticle= getText(element, platform);
						extentTest.log(LogStatus.INFO,getSerachedArticle);
					}
				}
				else {
					extentTest.log(LogStatus.FAIL, "VPN Releated Search Items Are Not Shown Correctly");
				}
				clickOnElement("Search Text Box", clickElement(kb.verifyArticlePageSearchTextBox()));
				verifyElement("Clear Icon", elementIsDisplayed(kb.verifyClearIcon()));
				clickOnElement("Clear Icon", clickElement(kb.verifyClearIcon()));
				verifyLabel("Data Cleared And Search Any Keyword Text", containsData(actualSearchKeywordLabel, expectedSearchKeywordLabel));
				kb.verifyArticlePageSearchTextBox().sendKeys("Hello\n");
				((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
			}

			String actualNoResultsLabel=getText(kb.verifyNoResultsText(),platform);;
			String actualNoResultsHelpLabel=getText(kb.verifyNoResultsHelpText(),platform);
			String actualSearchItemText=getText(kb.verifyArticlePageSearchTextBox(),platform);
			System.out.println(actualNoResultsLabel);
			System.out.println(actualNoResultsHelpLabel);
			System.out.println(actualSearchItemText);
			verifyElement("Search Icon",elementIsDisplayed(kb.verifySearchIcon())) ;
			verifyLabel("No Results", matchData(expectedNoResultsLabel,actualNoResultsLabel));
			verifyLabel("No Results Help", matchData(expectedNoResultsHelpLabel,actualNoResultsHelpLabel));
			verifyLabel("Data Retained as Search Text " +expectedSearchItemText+" ", containsData(actualSearchItemText,expectedSearchItemText));
		}catch(Exception e) {
			e.printStackTrace();
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
		}
		clickOnElement("Cancel Button", clickElement(kb.clickCancelButton()));
		s.assertAll();
	}
}
