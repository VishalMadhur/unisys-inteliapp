package com.apd.inteliserve.regression;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.general.Lib;
import com.apd.inteliserve.pompages.EditProfilePage;
import com.apd.inteliserve.pompages.LandingPage;
import com.relevantcodes.extentreports.LogStatus;


public class VerifyProfilePhoto extends BaseTest {
	@Test
	@Parameters({"platform","Title","transalationSheet"})
	public void verifyProfilePhoto(@Optional String platform,@Optional String Title,@Optional String transalationSheet) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2517, Verify Profile Photo Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-2445, Verify Profile Photo Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");

			String expectedAddPhotoLabel =getCellValue(transalationSheet, 175, 1);
			String expectedTakePhotLabel =getCellValue(transalationSheet, 176, 1);
			String expectedCancelLabel =getCellValue(transalationSheet, 177, 1);
			String expectedRemoveCurrentPhotoLabel =getCellValue(transalationSheet, 178, 1);
			String expectedChangePhotoLabel =getCellValue(transalationSheet, 179, 1);



			LandingPage ld = new LandingPage(driver);
			EditProfilePage ep = new EditProfilePage(driver);

			clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
			clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
			clickOnElement("Camera Icon", clickElement(ep.verifyCameraIcon()));
			String actualAddPhotoLabel =getText(ep.verifyAddProfilePhotoLabel(),platform);
			String actualTakePhotLabel =getText(ep.verifyTakePhotoLabel(),platform);
			String actualCancelLabel =getText(ep.verifyCancelLabel(),platform);
			verifyLabel("Add Profile Photo", matchData(expectedAddPhotoLabel,actualAddPhotoLabel));
			verifyLabel("Take a Photo", matchData(expectedTakePhotLabel,actualTakePhotLabel));
			verifyLabel("Cancel", matchData(expectedCancelLabel,actualCancelLabel));
			clickOnElement("Cancel Link",clickElement(ep.verifyCancelLabel()));
			verifyElement("Edit Profile Page", elementDisplayed(ep.verifyEditProfileHeader()));
			if(platform.equals("android")) {
				clickOnElement("Camera Icon", clickElement(ep.verifyCameraIcon()));
				clickOnElement("Take Photo Link", clickElement(ep.verifyTakePhotoLabel()));
				try {
					clickOnElement("Camera Access Allow Button",clickElement(ep.verifyAllowButton()));
					clickOnElement("Camera Premisons Allow Button",clickElement(ep.verifyAllowButton()));
				}catch(Exception e) {
					extentTest.log(LogStatus.INFO,"Camera Permisions Are Alraedy Allowed");
				}
				clickOnElement("Photo Capture Button",clickElement(ep.verifyCapturePhotoButton()));
				waitForVisibityOfSpecificElement("Retake Button", ep.verifyRetakePhotoButton());
				clickOnElement("Retake Button",clickElement(ep.verifyRetakePhotoButton()));
				verifyElement("Capture Photo Screen", elementDisplayed(ep.verifyCapturePhotoButton()));
				clickOnElement("Photo Capture Button",clickElement(ep.verifyCapturePhotoButton()));
				clickOnElement("Cancel Photo Button",clickElement(ep.verifyCancelPhotoButton()));
				verifyElement("Edit Profile Page", elementDisplayed(ep.verifyEditProfileHeader()));
				clickOnElement("Camera Icon", clickElement(ep.verifyCameraIcon()));
				clickOnElement("Take Photo Link", clickElement(ep.verifyTakePhotoLabel()));
				clickOnElement("Photo Capture Button",clickElement(ep.verifyCapturePhotoButton()));
				clickOnElement("Confirm Photo Capture Button",clickElement(ep.verifySelectPhotoButton()));
				verifyElement("Option to Rotate Photo", elementDisplayed(ep.clickRotateButton()));
				clickOnElement("Rotate Photo Button",clickElement(ep.clickRotateButton()));
				rotatePhotoByAngle();
				rotatePhotoByAngle();
				rotatePhotoByAngle();
				rotatePhotoByAngle();
				clickOnElement("Confirm Photo After Croping",clickElement(ep.verifyCropButton()));
				verifyElement("Edit Profile Page", elementDisplayed(ep.verifyEditProfileHeader()));
				verifyElement("Edit Profile Picture", elementDisplayed(ep.verifyEditProfileImage()));
				clickOnElement("Save Button", clickElement(ep.verifyEnableSaveButton()));
				logOut(platform);
				login(platform);
				clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
				verifyElement("In Profile Page Update Profile Picture", elementDisplayed(ep.verifyProfileImage()));
				clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
				verifyElement("Edit Profile Picture", elementDisplayed(ep.verifyEditProfileImage()));
				clickOnElement("Camera Icon", clickElement(ep.verifyCameraIcon()));
				String actualChangePhotoLabel =getText(ep.verifyChangeProfilePhotoLabel(),platform);
				actualTakePhotLabel =getText(ep.verifyTakePhotoLabel(),platform);
				actualCancelLabel =getText(ep.verifyCancelLabel(),platform);
				String actualRemovePhotoLabel=getText(ep.verifyRemoveCurrentPhotoLabel(),platform);
				verifyElementNotDisplayed("Add Profile Photo Label", elementDisplayed(ep.verifyAddProfilePhotoLabel()));
				verifyLabel("Change Profile Photo", matchData(expectedChangePhotoLabel,actualChangePhotoLabel));
				verifyLabel("Take a Photo", matchData(expectedTakePhotLabel,actualTakePhotLabel));
				verifyLabel("Cancel", matchData(expectedCancelLabel,actualCancelLabel));
				verifyLabel("Remove Current Photo", matchData(expectedRemoveCurrentPhotoLabel,actualRemovePhotoLabel));
				clickOnElement("Remove Current Photo Link", clickElement(ep.verifyRemoveCurrentPhotoLabel()));
				verifyElement("Profile Initials", elementDisplayed(ep.verifyUpdateProfileInitials()));
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Verify Profile Photo Test Failed");
		}
	}	

	public void rotatePhotoByAngle() {
		EditProfilePage ep = new EditProfilePage(driver);
		try {
			extentTest.log(LogStatus.INFO,"Rotating Captured Pic By 90 Degree");
			clickOnElement("Rotate By Angle", clickElement(ep.clickRotateByAngleButton()));
			extentTest.log(LogStatus.PASS,"Picture Rotated By 90 Degree");
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL,"Failed to Rotate Picture By 90 Degree");
		}
	}
}