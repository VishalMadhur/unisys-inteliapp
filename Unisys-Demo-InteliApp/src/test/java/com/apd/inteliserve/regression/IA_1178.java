package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.EPAPage;
import com.apd.inteliserve.pompages.LandingPage;
import com.apd.inteliserve.pompages.OpenTicketListPage;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;


public class IA_1178 extends BaseTest {
	public	String oldTicketNumber=null;
	SoftAssert s = new SoftAssert();

	@Test
	@Parameters({"platform"})
	public void updateTicketFromAmelia(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String lastTicketToUpdate =null;
		String secondLastTicketToUpdate=null;
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-1178, Update Ticket From Amelia Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-1178, Update Ticket From Amelia Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			OpenTicketListPage ot = new OpenTicketListPage(driver);
			EPAPage epa = new EPAPage(driver);
			if(elementDisplayed(ot.verifyNoTicketText())){
				extentTest.log(LogStatus.FAIL, "Ticket is Present For user But Not Displayed"); 
				extentTest.log(LogStatus.INFO, "Instead of Ticket Got Message --- " + getText(ot.verifyNoTicketText(),platform));
			}
			else if (elementDisplayed(ot.verifyTicketErrorMessage())) {
				extentTest.log(LogStatus.PASS, "Ticket is Present For user But Not Displayed Due to API or Internet Issue");
				extentTest.log(LogStatus.INFO, "Instead of Ticket Got Message --- " + getText(ot.verifyTicketErrorMessage(),platform));
			}else{
				extentTest.log(LogStatus.PASS, "Ticket is Present For User");
				extentTest.log(LogStatus.INFO, "Checking If Ticket is Displyed in Open Ticket List");
				if(elementIsDisplayed(ot.verifyTicketNumber())){
					extentTest.log(LogStatus.PASS, "Ticket is Displayed");
					oldTicketNumber=getText(epa.verifyTicketNumText(),platform);
					if(platform.equalsIgnoreCase("ios")){
						String [] getOldTicketNumber = oldTicketNumber.split(" ");
						oldTicketNumber=getOldTicketNumber[5];
					}
					if(platform.equalsIgnoreCase("android")){
						extentTest.log(LogStatus.PASS, "Got Old  Ticket Number --- " +oldTicketNumber);
					}
					else {
						extentTest.log(LogStatus.PASS, "Got Old  Ticket Number --- " +oldTicketNumber);
					}
				}
				else{
					extentTest.log(LogStatus.FAIL, "Ticket is Not Displayed");
				}
				click(epa.ivaIcon());
				extentTest.log(LogStatus.PASS, "User is Able to Navigate to Amelia Page");
				waitForVisibityOfSpecificElement("Send Message Button", epa.sendMessage());
				click(epa.sendMessage());
				click(epa.sendMessage());
				sendExcelTestData(epa.editBox(), "TestData", 8, 1);
				extentTest.log(LogStatus.INFO, "Fetched Query From Excel Sheet");
				click(epa.sendMessage());
				extentTest.log(LogStatus.PASS, "Query  Entered Successfully");
				waitForVisibityOfSpecificElement("Chat Toggle Button", epa.chatToggleButton());
				click(epa.chatToggleButton());
				extentTest.log(LogStatus.PASS, "Clicked on Ticket Managemnet Button");
				scroll(platform, "OPEN TICKETS");
				if(elementDisplayed(epa.verifyTicketNumEPA())){
					extentTest.log(LogStatus.PASS, "Open Tickets are Dispalyed for Selection");
					List<MobileElement> getOpenTickets= getElements(epa.verifyOpenTicketsList());
					extentTest.log(LogStatus.PASS, "Got List of Open Tickets");
					extentTest.log(LogStatus.INFO, "List of Open Tickets Are ---- ");
					for(MobileElement element : getOpenTickets) {
						String OpenTickets = getText(element, platform);
						extentTest.log(LogStatus.INFO, OpenTickets);
					}
					lastTicketToUpdate = getText(epa.verifyLastTicketNumEPA(),platform);
					secondLastTicketToUpdate= getText(epa.verifySecondLastTicketNumEPA(),platform);
					click(epa.closeChatIcon());
					extentTest.log(LogStatus.PASS, "Closed Ticket Mangement Menu");
					String[]strr= lastTicketToUpdate.split(" ");
					String ticketOne= strr[0];
					String[]strr1= secondLastTicketToUpdate.split(" ");
					String ticketTwo= strr1[0];
					extentTest.log(LogStatus.INFO, "Selected Last Ticket to Esclate ----- " + ticketOne);
					extentTest.log(LogStatus.INFO, "Checking if Ticket to Update is Already Latest Ticket");
					if(!ticketOne.equals(oldTicketNumber)){
						extentTest.log(LogStatus.PASS, "Ticket to Update is Not Latest Ticket");
						extentTest.log(LogStatus.INFO, "Updating  Ticket Number ----- " + ticketOne);
						updateTicket(platform, ticketOne);

					}
					else{
						extentTest.log(LogStatus.PASS, "Ticket to Update is Not Latest Ticket Hence Escalating Another Ticket ----- " + ticketTwo);
						extentTest.log(LogStatus.PASS, "Updating Ticket Number ----- " + ticketTwo);
						updateTicket(platform, ticketTwo);
					}
				}
				else{
					extentTest.log(LogStatus.FAIL, "Open Tickets are Not Dispalyed for Selection");
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Update ticket From Amelia Test Failed");
		}
	}

	@SuppressWarnings({ "rawtypes" })
	@Parameters({"platform"})
	public void updateTicket(@Optional String platform, String ticketNumber) throws Exception{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		EPAPage epa = new EPAPage(driver);
		if(platform.equalsIgnoreCase("ios")){
			TouchAction action = new TouchAction(driver);
			action.press(PointOption.point(16,767)).release().perform();
			sendTestData(epa.messageBox(), ticketNumber);
			extentTest.log(LogStatus.PASS, "Entered Ticket to Update---- " +ticketNumber);
			clickElement(epa.clickReturnButton());
			clickElement(epa.clickDeleteButton());
			clickElement(epa.clickReturnButton());
			clickElement(epa.clickReturnButton());
			extentTest.log(LogStatus.PASS, "Clicked on Ticket Successfully");
		}
		else {
			sendTestData(epa.editBox(), ticketNumber);
			extentTest.log(LogStatus.PASS, "Entered Ticket to Update---- " +ticketNumber);
			click(epa.verifyTicketNumEPA());
			extentTest.log(LogStatus.PASS, "Entered Ticket to Update---- " +ticketNumber);
			clickElement(epa.toggleButton());
			Thread.sleep(2000);
			clickElement(epa.toggleButton());
			click(epa.sendMessage());
			extentTest.log(LogStatus.PASS, "Clicked on Ticket Successfully");
		}
		Thread.sleep(5000);
		click(epa.addCommentButton());
		extentTest.log(LogStatus.PASS, "Clicked on Add Comment Button");
		click(epa.sendMessage());
		sendExcelTestData(epa.editBox(), "TestData", 9, 1);
		extentTest.log(LogStatus.PASS, "Fetched Description From Excel Sheet");
		click(epa.sendMessage());
		extentTest.log(LogStatus.PASS, "Description  Entered Successfully");
		Thread.sleep(10000);
		clickOnElement("Yes Radio Button", clickElement(epa.yesRadioButton()));
		//waitForVisibityOfSpecificElement("No Radio Button", epa.noRadioButton());
		Thread.sleep(10000);
		click(epa.noRadioButton());
		extentTest.log(LogStatus.PASS, "Clicked on No Button");
		extentTest.log(LogStatus.PASS, "Ticket Updated--- " +ticketNumber);
		Thread.sleep(5000);
		logOut(platform);
		login(platform);
		LandingPage ld = new LandingPage(driver);
		waitForVisibityOfSpecificElement("Profile Icon", ld.profileIcon());
		try{
			Thread.sleep(5000);
			String latestTicketNumber=getText(epa.verifyTicketNumText(),platform);
			if(platform.equalsIgnoreCase("ios")){
				String [] getLatestTicketNumber = latestTicketNumber.split(" ");
				latestTicketNumber=getLatestTicketNumber[5];
			}
			extentTest.log(LogStatus.INFO, "Checking if Updated Ticket Appears on Top of Open Ticket List");
			if(latestTicketNumber.contains(ticketNumber)){
				extentTest.log(LogStatus.PASS, "Updated Ticket Appears on Top of Open Ticket List ----- " + ticketNumber);
			}
			else{
				extentTest.log(LogStatus.FAIL, "Updated Ticket Doesn't Appears on Top of Open Ticket List, Instead Some Different Ticket is Displayed on Top ---- " + latestTicketNumber);
				if(latestTicketNumber.equals(oldTicketNumber)){
					extentTest.log(LogStatus.INFO, "Its Still Shows the old Ticket on Top ---- " + latestTicketNumber);
				}
				else{
					extentTest.log(LogStatus.INFO, "Even Old Ticket is Not Dispalyed ,Instead Some Difefrent Ticket is Displayed --- " + latestTicketNumber);
				}
			}
			s.assertTrue(latestTicketNumber.contains(ticketNumber));
			s.assertAll();
		}catch(Exception e){
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, "Ticket is Present For user But Not Displayed"); 
		}
	}
}