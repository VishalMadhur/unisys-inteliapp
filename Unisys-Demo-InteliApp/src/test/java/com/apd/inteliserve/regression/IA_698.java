package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.SplashScreenPage;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.MobileElement;

/**
 * @author  VishalMadhur
 *
 */

public class IA_698 extends BaseTest{
	@Test
	@Parameters({"platform"})
	public void splashScreenTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-698, Splash Screen Test-iOS");
		}else {
			extentTest = extent.startTest("IA-698, Splash Screen Test-Android");
		}
		extentTest.log(LogStatus.PASS, "Test Started");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
	//	Thread.sleep(2000);
		try {
//			logOut(platform);
//			login(platform);
			SplashScreenPage sp = new SplashScreenPage(driver);
		//	if(platform.equalsIgnoreCase("ios")){
			//	sp.clickDismiss();
		//	}
			List<MobileElement> logo= sp.getlogoImgCount();
			s.assertTrue(sp.verifyLoadingIcon().isDisplayed());
			extentTest.log(LogStatus.PASS, "Loading Icon is Displayed");
//			s.assertTrue(sp.verifyPoweredByText().isDisplayed(),"Powered By Text Asserations Failed");
//			extentTest.log(LogStatus.PASS, "Powered By Text is Displayed");
//			s.assertTrue(sp.verifyCopyRightText().isDisplayed(),"CopyRight Asserations Failed");
//			extentTest.log(LogStatus.PASS, "CopyRight Text is Displayed");
			int expectedImageCount=3;
			int actualImageCount=logo.size();
			Reporter.log("Total Count of Logo Image is " +actualImageCount,true);
			if(expectedImageCount==actualImageCount) {
				extentTest.log(LogStatus.PASS, "ALL Two Logos - InteliApp And Unisys Are Dispalyed");
			}
			else {
				extentTest.log(LogStatus.FAIL, "ALL Two Logos - InteliApp And Unisys Are Not Dispalyed");
			}
			s.assertEquals(actualImageCount, expectedImageCount);
			s.assertAll();
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("splash Screen Test Failed");
		}
	}
}