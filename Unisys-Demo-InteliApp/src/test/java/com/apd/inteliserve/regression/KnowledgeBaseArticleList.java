package com.apd.inteliserve.regression;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.general.Lib;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.apd.inteliserve.pompages.KnowledgeBasePage;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class KnowledgeBaseArticleList extends BaseTest {
	@SuppressWarnings("rawtypes")
	@Test
	@Parameters({"platform","language","transalationSheet","device"})
	public void KnowledgeBaseArticleListTest(@Optional String platform,@Optional String language,@Optional String transalationSheet,@Optional String device) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2262, Knowledge Base Articles List Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-2244, Knowledge Base Articles List Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			clickOnElement("Knowledge Base Button", clickElement(fm.clickKnowledgeBaseButton()));
			String expectedKnowledgeBaseHeaderLabel =getCellValue(transalationSheet, 23, 1);
			String expectedSearchKeywordLabel =getCellValue(transalationSheet, 24, 1);
			String expectedCanNotFindAswerText =getCellValue(transalationSheet, 28, 1);
			String expectedChatWithAmeliaLabel =getCellValue(transalationSheet, 30, 1);
			String expectedWriteToUsLabel=getCellValue(transalationSheet, 29, 1);
			String expectedCancelLabel=getCellValue(transalationSheet, 31, 1);
			String expectedArticlePageTitle=getCellValue("ExpectedText", 10, 1);

			KnowledgeBasePage kb = new KnowledgeBasePage(driver);


			try {
				List<MobileElement> categories=kb.getcategoriesCount();
				int actualCategoriesCount=categories.size();
				if(platform.equalsIgnoreCase("ios")){
					String categoriesCountIos=getPropertyValue("DefaultCategoriesCountIos");
					int expectedCategoriesCountIos = Integer.parseInt(categoriesCountIos);
					if(expectedCategoriesCountIos==actualCategoriesCount) {
						extentTest.log(LogStatus.PASS, "Only Five Categories Are Displayed By Default");
						extentTest.log(LogStatus.INFO,"Categories Displayed Are --------");
						for(MobileElement element : categories) {
							String getCategories= getText(element, platform);
							if(!getCategories.equals("category_item forward_icon")) {
								extentTest.log(LogStatus.INFO,getCategories);
							}
						}
					}
					else {
						extentTest.log(LogStatus.FAIL, "Only Five Categories Are Not Displayed By Default");
					}
					s.assertEquals(actualCategoriesCount, expectedCategoriesCountIos);

				}else {
					String categoriesCountAndroid=getPropertyValue("DefaultCategoriesCountAndroid");
					int expectedCategoriesCountAndroid = Integer.parseInt(categoriesCountAndroid);
					if(expectedCategoriesCountAndroid==actualCategoriesCount) {
						extentTest.log(LogStatus.PASS, "Only Five Categories Are Displayed By Default");
						extentTest.log(LogStatus.INFO,"Categories Displayed Are --------");
						for(MobileElement element : categories) {
							String getCategories= getText(element, platform);
							extentTest.log(LogStatus.INFO,getCategories);
						}
					}
					else {
						extentTest.log(LogStatus.FAIL, "Only Five Categories Are Not Displayed By Default");
					}
					s.assertEquals(actualCategoriesCount, expectedCategoriesCountAndroid);

				}
			}catch(Exception e) {
				e.printStackTrace();
				String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
				extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			}
			clickOnElement("Knowledge Base First Category", clickElement(kb.clickFirstCategory()));
			String actualKnowledgeBaseHeaderLabel=getText(kb.verifyKnowledgeHeaderText(),platform);
			String actualSearchKeywordLabel=getText(kb.verifyArticlePageSearchTextBox(),platform);
			String actualArticleTitle=getText(kb.verifyArticlePageTitle(),platform);
			String actualArticlePageTitle=actualArticleTitle.toUpperCase();
			System.out.println(actualArticlePageTitle);
			verifyLabel("Knowledge Base Header", matchData(expectedKnowledgeBaseHeaderLabel,actualKnowledgeBaseHeaderLabel));
			verifyElement("Contextual Menu",elementDisplayed(kb.verifyContextualMenu())) ;
			verifyElement("Search Text Box",elementDisplayed(kb.verifyArticlePageSearchTextBox())) ;
			verifyLabel("Search Any Keyword", containsData(actualSearchKeywordLabel, expectedSearchKeywordLabel));
			verifyLabel("Article Page Title", matchData(expectedArticlePageTitle,actualArticlePageTitle));
			verifyElement("Forward Icon Along With Article Name", elementDisplayed(kb.verifyForwardIcon()));
			verifyElement("Back Icon", elementDisplayed(kb.clickBackIcon()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			verifyElement("Knowledge Base Landing Screen", elementDisplayed(kb.clickFirstCategory()));
			clickOnElement("Knowledge Base First Category", clickElement(kb.clickFirstCategory()));
			clickOnElement("Search Text Box", clickElement(kb.verifyArticlePageSearchTextBox()));
			kb.verifyArticlePageSearchTextBox().sendKeys("VPN\n");
			if(platform.equalsIgnoreCase("android")){
				//			try {
				//				kb.verifyArticlePageSearchTextBox().sendKeys("VPN");
				//			}catch(Exception e) {
				//
				//			}
				((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
			}
			List<MobileElement> searchItems=kb.getsearchItemsCount();
			List<MobileElement> searchTexts=kb.getsearchTextsCount();
			int actualSearchItemsCount=searchItems.size();
			int actualSearchTextsCount=searchTexts.size();
			if(platform.equalsIgnoreCase("ios")){
				if(actualSearchTextsCount==actualSearchItemsCount/2) {
					extentTest.log(LogStatus.PASS, "VPN Releated Search Items Are Shown Correctly");
					extentTest.log(LogStatus.INFO,"Articles Displayed Are --------");
					for(MobileElement element : searchTexts) {
						String getSerachedArticle= getText(element, platform);
						extentTest.log(LogStatus.INFO,getSerachedArticle);
					}
				}
				else {
					extentTest.log(LogStatus.FAIL, "VPN Releated Search Items Are Not Shown Correctly");
				}

			}else {
				if(actualSearchTextsCount==actualSearchItemsCount) {
					extentTest.log(LogStatus.PASS, "VPN Releated Search Items Are Shown Correctly");
					extentTest.log(LogStatus.INFO,"Articles Displayed Are --------");
					for(MobileElement element : searchTexts) {
						String getSerachedArticle= getText(element, platform);
						extentTest.log(LogStatus.INFO,getSerachedArticle);
					}
				}
				else {
					extentTest.log(LogStatus.FAIL, "VPN Releated Search Items Are Not Shown Correctly");
				}
			}
			Thread.sleep(5000);
			clickOnElement("Cancel Button", clickElement(kb.clickCancelButton()));
			clickOnElement("Contextual Menu", clickElement(kb.verifyContextualMenu()));
			String actualCanNotFindAswerLabel=getText(kb.verifyCanNotFindAnswerText(), platform);
			String actualChatWithAmeliaLabel=getText(kb.verifyChatWithAmeliaText(), platform);;
			String actualWriteToUsLabel=getText(kb.verifyWriteToUsText(), platform);;
			verifyLabel("Can't Find Answer Text", matchData(expectedCanNotFindAswerText,actualCanNotFindAswerLabel));
			verifyLabel("Chat With Amelia Text", matchData(expectedChatWithAmeliaLabel,actualChatWithAmeliaLabel));
			verifyLabel("Write To Us Text", matchData(expectedWriteToUsLabel,actualWriteToUsLabel));
			if(!device.equalsIgnoreCase("iPad")){
				String actualCancelLabel=getText(kb.verifyCancelText(), platform);
				verifyLabel("Cancel Text", matchData(expectedCancelLabel,actualCancelLabel));
				clickOnElement("Cancel Text on Contextual Menu", clickElement(kb.verifyCancelText()));
				verifyElement("Article List Page", elementDisplayed(kb.clickFirstArticle()));

			}
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Knowledge Base Articles List  Test Failed");
		}
	}	
}
