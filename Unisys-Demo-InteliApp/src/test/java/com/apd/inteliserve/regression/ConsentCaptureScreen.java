package com.apd.inteliserve.regression;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.general.Lib;
import com.apd.inteliserve.pompages.ConsentCapturePage;
import com.apd.inteliserve.pompages.LandingPage;
import com.apd.inteliserve.pompages.LoginPage;
import com.relevantcodes.extentreports.LogStatus;

public class ConsentCaptureScreen extends BaseTest {
	@Test
	@Parameters({"platform","language","transalationSheet"})
	public void consentCaptureScreenTest(@Optional String platform,@Optional String language,@Optional String transalationSheet) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2973, Consent Capture Screen Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-2386, Consent Capture Screen Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");

			String expectedPolicyAndNoticesHeaderLabel =getCellValue(transalationSheet, 118, 1);
			String expectedPolicyHelplLabel =getCellValue(transalationSheet, 119,1);
			String expectedPrivacyPloicyLabel =getCellValue(transalationSheet, 120, 1);
			String expectedTermsOfUseLabel=getCellValue(transalationSheet, 121, 1);
			String expectedEULALabel=getCellValue(transalationSheet, 122, 1);
			String expectedPolicyConfirmLabel=getCellValue(transalationSheet, 123, 1);
			String expectedConfirmAndProceedLabel=getCellValue(transalationSheet, 124, 1);
			String expectedNoThanksLabel=getCellValue(transalationSheet, 125, 1);
			String expectedPrivacyPolicyHeaderLabel=getCellValue(transalationSheet, 126, 1);
			String expectedTermsOfUseHeaderLabel=getCellValue(transalationSheet, 130, 1);
			String expectedEulaHeaderLabel=getCellValue(transalationSheet, 131, 1);
			String expectedAgreeLabel=getCellValue(transalationSheet, 127, 1);
			String expectedDisAgreeLabel=getCellValue(transalationSheet, 128, 1);

			logOut(platform);
			LoginPage lp = new LoginPage(driver);
			String email =getCellValue("TestData", 43, 1)+new Random().nextInt(100000)+getCellValue("TestData", 44, 1);
			extentTest.log(LogStatus.INFO, "Logging With New Email----- " + email);
			lp.setEmail(email);
			extentTest.log(LogStatus.PASS, "New  Email Entered");
			lp.clickNextButton();
			String password =Lib.getPropertyValue("Password");
			lp.setPassword(password);
			Thread.sleep(2000);
			lp.clickNextButton();
			extentTest.log(LogStatus.PASS, "Login Button is Clicked");

			ConsentCapturePage cc = new ConsentCapturePage(driver);

			String actualPolicyAndNoticesHeaderLabel=getText(cc.verifyPolicyAndNoticesHeader(),platform);
			String actualPolicyHelplLabel=getText(cc.verifyPolicyHelpText(),platform);
			String actualPrivacyPloicyLabel=getText(cc.verifyPolicyLink(),platform);
			String actualTermsOfUseLabele=getText(cc.verifyTermsOfUseLink(),platform);
			String actualEULALabel=getText(cc.verifyEndUserLiscenceLink(),platform);
			String actualPolicyConfirmLabel=getText(cc.verifyPolicyConfimText(),platform);
			String actualConfirmAndProceedLabel=getText(cc.verifyConfirmAndProceedText(),platform);
			String actualNoThanksLabel=getText(cc.verifyNoThanksButton(),platform);
			String actualAgreeLabel="";
			String actualDisAgreeLabel="";

			verifyLabel("Policy And Notices Header", matchData(expectedPolicyAndNoticesHeaderLabel,actualPolicyAndNoticesHeaderLabel));
			verifyLabel("Policy Help Text", matchData(expectedPolicyHelplLabel,actualPolicyHelplLabel));
			verifyLabel("Privacy Policy Link", matchData(expectedPrivacyPloicyLabel,actualPrivacyPloicyLabel));
			verifyLabel("Terms Of Use Link", matchData(expectedTermsOfUseLabel,actualTermsOfUseLabele));
			verifyLabel("EULA Link", matchData(expectedEULALabel,actualEULALabel));
			verifyLabel("Policy Confirm Text", matchData(expectedPolicyConfirmLabel,actualPolicyConfirmLabel));
			verifyLabel("Confirm And Proceed Button", matchData(expectedConfirmAndProceedLabel,actualConfirmAndProceedLabel));
			verifyLabel("No Thanks Button", matchData(expectedNoThanksLabel,actualNoThanksLabel));
			verifyElement("Disabled Confirm And Proceed Button", elementDisplayed(cc.verifyDisableConfirmButton()));
			clickOnElement("Privacy Policy Link", clickElement(cc.verifyPolicyLink()));
			String actualPrivacyPolicyHeaderLabel=getText(cc.verifyPolicyHeader(),platform);
			Thread.sleep(3000);
			verifyLabel("Privacy Policy Header", matchData(expectedPrivacyPolicyHeaderLabel,actualPrivacyPolicyHeaderLabel));
			waitForVisibityOfSpecificElement("Enable Agree Button", cc.verifyEnableAgreeButton());
			actualAgreeLabel=getText(cc.verifyAgreeText(),platform);
			actualNoThanksLabel=getText(cc.verifyNoThanksButton(),platform);
			verifyLabel("Yes,I Agree", matchData(expectedAgreeLabel,actualAgreeLabel));
			verifyLabel("No Thanks Button", matchData(expectedNoThanksLabel,actualNoThanksLabel));
			clickOnElement("Agree Button", clickElement(cc.verifyEnableAgreeButton()));
			verifyElement("Tick Icon With Privacy Policy Link", elementDisplayed(cc.verifyPrivacyPolicyTickItem()));
			verifyElement("Disabled Confirm And Proceed Button", elementDisplayed(cc.verifyDisableConfirmButton()));
			clickOnElement("No Thanks Button", clickElement(cc.verifyNoThanksButton()));

			loginWithSameEmailId(email);

			verifyElement("Tick Icon With Privacy Policy Link", elementDisplayed(cc.verifyPrivacyPolicyTickItem()));
			verifyElement("Disabled Confirm And Proceed Button", elementDisplayed(cc.verifyDisableConfirmButton()));
			clickOnElement("Privacy Policy Link", clickElement(cc.verifyPolicyLink()));
			Thread.sleep(15000);
		//	waitForVisibityOfSpecificElement("No Thanks Button", cc.verifyNoThanksButton());
			clickOnElement("No Thanks", clickElement(cc.verifyNoThanksButton()));
			verifyElement("Tick Icon With Privacy Policy Link", elementDisplayed(cc.verifyPrivacyPolicyTickItem()));
			clickOnElement("Privacy Policy Link", clickElement(cc.verifyPolicyLink()));
			actualDisAgreeLabel=getText(cc.verifyDisagreeText(),platform);
			verifyLabel("Yes,I DisAgree", matchData(expectedDisAgreeLabel,actualDisAgreeLabel));
			waitForVisibityOfSpecificElement("Enable DisAgree Button", cc.verifyEnableDisAgreeButton());
			clickOnElement("DisAgree Button", clickElement(cc.verifyEnableDisAgreeButton()));
			verifyElementNotDisplayed("Tick Icon With Privacy Policy Link", elementDisplayed(cc.verifyPrivacyPolicyTickItem()));
			verifyElement("Disabled Confirm And Proceed Button", elementDisplayed(cc.verifyDisableConfirmButton()));

			clickOnElement("Terms Of Use Link", clickElement(cc.verifyTermsOfUseLink()));
			String actualTermsOfUseHeaderLabel=getText(cc.verifyPolicyHeader(),platform);
			actualAgreeLabel=getText(cc.verifyAgreeText(),platform);
			actualNoThanksLabel=getText(cc.verifyNoThanksButton(),platform);
			verifyLabel("Terms Of Use Header", matchData(expectedTermsOfUseHeaderLabel,actualTermsOfUseHeaderLabel));
			verifyLabel("Yes,I Agree", matchData(expectedAgreeLabel,actualAgreeLabel));
			verifyLabel("No Thanks Button", matchData(expectedNoThanksLabel,actualNoThanksLabel));
			waitForVisibityOfSpecificElement("Enable Agree Button", cc.verifyEnableAgreeButton());
			clickOnElement("Agree Button", clickElement(cc.verifyEnableAgreeButton()));
			verifyElement("Tick Icon With Terms Of Use Link", elementDisplayed(cc.verifyTermsOfUseTickItem()));
			verifyElement("Disabled Confirm And Proceed Button", elementDisplayed(cc.verifyDisableConfirmButton()));
			clickOnElement("No Thanks Button", clickElement(cc.verifyNoThanksButton()));

			loginWithSameEmailId(email);

			verifyElement("Tick Icon With Terms Of Use Link", elementDisplayed(cc.verifyTermsOfUseTickItem()));
			verifyElement("Disabled Confirm And Proceed Button", elementDisplayed(cc.verifyDisableConfirmButton()));
			clickOnElement("Terms Of Use Link", clickElement(cc.verifyTermsOfUseLink()));
			actualDisAgreeLabel=getText(cc.verifyDisagreeText(),platform);
			verifyLabel("Yes,I DisAgree", matchData(expectedDisAgreeLabel,actualDisAgreeLabel));
			waitForVisibityOfSpecificElement("Enable Agree Button", cc.verifyEnableDisAgreeButton());
			clickOnElement("DisAgree Button", clickElement(cc.verifyEnableDisAgreeButton()));
			verifyElementNotDisplayed("Tick Icon With Terms Of Use Link", elementDisplayed(cc.verifyTermsOfUseTickItem()));
			verifyElement("Disabled Confirm And Proceed Button", elementDisplayed(cc.verifyDisableConfirmButton()));

			clickOnElement("EULA Link", clickElement(cc.verifyEndUserLiscenceLink()));
			waitForVisibityOfSpecificElement("Enable Agree Button", cc.verifyEnableAgreeButton());
			String actualEulaeHeaderLabel=getText(cc.verifyPolicyHeader(),platform);
			actualAgreeLabel=getText(cc.verifyAgreeText(),platform);
			actualNoThanksLabel=getText(cc.verifyNoThanksButton(),platform);
			verifyLabel("Eula Header", matchData(expectedEulaHeaderLabel,actualEulaeHeaderLabel));
			verifyLabel("Yes,I Agree", matchData(expectedAgreeLabel,actualAgreeLabel));
			verifyLabel("No Thanks Button", matchData(expectedNoThanksLabel,actualNoThanksLabel));
			waitForVisibityOfSpecificElement("Enable Agree Button", cc.verifyEnableAgreeButton());
			clickOnElement("Agree Button", clickElement(cc.verifyEnableAgreeButton()));
			verifyElement("Tick Icon With EULA  Link", elementDisplayed(cc.verifyEulaTickItem()));
			verifyElement("Disabled Confirm And Proceed Button", elementDisplayed(cc.verifyDisableConfirmButton()));
			clickOnElement("No Thanks Button", clickElement(cc.verifyNoThanksButton()));

			loginWithSameEmailId(email);

			verifyElement("Tick Icon With Eula  Link", elementDisplayed(cc.verifyEulaTickItem()));
			verifyElement("Disabled Confirm And Proceed Button", elementDisplayed(cc.verifyDisableConfirmButton()));
			clickOnElement("EULA Link", clickElement(cc.verifyEndUserLiscenceLink()));
			actualDisAgreeLabel=getText(cc.verifyDisagreeText(),platform);
			verifyLabel("Yes,I DisAgree", matchData(expectedDisAgreeLabel,actualDisAgreeLabel));
			waitForVisibityOfSpecificElement("Enable Agree Button", cc.verifyEnableDisAgreeButton());
			clickOnElement("DisAgree Button", clickElement(cc.verifyEnableDisAgreeButton()));
			verifyElementNotDisplayed("Tick Icon With EULA Link", elementDisplayed(cc.verifyEulaTickItem()));
			verifyElement("Disabled Confirm And Proceed Button", elementDisplayed(cc.verifyDisableConfirmButton()));

			clickOnElement("Privacy Policy Link", clickElement(cc.verifyPolicyLink()));
			waitForVisibityOfSpecificElement("Enable Agree Button", cc.verifyEnableAgreeButton());
			clickOnElement("Agree Button", clickElement(cc.verifyEnableAgreeButton()));
			clickOnElement("Terms Of Use Link", clickElement(cc.verifyTermsOfUseLink()));
			waitForVisibityOfSpecificElement("Enable Agree Button", cc.verifyEnableAgreeButton());
			clickOnElement("Agree Button", clickElement(cc.verifyEnableAgreeButton()));
			clickOnElement("EULA Link", clickElement(cc.verifyEndUserLiscenceLink()));
			waitForVisibityOfSpecificElement("Enable Agree Button", cc.verifyEnableAgreeButton());
			clickOnElement("Agree Button", clickElement(cc.verifyEnableAgreeButton()));
			verifyElement("Tick Icon With Privacy Policy Link", elementDisplayed(cc.verifyPrivacyPolicyTickItem()));
			verifyElement("Tick Icon With Terms Of Use Link", elementDisplayed(cc.verifyTermsOfUseTickItem()));
			verifyElement("Tick Icon With EULA  Link", elementDisplayed(cc.verifyEulaTickItem()));
			verifyElement("Enabled Confirm And Proceed Button", elementDisplayed(cc.verifyEnableConfirmButton()));
			clickOnElement("Enabled Confirm And Proceed Button", clickElement(cc.verifyEnableConfirmButton()));
			LandingPage ld = new LandingPage(driver);
			waitForVisibityOfSpecificElement("Landing Page", ld.profileIcon());
			logOut(platform);
			extentTest.log(LogStatus.INFO, "Logging to Inteliapp After Confirming All Consent ");
			loginWithSameEmailId(email);
			waitForVisibityOfSpecificElement("Landing Page", ld.profileIcon());
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Consent Capture Screen  Test Failed");
		}
	}	



	public void loginWithSameEmailId(String email) {
		try {
			LoginPage lp = new LoginPage(driver);
			extentTest.log(LogStatus.INFO, "Logging With Same Email----- " + email);
			lp.setEmail(email);
			extentTest.log(LogStatus.PASS, "Same  Email Entered");
			lp.clickNextButton();
			String password =Lib.getPropertyValue("Password");
			lp.setPassword(password);
			Thread.sleep(2000);
			lp.clickNextButton();
			extentTest.log(LogStatus.PASS, "Login Button is Clicked");
		}catch(Exception e) {
			e.printStackTrace();
			Reporter.log("Failed to Login With Same Email id ---" + email);

		}

	}
}
