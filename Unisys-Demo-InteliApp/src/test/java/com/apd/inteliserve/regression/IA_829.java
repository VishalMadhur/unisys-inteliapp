package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.LandingPage;
import com.apd.inteliserve.pompages.OpenTicketListPage;
import com.relevantcodes.extentreports.LogStatus;


/**
 * @author  VishalMadhur
 *
 */

public class IA_829 extends BaseTest{
	@Test
	@Parameters({"platform"})
	public void openTicketListTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		String oldTicketNumber=null;
		String newTicketNumber=null;
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-829, Open Ticket List Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-829, Open Ticket Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			OpenTicketListPage ot = new OpenTicketListPage(driver);
			if(elementDisplayed(ot.verifyNoTicketText())){
				extentTest.log(LogStatus.FAIL, "Ticket is Present For user But Not Displayed"); 
				extentTest.log(LogStatus.INFO, "Instead of Ticket Got Message --- " + getText(ot.verifyNoTicketText()));
			}
			else if (elementDisplayed(ot.verifyTicketErrorMessage())) {
				extentTest.log(LogStatus.PASS, "Ticket is Present For user But Not Displayed Due to API or Internet Issue");
				extentTest.log(LogStatus.INFO, "Instead of Ticket Got Message --- " + getText(ot.verifyTicketErrorMessage()));
			}else{
				extentTest.log(LogStatus.PASS, "Ticket is Present For User");
				extentTest.log(LogStatus.INFO, "Checking If Ticket is Displyed in Open Ticket List");
				if(elementIsDisplayed(ot.verifyTicketNumber())){
					extentTest.log(LogStatus.PASS, "Ticket Number is Displayed");
					oldTicketNumber=getText(ot.verifyTicketNumber());
					if(platform.equalsIgnoreCase("android")){
						extentTest.log(LogStatus.PASS, "Got  Ticket Number Before Scrolling --- " +oldTicketNumber);
					}
					else {
						extentTest.log(LogStatus.PASS, "Got  Ticket Number Before Scrolling");
					}
				}
				else{
					extentTest.log(LogStatus.FAIL, "Ticket Number is Not Displayed");
				}

				if(elementIsDisplayed(ot.verifyPriorityText())) {
					extentTest.log(LogStatus.PASS, "Priority  is Displayed");
					String priorityText=getText(ot.verifyPriorityText());
					Reporter.log(priorityText,true);
					if(platform.equalsIgnoreCase("android")){
						extentTest.log(LogStatus.INFO, "Priority Displayed is : " + priorityText);
					}
				}
				else{
					extentTest.log(LogStatus.FAIL, "Priority is  Not Displayed");
				}
				if(elementIsDisplayed(ot.verifyStatusText())) {
					extentTest.log(LogStatus.PASS, "Status  is Displayed");
					String statusText=getText(ot.verifyStatusText());
					Reporter.log(statusText,true);
					if(platform.equalsIgnoreCase("android")){
						extentTest.log(LogStatus.INFO, "Status Displayed is : " + statusText);
					}
				}
				else{
					extentTest.log(LogStatus.FAIL, "Status  is Not Displayed");
				}
				if(elementIsDisplayed(ot.verifyTicketDate())){
					extentTest.log(LogStatus.PASS, "Ticket Date  is Displayed");
					String ticketDate=getText(ot.verifyTicketDate());
					if(platform.equalsIgnoreCase("android")){
						extentTest.log(LogStatus.INFO, "Ticket Date Displayed is : " + ticketDate);
					}
				}
				else{
					extentTest.log(LogStatus.FAIL, "Ticket Date  is Not Displayed");
				}
				swipe(ot.upSwipeBar(), ot.swipeTillHelloText());
				String scrollToTicket = getCellValue("TestData", 11, 1);
				//	if(platform.equalsIgnoreCase("ios")){
				//scroll(platform,"INC0395837");
				//}
				//	if(platform.equalsIgnoreCase("android")){
				scroll(platform,scrollToTicket);
				//Thread.sleep(3000);
				clickElement(ot.randomTicket());
				if(elementIsDisplayed(ot.verifyTicketHeaderText())){
					extentTest.log(LogStatus.PASS, "Clicked on Random Ticket Successfully After Scrollin");
				}
				else{
					extentTest.log(LogStatus.FAIL, "Fail to Clicked on Any Random Ticket After Scrolling");
				}
				if(elementIsDisplayed(ot.clickBackButton())){
					clickElement(ot.clickBackButton());
					extentTest.log(LogStatus.PASS, "Clicked on Back Button" );
				}
				else{
					extentTest.log(LogStatus.FAIL, "Failed to Click on Back Button");
				}
			}
			LandingPage lp = new LandingPage(driver);
			clickElement(lp.profileIcon());
			clickElement(lp.homeIcon());
			if(elementIsDisplayed(lp.verifyKnowledgeBaseButton())){ 
				extentTest.log(LogStatus.PASS, "Swipe Bar is Closed Properly");
			}
			extentTest.log(LogStatus.INFO, "Checking If Ticket is Displayed After Closing Swipe Bar");
			Thread.sleep(3000);
			if(elementIsDisplayed(ot.verifyTicketNumber())){
				extentTest.log(LogStatus.PASS, "Ticket Number is Displayed");
				Thread.sleep(3000);
				if(platform.equalsIgnoreCase("android")){
					newTicketNumber=getText(ot.verifyTicketNumber());
					extentTest.log(LogStatus.PASS, "Got  Ticket Number After Scrolling --- " +newTicketNumber);
					if(oldTicketNumber.equals(newTicketNumber)){
						extentTest.log(LogStatus.PASS, "Old And New Ticket Number is Same After Scrolling");
					}
					else{
						extentTest.log(LogStatus.FAIL, "Old And New Ticket Number is Not Same After Scrolling");
					}
					s.assertEquals(oldTicketNumber, newTicketNumber);
				}
			}
			else{
				extentTest.log(LogStatus.FAIL, "Ticket Number is Not Displayed");
				s.assertAll();
			}
			}catch(Exception e) {
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL, e.getMessage());
				Assert.fail("Open Ticket List Test Failed");
			}
		}
	}

