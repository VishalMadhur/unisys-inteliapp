package com.apd.inteliserve.regression;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.general.Lib;
import com.apd.inteliserve.pompages.EditProfilePage;
import com.apd.inteliserve.pompages.LandingPage;
import com.apd.inteliserve.pompages.LoginPage;
import com.relevantcodes.extentreports.LogStatus;

public class EditProfileScreen extends BaseTest {
	@Test
	@Parameters({"platform","language","transalationSheet"})
	public void editProfileScreenTest(@Optional String platform,@Optional String language,@Optional String transalationSheet) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2953, Edit Profile Screen Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-2942, Edit Profile Screen Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");

			String expectedEditProfileHeaderLabel =getCellValue(transalationSheet, 97, 1);
			String expectedCancelLabel =getCellValue(transalationSheet, 102, 1);
			String expectedSaveLabel =getCellValue(transalationSheet, 109, 1);
			String expectedProfileName=getCellValue("TestData", 40, 1);
			String expectedEmailLabel=getCellValue(transalationSheet, 110, 1);
			String expectedProfileEmail=getCellValue("TestData", 41, 1);
			String expectedTitleLabel=getCellValue(transalationSheet, 113, 1);
			String expectedLocationLabel=getCellValue(transalationSheet, 111, 1);
			String expectedPhoneTitleLabel=getCellValue(transalationSheet, 98, 1);
			//			String expectedPhoneSubTitleLabel=getCellValue(transalationSheet, 99, 1);
			String expectedphoneNumberInfoLabel=getCellValue(transalationSheet, 108, 1);
			String expectedLanguageLabel=getCellValue(transalationSheet, 112, 1);
			String expectedUnableToEditLael=getCellValue(transalationSheet, 114, 1);
			String expectedServiceAccountNeededlabel=getCellValue(transalationSheet, 115, 1);
			String expectedOkButtonLabel=getCellValue(transalationSheet, 105, 1);

			LandingPage ld = new LandingPage(driver);
			EditProfilePage ep = new EditProfilePage(driver);

			clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
			clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
			String actualEditProfileHeaderLabel=getText(ep.verifyEditProfileHeader(),platform);
			String actualCancelLabel=getText(ep.verifyCancelButton(),platform);
			String actualSaveLabel=getText(ep.verifyDisableSaveButton(),platform);
			String actualProfileName=getText(ep.verifyProfileName(),platform);
			String actualEmailLabel=getText(ep.verifyEmailLabel(),platform);
			String actualProfileEmail=getText(ep.verifyProfileEmail(),platform);
			String actualTitleLabel=getText(ep.verifyTitleLabel(),platform);
			String actualProfileTitle=getText(ep.verifyProfileTitle(),platform);
			String actualLocationLabel=getText(ep.verifyLocationLabel(),platform);
			String actualProfileLocation=getText(ep.verifyProfileLocation(),platform);
			String actualPhoneTitleLabel=getText(ep.verifyPhoneTitle(),platform);
			String actualPhoneNumberInfoLabel=getText(ep.verifyPhoneNumberInfo(),platform);
			String actualProfilePhone=getText(ep.verifyPhoneSubTitle(),platform);
			String actualLanguageLabel=getText(ep.verifyLangauageLabel(),platform);
			String actualProfileLanguage=getText(ep.verifyProfileLanguage(),platform);
			if(platform.equalsIgnoreCase("ios")){
				String actualTitle[]=actualTitleLabel.split(" ", 2);
				actualTitleLabel=actualTitle[0];
				String actualLocation[]=actualLocationLabel.split(" ", 2);
				actualLocationLabel=actualLocation[0];
				String actualLanguage[]=actualLanguageLabel.split(" ", 2);
				actualLanguageLabel=actualLanguage[0];
			}
//			System.out.println(actualEditProfileHeaderLabel);
//			System.out.println(actualCancelLabel);
//			System.out.println(actualSaveLabel);
//			System.out.println(actualProfileName);
//			System.out.println(actualEmailLabel);
//			System.out.println(actualProfileEmail);
//			System.out.println(actualTitleLabel);
//			System.out.println(actualProfileTitle);
//			System.out.println(actualLocationLabel);
//			System.out.println(actualProfileLocation);
//			System.out.println(actualPhoneTitleLabel);
//			System.out.println(actualPhoneNumberInfoLabel);
//			System.out.println(actualProfilePhone);
//			System.out.println(actualLanguageLabel);
//			System.out.println(actualProfileLanguage);

			verifyLabel("Edit Profile Header", matchData(expectedEditProfileHeaderLabel,actualEditProfileHeaderLabel));
			verifyLabel("Cancel Button", matchData(expectedCancelLabel,actualCancelLabel));
			verifyLabel("Save Button", matchData(expectedSaveLabel,actualSaveLabel));
			verifyData(actualProfileName, "Profile Name", matchData(expectedProfileName, actualProfileName));
			verifyElement("Camera Icon", elementDisplayed(ep.verifyCameraIcon()));
			verifyLabel("Email Label", matchData(expectedEmailLabel,actualEmailLabel));
			verifyData(actualProfileEmail, "Profile Email", matchData(expectedProfileEmail, actualProfileEmail));
			verifyLabel("Title", matchData(expectedTitleLabel,actualTitleLabel));
			verifyElement("Title Forward Icon", elementDisplayed(ep.verifyTitleForwardIcon()));
			try {
				verifyElement("Profile Title", elementDisplayed(ep.verifyProfileTitle()));
				extentTest.log(LogStatus.INFO, "Profile Title Displayed is  -- " + actualProfileTitle);
			}catch (Exception e) {
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL, actualProfileTitle +"cis Displayed Instead of Profile Title -- " );
			}
			verifyLabel("Location", matchData(expectedLocationLabel,actualLocationLabel));
			verifyElement("Location Forward Icon", elementDisplayed(ep.verifyLocationForwardIcon()));
			try {
				verifyElement("Profile Location", elementDisplayed(ep.verifyProfileLocation()));
				extentTest.log(LogStatus.INFO, "Profile Location Dispalyed is -- " + actualProfileLocation);
			}catch (Exception e) {
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL, actualProfileLocation +" is Displayed Instead of Profile Location");
			}
			verifyLabel("Phone", matchData(expectedPhoneTitleLabel,actualPhoneTitleLabel));
			verifyLabel("Phone Number Information ", matchData(expectedphoneNumberInfoLabel,actualPhoneNumberInfoLabel));
			try {
				verifyElement("Profile Phone", elementDisplayed(ep.verifyPhoneSubTitle()));
				extentTest.log(LogStatus.INFO, "Profile Phone Displayed is -- " + actualProfilePhone);
			}catch (Exception e) {
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL, actualProfilePhone +" is Displayed Instead of Profile Phone");
			}
			verifyLabel("Language", matchData(expectedLanguageLabel,actualLanguageLabel));
			verifyElement("Language Forward Icon", elementDisplayed(ep.verifyLanguageForwardIcon()));
			try {
				verifyElement("Profile Language", elementDisplayed(ep.verifyProfileLanguage()));
				extentTest.log(LogStatus.INFO, "Profile Language Displayed is -- " + actualProfileLanguage);
			}catch (Exception e) {
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL, actualProfileLanguage +" is Displayed Instead of Profile Language");
			}
			clickOnElement("Cancel Button", clickElement(ep.verifyCancelButton()));
			logOut(platform);
			LoginPage lp = new LoginPage(driver);
			extentTest.log(LogStatus.INFO, "Logging With Email Not Registerd at Unisys Service Now");
			String email =Lib.getPropertyValue("UnregisteredEmail");
			lp.setEmail(email);
			extentTest.log(LogStatus.PASS, "Unregistered  Email Entered");
			lp.clickNextButton();
			String password =Lib.getPropertyValue("Password");
			lp.setPassword(password);
			Thread.sleep(2000);
			lp.clickNextButton();
			extentTest.log(LogStatus.PASS, "Login Button is Clicked");
			waitForVisibityOfSpecificElement("Profile Icon", ld.profileIcon());
			clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
			clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
			verifyElement("Pop up With Service Now Needed to Edit Profile Message",elementDisplayed(ep.verifyServiceAccountNeedText()));
			String actualUnableToEditlabel=getText(ep.verifyUnableToEditText(), platform);
			String actualServiceAccountNeededlabel=getText(ep.verifyServiceAccountNeedText(), platform);
			String actualOkButtonLabel=getText(ep.verifyOkButton(), platform);
			System.out.println(actualUnableToEditlabel);
			System.out.println(actualServiceAccountNeededlabel);
			System.out.println(actualOkButtonLabel);
			verifyLabel("Unable to Edit", matchData(expectedUnableToEditLael,actualUnableToEditlabel));
			verifyLabel("Service Now Needed to Edit Profile Message", matchData(expectedServiceAccountNeededlabel,actualServiceAccountNeededlabel));
			verifyLabel("Ok Button", matchData(expectedOkButtonLabel,actualOkButtonLabel));
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Edit Profile Screen Test Failed");
		}
	}	
}
