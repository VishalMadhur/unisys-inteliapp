package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.LandingPage;
import com.relevantcodes.extentreports.LogStatus;

public class VerifyNineDotLauncherIcon extends BaseTest {

	@Test
	@Parameters({"platform"})
	public void navigationBarTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2484, Nine Dot Launcher Icon Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-2434, Nine Dot Launcher Icon Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			LandingPage ld = new LandingPage(driver);
			verifyElement("Nine Dot Launcher Icon", elementIsDisplayed(ld.launcherIcon()));
			clickOnElement("Launcher Icon", clickElement(ld.launcherIcon()));
			verifyElement("My Places Header", elementIsDisplayed(ld.verifyMyPlacesHeader()));
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Nine Dot Launcher Icon Test Failed");
		}
	}
}
