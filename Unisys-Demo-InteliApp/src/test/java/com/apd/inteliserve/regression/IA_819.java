package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */

public class IA_819 extends BaseTest{
	
	@Test
	@Parameters({"platform"})
	public void companyNews(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-819, Favrouite Module-Company News Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-819, Favrouite Module-Company News Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			click(fm.clickCompanyNewsButton());
			extentTest.log(LogStatus.PASS, "Clicked  on Company News Buton");
			String expectedUrl =getPropertyValue("CompanyNewsUrl");
			if(platform.equalsIgnoreCase("ios")){
				clickElement(fm.getURL());
			}
			String actualUrl= getText(fm.getCurrentURL());
			Reporter.log(actualUrl,true);
			if(actualUrl.contains(expectedUrl)) {
				extentTest.log(LogStatus.PASS, "Navigated to Corerect URL -- " + actualUrl);
			}
			else {
				extentTest.log(LogStatus.FAIL, "Navigated to Wrong URL -- " + actualUrl);
			}
			s.assertTrue(actualUrl.contains(expectedUrl));
			//}
			s.assertAll();
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Favrouite Module- Company News Test Failed");
		}
	}
	
	@Test
	@Parameters({"platform"})
	public void supportCornerTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-819, Favrouite Module-Support Corner Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-819, Favrouite Module-Support Corner Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			click(fm.clickCoreSupportButton());
			extentTest.log(LogStatus.PASS, "Clicked  on Core Support Buton");
			if(elementIsDisplayed(fm.verifyHeaderText())){
				extentTest.log(LogStatus.PASS, "Core Support Header Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Core Support Header Text is Not Displayed");
			}
			if(elementIsDisplayed(fm.verifyItText())){
				extentTest.log(LogStatus.PASS, "It text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "It text is Not Displayed");
			}
			if(elementIsDisplayed(fm.verifyItContentText())){
				extentTest.log(LogStatus.PASS, "IT Context Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "IT Context Text is Not Displayed");
			}
			if(elementIsDisplayed(fm.emailButton())){
				extentTest.log(LogStatus.PASS, "Email Button is Present");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Email Button is Not Present");
			}
			if(elementIsDisplayed(fm.chatButton())){
				extentTest.log(LogStatus.PASS, "Chat Button is Present");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Chat Button is Not Present");
			}
			if(elementIsDisplayed(fm.callButton())){
				extentTest.log(LogStatus.PASS, "Call Button is Present");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Call Button is Not Present");
			}
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Favrouite Module-Support Corner Test Failed");
		}
	}
	
	@Test
	@Parameters({"platform"})
	public void supportCornerEmailTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-819, Support Corner Email Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-819, Support Corner Email Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			click(fm.clickCoreSupportButton());
			extentTest.log(LogStatus.PASS, "Clicked  on Core Support Buton");
			click(fm.emailButton());
			extentTest.log(LogStatus.PASS, "Clicked  on Email Buton");
			String expectedToEmail =getPropertyValue("ToEmail");
			if(platform.equalsIgnoreCase("android")){
				Thread.sleep(3000);
				clickElement(fm.getToEmailText());
				String actualToEmail =getText(fm.getToEmailText());
				Reporter.log(actualToEmail,true);
				if(actualToEmail.contains(expectedToEmail)) {
					extentTest.log(LogStatus.PASS, "To Email is Verified -- " +actualToEmail);
				}
				else {
					extentTest.log(LogStatus.FAIL, "To Email is Not Verified -- " +actualToEmail);
				}
				s.assertTrue(actualToEmail.contains(expectedToEmail));
				//			else {
				//				String actualToEmail =fm.getToEmailText().getText();
				//				Reporter.log(actualToEmail,true);
				//				if(actualToEmail.contains(expectedToEmail)) {
				//					extentTest.log(LogStatus.PASS, "To Email is Verified");
				//				}
				//				else {
				//					extentTest.log(LogStatus.FAIL, "To Email is Not Verified");
				//				}
				//				s.assertEquals(actualToEmail, expectedToEmail);
				//			}
				s.assertAll();
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Support Corner Chat Test Failed");
		}
	}
	
	@Test
	@Parameters({"platform"})
	public void supportCornerChatTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-819, Support Corner Chat Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-819, Support Corner Chat Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			click(fm.clickCoreSupportButton());
			extentTest.log(LogStatus.PASS, "Clicked  on Core Support Buton");
			click(fm.chatButton());
			extentTest.log(LogStatus.PASS, "Clicked  on chat Buton");
			if(platform.equalsIgnoreCase("ios")){
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//XCUIElementTypeOther[@name='toggle menu button']"))).click();
			}
			if(platform.equalsIgnoreCase("android")){
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.View[contains(@text,'menu toggle button.')]"))).click();
			}
			if(elementIsDisplayed(fm.toggleButton())){
				extentTest.log(LogStatus.PASS, "Navigated to IVA Screen");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Failed to Navigate to IVA Screen");
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Support Corner Email Test Failed");
		}
	}
}
