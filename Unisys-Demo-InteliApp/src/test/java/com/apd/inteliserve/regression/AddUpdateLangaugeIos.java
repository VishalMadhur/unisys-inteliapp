package com.apd.inteliserve.regression;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.EditProfilePage;
import com.apd.inteliserve.pompages.KnowledgeBasePage;
import com.apd.inteliserve.pompages.LandingPage;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.MobileElement;

public class AddUpdateLangaugeIos extends BaseTest {
	@Test
	@Parameters({"platform","language","transalationSheet"})
	public void updateProfileLanguage(@Optional String platform,@Optional String language,@Optional String transalationSheet) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2949, Add/Update Profile Language Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-2951, Add/Update Profile Language Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");

			String expectedLanguageHeaderLabel =getCellValue(transalationSheet, 133, 1);
			String expectedAllLanguagesLabel =getCellValue(transalationSheet, 134, 1);
			String expectedFirstLanguage =getCellValue(transalationSheet, 135, 1);
			String expectedSecondLanguage =getCellValue(transalationSheet, 136, 1);
			String expectedThirdLanguage =getCellValue(transalationSheet, 137, 1);
			String expectedFourthLanguage =getCellValue(transalationSheet, 138, 1);
			String expectedFifthLanguage =getCellValue(transalationSheet, 139, 1);
			String expectedSixthLanguage =getCellValue(transalationSheet, 140, 1);
			String expectedSeventhLanguage =getCellValue(transalationSheet,141, 1);
			String expectedEighthLanguage =getCellValue(transalationSheet, 142, 1);
			String expectedNinthLanguage =getCellValue(transalationSheet, 143, 1);
			String expectedTenthLanguage =getCellValue(transalationSheet, 144, 1);
			String expectedEleventhLanguage =getCellValue(transalationSheet, 145, 1);
			String expectedTwelevethLanguage =getCellValue(transalationSheet, 146, 1);
			String expectedFirstBasicLanguage =getCellValue(transalationSheet, 147, 1);
			String expectedSecondBasicLanguage =getCellValue(transalationSheet, 148, 1);
			String expectedThirdBasicLanguage =getCellValue(transalationSheet, 149, 1);
			String expectedFourthBasicLanguage =getCellValue(transalationSheet, 150, 1);
			String expectedFifthBasicLanguage =getCellValue(transalationSheet, 151, 1);
			String expectedSixthBasicLanguage =getCellValue(transalationSheet, 152, 1);
			String expectedSeventhBasicLanguage =getCellValue(transalationSheet, 153, 1);
			String expectedEighthBasicLanguage =" "+getCellValue(transalationSheet, 154, 1);
			String expectedNinthBasicLanguage =getCellValue(transalationSheet, 155, 1);
			String expectedTenthBasicLanguage =getCellValue(transalationSheet, 156, 1);
			String expectedEleventhBasicLanguage =getCellValue(transalationSheet, 157, 1);
			String expectedTwelevethBasicLanguage =getCellValue(transalationSheet, 158, 1);


			LandingPage ld = new LandingPage(driver);
			EditProfilePage ep = new EditProfilePage(driver);
			KnowledgeBasePage kb = new KnowledgeBasePage(driver);

			clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
			String profilePageLanguage=getText(ep.verifyProfilePageLanguage(), platform);
			String actualProfileLanguage[]=profilePageLanguage.split(" ", 2);
			profilePageLanguage=actualProfileLanguage[1];
			System.out.println(profilePageLanguage);
			clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
			clickOnElement("Language", clickElement(ep.verifyProfileLanguage()));
			List<MobileElement> allLanguage=ep.getAllLanguage();
			for(MobileElement element :allLanguage) {
				String getLanguages= getText(element, platform);
				if(getLanguages.contains(expectedFirstLanguage)) {
					clickOnElement("Danish Language", clickElement(element));
				}
			}
			verifyElement("Back Icon", elementDisplayed(kb.clickBackIcon()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			verifyElement("Edit Profile Page", elementDisplayed(ep.verifyEditProfileHeader()));
			try {
				ep.verifyEnableSaveButton().click();
				waitForVisibityOfSpecificElement("Edit Profile Icon", ep.verifyEditProfileIcon());
				clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
			}catch(Exception e) {
				e.printStackTrace();
				String getChangedLangauge=getText(ep.verifyProfileLanguage(), platform);
				String actualLanguage []=getChangedLangauge.split(" ");
				getChangedLangauge=actualLanguage[1];
				System.out.println(getChangedLangauge);
				if(profilePageLanguage.contains(getChangedLangauge)) {
					extentTest.log(LogStatus.PASS, "Save Button is Disabled because Profile Langauge And Selected Language is Same");
				}else {
					extentTest.log(LogStatus.FAIL, "Save Button is Disabled Even though Profile Langauge And Selected Language is Not Same");
				}
				s.assertTrue(profilePageLanguage.contains(getChangedLangauge));
			}
			clickOnElement("Language", clickElement(ep.verifyProfileLanguage()));

			String actualLanguageHeaderLabel =getText(ep.verifyLanguageHeader(),platform);
			String actualAllLanguagesLabel =getText(ep.verifyLanguageSubHeader(),platform);
			String actualFirstLanguage =getText(ep.verifyFirstLanguage(),platform);
			String[] firstLanguage = actualFirstLanguage.split(" ");
			actualFirstLanguage=firstLanguage[0];
			String actualFirstBasicLanguage =firstLanguage[1];
			String actualSecondLanguage =getText(ep.verifySecondLanguage(),platform);
			String[] secondLanguage = actualSecondLanguage.split("\\) ");
			actualSecondLanguage=secondLanguage[0]+")";
			String actualSecondBasicLanguage =secondLanguage[1];
			String actualThirdLanguage =getText(ep.verifyThirdLanguage(),platform);
			String[] thirdLanguage = actualThirdLanguage.split("\\) ");
			actualThirdLanguage=thirdLanguage[0]+")";
			String actualThirdBasicLanguage =thirdLanguage[1];
			String actualFourthLanguage =getText(ep.verifyFourthLanguage(),platform);
			String[] fourthLanguage = actualFourthLanguage.split("\\) ");
			actualFourthLanguage=fourthLanguage[0]+")";
			String actualFourthBasicLanguage =fourthLanguage[1];
			String actualFifthLanguage =getText(ep.verifyFifthLanguage(),platform);
			String[] fifthLanguage = actualFifthLanguage.split("\\) ");
			actualFifthLanguage=fifthLanguage[0]+")";
			String actualFifthBasicLanguage =fifthLanguage[1];
			String actualSixthLanguage =getText(ep.verifySixthLanguage(),platform);
			String[] sixthLanguage = actualSixthLanguage.split("\\) ");
			actualSixthLanguage=sixthLanguage[0]+")";
			String actualSixthBasicLanguage =sixthLanguage[1];
			String actualSeventhLanguage =getText(ep.verifySeventhLanguage(),platform);
			String[] seventhLanguage = actualSeventhLanguage.split("\\) ");
			actualSeventhLanguage=seventhLanguage[0]+")";
			String actualSeventhBasicLanguage =seventhLanguage[1];
			String actualEighthLanguage =getText(ep.verifyEighthLanguage(),platform);;
			String[] eighthLanguage = actualEighthLanguage.split("\\)");
			actualEighthLanguage=eighthLanguage[0]+")";
			System.out.println(actualEighthLanguage);
			String actualEighthBasicLanguage =eighthLanguage[1]+")";
			System.out.println(actualEighthBasicLanguage);
			String actualNinthLanguage =getText(ep.verifyNinthLanguage(),platform);
			String[] ninthLanguage = actualNinthLanguage.split(" ");
			actualNinthLanguage=ninthLanguage[0];
			String actualNinthBasicLanguage =ninthLanguage[1];
			String actualTenthLanguage =getText(ep.verifyTenthLanguage(),platform);
			String[] tenthLanguage = actualTenthLanguage.split("\\) ");
			actualTenthLanguage=tenthLanguage[0]+")";
			String actualTenthBasicLanguage =tenthLanguage[1];
			String actualEleventhLanguage =getText(ep.verifyEleventhLanguage(),platform);
			String[] eleventhLanguage = actualEleventhLanguage.split("\\) ");
			actualEleventhLanguage=eleventhLanguage[0]+")";
			String actualEleventhBasicLanguage =eleventhLanguage[1];


			verifyLabel("Langauge Header", matchData(expectedLanguageHeaderLabel,actualLanguageHeaderLabel));
			verifyLabel("All Languages", matchData(expectedAllLanguagesLabel,actualAllLanguagesLabel));
			verifyLabel("Danish", matchData(expectedFirstBasicLanguage,actualFirstBasicLanguage));
			verifyLabel("Dutch (Netherlands)", matchData(expectedSecondBasicLanguage,actualSecondBasicLanguage));
			verifyLabel("English (US)", matchData(expectedThirdBasicLanguage,actualThirdBasicLanguage));
			verifyLabel("English (UK)", matchData(expectedFourthBasicLanguage,actualFourthBasicLanguage));
			verifyLabel("French (France)", matchData(expectedFifthBasicLanguage,actualFifthBasicLanguage));
			verifyLabel("French (Canada)", matchData(expectedSixthBasicLanguage,actualSixthBasicLanguage));
			verifyLabel("German (Germany)", matchData(expectedSeventhBasicLanguage,actualSeventhBasicLanguage));
			verifyLabel("Italian (Italy)", matchData(expectedEighthBasicLanguage,actualEighthBasicLanguage));
			verifyLabel("Japanese", matchData(expectedNinthBasicLanguage,actualNinthBasicLanguage));
			verifyLabel("Portuguese (Brazil)", matchData(expectedTenthBasicLanguage,actualTenthBasicLanguage));
			verifyLabel("Spanish (Colombia)", matchData(expectedEleventhBasicLanguage,actualEleventhBasicLanguage));
			scroll(platform,"Española (México)");
			String actualTwlevethLanguage =getText(ep.verifyTwelvethLanguage(),platform);
			String[] twelevethLanguage = actualTwlevethLanguage.split("\\) ");
			actualTwlevethLanguage=twelevethLanguage[0]+")";
			String actualTwelevethBasicLanguage =twelevethLanguage[1];
			verifyLabel("Spanish (Mexico)", matchData(expectedTwelevethBasicLanguage,actualTwelevethBasicLanguage));
			verifyLabel("Dansk", matchData(expectedFirstLanguage,actualFirstLanguage));
			verifyLabel("Nederlands (Netherland)", matchData(expectedSecondLanguage,actualSecondLanguage));
			verifyLabel("English (US)", matchData(expectedThirdLanguage,actualThirdLanguage));
			verifyLabel("English (UK)", matchData(expectedFourthLanguage,actualFourthLanguage));
			verifyLabel("Français (France)", matchData(expectedFifthLanguage,actualFifthLanguage));
			verifyLabel("Français (Canada)", matchData(expectedSixthLanguage,actualSixthLanguage));
			verifyLabel("Deutsch (Deutschland)", matchData(expectedSeventhLanguage,actualSeventhLanguage));
			verifyLabel("Italiano (Italia)", matchData(expectedEighthLanguage,actualEighthLanguage));
			verifyLabel("日本語", matchData(expectedNinthLanguage,actualNinthLanguage));
			verifyLabel("Português (Brasil)", matchData(expectedTenthLanguage,actualTenthLanguage));
			verifyLabel("Español (Colombia)", matchData(expectedEleventhLanguage,actualEleventhLanguage));
			verifyLabel("Española (México)", matchData(expectedTwelevethLanguage,actualTwlevethLanguage));

			clickOnElement("First Langauge", clickElement(ep.verifyFirstBasicLanguage()));
			verifyElement("Tick Icon Along with Selected Langauge", elementDisplayed(ep.verifyFirstLangaugeTickItem()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			String actualBasicLanguageSelected ="";
			actualBasicLanguageSelected =getText(ep.verifyProfileLanguage(), platform);
			verifyData(actualBasicLanguageSelected, "Selected BasicLanguage at Edit Profile Page", containsData(actualBasicLanguageSelected,expectedFirstBasicLanguage));
			clickOnElement("Profile Language", clickElement(ep.verifyProfileLanguage()));
			clickOnElement("Second Langauge", clickElement(ep.verifySecondBasicLanguage()));
			verifyElement("Tick Icon Along with Selected Langauge", elementDisplayed(ep.verifySecondLanguageTickItem()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(2000);
			actualBasicLanguageSelected =getText(ep.verifyProfileLanguage(), platform);
			verifyData(actualBasicLanguageSelected, "Selected BasicLanguage at Edit Profile Page", containsData(actualBasicLanguageSelected,expectedSecondBasicLanguage));
			clickOnElement("Profile Language", clickElement(ep.verifyProfileLanguage()));
			clickOnElement("Third Langauge", clickElement(ep.verifyThirdBasicLanguage()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(2000);
			actualBasicLanguageSelected =getText(ep.verifyProfileLanguage(), platform);
			verifyData(actualBasicLanguageSelected, "Selected BasicLanguage at Edit Profile Page", containsData(actualBasicLanguageSelected,expectedThirdBasicLanguage));
			clickOnElement("Profile Language", clickElement(ep.verifyProfileLanguage()));
			clickOnElement("Fourth Langauge", clickElement(ep.verifyFourthBasicLanguage()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(2000);
			actualBasicLanguageSelected =getText(ep.verifyProfileLanguage(), platform);
			verifyData(actualBasicLanguageSelected, "Selected BasicLanguage at Edit Profile Page", containsData(actualBasicLanguageSelected,expectedFourthBasicLanguage));
			clickOnElement("Profile Language", clickElement(ep.verifyProfileLanguage()));
			clickOnElement("Fifth Langauge", clickElement(ep.verifyFifthBasicLanguage()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(2000);
			actualBasicLanguageSelected =getText(ep.verifyProfileLanguage(), platform);
			verifyData(actualBasicLanguageSelected, "Selected BasicLanguage at Edit Profile Page", containsData(actualBasicLanguageSelected,expectedFifthBasicLanguage));
			clickOnElement("Profile Language", clickElement(ep.verifyProfileLanguage()));
			clickOnElement("Sixth Langauge", clickElement(ep.verifySixthBasicLanguage()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(2000);
			actualBasicLanguageSelected =getText(ep.verifyProfileLanguage(), platform);
			verifyData(actualBasicLanguageSelected, "Selected BasicLanguage at Edit Profile Page", containsData(actualBasicLanguageSelected,expectedSixthBasicLanguage));
			clickOnElement("Profile Language", clickElement(ep.verifyProfileLanguage()));
			clickOnElement("Seventh Langauge", clickElement(ep.verifySeventhBasicLanguage()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(2000);
			actualBasicLanguageSelected =getText(ep.verifyProfileLanguage(), platform);
			verifyData(actualBasicLanguageSelected, "Selected BasicLanguage at Edit Profile Page", containsData(actualBasicLanguageSelected,expectedSeventhBasicLanguage));
			clickOnElement("Profile Language", clickElement(ep.verifyProfileLanguage()));
			clickOnElement("Eighth Langauge", clickElement(ep.verifyEighthBasicLanguage()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(2000);
			actualBasicLanguageSelected =getText(ep.verifyProfileLanguage(), platform);
			verifyData(actualBasicLanguageSelected, "Selected BasicLanguage at Edit Profile Page", containsData(actualBasicLanguageSelected,expectedEighthBasicLanguage));
			clickOnElement("Profile Language", clickElement(ep.verifyProfileLanguage()));
			clickOnElement("Nineth Langauge", clickElement(ep.verifyNinthBasicLanguage()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(2000);
			actualBasicLanguageSelected =getText(ep.verifyProfileLanguage(), platform);
			verifyData(actualBasicLanguageSelected, "Selected BasicLanguage at Edit Profile Page", containsData(actualBasicLanguageSelected,expectedNinthBasicLanguage));
			clickOnElement("Profile Language", clickElement(ep.verifyProfileLanguage()));
			clickOnElement("Tenth Langauge", clickElement(ep.verifyTenthBasicLanguage()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(2000);
			actualBasicLanguageSelected =getText(ep.verifyProfileLanguage(), platform);
			verifyData(actualBasicLanguageSelected, "Selected BasicLanguage at Edit Profile Page", containsData(actualBasicLanguageSelected,expectedTenthBasicLanguage));
			clickOnElement("Profile Language", clickElement(ep.verifyProfileLanguage()));
			clickOnElement("Eleventh Langauge", clickElement(ep.verifyEleventhBasicLanguage()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(2000);
			actualBasicLanguageSelected =getText(ep.verifyProfileLanguage(), platform);
			verifyData(actualBasicLanguageSelected, "Selected BasicLanguage at Edit Profile Page", containsData(actualBasicLanguageSelected,expectedEleventhBasicLanguage));
			clickOnElement("Profile Language", clickElement(ep.verifyProfileLanguage()));
			scroll(platform,"Española (México)");
			clickOnElement("Twelveth Langauge", clickElement(ep.verifyTwelvethBasicLanguage()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(2000);
			actualBasicLanguageSelected =getText(ep.verifyProfileLanguage(), platform);
			verifyData(actualBasicLanguageSelected, "Selected BasicLanguage at Edit Profile Page", containsData(actualBasicLanguageSelected,expectedTwelevethBasicLanguage));
			clickOnElement("Save Button",clickElement(ep.verifyEnableSaveButton()));
			logOut(platform);
			login(platform);
			clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
			profilePageLanguage=getText(ep.verifyProfilePageLanguage(), platform);
			String actualLanguage[]=profilePageLanguage.split(" ", 2);
			profilePageLanguage=actualLanguage[1];
			verifyData(profilePageLanguage, "Profile Page Language", containsData(expectedTwelevethBasicLanguage, profilePageLanguage));
			s.assertAll();
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Add/Update Profile Language Test Failed");
		}
	}	
}
