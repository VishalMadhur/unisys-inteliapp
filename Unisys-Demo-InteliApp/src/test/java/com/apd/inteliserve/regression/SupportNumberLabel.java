package com.apd.inteliserve.regression;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.apd.inteliserve.pompages.LoginPage;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class SupportNumberLabel extends BaseTest {
	@Test
	@Parameters({"platform","language","transalationSheet"})
	public void supportNumberLabelTest(@Optional String platform,@Optional String language,@Optional String transalationSheet) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		String expectedPhoneHelpLabel="";
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-3854, Support Number Label Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
			expectedPhoneHelpLabel =getCellValue(transalationSheet, 116, 1);
		}else {
			extentTest = extent.startTest("IA-3626, Support Number Label Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
			expectedPhoneHelpLabel =getCellValue(transalationSheet, 116, 1)+" ";
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");


			//	String expectedSupportNumberLabel=getCellValue("ExpectedText", 11, 1);


			LoginPage lp = new LoginPage(driver);

			logOut(platform);
			Thread.sleep(5000);
			String actualPhoneHelpLabel=getText(lp.verifyPhoneHelpText(),platform);
			//			String actualSupportNumberLabel=getText(lp.verifySupportNumber(),platform);
			verifyLabel("Support Help", matchData(expectedPhoneHelpLabel, actualPhoneHelpLabel));
			//			verifyData(actualSupportNumberLabel, "Support Number", matchData(expectedSupportNumberLabel, actualSupportNumberLabel));
			//	TouchAction touchAction = new TouchAction(driver);
			//	touchAction.tap(PointOption.point(172, 1348)).perform();



			//	WebElement element = driver.findElement(By.xpath("//android.widget.TextView[@content-desc='support_text']"));
			//		JavascriptExecutor executor = (JavascriptExecutor)driver;
			//		executor.executeScript("arguments[0].click();", element);

			//			
			//		Point point = element.getLocation();

			//you can change follwing point based on your link location
			//			int x = point.x +20;  
			//			int y = point.y + element.getSize().getHeight() - 1;
			//			touchAction.tap(PointOption.point(x, y)).perform();
			//			System.out.println(x);
			//			System.out.println(y);



			//		driver.findElement(MobileBy.AndroidUIAutomator("new UiSelector().textContains('Americas')")).click();
			//		Thread.sleep(10000);
			//		scroll(platform,"Americas");
			//			verifyElement("Call Icon", elementDisplayed(lp.verifyCallIcon()));
			//			clickOnElement("Phone Icon",clickElement(lp.verifyCallIcon()));
			//			if(platform.equalsIgnoreCase("android")){
			//				FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			//				expectedSupportNumberLabel=getCellValue("ExpectedText", 12, 1);
			//				actualSupportNumberLabel=getText(fm.getSupportNumber(), platform);
			//
			//				if(actualSupportNumberLabel.equals(expectedSupportNumberLabel)) {
			//					extentTest.log(LogStatus.PASS, "Support Number is Verified in Phone Dailer -- " +actualSupportNumberLabel);
			//				}
			//				else {
			//					extentTest.log(LogStatus.FAIL, "Support Number is Not Verified in Phone Dailer -- " +actualSupportNumberLabel);
			//				}
			//				s.assertEquals(actualSupportNumberLabel, expectedSupportNumberLabel);
			//	}
			s.assertAll();
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Support Number Label  Test Failed");
		}
	}	
}
