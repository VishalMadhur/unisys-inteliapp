package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.apd.inteliserve.pompages.KnowledgeBasePage;
import com.apd.inteliserve.pompages.LandingPage;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class KnowledgeBaseContextualMenu extends BaseTest {
	@SuppressWarnings("rawtypes")
	@Test
	@Parameters({"platform","language","transalationSheet","device"})
	public void knowledgeBaseContextualMenuTest(@Optional String platform,@Optional String language,@Optional String transalationSheet, @Optional String device) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2069, Knowledge Base Contextual Menu Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-2037, Knowledge Base Contextual Menu Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			KnowledgeBasePage kb = new KnowledgeBasePage(driver);
			clickOnElement("Knowledge Base Button", clickElement(fm.clickKnowledgeBaseButton()));
			contextualMenuTest(platform, language, transalationSheet, device);
			if(platform.equalsIgnoreCase("ios")){
				clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
				//clickOnElement("Knowledge Base Button", clickElement(fm.clickKnowledgeBaseButton()));
			}else {
				((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.BACK));
				((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.BACK));
				((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.BACK));
			}
			try {
				elementDisplayed(fm.clickKnowledgeBaseButton());
				clickOnElement("Knowledge Base Button", clickElement(fm.clickKnowledgeBaseButton()));
				clickOnElement("Knowledge Base First category", clickElement(kb.clickFirstCategory()));
			}catch(Exception e) {
				clickOnElement("Knowledge Base First category", clickElement(kb.clickFirstCategory()));
			}
			contextualMenuTest(platform, language, transalationSheet, device);
			if(platform.equalsIgnoreCase("ios")){
				clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			}else {
				((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.BACK));
				((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.BACK));
				((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.BACK));
			}
			try {
				elementDisplayed(fm.clickKnowledgeBaseButton());
				clickOnElement("Knowledge Base Button", clickElement(fm.clickKnowledgeBaseButton()));
				clickOnElement("Knowledge Base First category", clickElement(kb.clickFirstCategory()));
			}catch(Exception e) {
				clickOnElement("Knowledge Base First category", clickElement(kb.clickFirstCategory()));
			}
			//clickOnElement("Knowledge Base First Category", clickElement(kb.clickFirstCategory()));
			clickOnElement("Knowledge Base First Article", clickElement(kb.clickFirstArticle()));
			contextualMenuTest(platform, language, transalationSheet, device);
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Knowledge Base Contextual Menu Test Failed");
		}
	}

	@Parameters({"platform","language","transalationSheet","device"})
	public void contextualMenuTest(@Optional String platform,@Optional String language,@Optional String transalationSheet, @Optional String device) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();

		String expectedKnowledgeBaseHeaderLabel =getCellValue(transalationSheet, 23, 1);
		String expectedCanNotFindAswerText =getCellValue(transalationSheet, 28, 1);
		String expectedChatWithAmeliaLabel =getCellValue(transalationSheet, 30, 1);
		String expectedWriteToUsLabel=getCellValue(transalationSheet, 29, 1);
		String expectedCancelLabel=getCellValue(transalationSheet, 31, 1);

		FavouriteModulesPage fm = new FavouriteModulesPage(driver);
		KnowledgeBasePage kb = new KnowledgeBasePage(driver);

		String actualKnowledgeBaseHeaderLabel=getText(kb.verifyKnowledgeHeaderText(),platform);
		verifyLabel("Knowledge Base Header", matchData(expectedKnowledgeBaseHeaderLabel,actualKnowledgeBaseHeaderLabel));
		verifyElement("Contextual Menu",elementIsDisplayed(kb.verifyContextualMenu())) ;
		clickOnElement("Contextual Menu", clickElement(kb.verifyContextualMenu()));
		String actualCanNotFindAswerLabel=getText(kb.verifyCanNotFindAnswerText(), platform);
		String actualChatWithAmeliaLabel=getText(kb.verifyChatWithAmeliaText(), platform);;
		String actualWriteToUsLabel=getText(kb.verifyWriteToUsText(), platform);;
		verifyLabel("Can't Find Answer Text", matchData(expectedCanNotFindAswerText,actualCanNotFindAswerLabel));
		verifyLabel("Chat With Amelia Text", matchData(expectedChatWithAmeliaLabel,actualChatWithAmeliaLabel));
		verifyLabel("Write To Us Text", matchData(expectedWriteToUsLabel,actualWriteToUsLabel));
		if(!device.equalsIgnoreCase("iPad")){
			String actualCancelLabel=getText(kb.verifyCancelText(), platform);
			verifyLabel("Cancel Text", matchData(expectedCancelLabel,actualCancelLabel));
			clickOnElement("Cancel Text on Contextual Menu", clickElement(kb.verifyCancelText()));
			verifyElement("Knowledge Base Landing Screen", elementIsDisplayed(kb.verifyKnowledgeHeaderText()));
			clickOnElement("Contextual Menu", clickElement(kb.verifyContextualMenu()));
			Thread.sleep(3000);
		}
		clickOnElement("Chat With Amelia Text on Contextual Menu", clickElement(kb.verifyChatWithAmeliaText()));
		wait.until(ExpectedConditions.visibilityOf(fm.toggleButton()));
		verifyElement("IVA Screen",elementIsDisplayed(fm.toggleButton())) ;
		LandingPage lp = new LandingPage(driver);
		clickOnElement("Home Naviagation Bar", clickElement(lp.homeIcon()));
		clickOnElement("Knowledge Base Button", clickElement(fm.clickKnowledgeBaseButton()));
		clickOnElement("Contextual Menu", clickElement(kb.verifyContextualMenu()));
		clickOnElement("Write To Us Text on Contextual Menu", clickElement(kb.verifyWriteToUsText()));
		Thread.sleep(5000);
		String expectedToEmail =getPropertyValue("ToEmail");
		if(platform.equalsIgnoreCase("android")){
			Thread.sleep(3000);
			clickElement(fm.getToEmailText());
			String actualToEmail =getText(fm.getToEmailText());
			Reporter.log(actualToEmail,true);
			if(actualToEmail.contains(expectedToEmail)) {
				extentTest.log(LogStatus.PASS, "To Email is Verified -- " +actualToEmail);
			}
			else {
				extentTest.log(LogStatus.FAIL, "To Email is Not Verified -- " +actualToEmail);
			}
			s.assertTrue(actualToEmail.contains(expectedToEmail));
			s.assertAll();
		}
	}
}
