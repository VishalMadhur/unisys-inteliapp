package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.general.Lib;
import com.apd.inteliserve.pompages.ProfileScreenLegalPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */

public class IA_1011 extends BaseTest {
	@Test
	@Parameters({"platform"})
	public void aboutScreenPageTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-1011, About Screen Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-1011, About Screen Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			ProfileScreenLegalPage pl= new ProfileScreenLegalPage(driver);
			click(pl.profileIcon());
			extentTest.log(LogStatus.PASS, "User is Able to Navigate to Profile Icon");
			if(elementIsDisplayed(pl.aboutText())){
				extentTest.log(LogStatus.PASS, "About Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "About Text is Not Displayed");
			}
			click(pl.aboutText());
			extentTest.log(LogStatus.PASS, "Clink on About Link");
			if(elementIsDisplayed(pl.verifyAboutHeader())){
				extentTest.log(LogStatus.PASS, "About Header Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "About Header Text is Not Displayed");
			}
			if(elementIsDisplayed(pl.privacyPolicyText())){
				extentTest.log(LogStatus.PASS, "Privacy Policy Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Privacy Policy Text is Not Displayed");
			}
			if(elementIsDisplayed(pl.termsOfUseText())){
				extentTest.log(LogStatus.PASS, "Terms of Use Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Terms of Use Text is Not Displayed");
			}
			if(elementIsDisplayed(pl.endUserText())){
				extentTest.log(LogStatus.PASS, "End User License Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "End User License Text is Not Displayed");
			}
			if(elementIsDisplayed(pl.thirdPartyPolicyText())){
				extentTest.log(LogStatus.PASS, "Third-Party Policy Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Third-Party Policy Text is Not Displayed");
			}
			String expectedVersionNumber=Lib.getCellValue("ExpectedText", 1, 1);
			String expectedTagLine=Lib.getCellValue("ExpectedText", 2, 1);
			String actualVersionNumber= " ";
			String actualTagLine =" ";
			if(platform.equalsIgnoreCase("ios")){
				actualVersionNumber=pl.VersionNumberText().getAttribute("name");
				actualTagLine=pl.taglineText().getAttribute("name");
			}else {
				actualVersionNumber=pl.VersionNumberText().getText();
				actualTagLine=pl.taglineText().getText();
			}
			
			if(actualVersionNumber.equals(expectedVersionNumber)){
				extentTest.log(LogStatus.PASS, "Correct Version Number is Displayed ------  " + actualVersionNumber);
			}
			else{
				extentTest.log(LogStatus.FAIL, "Wrong Version Number is Displayed");	
			}
			s.assertEquals(actualVersionNumber, expectedVersionNumber);
			if(actualTagLine.equals(expectedTagLine)){
				extentTest.log(LogStatus.PASS, "Correct Tagline is Displayed ----" + actualTagLine);
			}
			else{
				extentTest.log(LogStatus.FAIL, "Wrong Tagline is Displayed-----" + actualTagLine);	
			}
			s.assertEquals(actualTagLine, expectedTagLine);
			if(elementIsDisplayed(pl.copyrightText())){
				extentTest.log(LogStatus.PASS, "Copy Right Text  is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Copy Right Text  is Not Displayed");
			}
			click(pl.clickBackButton());
			extentTest.log(LogStatus.PASS, "User is Able Click Back Button");
			if(elementIsDisplayed(pl.profileHeaderText())){
				extentTest.log(LogStatus.PASS, "User is Navigated Back to Profile Screen");
			}
			else{
				extentTest.log(LogStatus.FAIL, "User is Failed to Navigate Back to Profile Screen");
			}
			click(pl.aboutText());
			extentTest.log(LogStatus.PASS, "Clink on About Link");
				if(platform.equalsIgnoreCase("ios")){
					click(pl.privacyPolicyText());
				}
				else {
					click(pl.privacyPloicyLink());
				}
				extentTest.log(LogStatus.PASS, "Clicked on Privacy Policy");
				String expectedUrl =getPropertyValue("PrivacyPolicyUrl");
				if(platform.equalsIgnoreCase("ios")){
					clickElement(pl.getURL());
				}
				String actualUrl= getText(pl.getCurrentURL());
				Reporter.log(actualUrl,true);
				if(actualUrl.contains(expectedUrl)) {
					extentTest.log(LogStatus.PASS, "Navigated to Corerect URL -- " +actualUrl);
				}
				else {
					extentTest.log(LogStatus.FAIL, "Navigated to Wrong URL -- " +actualUrl);
				}
				s.assertTrue(actualUrl.contains(expectedUrl));
				s.assertAll();
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			extentTest.log(LogStatus.FAIL, "Test Failed");
			Assert.fail("About Screen Test Failed");
		}
	}
	
	@Test
	@Parameters({"platform"})
	public void aboutScreenTermsOfUseTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 240);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-1011, About Screen Terms of Use Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-1011, About Screen Test Terms of Use -Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			ProfileScreenLegalPage pl= new ProfileScreenLegalPage(driver);
			click(pl.profileIcon());
			extentTest.log(LogStatus.PASS, "User is Able to Navigate to Profile Icon");
			click(pl.aboutText());
			extentTest.log(LogStatus.PASS, "Clink on About Link");
			if(platform.equalsIgnoreCase("ios")){
				click(pl.termsOfUseText());
			}
			else {
				click(pl.termsOfUseLink());
			}
			extentTest.log(LogStatus.PASS, "Clicked on Terms Of Use Link");
			String expectedUrl =getPropertyValue("TermsOfUseURL");
			if(platform.equalsIgnoreCase("ios")){
				clickElement(pl.getURL());
			}
			String actualUrl= getText(pl.getCurrentURL());
			Reporter.log(actualUrl,true);
			if(actualUrl.contains(expectedUrl)) {
				extentTest.log(LogStatus.PASS, "Navigated to Corerect URL -- " +actualUrl);
			}
			else {
				extentTest.log(LogStatus.FAIL, "Navigated to Wrong URL -- " +actualUrl);
			}
			s.assertTrue(actualUrl.contains(expectedUrl));
			s.assertAll();
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			extentTest.log(LogStatus.FAIL, "Test Failed");
			Assert.fail("About Screen Terms of Use Test Failed");
		}
	}
}

