package com.apd.inteliserve.regression;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.apd.inteliserve.pompages.KnowledgeBasePage;
import com.apd.inteliserve.pompages.LandingPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */


public class LandingPageTiles extends BaseTest{
	@Test
	@Parameters({"platform","language","transalationSheet"})
	public void KnowledgeBaseTilesTest(@Optional String platform,@Optional String language,@Optional String transalationSheet) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		String expectedFirstTileLabel ="";
		String expectedSecondTileLabel ="";
		String expectedThirdTileLabel ="";
		String expectedFourthTileLabel ="";
		String expectedFifthTileLabel ="";
		String expectedSixthTileLabel ="";
		String expectedSeventhTileLabel ="";
		String expectedKnowledgeBaseHeaderLabel ="";
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2621 Landing Page Tiles Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
			expectedFirstTileLabel =getCellValue(transalationSheet, 45, 1);
			expectedSecondTileLabel =getCellValue(transalationSheet, 46, 1);
			expectedThirdTileLabel =getCellValue(transalationSheet, 47, 1);
			expectedFourthTileLabel =getCellValue(transalationSheet, 48, 1);
			expectedFifthTileLabel =getCellValue(transalationSheet, 49, 1);
			expectedSixthTileLabel =getCellValue(transalationSheet, 50, 1);
			expectedSeventhTileLabel =getCellValue(transalationSheet, 51, 1);
			expectedKnowledgeBaseHeaderLabel =getCellValue(transalationSheet, 23, 1);
		}else {
			extentTest = extent.startTest("IA-2622, Landing Page Tiles Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
			expectedFirstTileLabel =getCellValue(transalationSheet, 38, 1);
			expectedSecondTileLabel =getCellValue(transalationSheet, 39, 1);
			expectedThirdTileLabel =getCellValue(transalationSheet, 40, 1);
			expectedFourthTileLabel =getCellValue(transalationSheet, 41, 1);
			expectedFifthTileLabel =getCellValue(transalationSheet, 42, 1);
			expectedSixthTileLabel =getCellValue(transalationSheet, 43, 1);
			expectedSeventhTileLabel =getCellValue(transalationSheet, 44, 1);
			expectedKnowledgeBaseHeaderLabel =getCellValue(transalationSheet, 23, 1);
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			LandingPage ld = new LandingPage(driver);
			try {
				String actualFirstTileLabel=getText(ld.verifyFirstTile(), platform);
				String actualSecondTileLabel=getText(ld.verifySecondTile(), platform);
				String actualThirdTileLabel=getText(ld.verifyThirdTile(), platform);
				verifyLabel("Knowledge Base", matchData(expectedFirstTileLabel,actualFirstTileLabel));
				verifyLabel("Company News", matchData(expectedSecondTileLabel,actualSecondTileLabel));
				verifyLabel("Support Corner", matchData(expectedThirdTileLabel,actualThirdTileLabel));
				extentTest.log(LogStatus.INFO, "Performing Left Swipe");
				swipe(ld.verifyCoreSupportButton(), ld.verifyKnowledgeBaseButton());
				String actualFourthTileLabel=getText(ld.verifyFourthTile(), platform);
				String actualFifthTileLabel=getText(ld.verifyFifthTile(), platform);
				verifyLabel("Human Resources", matchData(expectedFourthTileLabel,actualFourthTileLabel));
				verifyLabel("University", matchData(expectedFifthTileLabel,actualFifthTileLabel));
				extentTest.log(LogStatus.INFO, "Performing Left Swipe Second Time");
				swipe(ld.verifypayrollButton(), ld.verifyCoreSupportButton());
				String actualSixthTileLabel=getText(ld.verifySixthTile(), platform);
				String actualSeventhTileLabel=getText(ld.verifySeventhTile(), platform);
				verifyLabel("Payroll", matchData(expectedSixthTileLabel,actualSixthTileLabel));
				verifyLabel("Online Training", matchData(expectedSeventhTileLabel,actualSeventhTileLabel));
				extentTest.log(LogStatus.PASS, "Correct Tile Order is Displayed --------");
				extentTest.log(LogStatus.PASS, "01------"+actualFirstTileLabel);
				extentTest.log(LogStatus.PASS, "02------"+actualSecondTileLabel);
				extentTest.log(LogStatus.PASS, "03------"+actualThirdTileLabel);
				extentTest.log(LogStatus.PASS, "04------"+actualFourthTileLabel);
				extentTest.log(LogStatus.PASS, "05------"+actualFifthTileLabel);
				extentTest.log(LogStatus.PASS, "06------"+actualSixthTileLabel);
				extentTest.log(LogStatus.PASS, "07------"+actualSeventhTileLabel);
			}catch(Exception e) {
				extentTest.log(LogStatus.FAIL, "Correct Tile Order is Not Displayed");	
			}
			extentTest.log(LogStatus.INFO, "Performing Right Swipe");
			swipe(ld.verifyHumanButton(), ld.verifyOnlineTrainingButton());
			extentTest.log(LogStatus.INFO, "Performing Right Swipe Second Time");
			swipe(ld.verifyCompanyNewsButton(), ld.verifyCoreSupportButton());
			//clickElement(ld.verifyFirstTile());
			Thread.sleep(2000);
			clickOnElement("Knowledge Base", clickElement(ld.verifyFirstTile()));
			KnowledgeBasePage kb = new KnowledgeBasePage(driver);
			String actualKnowledgeBaseHeaderLabel=getText(kb.verifyKnowledgeHeaderText(),platform);
			verifyLabel("Knowledge Base Landing Page", matchData(expectedKnowledgeBaseHeaderLabel,actualKnowledgeBaseHeaderLabel));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			//clickElement(ld.verifyThirdTile());
			Thread.sleep(2000);
			clickOnElement("Support Corner Button",clickElement(ld.verifyThirdTile()));
			verifyElement("Support Corner Page", elementDisplayed(kb.verifyAskITText()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			//clickElement(ld.verifyFourthTile());
			Thread.sleep(2000);
			clickOnElement("Human Resources Button",clickElement(ld.verifyFourthTile()) );
			verifyElement("Your Company Content Text", elementDisplayed(ld.verifyCompanyContentLabel()));
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			Thread.sleep(2000);
			//clickElement(ld.verifySecondTile());
			clickOnElement("Company News Button", clickElement(ld.verifySecondTile()));
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			String expectedUrl =getPropertyValue("CompanyNewsUrl");
			if(platform.equalsIgnoreCase("ios")){
				clickElement(fm.getURL());
			}
			String actualUrl= getText(fm.getCurrentURL());
			Reporter.log(actualUrl,true);
			if(actualUrl.contains(expectedUrl)) {
				extentTest.log(LogStatus.PASS, "Navigated to Corerect URL -- " + actualUrl);
			}
			else {
				extentTest.log(LogStatus.FAIL, "Navigated to Wrong URL -- " + actualUrl);
			}
			s.assertTrue(actualUrl.contains(expectedUrl));
			s.assertAll();
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Landing Page Tiles Test Failed");
		}	
	}

}
