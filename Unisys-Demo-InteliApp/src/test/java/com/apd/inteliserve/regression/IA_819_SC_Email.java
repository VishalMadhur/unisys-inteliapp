package com.apd.inteliserve.regression;


import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */



public class IA_819_SC_Email extends BaseTest{
	@Test
	@Parameters({"platform"})
	public void supportCornerEmailTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-819, Support Corner Email Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-819, Support Corner Email Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			click(fm.clickCoreSupportButton());
			extentTest.log(LogStatus.PASS, "Clicked  on Core Support Buton");
			click(fm.emailButton());
			extentTest.log(LogStatus.PASS, "Clicked  on Email Buton");
			String expectedToEmail =getPropertyValue("ToEmail");
			if(platform.equalsIgnoreCase("android")){
				Thread.sleep(3000);
				clickElement(fm.getToEmailText());
				String actualToEmail =getText(fm.getToEmailText());
				Reporter.log(actualToEmail,true);
				if(actualToEmail.contains(expectedToEmail)) {
					extentTest.log(LogStatus.PASS, "To Email is Verified -- " +actualToEmail);
				}
				else {
					extentTest.log(LogStatus.FAIL, "To Email is Not Verified -- " +actualToEmail);
				}
				s.assertTrue(actualToEmail.contains(expectedToEmail));
				//			else {
				//				String actualToEmail =fm.getToEmailText().getText();
				//				Reporter.log(actualToEmail,true);
				//				if(actualToEmail.contains(expectedToEmail)) {
				//					extentTest.log(LogStatus.PASS, "To Email is Verified");
				//				}
				//				else {
				//					extentTest.log(LogStatus.FAIL, "To Email is Not Verified");
				//				}
				//				s.assertEquals(actualToEmail, expectedToEmail);
				//			}
				s.assertAll();
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Support Corner Chat Test Failed");
		}
	}
}
