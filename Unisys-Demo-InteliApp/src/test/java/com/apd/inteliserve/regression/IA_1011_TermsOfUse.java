package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.ProfileScreenLegalPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */

public class IA_1011_TermsOfUse extends BaseTest {
	@Test
	@Parameters({"platform"})
	public void aboutScreenPageTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-1011, About Screen Terms of Use Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-1011, About Screen Test Terms of Use -Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			ProfileScreenLegalPage pl= new ProfileScreenLegalPage(driver);
			click(pl.profileIcon());
			extentTest.log(LogStatus.PASS, "User is Able to Navigate to Profile Icon");
			click(pl.aboutText());
			extentTest.log(LogStatus.PASS, "Clink on About Link");
			if(platform.equalsIgnoreCase("ios")){
				click(pl.termsOfUseText());
			}
			else {
				click(pl.termsOfUseLink());
			}
			extentTest.log(LogStatus.PASS, "Clicked on Terms Of Use Link");
			String expectedUrl =getPropertyValue("TermsOfUseURL");
			if(platform.equalsIgnoreCase("ios")){
				clickElement(pl.getURL());
			}
			String actualUrl= getText(pl.getCurrentURL());
			Reporter.log(actualUrl,true);
			if(actualUrl.contains(expectedUrl)) {
				extentTest.log(LogStatus.PASS, "Navigated to Corerect URL -- " +actualUrl);
			}
			else {
				extentTest.log(LogStatus.FAIL, "Navigated to Wrong URL -- " +actualUrl);
			}
			s.assertTrue(actualUrl.contains(expectedUrl));
			s.assertAll();
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			extentTest.log(LogStatus.FAIL, "Test Failed");
			Assert.fail("About Screen Terms of Use Test Failed");
		}
	}
}
