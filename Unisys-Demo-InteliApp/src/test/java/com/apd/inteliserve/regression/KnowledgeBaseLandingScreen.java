package com.apd.inteliserve.regression;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.general.Lib;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.apd.inteliserve.pompages.KnowledgeBasePage;
import com.apd.inteliserve.pompages.LandingPage;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class KnowledgeBaseLandingScreen extends BaseTest {
	@SuppressWarnings("rawtypes")
	@Test
	@Parameters({"platform","language","transalationSheet","device"})
	public void KnowledgeBaseLandingScreenTest(@Optional String platform,@Optional String language,@Optional String transalationSheet,@Optional String device) {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2255, Knowledge Base Landing Screen Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-1912, Knowledge Base Landing Screen Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			clickOnElement("Knowledge Base Button", clickElement(fm.clickKnowledgeBaseButton()));
			String expectedKnowledgeBaseHeaderLabel =getCellValue(transalationSheet, 23, 1);
			String expectedCategoriesLabel =getCellValue(transalationSheet, 25, 1);
			String expectedSeeMoreLabel =getCellValue(transalationSheet, 26, 1);
			String expectedSearchKeywordLabel =getCellValue(transalationSheet, 24, 1);
			String expectedSeeLessLabel=getCellValue(transalationSheet, 27, 1);
			String expectedCanNotFindAswerText =getCellValue(transalationSheet, 28, 1);
			String expectedChatWithAmeliaLabel =getCellValue(transalationSheet, 30, 1);
			String expectedWriteToUsLabel=getCellValue(transalationSheet, 29, 1);
			String expectedCancelLabel=getCellValue(transalationSheet, 31, 1);

			KnowledgeBasePage kb = new KnowledgeBasePage(driver);

			String actualKnowledgeBaseHeaderLabel=getText(kb.verifyKnowledgeHeaderText(),platform);
			String actualCategoriesLabel=getText(kb.verifyCategoriesText(),platform);
			String actualSeeMoreLabel=getText(kb.verifySeeMoreText(),platform);
			String actualSearchKeywordLabel=getText(kb.verifySearchTextBox(),platform);
			verifyLabel("Knowledge Base Header", matchData(expectedKnowledgeBaseHeaderLabel,actualKnowledgeBaseHeaderLabel));
			verifyLabel("Categories Text", matchData(expectedCategoriesLabel,actualCategoriesLabel));
			verifyLabel("See More Text", matchData(expectedSeeMoreLabel,actualSeeMoreLabel));
			verifyElement("Contextual Menu",elementDisplayed(kb.verifyContextualMenu())) ;
			verifyElement("Search Text Box",elementDisplayed(kb.verifySearchTextBox())) ;
			verifyLabel("Search Any Keyword", containsData(actualSearchKeywordLabel, expectedSearchKeywordLabel));
			try {
				List<MobileElement> categories=kb.getcategoriesCount();
				int actualCategoriesCount=categories.size();
				if(platform.equalsIgnoreCase("ios")){
					String categoriesCountIos=getPropertyValue("DefaultCategoriesCountIos");
					int expectedCategoriesCountIos = Integer.parseInt(categoriesCountIos);
					if(expectedCategoriesCountIos==actualCategoriesCount) {
						extentTest.log(LogStatus.PASS, "Only Five Categories Are Displayed By Default");
						extentTest.log(LogStatus.INFO,"Categories Displayed Are --------");
						for(MobileElement element : categories) {
							String getCategories= getText(element, platform);
							if(!getCategories.equals("category_item forward_icon")) {
								extentTest.log(LogStatus.INFO,getCategories);
							}
						}
					}
					else {
						extentTest.log(LogStatus.FAIL, "Only Five Categories Are Not Displayed By Default");
					}
					s.assertEquals(actualCategoriesCount, expectedCategoriesCountIos);
					clickOnElement("Search Text Box", clickElement(kb.verifySearchTextBox()));
					kb.verifySearchTextBox().sendKeys("VPN\n");
				}else {
					String categoriesCountAndroid=getPropertyValue("DefaultCategoriesCountAndroid");
					int expectedCategoriesCountAndroid = Integer.parseInt(categoriesCountAndroid);
					if(expectedCategoriesCountAndroid==actualCategoriesCount) {
						extentTest.log(LogStatus.PASS, "Only Five Categories Are Displayed By Default");
						extentTest.log(LogStatus.INFO,"Categories Displayed Are --------");
						for(MobileElement element : categories) {
							String getCategories= getText(element, platform);
							if(!getCategories.equals("category_item forward_icon")) {
								extentTest.log(LogStatus.INFO,getCategories);
							}
						}
					}
					else {
						extentTest.log(LogStatus.FAIL, "Only Five Categories Are Not Displayed By Default");
					}
					s.assertEquals(actualCategoriesCount, expectedCategoriesCountAndroid);
					clickOnElement("Search Text Box", clickElement(kb.verifySearchTextBox()));
					kb.verifySearchTextBox().sendKeys("VPN\n");
					((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
				}
				List<MobileElement> searchItems=kb.getsearchItemsCount();
				List<MobileElement> searchTexts=kb.getsearchTextsCount();
				int actualSearchItemsCount=searchItems.size();
				int actualSearchTextsCount=searchTexts.size();
				if(platform.equalsIgnoreCase("ios")){
					if(actualSearchTextsCount==actualSearchItemsCount/2) {
						extentTest.log(LogStatus.PASS, "VPN Releated Search Items Are Shown Correctly");
						for(MobileElement element : searchTexts) {
							String getSerachedArticle= getText(element, platform);
							extentTest.log(LogStatus.INFO,getSerachedArticle);
						}

					}
					else {
						extentTest.log(LogStatus.FAIL, "VPN Releated Search Items Are Not Shown Correctly");
					}
				}else {
					if(actualSearchTextsCount==actualSearchItemsCount) {
						extentTest.log(LogStatus.PASS, "VPN Releated Search Items Are Shown Correctly");
						for(MobileElement element : searchTexts) {
							String getSerachedArticle= getText(element, platform);
							extentTest.log(LogStatus.INFO,getSerachedArticle);
						}
					}
					else {
						extentTest.log(LogStatus.FAIL, "VPN Releated Search Items Are Not Shown Correctly");
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
				String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
				extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			}
			clickOnElement("Cancel Button", clickElement(kb.clickCancelButton()));
			Thread.sleep(3000);
			clickOnElement("See More Text", clickElement(kb.verifySeeMoreText()));
			Thread.sleep(1000);
			String actualSeeLessLabel=getText(kb.verifySeeMoreText(),platform);
			System.out.println(actualSeeLessLabel);
			verifyLabel("See Less Text", matchData(expectedSeeLessLabel,actualSeeLessLabel));
			scroll(platform, "FAQ");
			clickOnElement("Back Icon", clickElement(kb.clickBackIcon()));
			verifyElement("Landing Screen", elementDisplayed(fm.clickKnowledgeBaseButton()));
			clickOnElement("Knowledge Base Button", clickElement(fm.clickKnowledgeBaseButton()));
			clickOnElement("Contextual Menu", clickElement(kb.verifyContextualMenu()));
			String actualCanNotFindAswerLabel=getText(kb.verifyCanNotFindAnswerText(), platform);
			String actualChatWithAmeliaLabel=getText(kb.verifyChatWithAmeliaText(), platform);;
			String actualWriteToUsLabel=getText(kb.verifyWriteToUsText(), platform);;
			verifyLabel("Can't Find Answer Text", matchData(expectedCanNotFindAswerText,actualCanNotFindAswerLabel));
			verifyLabel("Chat With Amelia Text", matchData(expectedChatWithAmeliaLabel,actualChatWithAmeliaLabel));
			verifyLabel("Write To Us Text", matchData(expectedWriteToUsLabel,actualWriteToUsLabel));
			if(!device.equalsIgnoreCase("iPad")){
				String actualCancelLabel=getText(kb.verifyCancelText(), platform);
				verifyLabel("Cancel Text", matchData(expectedCancelLabel,actualCancelLabel));
				//			verifyElement("Can't Find Answer Text",elementDisplayed(kb.verifyCanNotFindAnswerText())) ;
				//			verifyElement("Chat With Amelia Text",elementDisplayed(kb.verifyChatWithAmeliaText())) ;
				//			verifyElement("Write To Us Text",elementDisplayed(kb.verifyWriteToUsText())) ;
				//			verifyElement("Cancel Text",elementDisplayed(kb.verifyCancelText())) ;
				clickOnElement("Cancel Text on Contextual Menu", clickElement(kb.verifyCancelText()));
				verifyElement("Knowledge Base Landing Screen", elementDisplayed(kb.verifyCategoriesText()));
				clickOnElement("Contextual Menu", clickElement(kb.verifyContextualMenu()));
				Thread.sleep(3000);
			}
			clickOnElement("Chat With Amelia Text on Contextual Menu", clickElement(kb.verifyChatWithAmeliaText()));
			wait.until(ExpectedConditions.visibilityOf(fm.toggleButton()));
			verifyElement("IVA Screen",elementDisplayed(fm.toggleButton())) ;
			LandingPage lp = new LandingPage(driver);
			clickOnElement("Home Naviagation Bar", clickElement(lp.homeIcon()));
			clickOnElement("Knowledge Base Button", clickElement(fm.clickKnowledgeBaseButton()));
			clickOnElement("Contextual Menu", clickElement(kb.verifyContextualMenu()));
			clickOnElement("Write To Us Text on Contextual Menu", clickElement(kb.verifyWriteToUsText()));
			String expectedToEmail =getPropertyValue("ToEmail");
			if(platform.equalsIgnoreCase("android")){
				Thread.sleep(3000);
				clickElement(fm.getToEmailText());
				String actualToEmail =getText(fm.getToEmailText());
				Reporter.log(actualToEmail,true);
				if(actualToEmail.contains(expectedToEmail)) {
					extentTest.log(LogStatus.PASS, "To Email is Verified -- " +actualToEmail);
				}
				else {
					extentTest.log(LogStatus.FAIL, "To Email is Not Verified -- " +actualToEmail);
				}
				s.assertTrue(actualToEmail.contains(expectedToEmail));
			}
			s.assertAll();
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Knowledge Base Landing Screen Test Failed");
		}

	}
}
