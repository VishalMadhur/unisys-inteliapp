package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.AccessTicketDetailsPage;
import com.apd.inteliserve.pompages.OpenTicketListPage;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

/**
 * @author  VishalMadhur
 *
 */



public class IA_828 extends BaseTest{

	@Test
	@Parameters({"platform"})
	@SuppressWarnings("rawtypes")
	public void tickeDetailsTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-828, Access Tickets Details Page Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-828, Access Tickets Details Page Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			AccessTicketDetailsPage at = new AccessTicketDetailsPage(driver);
			OpenTicketListPage ot = new OpenTicketListPage(driver);
			if(elementDisplayed(ot.verifyNoTicketText())){
				extentTest.log(LogStatus.FAIL, "Ticket is Present For user But Not Displayed"); 
				extentTest.log(LogStatus.INFO, "Instead of Ticket Got Message --- " + getText(ot.verifyNoTicketText(), platform));
			}
			else if (elementDisplayed(ot.verifyTicketErrorMessage())) {
				extentTest.log(LogStatus.PASS, "Ticket is Present For user But Not Displayed Due to API or Internet Issue");
				extentTest.log(LogStatus.INFO, "Instead of Ticket Got Message --- " +getText(ot.verifyTicketErrorMessage(), platform));
			}else{
				extentTest.log(LogStatus.PASS, "Ticket is Present For User");
				if(platform.equalsIgnoreCase("ios")){
					TouchAction action = new TouchAction(driver);
					action.press(PointOption.point(16,727)).release().perform();
					action.press(PointOption.point(24,868)).release().perform();
				}
				else {
					click(at.clickFirstTicket());
				}
				extentTest.log(LogStatus.PASS, "Clicked on First Ticket");
				Thread.sleep(3000);
				if(elementIsDisplayed(at.verifyTicketHeaderText())){
					extentTest.log(LogStatus.PASS, "Ticket Header Text is Displayed");	
				}
				else{
					extentTest.log(LogStatus.FAIL, "Ticket Header Text is Not Displayed");
				}
				if(elementIsDisplayed(at.verifyTicketNumber())){
					extentTest.log(LogStatus.PASS, "Ticket Number is Displayed --- " +getText(at.verifyTicketNumber(), platform));
				}
				else{
					extentTest.log(LogStatus.FAIL, "Ticket Number is Not Displayed");
				}
				if(elementIsDisplayed(at.verifyPriorityText())){
					extentTest.log(LogStatus.PASS, "Priority  is Displayed --- " +getText(at.verifyPriorityText(), platform));
					/*if(platform.equalsIgnoreCase("android")){
					String priorityText=at.verifyPriorityText().getText();
					Reporter.log(priorityText,true);
					extentTest.log(LogStatus.INFO, "Priority Displayed is : " + priorityText);
				}*/
				}
				else{
					extentTest.log(LogStatus.FAIL, "Priority  is Not Displayed");
				}
				if(elementIsDisplayed(at.verifyStatusText())) {
					extentTest.log(LogStatus.PASS, "Status  is Displayed --- " + getText(at.verifyStatusText(), platform));
					/*if(platform.equalsIgnoreCase("android")){
					String statusText=at.verifyStatusText().getText();
					Reporter.log(statusText,true);
					extentTest.log(LogStatus.INFO, "Status Displayed is : " + statusText);
				}*/
				}
				else{
					extentTest.log(LogStatus.FAIL, "Status  is Not Displayed");
				}
				if(elementIsDisplayed(at.verifyTicketDate())){
					extentTest.log(LogStatus.PASS, "Ticket Date  is Displayed --- " +getText(at.verifyTicketDate(), platform));
				}
				else{
					extentTest.log(LogStatus.PASS, "Ticket Date  is Not Displayed");
				}

				if(elementIsDisplayed(at.verifyIncidentSummaryText())){
					extentTest.log(LogStatus.PASS, "Incident Summary Text is Displayed");
				}
				else{
					extentTest.log(LogStatus.FAIL, "Incident Summary Text is Not Displayed");;
				}
				if(elementIsDisplayed(at.verifyOpenedByText())){
					extentTest.log(LogStatus.PASS, "Opened By Text is Displayed");
				}
				else{
					extentTest.log(LogStatus.FAIL, "Opened By Text is Not Displayed");	
				}
				if(elementIsDisplayed(at.verifyCreatedOnText())){
					extentTest.log(LogStatus.PASS, "CreatedOn Text is Displayed");
				}
				else{
					extentTest.log(LogStatus.FAIL, "CreatedOn Text is Not Displayed");
				}
				try{
					at.verifyAssignedToText().isDisplayed();
					extentTest.log(LogStatus.PASS, "AssignedTo Text is Displayed");
				}catch(Exception e){
					extentTest.log(LogStatus.INFO, "AssignedTo Text is Not Displayed as Ticket is Not Assigned to Anyone");
				}
				if(elementIsDisplayed(at.verifyDescriptionText())){
					extentTest.log(LogStatus.PASS, "Description Text is Displayed");
				}
				else{
					extentTest.log(LogStatus.FAIL, "Description Text is Not Displayed");
				}
				click(at.clickBackButton());
				extentTest.log(LogStatus.PASS, "Clicked  on Back Buton");
				if(elementIsDisplayed(at.verifyHelloText())){
					extentTest.log(LogStatus.PASS, "Navigated Back to Home Page");
				}
				else{
					extentTest.log(LogStatus.FAIL, "Failed to Navigate Back to Home Page");
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Ticket Details Page Test Failed");
		}
	}
}
