package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.EPAPage;
import com.relevantcodes.extentreports.LogStatus;



public class IA_1173 extends BaseTest {
	@Test
	@Parameters({"platform"})
	public void createTicketFromAmelia(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		String createdTicket = null;
		String createdNewTicketNumber =null;
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-1173, Create Ticket From Amelia Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-1173, Create Ticket From Amelia Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			EPAPage epa = new EPAPage(driver);
			click(epa.ivaIcon());
			extentTest.log(LogStatus.PASS, "User is Able to Navigate to Amelia Page");
			waitForVisibityOfSpecificElement("Send Message Button", epa.sendMessage());
			Thread.sleep(5000);
			click(epa.sendMessage());
			click(epa.sendMessage());
			sendExcelTestData(epa.editBox(), "TestData", 13, 1);
			extentTest.log(LogStatus.INFO, "Fetched Query From Excel Sheet");
			click(epa.sendMessage());
			Thread.sleep(5000);
			extentTest.log(LogStatus.PASS, "Query Entered Successfully");
			sendExcelTestData(epa.titleEditBox(), "TestData", 14, 1);
			extentTest.log(LogStatus.PASS, "Fetched Ticket Title From Excel Sheet");
			sendExcelTestData(epa.descriptionEditBox(), "TestData", 15, 1);
			click(epa.sendMessage());
			extentTest.log(LogStatus.PASS, "Clicked on Send Message Button Successfully");
			Thread.sleep(5000);
			click(epa.createTicketButton());
			extentTest.log(LogStatus.PASS, "Clicked on Create Ticket Button");
			Thread.sleep(10000);
			click(epa.noRadioButton());
			extentTest.log(LogStatus.PASS, "Clicked on No Button");
			click(epa.chatToggleButton());
			extentTest.log(LogStatus.PASS, "Clicked on Ticket Managemnet Button");
			if(elementIsDisplayed(epa.getCreatedTicketNumber())){
				extentTest.log(LogStatus.PASS, "Ticket is Dispalyed in Ticket Management");
				createdTicket=getText(epa.getCreatedTicketNumber(),platform);
				click(epa.closeChatIcon());
				String[]strr= createdTicket.split(" ");
				createdNewTicketNumber = strr[0];
				extentTest.log(LogStatus.PASS, "Got Created Ticket from Management ----- " + createdNewTicketNumber);
			}
			else{
				extentTest.log(LogStatus.FAIL, "Ticket is Not Dispalyed in Ticket Managemnet");
			}
			Thread.sleep(5000);
			logOut(platform);
			//loginWithOutEnablingLocation();
			login(platform);
			if(platform.equalsIgnoreCase("ios")){
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
			}
			else{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
			}
			try{
				Thread.sleep(5000);
				String ticketNumber=getText(epa.verifyTicketNumText(),platform);
				if(platform.equalsIgnoreCase("ios")){
					String [] getLatestTicketNumber =ticketNumber.split(" ");
					ticketNumber=getLatestTicketNumber[5];
				}
				extentTest.log(LogStatus.INFO, "Checking if Created Ticket Appears on Top of Open Ticket List");
				if(createdNewTicketNumber.contains(ticketNumber)){
					extentTest.log(LogStatus.PASS, "Created Ticket is Displayed in Open Ticket List ----- " + ticketNumber);
				}
				else{
					extentTest.log(LogStatus.FAIL, "Created  Ticket is Not Displayed on Top of Open Ticket List  ----- " + ticketNumber);
				}
				s.assertTrue(createdNewTicketNumber.contains(ticketNumber));
				s.assertAll();
			}catch(Exception e){
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL, "No Tickets is Displayed For The User");
			}
			s.assertAll();
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Create Ticket From Amelia Test Failed");
		}
	}
}


