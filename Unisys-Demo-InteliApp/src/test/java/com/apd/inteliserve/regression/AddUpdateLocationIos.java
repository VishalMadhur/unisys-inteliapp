package com.apd.inteliserve.regression;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.EditProfileLocationPage;
import com.apd.inteliserve.pompages.EditProfilePage;
import com.apd.inteliserve.pompages.LandingPage;
import com.relevantcodes.extentreports.LogStatus;


public class AddUpdateLocationIos extends BaseTest {
	@Test
	@Parameters({"platform","language","transalationSheet"})
	public void updateProfileLocation(@Optional String platform,@Optional String language,@Optional String transalationSheet) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-2949, Add/Update Profile Location Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-3287, Add/Update Profile Location-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");

			String expectedCountryHeaderLabel =getCellValue(transalationSheet, 181, 1);
			String expectedAllCountriesLabel =getCellValue(transalationSheet, 182, 1);
			String expectedFirstCountry =getCellValue(transalationSheet, 183, 1);
			String expectedSecondCountry =getCellValue(transalationSheet, 184, 1);
			String expectedThirdCountry =getCellValue(transalationSheet, 185, 1);
			String expectedFourthCountry =getCellValue(transalationSheet, 186, 1);
			String expectedFifthCountry =getCellValue(transalationSheet, 187, 1);
			String expectedSixthCountry =getCellValue(transalationSheet, 188, 1);
			String expectedSeventhCountry =getCellValue(transalationSheet, 189, 1);
			String expectedEighthCountry =getCellValue(transalationSheet, 190, 1);
			String expectedNinthCountry =getCellValue(transalationSheet, 191, 1);
			String expectedTenthCountry =getCellValue(transalationSheet, 192, 1);
			String expectedEleventhCountry =getCellValue(transalationSheet, 193, 1);
			String expectedTwelvethCountry =getCellValue(transalationSheet, 194, 1);
			String expectedThirteenthCountry =getCellValue(transalationSheet, 195, 1);
			String expectedFourtteenthCountry =getCellValue(transalationSheet, 196, 1);
			String expectedFiftteenthCountry =getCellValue(transalationSheet, 197, 1);
			String expectedSixteenthCountry =getCellValue(transalationSheet, 198, 1);
			String expectedSeventeenthCountry =getCellValue(transalationSheet, 199, 1);
			String expectedEighteenthCountry =getCellValue(transalationSheet, 200, 1);
			String expectedNineteenthCountry =getCellValue(transalationSheet, 201, 1);
			String expectedTwentiethCountry =getCellValue(transalationSheet, 202, 1);
			String expectedTwentyFirstCountry =getCellValue(transalationSheet, 203, 1);
			String expectedTwentySecondCountry =getCellValue(transalationSheet, 204, 1);
			String expectedTwentyThirdCountry =getCellValue(transalationSheet, 205, 1);
			String expectedTwentyFourthCountry =getCellValue(transalationSheet, 206, 1);
			String expectedTwentyFifthCountry =getCellValue(transalationSheet, 207, 1);
			String expectedTwentySixthCountry =getCellValue(transalationSheet, 208, 1);
			String expectedTwentySeventhCountry =getCellValue(transalationSheet, 209, 1);
			String expectedTwentyEighthCountry =getCellValue(transalationSheet, 210, 1);
			String expectedlLocationTitleLabel=getCellValue(transalationSheet, 265, 1);
			String expectedCountryLabel=getCellValue(transalationSheet, 266, 1);
			String aexpectedAllLocationsLabel=getCellValue(transalationSheet, 267, 1);
			String expectedArgentinaFirstOffice=getCellValue(transalationSheet, 211, 1);
			String expectedArgentinaSecondOffice=getCellValue(transalationSheet, 212, 1);
			String expectedArgentinaThirdOffice=getCellValue(transalationSheet, 213, 1);
			String expectedArgentinaFirstCity=getCellValue(transalationSheet, 214, 1);
			String expectedArgentinaSecondCity=getCellValue(transalationSheet, 215, 1);
			String expectedArgentinaThirdCity=getCellValue(transalationSheet, 216, 1);
			String expectedAustraliaFirstOffice=getCellValue(transalationSheet, 217, 1);
			String expectedAustraliaSecondOffice=getCellValue(transalationSheet, 218, 1);
			String expectedAustraliaFirstCity=getCellValue(transalationSheet, 219, 1);
			String expectedAustraliaSecondCity=getCellValue(transalationSheet, 220, 1);
			String expectedAustriaFirstOffice=getCellValue(transalationSheet, 221, 1);
			String expectedAustriaFirstCity=getCellValue(transalationSheet, 222, 1);
			String expectedBelgiumFirstOffice=getCellValue(transalationSheet, 223, 1);
			String expectedBelgiumFirstCity=getCellValue(transalationSheet, 224, 1);
			String expectedBrazilFirstOffice=getCellValue(transalationSheet, 225, 1);
			String expectedBrazilSecondOffice=getCellValue(transalationSheet, 226, 1);
			String expectedBrazilThirdOffice=getCellValue(transalationSheet, 227, 1);
			String expectedBrazilFourthOffice=getCellValue(transalationSheet, 228, 1);
			String expectedBrazilFifthOffice=getCellValue(transalationSheet, 229, 1);
			String expectedBrazilSixthOffice=getCellValue(transalationSheet, 230, 1);
			String expectedBrazilSeventhOffice=getCellValue(transalationSheet, 231, 1);
			String expectedBrazilEighthOffice=getCellValue(transalationSheet, 232, 1);
			String expectedBrazilFirstCity=getCellValue(transalationSheet, 233, 1);
			String expectedBrazilSecondCity=getCellValue(transalationSheet, 234, 1);
			String expectedBrazilThirdCity=getCellValue(transalationSheet, 235, 1);
			String expectedBrazilFourthCity=getCellValue(transalationSheet, 236, 1);
			String expectedBrazilFifthCity=getCellValue(transalationSheet, 237, 1);
			String expectedBrazilSixthCity=getCellValue(transalationSheet, 238, 1);
			String expectedBrazilSeventhCity=getCellValue(transalationSheet, 239, 1);
			String expectedBrazilEighthCity=getCellValue(transalationSheet, 240, 1);
			String expectedCanadaFirstOffice=getCellValue(transalationSheet, 241, 1);
			String expectedCanadaSecondOffice=getCellValue(transalationSheet, 242, 1);
			String expectedCanadaFirstCity=getCellValue(transalationSheet, 243, 1);
			String expectedCanadaSecondCity=getCellValue(transalationSheet, 244, 1);
			String expectedChinaFirstOffice=getCellValue(transalationSheet, 245, 1);
			String expectedChinaSecondOffice=getCellValue(transalationSheet, 246, 1);
			String expectedChinaThirdOffice=getCellValue(transalationSheet, 247, 1);
			String expectedChinaFourthOffice=getCellValue(transalationSheet, 248, 1);
			String expectedChinaFifthOffice=getCellValue(transalationSheet, 249, 1);
			String expectedChinaFirstCity=getCellValue(transalationSheet, 250, 1);
			String expectedChinaSecondCity=getCellValue(transalationSheet, 251, 1);
			String expectedChinaThirdCity=getCellValue(transalationSheet, 252, 1);
			String expectedChinaFourthCity=getCellValue(transalationSheet, 253, 1);
			String expectedChinaFifthCity=getCellValue(transalationSheet, 254, 1);
			String expectedColombiaFirstOffice=getCellValue(transalationSheet, 255, 1);
			String expectedColombiaSecondOffice=getCellValue(transalationSheet, 256, 1);
			String expectedColombiaThirdOffice=getCellValue(transalationSheet, 257, 1);
			String expectedColombiaFirstCity=getCellValue(transalationSheet, 258, 1);
			String expectedColombiaSecondCity=getCellValue(transalationSheet, 259, 1);
			String expectedColombiaThirdCity=getCellValue(transalationSheet, 260, 1);
			String expectedCostaRicaFirstOffice=getCellValue(transalationSheet, 261, 1);
			String expectedCostaRicaFirstCity=getCellValue(transalationSheet, 262, 1);
			String expectedFranceFirstOffice=getCellValue(transalationSheet, 263, 1);
			String expectedFranceFirstCity=getCellValue(transalationSheet, 264, 1);
			String expectedGermanyFirstOffice=getCellValue(transalationSheet, 268, 1);
			String expectedGermanySecondOffice=getCellValue(transalationSheet, 269, 1);
			String expectedGermanyThirdOffice=getCellValue(transalationSheet, 270, 1);
			String expectedGermanyFirstCity=getCellValue(transalationSheet, 271, 1);
			String expectedGermanySecondCity=getCellValue(transalationSheet, 272, 1);
			String expectedGermanyThirdCity=getCellValue(transalationSheet, 273, 1);
			String expectedHongKongFirstOffice=getCellValue(transalationSheet, 274, 1);
			String expectedHongKongFirstCity=getCellValue(transalationSheet, 275, 1);
			String expectedHungaryFirstOffice=getCellValue(transalationSheet, 276, 1);
			String expectedHungarySecondOffice=getCellValue(transalationSheet, 277, 1);
			String expectedHungaryThirdOffice=getCellValue(transalationSheet, 278, 1);
			String expectedHungaryFirstCity=getCellValue(transalationSheet, 279, 1);
			String expectedHungarySecondCity=getCellValue(transalationSheet, 280, 1);
			String expectedHungaryThirdCity=getCellValue(transalationSheet, 281, 1);
			String expectedIndiaFirstOffice=getCellValue(transalationSheet, 282, 1);
			String expectedIndiaSecondOffice=getCellValue(transalationSheet, 283, 1);
			String expectedIndiaThirdOffice=getCellValue(transalationSheet, 284, 1);
			String expectedIndiaFourthOffice=getCellValue(transalationSheet, 285, 1);
			String expectedIndiaFirstCity=getCellValue(transalationSheet, 286, 1);
			String expectedIndiaSecondCity=getCellValue(transalationSheet, 287, 1);
			String expectedIndiaThirdCity=getCellValue(transalationSheet, 288, 1);
			String expectedIndiaFourthCity=getCellValue(transalationSheet, 289, 1);
			String expectedJapanFirstOffice=getCellValue(transalationSheet, 290, 1);
			String expectedJapanFirstCity=getCellValue(transalationSheet, 291, 1);
			String expectedLuxembourgFirstOffice=getCellValue(transalationSheet, 292, 1);
			String expectedLuxembourgFirstCity=getCellValue(transalationSheet, 293, 1);
			String expectedMalaysiaFirstOffice=getCellValue(transalationSheet, 294, 1);
			String expectedMalaysiaSecondOffice=getCellValue(transalationSheet, 295, 1);
			String expectedMalaysiaThirdOffice=getCellValue(transalationSheet, 296, 1);
			String expectedMalaysiaFourthOffice=getCellValue(transalationSheet, 297, 1);
			String expectedMalaysiaFifthOffice=getCellValue(transalationSheet, 298, 1);
			String expectedMalaysiaSixthOffice=getCellValue(transalationSheet, 299, 1);
			String expectedMalaysiaSeventhOffice=getCellValue(transalationSheet, 300, 1);
			String expectedMalaysiaEighthOffice=getCellValue(transalationSheet, 301, 1);
			String expectedMalaysiaFirstCity=getCellValue(transalationSheet, 302, 1);
			String expectedMalaysiaSecondCity=getCellValue(transalationSheet, 303, 1);
			String expectedMalaysiaThirdCity=getCellValue(transalationSheet, 304, 1);
			String expectedMalaysiaFourthCity=getCellValue(transalationSheet, 305, 1);
			String expectedMalaysiaFifthCity=getCellValue(transalationSheet, 306, 1);
			String expectedMalaysiaSixthCity=getCellValue(transalationSheet, 307, 1);
			String expectedMalaysiaSeventhCity=getCellValue(transalationSheet, 308, 1);
			String expectedMalaysiaEighthCity=getCellValue(transalationSheet, 309, 1);
			String expectedMexicoFirstOffice=getCellValue(transalationSheet, 310, 1);
			String expectedMexicoFirstCity=getCellValue(transalationSheet, 311, 1);
			String expectedNetherlandsFirstOffice=getCellValue(transalationSheet, 312, 1);
			String expectedNetherlandsSecondOffice=getCellValue(transalationSheet, 313, 1);
			String expectedNetherlandsFirstCity=getCellValue(transalationSheet, 314, 1);
			String expectedNetherlandsSecondCity=getCellValue(transalationSheet, 315, 1);
			String expectedNewZealandFirstOffice=getCellValue(transalationSheet, 316, 1);
			String expectedNewZealandSecondOffice=getCellValue(transalationSheet, 317, 1);
			String expectedNewZealandThirdOffice=getCellValue(transalationSheet, 318, 1);
			String expectedNewZealandFirstCity=getCellValue(transalationSheet, 319, 1);
			String expectedNewZealandSecondCity=getCellValue(transalationSheet, 320, 1);
			String expectedNewZealandThirdCity=getCellValue(transalationSheet, 321, 1);
			String expectedPeruFirstOffice=getCellValue(transalationSheet, 322, 1);
			String expectedPeruFirstCity=getCellValue(transalationSheet, 323, 1);
			String expectedPhilippinesFirstOffice=getCellValue(transalationSheet, 324, 1);
			String expectedPhilippinesSecondOffice=getCellValue(transalationSheet, 325, 1);
			String expectedPhilippinesThirdOffice=getCellValue(transalationSheet, 326, 1);
			String expectedPhilippinesFourthOffice=getCellValue(transalationSheet, 327, 1);
			String expectedPhilippinesFifthOffice=getCellValue(transalationSheet, 328, 1);
			String expectedPhilippinesFirstCity=getCellValue(transalationSheet, 329, 1);
			String expectedPhilippinesSecondCity=getCellValue(transalationSheet, 330, 1);
			String expectedPhilippinesThirdCity=getCellValue(transalationSheet, 331, 1);
			String expectedPhilippinesFourthCity=getCellValue(transalationSheet, 332, 1);
			String expectedPhilippinesFifthCity=getCellValue(transalationSheet, 333, 1);
			String expectedSingaporeFirstOffice=getCellValue(transalationSheet, 334, 1);
			String expectedSingaporeFirstCity=getCellValue(transalationSheet, 335, 1);
			String expectedSpainFirstOffice=getCellValue(transalationSheet, 336, 1);
			String expectedSpainSecondOffice=getCellValue(transalationSheet, 337, 1);
			String expectedSpainFirstCity=getCellValue(transalationSheet, 338, 1);
			String expectedSpainSecondCity=getCellValue(transalationSheet, 339, 1);
			String expectedSwitzerlandFirstOffice=getCellValue(transalationSheet, 340, 1);
			String expectedSwitzerlandSecondOffice=getCellValue(transalationSheet, 341, 1);
			String expectedSwitzerlandThirdOffice=getCellValue(transalationSheet, 342, 1);
			String expectedSwitzerlandFirstCity=getCellValue(transalationSheet, 343, 1);
			String expectedSwitzerlandSecondCity=getCellValue(transalationSheet, 344, 1);
			String expectedSwitzerlandThirdCity=getCellValue(transalationSheet, 345, 1);
			String expectedTaiwanFirstOffice=getCellValue(transalationSheet, 346, 1);
			String expectedTaiwanSecondOffice=getCellValue(transalationSheet, 347, 1);
			String expectedTaiwanThirdOffice=getCellValue(transalationSheet, 348, 1);
			String expectedTaiwanFirstCity=getCellValue(transalationSheet, 349, 1);
			String expectedTaiwanSecondCity=getCellValue(transalationSheet, 350, 1);
			String expectedTaiwanThirdCity=getCellValue(transalationSheet, 351, 1);
			String expectedUnitedKingdomFirstOffice=getCellValue(transalationSheet, 352, 1);
			String expectedUnitedKingdomSecondOffice=getCellValue(transalationSheet, 353, 1);
			String expectedUnitedKingdomFirstCity=getCellValue(transalationSheet, 354, 1);
			String expectedUnitedKingdomSecondCity=getCellValue(transalationSheet, 355, 1);
			String expectedUSAFirstOffice=getCellValue(transalationSheet, 356, 1);
			String expectedUSASecondOffice=getCellValue(transalationSheet, 357, 1);
			String expectedUSAThirdOffice=getCellValue(transalationSheet, 358, 1);
			String expectedUSAFourthOffice=getCellValue(transalationSheet, 359, 1);
			String expectedUSAFifthOffice=getCellValue(transalationSheet, 360, 1);
			String expectedUSASixthOffice=getCellValue(transalationSheet, 361, 1);
			String expectedUSASeventhOffice=getCellValue(transalationSheet, 362, 1);
			String expectedUSAEighthOffice=getCellValue(transalationSheet, 363, 1);
			String expectedUSANinthOffice=getCellValue(transalationSheet, 364, 1);
			String expectedUSATenthOffice=getCellValue(transalationSheet, 365, 1);
			String expectedUSAEleventhOffice=getCellValue(transalationSheet, 366, 1);
			String expectedUSATwevelthOffice=getCellValue(transalationSheet, 367, 1);
			String expectedUSAThirteenthOffice=getCellValue(transalationSheet, 368, 1);
			String expectedUSAFourteenthOffice=getCellValue(transalationSheet, 369, 1);
			String expectedUSAFirstCity=getCellValue(transalationSheet,370 , 1);
			String expectedUSASecondCity=getCellValue(transalationSheet, 371, 1);
			String expectedUSAThirdCity=getCellValue(transalationSheet, 372, 1);
			String expectedUSAFourthCity=getCellValue(transalationSheet, 373, 1);
			String expectedUSAFifthCity=getCellValue(transalationSheet, 374, 1);
			String expectedUSASixthCity=getCellValue(transalationSheet, 375, 1);
			String expectedUSASeventhCity=getCellValue(transalationSheet, 376, 1);
			String expectedUSAEighthCity=getCellValue(transalationSheet, 377, 1);
			String expectedUSANinthCity=getCellValue(transalationSheet, 378, 1);
			String expectedUSATenthCity=getCellValue(transalationSheet, 379, 1);
			String expectedUSAEleventhCity=getCellValue(transalationSheet, 380, 1);
			String expectedUSATwevelthCity=getCellValue(transalationSheet, 381, 1);
			String expectedUSAThirteenthCity=getCellValue(transalationSheet, 382, 1);
			String expectedUSAFourteenthCity=getCellValue(transalationSheet, 383, 1);


			LandingPage ld = new LandingPage(driver);
			EditProfilePage ep = new EditProfilePage(driver);
			EditProfileLocationPage el = new EditProfileLocationPage(driver);

			clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
			String profilePageLocation=getText(el.verifyProfilePageLocation(), platform);
			String actualProfileLocation[]=profilePageLocation.split(" ", 2);
			profilePageLocation=actualProfileLocation[1];
			clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
			clickOnElement("Location", clickElement(ep.verifyProfileLocation()));
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));

			String actualCountryHeaderLabel =getText(el.verifyLocationHeader(),platform);
			String actualAllCountriesLabel =getText(el.verifyLocationSubHeader(),platform);
			String actualFirstCountry =getText(el.verifyFirstCountry(),platform);
			String actualSecondCountry =getText(el.verifySecondCountry(),platform);
			String actualThirdCountry =getText(el.verifyThirdCountry(),platform);
			String actualFourthCountry =getText(el.verifyFourthCountry(),platform);
			String actualFifthCountry =getText(el.verifyFifthCountry(),platform);
			String actualSixthCountry =getText(el.verifySixthCountry(),platform);
			String actualSeventhCountry =getText(el.verifySeventhCountry(),platform);
			String actualEighthCountry =getText(el.verifyEighthCountry(),platform);;
			String actualNinthCountry =getText(el.verifyNinethCountry(),platform);
			String actualTenthCountry =getText(el.verifyTenthCountry(),platform);
			String actualEleventhCountry =getText(el.verifyEleventhCountry(),platform);
			String actualTwelvethCountry =getText(el.verifyTwelevethCountry(),platform);
			String actualThirteenthCountry =getText(el.verifyThirteenthCountry(),platform);
			////scroll(platform, "Spain");
			//Thread.sleep(3000);
			String actualFourtteenthCountry =getText(el.verifyFourteenthCountry(),platform);
			String actualFiftteenthCountry =getText(el.verifyFifteenthCountry(),platform);
			String actualSixteenthCountry =getText(el.verifySixteenthCountry(),platform);
			String actualSeventeenthCountry =getText(el.verifySeventeenthCountry(),platform);
			String actualEighteenthCountry =getText(el.verifyEighteenthCountry(),platform);;
			String actualNineteenthCountry =getText(el.verifyNineteenthCountry(),platform);
			String actualTwentiethCountry =getText(el.verifyTwenteithCountry(),platform);
			String actualTwentyFirstCountry =getText(el.verifyTwentyFirstCountry(),platform);
			String actualTwentySecondCountry =getText(el.verifyTwentySecondCountry(),platform);
			String actualTwentyThirdCountry =getText(el.verifyTwentyThirdCountry(),platform);
			String actualTwentyFourthCountry =getText(el.verifyTwentyFourthCountry(),platform);
			////scroll(platform, "United Kingdom");
			////scroll(platform, "United states of America");
			//Thread.sleep(3000);
			String actualTwentyFifthCountry =getText(el.verifyTwentyFifthCountry(),platform);
			String actualTwentySixthCountry =getText(el.verifyTwentySixthCountry(),platform);
			String actualTwentySeventhCountry =getText(el.verifyTwentySeventhCountry(),platform);
			String actualTwentyEighthCountry =getText(el.verifyTwentyEighthCountry(),platform);

			verifyLabel("Location Header", containsData(expectedCountryHeaderLabel,actualCountryHeaderLabel));
			verifyLabel("COUNTRIES", containsData(expectedAllCountriesLabel,actualAllCountriesLabel));
			verifyLabel("Argentina", containsData(expectedFirstCountry,actualFirstCountry));
			verifyLabel("Australia", containsData(expectedSecondCountry,actualSecondCountry));
			verifyLabel("Austria", containsData(expectedThirdCountry,actualThirdCountry));
			verifyLabel("Belgium", containsData(expectedFourthCountry,actualFourthCountry));
			verifyLabel("Brazil", containsData(expectedFifthCountry,actualFifthCountry));
			verifyLabel("Canada", containsData(expectedSixthCountry,actualSixthCountry));
			verifyLabel("China", containsData(expectedSeventhCountry,actualSeventhCountry));
			verifyLabel("Colombia", containsData(expectedEighthCountry,actualEighthCountry));
			verifyLabel("Costa Rica", containsData(expectedNinthCountry,actualNinthCountry));
			verifyLabel("France", containsData(expectedTenthCountry,actualTenthCountry));
			verifyLabel("Germany", containsData(expectedEleventhCountry,actualEleventhCountry));
			verifyLabel("Hong Kong", containsData(expectedTwelvethCountry,actualTwelvethCountry));
			verifyLabel("Hungary", containsData(expectedThirteenthCountry,actualThirteenthCountry));
			verifyLabel("India", containsData(expectedFourtteenthCountry,actualFourtteenthCountry));
			verifyLabel("Japan", containsData(expectedFiftteenthCountry,actualFiftteenthCountry));
			verifyLabel("Luxembourg", containsData(expectedSixteenthCountry,actualSixteenthCountry));
			verifyLabel("Malaysia", containsData(expectedSeventeenthCountry,actualSeventeenthCountry));
			verifyLabel("Mexico", containsData(expectedEighteenthCountry,actualEighteenthCountry));
			verifyLabel("Netherlands", containsData(expectedNineteenthCountry,actualNineteenthCountry));
			verifyLabel("New Zealand", containsData(expectedTwentiethCountry,actualTwentiethCountry));
			verifyLabel("Peru", containsData(expectedTwentyFirstCountry,actualTwentyFirstCountry));
			verifyLabel("Philippines", containsData(expectedTwentySecondCountry,actualTwentySecondCountry));
			verifyLabel("Singapore", containsData(expectedTwentyThirdCountry,actualTwentyThirdCountry));
			verifyLabel("Spain", containsData(expectedTwentyFourthCountry,actualTwentyFourthCountry));
			verifyLabel("Switzerland", containsData(expectedTwentyFifthCountry,actualTwentyFifthCountry));
			verifyLabel("Taiwan", containsData(expectedTwentySixthCountry,actualTwentySixthCountry));
			verifyLabel("United Kingdom", containsData(expectedTwentySeventhCountry,actualTwentySeventhCountry));
			verifyLabel("United states of America", containsData(expectedTwentyEighthCountry,actualTwentyEighthCountry));
			verifyElement("Back Icon", elementDisplayed(el.verifyCountryPageBackIcon()));
			clickOnElement("Back Icon", clickElement(el.verifyCountryPageBackIcon()));
			verifyElement("Office Selection Screen", elementDisplayed(el.verifyAllLocationsLabel()));
			String actualLocationTitleLabel=getText(el.verifyLocationTitle(),platform);
			String actualCountryLabel=getText(el.verifyCountryLabel(),platform);
			String actualAllLocationsLabel=getText(el.verifyAllLocationsLabel(),platform);
			verifyLabel("Location", containsData(expectedlLocationTitleLabel,actualLocationTitleLabel));
			verifyLabel("COUNTRY", containsData(expectedCountryLabel,actualCountryLabel));
			verifyLabel("All LOCATIONS", containsData(aexpectedAllLocationsLabel,actualAllLocationsLabel));
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("First Country", clickElement(el.verifyFirstCountry()));
			String actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedFirstCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualArgentinaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualArgentinaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualArgentinaThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualArgentinaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualArgentinaSecondCity=getText(el.verifySecondCity(),platform);
				String actualArgentinaThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("Av. Alicia Moreau de Justo ", containsData(actualArgentinaFirstOffice,expectedArgentinaFirstOffice));
				verifyLabel("Primitivo de la Reta ", containsData(actualArgentinaSecondOffice,expectedArgentinaSecondOffice));
				verifyLabel("Calle CÃ³rdoba", containsData(actualArgentinaThirdOffice,expectedArgentinaThirdOffice));
				verifyLabel("Buenos Aires", containsData(actualArgentinaFirstCity,expectedArgentinaFirstCity));
				verifyLabel("Mendoza", containsData(actualArgentinaSecondCity,expectedArgentinaSecondCity));
				verifyLabel("Parana", containsData(actualArgentinaThirdCity,expectedArgentinaThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedFirstCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Second Country", clickElement(el.verifySecondCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedSecondCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualAustraliaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualAustraliaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualAustraliaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualAustraliaSecondCity=getText(el.verifySecondCity(),platform);
				verifyLabel("Moore Street", containsData(actualAustraliaFirstOffice,expectedAustraliaFirstOffice));
				verifyLabel("Homebush Bay Drive", containsData(actualAustraliaSecondOffice,expectedAustraliaSecondOffice));
				verifyLabel("Canberra", containsData(actualAustraliaFirstCity,expectedAustraliaFirstCity));
				verifyLabel("Sydney", containsData(actualAustraliaSecondCity,expectedAustraliaSecondCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedSecondCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Third Country", clickElement(el.verifyThirdCountry()));
			Thread.sleep(2000);
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedThirdCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualAustriaFirstOffice=getText(el.getFirstOffice(),platform);
				String actualAustriaFirstCity=getText(el.getFirstCity(),platform);
				verifyLabel("Andromeda Tower", containsData(actualAustriaFirstOffice,expectedAustriaFirstOffice));
				verifyLabel("Vienna", containsData(actualAustriaFirstCity,expectedAustriaFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedThirdCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Fourth Country", clickElement(el.verifyFourthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedFourthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualBelgiumFirstOffice=getText(el.getFirstOffice(),platform);
				String actualBelgiumFirstCity=getText(el.getFirstCity(),platform);
				verifyLabel("Telecomlaan", containsData(actualBelgiumFirstOffice,expectedBelgiumFirstOffice));
				verifyLabel("Diegem", containsData(actualBelgiumFirstCity,expectedBelgiumFirstCity));
			}else{
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedFourthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Fifth Country", clickElement(el.verifyFifthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedFifthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualBrazilFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualBrazilSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualBrazilThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualBrazilFourthOffice=getText(el.verifyFourthOffice(),platform);
				String actualBrazilFifthOffice=getText(el.verifyFifthOffice(),platform);
				String actualBrazilSixthOffice=getText(el.verifySixthOffice(),platform);
				String actualBrazilSeventhOffice=getText(el.verifySeventhOffice(),platform);
				String actualBrazilEighthOffice=getText(el.verifyEighthOffice(),platform);
				String actualBrazilFirstCity=getText(el.verifyFirstCity(),platform);
				String actualBrazilSecondCity=getText(el.verifySecondCity(),platform);
				String actualBrazilThirdCity=getText(el.verifyThirdCity(),platform);
				String actualBrazilFourthCity=getText(el.verifyFourthCity(),platform);
				String actualBrazilFifthCity=getText(el.verifyFifthCity(),platform);
				String actualBrazilSixthCity=getText(el.verifySixthCity(),platform);
				String actualBrazilSeventhCity=getText(el.verifySeventhCity(),platform);
				String actualBrazilEighthCity=getText(el.verifyEighthCity(),platform);
				verifyLabel("SCN - Quadra 4", containsData(actualBrazilFirstOffice,expectedBrazilFirstOffice));
				verifyLabel("DH3 - condominio Techno Park", containsData(actualBrazilSecondOffice,expectedBrazilSecondOffice));
				verifyLabel("Rua Antonio Maria Coelho", containsData(actualBrazilThirdOffice,expectedBrazilThirdOffice));
				verifyLabel("Alameda Oscar Niemeyer", containsData(actualBrazilFourthOffice,expectedBrazilFourthOffice));
				verifyLabel("Rua da Gloria", containsData(actualBrazilFifthOffice,expectedBrazilFifthOffice));
				verifyLabel("Morumbi - Centro Corporativo", containsData(actualBrazilSixthOffice,expectedBrazilSixthOffice));
				verifyLabel("Sao Paulo - Birmann - Centro Operacional", containsData(actualBrazilSeventhOffice,expectedBrazilSeventhOffice));
				verifyLabel("Embu - Logistic Center", containsData(actualBrazilEighthOffice,expectedBrazilEighthOffice));
				verifyLabel("Brasilia", containsData(actualBrazilFirstCity,expectedBrazilFirstCity));
				verifyLabel("Campinas", containsData(actualBrazilSecondCity,expectedBrazilSecondCity));
				verifyLabel("Campo Grande", containsData(actualBrazilThirdCity,expectedBrazilThirdCity));
				verifyLabel("Nova Lima", containsData(actualBrazilFourthCity,expectedBrazilFourthCity));
				verifyLabel("Rio de Janeiro", containsData(actualBrazilFifthCity,expectedBrazilFifthCity));
				verifyLabel("Sao Paulo", containsData(actualBrazilSixthCity,expectedBrazilSixthCity));
				verifyLabel("Sao Paulo", containsData(actualBrazilSeventhCity,expectedBrazilSeventhCity));
				verifyLabel("Sao Paulo", containsData(actualBrazilEighthCity,expectedBrazilEighthCity));
			}else{
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedFifthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Sixth Country", clickElement(el.verifySixthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedSixthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualCanadaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualCanadaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualCanadaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualCanadaSecondCity=getText(el.verifySecondCity(),platform);
				verifyLabel("CIBC Building", containsData(actualCanadaFirstOffice,expectedCanadaFirstOffice));
				verifyLabel("270 Albert St.", containsData(actualCanadaSecondOffice,expectedCanadaSecondOffice));
				verifyLabel("Halifax", containsData(actualCanadaFirstCity,expectedCanadaFirstCity));
				verifyLabel("Ottawa", containsData(actualCanadaSecondCity,expectedCanadaSecondCity));
			}else{
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedSixthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Seventh Country", clickElement(el.verifySeventhCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedSeventhCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualChinaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualChinaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualChinaThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualChinaFourthOffice=getText(el.verifyFourthOffice(),platform);
				String actualChinaFifthOffice=getText(el.verifyFifthOffice(),platform);
				String actualChinaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualChinaSecondCity=getText(el.verifySecondCity(),platform);
				String actualChinaThirdCity=getText(el.verifyThirdCity(),platform);
				String actualChinaFourthCity=getText(el.verifyFourthCity(),platform);
				String actualChinaFifthCity=getText(el.verifyFifthCity(),platform);
				verifyLabel("Oriental Plaza", containsData(actualChinaFirstOffice,expectedChinaFirstOffice));
				verifyLabel("Silver Court", containsData(actualChinaSecondOffice,expectedChinaSecondOffice));
				verifyLabel("Zhangjiang Hi-Tech Area", containsData(actualChinaThirdOffice,expectedChinaThirdOffice));
				verifyLabel("Anlian Centre", containsData(actualChinaFourthOffice,expectedChinaFourthOffice));
				verifyLabel("Ginza Mansion", containsData(actualChinaFifthOffice,expectedChinaFifthOffice));
				verifyLabel("Beijing", containsData(actualChinaFirstCity,expectedChinaFirstCity));
				verifyLabel("Shanghai", containsData(actualChinaSecondCity,expectedChinaSecondCity));
				verifyLabel("Shanghai", containsData(actualChinaThirdCity,expectedChinaThirdCity));
				verifyLabel("Shenzhen", containsData(actualChinaFourthCity,expectedChinaFourthCity));
				verifyLabel("Tianjin", containsData(actualChinaFifthCity,expectedChinaFifthCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedSeventhCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Eighth Country", clickElement(el.verifyEighthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedEighthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualColombiaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualColombiaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualColombiaThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualColombiaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualColombiaSecondCity=getText(el.verifySecondCity(),platform);
				String actualColombiaThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("Bogota Distrito Capital de BogotÃ¡ 0", containsData(actualColombiaFirstOffice,expectedColombiaFirstOffice));
				verifyLabel("Medellin Antioquia", containsData(actualColombiaSecondOffice,expectedColombiaSecondOffice));
				verifyLabel("Vereda Chachafruto Zona Franca", containsData(actualColombiaThirdOffice,expectedColombiaThirdOffice));
				verifyLabel("Bogota", containsData(actualColombiaFirstCity,expectedColombiaFirstCity));
				verifyLabel("Medellin", containsData(actualColombiaSecondCity,expectedColombiaSecondCity));
				verifyLabel("Rionegro", containsData(actualColombiaThirdCity,expectedColombiaThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedEighthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Nineth Country", clickElement(el.verifyNinethCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedNinthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualCostaRicaFirstOffice=getText(el.getFirstOffice(),platform);
				String actualCostaRicaFirstCity=getText(el.getFirstCity(),platform);
				verifyLabel("Av. Alicia Moreau de Justo ", containsData(actualCostaRicaFirstOffice,expectedCostaRicaFirstOffice));
				verifyLabel("Mendoza", containsData(actualCostaRicaFirstCity,expectedCostaRicaFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedNinthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Tenth Country", clickElement(el.verifyTenthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualFranceFirstOffice=getText(el.getFirstOffice(),platform);
				String actualFranceFirstCity=getText(el.getFirstCity(),platform);
				verifyLabel("Tour Nova", containsData(actualFranceFirstOffice,expectedFranceFirstOffice));
				verifyLabel("Colombes", containsData(actualFranceFirstCity,expectedFranceFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Eleventh Country", clickElement(el.verifyEleventhCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedEleventhCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualGermanyFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualGermanySecondOffice=getText(el.verifySecondOffice(),platform);
				String actualGermanyThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualGermanyFirstCity=getText(el.verifyFirstCity(),platform);
				String actualGermanySecondCity=getText(el.verifySecondCity(),platform);
				String actualGermanyThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("Henkelstrasse", containsData(actualGermanyFirstOffice,expectedGermanyFirstOffice));
				verifyLabel("Philipp-Reis-Str. 2", containsData(actualGermanySecondOffice,expectedGermanySecondOffice));
				verifyLabel("Pelkovenstrasse", containsData(actualGermanyThirdOffice,expectedGermanyThirdOffice));
				verifyLabel("DÃ¼sseldorf", containsData(actualGermanyFirstCity,expectedGermanyFirstCity));
				verifyLabel("Hattersheim", containsData(actualGermanySecondCity,expectedGermanySecondCity));
				verifyLabel("Munich", containsData(actualGermanyThirdCity,expectedGermanyThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedEleventhCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Twelveth Country", clickElement(el.verifyTwelevethCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwelvethCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualHongKongFirstOffice=getText(el.getFirstOffice(),platform);
				String actualHongKongFirstCity=getText(el.getFirstCity(),platform);
				verifyLabel("Manulife Financial Centre", containsData(actualHongKongFirstOffice,expectedHongKongFirstOffice));
				verifyLabel("Hong Kong", containsData(actualHongKongFirstCity,expectedHongKongFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwelvethCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			clickOnElement("Thirteenth Country", clickElement(el.verifyThirteenthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedThirteenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualHungaryFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualHungarySecondOffice=getText(el.verifySecondOffice(),platform);
				String actualHungaryThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualHungaryFirstCity=getText(el.verifyFirstCity(),platform);
				String actualHungarySecondCity=getText(el.verifySecondCity(),platform);
				String actualHungaryThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("Westend City Centre ", containsData(actualHungaryFirstOffice,expectedHungaryFirstOffice));
				verifyLabel("Budapest One", containsData(actualHungarySecondOffice,expectedHungarySecondOffice));
				verifyLabel("Vince", containsData(actualHungaryThirdOffice,expectedHungaryThirdOffice));
				verifyLabel("Budapest", containsData(actualHungaryFirstCity,expectedHungaryFirstCity));
				verifyLabel("Budapest", containsData(actualHungarySecondCity,expectedHungarySecondCity));
				verifyLabel("Pecs", containsData(actualHungaryThirdCity,expectedHungaryThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedThirteenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			////scroll(platform, "Singapore");
			////scroll(platform, "Spain");
			//Thread.sleep(4000);
			clickOnElement("Fourteenth Country", clickElement(el.verifyFourteenthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedFourtteenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualIndiaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualIndiaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualIndiaThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualIndiaFourthOffice=getText(el.verifyFourthOffice(),platform);
				String actualIndiaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualIndiaSecondCity=getText(el.verifySecondCity(),platform);
				String actualIndiaThirdCity=getText(el.verifyThirdCity(),platform);
				String actualIndiaFourthCity=getText(el.verifyFourthCity(),platform);
				verifyLabel("Gopalan Global Axis", containsData(actualIndiaFirstOffice,expectedIndiaFirstOffice));
				verifyLabel("Rajajinagar IT Park", containsData(actualIndiaSecondOffice,expectedIndiaSecondOffice));
				verifyLabel("RGA Tech Park", containsData(actualIndiaThirdOffice,expectedIndiaThirdOffice));
				verifyLabel("DLF Cyber City", containsData(actualIndiaFourthOffice,expectedIndiaFourthOffice));
				verifyLabel("Bangalore", containsData(actualIndiaFirstCity,expectedIndiaFirstCity));
				verifyLabel("Bangalore", containsData(actualIndiaSecondCity,expectedIndiaSecondCity));
				verifyLabel("Bangalore", containsData(actualIndiaThirdCity,expectedIndiaThirdCity));
				verifyLabel("Hyderabad", containsData(actualIndiaFourthCity,expectedIndiaFourthCity));
			}else{
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedFourtteenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//	//scroll(platform, "Singapore");
			//	//scroll(platform, "Spain");
			//	Thread.sleep(3000);
			clickOnElement("Fifteen Country", clickElement(el.verifyFifteenthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedFiftteenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualJapanFirstOffice=getText(el.getFirstOffice(),platform);
				String actualJapanFirstCity=getText(el.getFirstCity(),platform);
				verifyLabel("Marunouchi 2 chome Bldg", containsData(actualJapanFirstOffice,expectedJapanFirstOffice));
				verifyLabel("Tokyo", containsData(actualJapanFirstCity,expectedJapanFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedFiftteenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//			//scroll(platform, "Spain");
			//			//scroll(platform, "Spain");
			//			Thread.sleep(3000);
			clickOnElement("Sixteenth Country", clickElement(el.verifySixteenthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedSixteenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualLuxembourgFirstOffice=getText(el.getFirstOffice(),platform);
				String actualLuxembourgFirstCity=getText(el.getFirstCity(),platform);
				verifyLabel("Route des Trois Cantons", containsData(actualLuxembourgFirstOffice,expectedLuxembourgFirstOffice));
				verifyLabel("Luxembourg", containsData(actualLuxembourgFirstCity,expectedLuxembourgFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedSixteenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//			//scroll(platform, "Singapore");
			//			//scroll(platform, "Spain");
			//			Thread.sleep(4000);
			clickOnElement("Seventeenth", clickElement(el.verifySeventeenthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedSeventeenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualMalaysiaFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualMalaysiaSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualMalaysiaThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualMalaysiaFourthOffice=getText(el.verifyFourthOffice(),platform);
				String actualMalaysiaFifthOffice=getText(el.verifyFifthOffice(),platform);
				String actualMalaysiaSixthOffice=getText(el.verifySixthOffice(),platform);
				String actualMalaysiaSeventhOffice=getText(el.verifySeventhOffice(),platform);
				String actualMalaysiaEighthOffice=getText(el.verifyEighthOffice(),platform);
				String actualMalaysiaFirstCity=getText(el.verifyFirstCity(),platform);
				String actualMalaysiaSecondCity=getText(el.verifySecondCity(),platform);
				String actualMalaysiaThirdCity=getText(el.verifyThirdCity(),platform);
				String actualMalaysiaFourthCity=getText(el.verifyFourthCity(),platform);
				String actualMalaysiaFifthCity=getText(el.verifyFifthCity(),platform);
				String actualMalaysiaSixthCity=getText(el.verifySixthCity(),platform);
				String actualMalaysiaSeventhCity=getText(el.verifySeventhCity(),platform);
				String actualMalaysiaEighthCity=getText(el.verifyEighthCity(),platform);
				verifyLabel("Komplek Sultan Abdul Hamid", containsData(actualMalaysiaFirstOffice,expectedMalaysiaFirstOffice));
				verifyLabel("JALAN LEONG SIN NAM", containsData(actualMalaysiaSecondOffice,expectedMalaysiaSecondOffice));
				verifyLabel("65 Jalan Trus", containsData(actualMalaysiaThirdOffice,expectedMalaysiaThirdOffice));
				verifyLabel("HP TOWERS", containsData(actualMalaysiaFourthOffice,expectedMalaysiaFourthOffice));
				verifyLabel("Sri Dagangan Business Centre", containsData(actualMalaysiaFifthOffice,expectedMalaysiaFifthOffice));
				verifyLabel("Jalan Melaka Raya", containsData(actualMalaysiaSixthOffice,expectedMalaysiaSixthOffice));
				verifyLabel("Bangunan KWSP", containsData(actualMalaysiaSeventhOffice,expectedMalaysiaSeventhOffice));
				verifyLabel("Damansara Uptown", containsData(actualMalaysiaEighthOffice,expectedMalaysiaEighthOffice));
				verifyLabel("ALOR SETAR", containsData(actualMalaysiaFirstCity,expectedMalaysiaFirstCity));
				verifyLabel("IPOH", containsData(actualMalaysiaSecondCity,expectedMalaysiaSecondCity));
				verifyLabel("Johor Bahru", containsData(actualMalaysiaThirdCity,expectedMalaysiaThirdCity));
				verifyLabel("KUALA LUMPUR", containsData(actualMalaysiaFourthCity,expectedMalaysiaFourthCity));
				verifyLabel("KUANTAN", containsData(actualMalaysiaFifthCity,expectedMalaysiaFifthCity));
				verifyLabel("Melaka", containsData(actualMalaysiaSixthCity,expectedMalaysiaSixthCity));
				verifyLabel("Penang", containsData(actualMalaysiaSeventhCity,expectedMalaysiaSeventhCity));
				verifyLabel("Petaling Jaya", containsData(actualMalaysiaEighthCity,expectedMalaysiaEighthCity));
			}else{
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedSeventeenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//			//scroll(platform, "Singapore");
			//			//scroll(platform, "Spain");
			//			Thread.sleep(3000);
			clickOnElement("Eighteenth Country", clickElement(el.verifyEighteenthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedEighteenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualMexicoFirstOffice=getText(el.getFirstOffice(),platform);
				String actualMexicoFirstCity=getText(el.getFirstCity(),platform);
				verifyLabel("Av. Paseo de la Reforma", containsData(actualMexicoFirstOffice,expectedMexicoFirstOffice));
				verifyLabel("Mexico City", containsData(actualMexicoFirstCity,expectedMexicoFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedEighteenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//			//scroll(platform, "Singapore");
			//			//scroll(platform, "Spain");
			//			Thread.sleep(3000);
			clickOnElement("Nineteenth", clickElement(el.verifyNineteenthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedNineteenthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualNetherlandsFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualNetherlandsSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualNetherlandsFirstCity=getText(el.verifyFirstCity(),platform);
				String actualNetherlandsSecondCity=getText(el.verifySecondCity(),platform);
				verifyLabel("Spicalaan", containsData(actualNetherlandsFirstOffice,expectedNetherlandsFirstOffice));
				verifyLabel("UPSS Leusden", containsData(actualNetherlandsSecondOffice,expectedNetherlandsSecondOffice));
				verifyLabel("Hoofddorp", containsData(actualNetherlandsFirstCity,expectedNetherlandsFirstCity));
				verifyLabel("Leusden", containsData(actualNetherlandsSecondCity,expectedNetherlandsSecondCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedNineteenthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//			//scroll(platform, "Singapore");
			//			//scroll(platform, "Spain");
			//			Thread.sleep(3000);
			clickOnElement("Twentieth Country", clickElement(el.verifyTwenteithCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentiethCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualNewZealandFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualNewZealandSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualNewZealandThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualNewZealandFirstCity=getText(el.verifyFirstCity(),platform);
				String actualNewZealandSecondCity=getText(el.verifySecondCity(),platform);
				String actualNewZealandThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("Great South Road", containsData(actualNewZealandFirstOffice,expectedNewZealandFirstOffice));
				verifyLabel("Te Roto Drive", containsData(actualNewZealandSecondOffice,expectedNewZealandSecondOffice));
				verifyLabel("Radio New Zealand House", containsData(actualNewZealandThirdOffice,expectedNewZealandThirdOffice));
				verifyLabel("Auckland", containsData(actualNewZealandFirstCity,expectedNewZealandFirstCity));
				verifyLabel("Kapiti", containsData(actualNewZealandSecondCity,expectedNewZealandSecondCity));
				verifyLabel("Wellington", containsData(actualNewZealandThirdCity,expectedNewZealandThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentiethCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//			//scroll(platform, "Singapore");
			//			//scroll(platform, "Spain");
			//			Thread.sleep(3000);
			clickOnElement("Twenty First Country", clickElement(el.verifyTwentyFirstCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentyFirstCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualPeruFirstOffice=getText(el.getFirstOffice(),platform);
				String actualPeruFirstCity=getText(el.getFirstCity(),platform);
				verifyLabel("Calle Las Camelias", containsData(actualPeruFirstOffice,expectedPeruFirstOffice));
				verifyLabel("Lima", containsData(actualPeruFirstCity,expectedPeruFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentyFirstCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//			scroll(platform, "Singapore");
			//		scroll(platform, "Spain");
			//	Thread.sleep(4000);
			clickOnElement("Twenty Second", clickElement(el.verifyTwentySecondCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentySecondCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualPhilippinesFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualPhilippinesSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualPhilippinesThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualPhilippinesFourthOffice=getText(el.verifyFourthOffice(),platform);
				String actualPhilippinesFifthOffice=getText(el.verifyFifthOffice(),platform);
				String actualPhilippinesFirstCity=getText(el.verifyFirstCity(),platform);
				String actualPhilippinesSecondCity=getText(el.verifySecondCity(),platform);
				String actualPhilippinesThirdCity=getText(el.verifyThirdCity(),platform);
				String actualPhilippinesFourthCity=getText(el.verifyFourthCity(),platform);
				String actualPhilippinesFifthCity=getText(el.verifyFifthCity(),platform);
				verifyLabel("Nepo Center", containsData(actualPhilippinesFirstOffice,expectedPhilippinesFirstOffice));
				verifyLabel("Barangay Highway Hills", containsData(actualPhilippinesSecondOffice,expectedPhilippinesSecondOffice));
				verifyLabel("One Cyberpod Centris", containsData(actualPhilippinesThirdOffice,expectedPhilippinesThirdOffice));
				verifyLabel("Diosdado Macapagal Ave", containsData(actualPhilippinesFourthOffice,expectedPhilippinesFourthOffice));
				verifyLabel("EDSA corner Cornel Street", containsData(actualPhilippinesFifthOffice,expectedPhilippinesFifthOffice));
				verifyLabel("Angeles", containsData(actualPhilippinesFirstCity,expectedPhilippinesFirstCity));
				verifyLabel("CityNet", containsData(actualPhilippinesSecondCity,expectedPhilippinesSecondCity));
				verifyLabel("Eton Centris", containsData(actualPhilippinesThirdCity,expectedPhilippinesThirdCity));
				verifyLabel("Macapagal", containsData(actualPhilippinesFourthCity,expectedPhilippinesFourthCity));
				verifyLabel("Polar Center", containsData(actualPhilippinesFifthCity,expectedPhilippinesFifthCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentySecondCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//			scroll(platform, "Singapore");
			//			scroll(platform, "Spain");
			//			Thread.sleep(3000);
			clickOnElement("Twenty Third Country", clickElement(el.verifyTwentyThirdCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentyThirdCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualSingaporeFirstOffice=getText(el.getFirstOffice(),platform);
				String actualSingaporeFirstCity=getText(el.getFirstCity(),platform);
				verifyLabel("Capital Tower", containsData(actualSingaporeFirstOffice,expectedSingaporeFirstOffice));
				verifyLabel("Singapore", containsData(actualSingaporeFirstCity,expectedSingaporeFirstCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentyThirdCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//			//scroll(platform, "Singapore");
			//			//scroll(platform, "Spain");
			//			Thread.sleep(4000);
			clickOnElement("Twenty Fourth", clickElement(el.verifyTwentyFourthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentyFourthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualSpainFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualSpainSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualSpainFirstCity=getText(el.verifyFirstCity(),platform);
				String actualSpainSecondCity=getText(el.verifySecondCity(),platform);
				verifyLabel("Edificio MERRIMACK-II", containsData(actualSpainFirstOffice,expectedSpainFirstOffice));
				verifyLabel("Cidade da Cultura de Galicia", containsData(actualSpainSecondOffice,expectedSpainSecondOffice));
				verifyLabel("Madrid", containsData(actualSpainFirstCity,expectedSpainFirstCity));
				verifyLabel("Santiago", containsData(actualSpainSecondCity,expectedSpainSecondCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentyFourthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//scroll(platform, "United Kingdom");
			//scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Twenty Fifth Country", clickElement(el.verifyTwentyFifthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentyFifthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualSwitzerlandFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualSwitzerlandSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualSwitzerlandThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualSwitzerlandFirstCity=getText(el.verifyFirstCity(),platform);
				String actualSwitzerlandSecondCity=getText(el.verifySecondCity(),platform);
				String actualSwitzerlandThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("St. Jakobs-Strasse", containsData(actualSwitzerlandFirstOffice,expectedSwitzerlandFirstOffice));
				verifyLabel("Chutzenstrasse", containsData(actualSwitzerlandSecondOffice,expectedSwitzerlandSecondOffice));
				verifyLabel("Zuercherstrasse", containsData(actualSwitzerlandThirdOffice,expectedSwitzerlandThirdOffice));
				verifyLabel("Basel", containsData(actualSwitzerlandFirstCity,expectedSwitzerlandFirstCity));
				verifyLabel("Bern", containsData(actualSwitzerlandSecondCity,expectedSwitzerlandSecondCity));
				verifyLabel("Thalwil", containsData(actualSwitzerlandThirdCity,expectedSwitzerlandThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentyFifthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//scroll(platform, "United Kingdom");
			//scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Twenty Sixth Country", clickElement(el.verifyTwentySixthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentySixthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualTaiwanFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualTaiwanSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualTaiwanThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualTaiwanFirstCity=getText(el.verifyFirstCity(),platform);
				String actualTaiwanSecondCity=getText(el.verifySecondCity(),platform);
				String actualTaiwanThirdCity=getText(el.verifyThirdCity(),platform);
				verifyLabel("13/F No. 21 Yixin 2nd Road", containsData(actualTaiwanFirstOffice,expectedTaiwanFirstOffice));
				verifyLabel("3/F Gongyi Road", containsData(actualTaiwanSecondOffice,expectedTaiwanSecondOffice));
				verifyLabel("DunHua North Rd", containsData(actualTaiwanThirdOffice,expectedTaiwanThirdOffice));
				verifyLabel("Kaohsiung", containsData(actualTaiwanFirstCity,expectedTaiwanFirstCity));
				verifyLabel("Taichung", containsData(actualTaiwanSecondCity,expectedTaiwanSecondCity));
				verifyLabel("Taipei", containsData(actualTaiwanThirdCity,expectedTaiwanThirdCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentySixthCountry, actualSelectedCountry);
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//scroll(platform, "United Kingdom");
			//scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Twenty Seventh", clickElement(el.verifyTwentySeventhCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentySeventhCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualUnitedKingdomFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualUnitedKingdomSecondOffice=getText(el.verifySecondOffice(),platform);
				String actualUnitedKingdomFirstCity=getText(el.verifyFirstCity(),platform);
				String actualUnitedKingdomSecondCity=getText(el.verifySecondCity(),platform);
				verifyLabel("Wavendon Business Park", containsData(actualUnitedKingdomFirstOffice,expectedUnitedKingdomFirstOffice));
				verifyLabel("Central Park", containsData(actualUnitedKingdomSecondOffice,expectedUnitedKingdomSecondOffice));
				verifyLabel("Enigma", containsData(actualUnitedKingdomFirstCity,expectedUnitedKingdomFirstCity));
				verifyLabel("Leeds", containsData(actualUnitedKingdomSecondCity,expectedUnitedKingdomSecondCity));
			}else {
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentySeventhCountry, actualSelectedCountry);
			if(profilePageLocation.contains(expectedUnitedKingdomSecondCity)) {
				clickOnElement("United Kingdom First Office", clickElement(el.verifyFirstOffice()));
			}else {
				clickOnElement("United Kingdom Second Office", clickElement(el.verifySecondOffice()));
			}
			waitForVisibityOfSpecificElement("Enabled Done Button", el.verifyEnabledDoneButton());
			clickOnElement("Done Button", clickElement(el.verifyEnabledDoneButton()));
			waitForVisibityOfSpecificElement("Enabled Save Button", ep.verifyEnableSaveButton());
			clickOnElement("Save Button", clickElement(ep.verifyEnableSaveButton()));
			Thread.sleep(5000);
			clickOnElement("Edit Profile Icon", clickElement(ep.verifyEditProfileIcon()));
			clickOnElement("Location", clickElement(ep.verifyProfileLocation()));
			clickOnElement("Change Button", clickElement(el.verifyChangeButton()));
			//scroll(platform, "United Kingdom");
			//scroll(platform, "United states of America");
			Thread.sleep(3000);
			clickOnElement("Twenty Eighth", clickElement(el.verifyTwentyEighthCountry()));
			actualSelectedCountry=getText(el.verifySelectedCountry(),platform);
			if(expectedTwentyEighthCountry.equals(actualSelectedCountry)) {
				extentTest.log(LogStatus.PASS, "Selected Country is  Displayed For Office Selection");
				String actualUSAFirstOffice=getText(el.verifyFirstOffice(),platform);
				String actualUSASecondOffice=getText(el.verifySecondOffice(),platform);
				String actualUSAThirdOffice=getText(el.verifyThirdOffice(),platform);
				String actualUSAFourthOffice=getText(el.verifyFourthOffice(),platform);
				String actualUSAFifthOffice=getText(el.verifyFifthOffice(),platform);
				String actualUSASixthOffice=getText(el.verifySixthOffice(),platform);
				String actualUSASeventhOffice=getText(el.verifySeventhOffice(),platform);
				String actualUSAEighthOffice=getText(el.verifyEighthOffice(),platform);
				String actualUSAFirstCity=getText(el.verifyFirstCity(),platform);
				String actualUSASecondCity=getText(el.verifySecondCity(),platform);
				String actualUSAThirdCity=getText(el.verifyThirdCity(),platform);
				String actualUSAFourthCity=getText(el.verifyFourthCity(),platform);
				String actualUSAFifthCity=getText(el.verifyFifthCity(),platform);
				String actualUSASixthCity=getText(el.verifySixthCity(),platform);
				String actualUSASeventhCity=getText(el.verifySeventhCity(),platform);
				String actualUSAEighthCity=getText(el.verifyEighthCity(),platform);
				//scroll(platform,"Wilmington");
				String actualUSANinethOffice=getText(el.verifyNinethOffice(),platform);
				String actualUSATenthOffice=getText(el.verifyTenthOffice(),platform);
				String actualUSAEleventhOffice=getText(el.verifyEleventhOffice(),platform);
				String actualUSATwelevethOffice=getText(el.verifyTwelvethOffice(),platform);
				String actualUSAThirteenthOffice=getText(el.verifyThirteenthOffice(),platform);
				String actualUSAFourteenthOffice=getText(el.verifyFourthteenthOffice(),platform);
				String actualUSANinethCity=getText(el.verifyNinethCity(),platform);
				String actualUSATenthCity=getText(el.verifyTenthCity(),platform);
				String actualUSAEleventhCity=getText(el.verifyEleventhCity(),platform);
				String actualUSATwelevethCity=getText(el.verifyTwelvethCity(),platform);
				String actualUSAThirteenthCity=getText(el.verifyThirteenthCity(),platform);
				String actualUSAFourteenthCity=getText(el.verifyFourthteenthCity(),platform);
				verifyLabel("Madison Ave Extension", containsData(actualUSAFirstOffice,expectedUSAFirstOffice));
				verifyLabel("44664 Guilford Drive", containsData(actualUSASecondOffice,expectedUSASecondOffice));
				verifyLabel("Discovery Plaza", containsData(actualUSAThirdOffice,expectedUSAThirdOffice));
				verifyLabel("Lakeview Drive", containsData(actualUSAFourthOffice,expectedUSAFourthOffice));
				verifyLabel("North Harwood St", containsData(actualUSAFifthOffice,expectedUSAFifthOffice));
				verifyLabel("Pilot Knob Rd", containsData(actualUSASixthOffice,expectedUSASixthOffice));
				verifyLabel("Lindle Road", containsData(actualUSASeventhOffice,expectedUSASeventhOffice));
				verifyLabel("Bishop Street Tower", containsData(actualUSAEighthOffice,expectedUSAEighthOffice));
				verifyLabel("Cromwell", containsData(actualUSANinethOffice,expectedUSANinthOffice));
				verifyLabel("Broad Street", containsData(actualUSATenthOffice,expectedUSATenthOffice));
				verifyLabel("Carry Street", containsData(actualUSAEleventhOffice,expectedUSAEleventhOffice));
				verifyLabel("480 North 2200 West", containsData(actualUSATwelevethOffice,expectedUSATwevelthOffice));
				verifyLabel("Great America Parkway", containsData(actualUSAThirteenthOffice,expectedUSAThirteenthOffice));
				verifyLabel("Silverside Road", containsData(actualUSAFourteenthOffice,expectedUSAFourteenthOffice));
				verifyLabel("Albany", containsData(actualUSAFirstCity,expectedUSAFirstCity));
				verifyLabel("Ashburn", containsData(actualUSASecondCity,expectedUSASecondCity));
				verifyLabel("Augutsa", containsData(actualUSAThirdCity,expectedUSAThirdCity));
				verifyLabel("Blue Bell", containsData(actualUSAFourthCity,expectedUSAFourthCity));
				verifyLabel("Dallas", containsData(actualUSAFifthCity,expectedUSAFifthCity));
				verifyLabel("Eagan", containsData(actualUSASixthCity,expectedUSASixthCity));
				verifyLabel("Harrisburg", containsData(actualUSASeventhCity,expectedUSASeventhCity));
				verifyLabel("Honolulu", containsData(actualUSAEighthCity,expectedUSAEighthCity));
				verifyLabel("Irvine", containsData(actualUSANinethCity,expectedUSANinthCity));
				verifyLabel("New York", containsData(actualUSATenthCity,expectedUSATenthCity));
				verifyLabel("Richmond", containsData(actualUSAEleventhCity,expectedUSAEleventhCity));
				verifyLabel("Salt Lake City", containsData(actualUSATwelevethCity,expectedUSATwevelthCity));
				verifyLabel("Santa Clara", containsData(actualUSAThirteenthCity,expectedUSAThirteenthCity));
				verifyLabel("Wilmington", containsData(actualUSAFourteenthCity,expectedUSAFourteenthCity));
			}else{
				extentTest.log(LogStatus.FAIL, "Selected Country is Not Displayed For Office Selection");
			}
			s.assertEquals(expectedTwentyEighthCountry, actualSelectedCountry);
			if(profilePageLocation.contains(expectedUSATenthCity)) {
				clickOnElement("USA Tenth Office", clickElement(el.verifyEleventhOffice()));
				waitForVisibityOfSpecificElement("Enabled Done Button", el.verifyEnabledDoneButton());
				clickOnElement("Done Button", clickElement(el.verifyEnabledDoneButton()));
				Thread.sleep(2000);
				String getEditProfileLocation=getText(ep.verifyProfileLocation(), platform);
				verifyData(getEditProfileLocation, "Selected City & Country at Edit Profile Page", containsData(getEditProfileLocation,expectedUSAEleventhCity));
				waitForVisibityOfSpecificElement("Enabled Save Button", ep.verifyEnableSaveButton());
				clickOnElement("Save Button", clickElement(ep.verifyEnableSaveButton()));
				Thread.sleep(5000);
				String getProfileLocation=getText(el.verifyProfilePageLocation(), platform);
				verifyData(getProfileLocation, "Selected City & Country at  Profile Page", containsData(getProfileLocation,expectedUSAEleventhCity));
				logOut(platform);
				login(platform);
				clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
				profilePageLocation=getText(el.verifyProfilePageLocation(), platform);
				String actualLocation[]=profilePageLocation.split(" ", 2);
				profilePageLocation=actualLocation[1];
				verifyData(profilePageLocation, "Profile Page Location", containsData(profilePageLocation,expectedUSAEleventhCity));
			}else {
				clickOnElement("USA Tenth Office", clickElement(el.verifyTenthOffice()));
				waitForVisibityOfSpecificElement("Enabled Done Button", el.verifyEnabledDoneButton());
				clickOnElement("Done Button", clickElement(el.verifyEnabledDoneButton()));
				Thread.sleep(2000);
				String getEditProfileLocation=getText(ep.verifyProfileLocation(), platform);
				verifyData(getEditProfileLocation, "Selected City & Country at Edit Profile Page",containsData(getEditProfileLocation,expectedUSATenthCity));
				waitForVisibityOfSpecificElement("Enabled Save Button", ep.verifyEnableSaveButton());
				clickOnElement("Save Button", clickElement(ep.verifyEnableSaveButton()));
				Thread.sleep(5000);
				String getProfileLocation=getText(el.verifyProfilePageLocation(), platform);
				verifyData(getProfileLocation, "Selected City & Country at  Profile Page", containsData(getProfileLocation,expectedUSATenthCity));
				logOut(platform);
				login(platform);
				clickOnElement("Profile Navigation Bar", clickElement(ld.profileIcon()));
				profilePageLocation=getText(el.verifyProfilePageLocation(), platform);
				String actualLocation[]=profilePageLocation.split(" ", 2);
				profilePageLocation=actualLocation[1];
				verifyData(profilePageLocation, "Profile Page Location", containsData(profilePageLocation,expectedUSATenthCity) );
			}
			
			s.assertAll();
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Add/Update Profile Location Test Failed");
		}
	}	
}