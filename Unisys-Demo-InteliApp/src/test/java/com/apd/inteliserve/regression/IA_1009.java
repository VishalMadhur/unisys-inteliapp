package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.ProfileScreenLegalPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */

public class IA_1009 extends BaseTest{
	@Test
	@Parameters({"platform"})
	public void profileLegalPageTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-1009, Legal Update Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-1009, Legal Update Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		extentTest.log(LogStatus.PASS, "Test Started");
		try {
			ProfileScreenLegalPage pl= new ProfileScreenLegalPage(driver);
			click(pl.profileIcon());
			extentTest.log(LogStatus.PASS, "User is Able to Navigate to Profile Icon");
			Thread.sleep(2000);
			if(elementIsDisplayed(pl.companyText())){
				extentTest.log(LogStatus.PASS, "Company Text [Your Company] is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Company Text [Your Company] is Not Displayed");
			}
			if(elementIsDisplayed(pl.privacyPolicyText())){
				extentTest.log(LogStatus.PASS, "Privacy Policy Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Privacy Policy Text is Not Displayed");
			}
			if(elementIsDisplayed(pl.termsOfUseText())){
				extentTest.log(LogStatus.PASS, "Terms of Use Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Terms of Use Text is Not Displayed");
			}
			if(elementIsDisplayed(pl.verifyUnisysText())){
				extentTest.log(LogStatus.PASS, "Unisys Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Unisys Text is Not Displayed");
			}
			if(elementIsDisplayed(pl.aboutText())){
				extentTest.log(LogStatus.PASS, "About Text is Displayed");
			}
			else{
				extentTest.log(LogStatus.FAIL, "About Text is Not Displayed");
			}
			click(pl.privacyPolicyText());
			extentTest.log(LogStatus.PASS, "Clicked on Privacy Policy");
			String expectedUrl =getPropertyValue("PrivacyPolicyUrl");
			if(platform.equalsIgnoreCase("ios")){
				clickElement(pl.getURL());
			}
			String actualUrl= getText(pl.getCurrentURL());
			Reporter.log(actualUrl,true);
			if(actualUrl.contains(expectedUrl)) {
				extentTest.log(LogStatus.PASS, "Navigated to Corerect URL -- " +actualUrl);
			}
			else {
				extentTest.log(LogStatus.FAIL, "Navigated to Wrong URL -- " +actualUrl);
			}
			s.assertTrue(actualUrl.contains(expectedUrl));
			s.assertAll();
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Legal Update Test Failed");
		}
	}
	
	@Test
	@Parameters({"platform"})
	public void profileLegalPageTermsOfUseTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 240);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-1009, Legal Update Terms of Use Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-1009, Legal Update Test Terms of Use -Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			ProfileScreenLegalPage pl= new ProfileScreenLegalPage(driver);
			click(pl.profileIcon());
			extentTest.log(LogStatus.PASS, "User is Able to Navigate to Profile Icon");
			click(pl.termsOfUseText());
			extentTest.log(LogStatus.PASS, "Clicked on Terms Of Use Link");
			String expectedUrl =getPropertyValue("TermsOfUseURL");
			if(platform.equalsIgnoreCase("ios")){
				clickElement(pl.getURL());
			}
			String actualUrl= getText(pl.getCurrentURL());
			Reporter.log(actualUrl,true);
			if(actualUrl.contains(expectedUrl)) {
				extentTest.log(LogStatus.PASS, "Navigated to Corerect URL -- " +actualUrl);
			}
			else {
				extentTest.log(LogStatus.FAIL, "Navigated to Wrong URL -- " +actualUrl);
			}
			s.assertTrue(actualUrl.contains(expectedUrl));
			s.assertAll();
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Legal Update Terms Of Use Test Failed");
		}
	}
}
