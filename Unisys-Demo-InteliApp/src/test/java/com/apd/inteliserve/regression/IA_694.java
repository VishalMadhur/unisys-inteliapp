package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.general.Lib;
import com.apd.inteliserve.pompages.LandingPage;
import com.apd.inteliserve.pompages.LoginPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */

public class IA_694 extends BaseTest {

	@Test
	@Parameters({"platform"})
	public void loginTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-694, Login Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-694, Login Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try{
			extentTest.log(LogStatus.PASS, "Test Started");
			logOut(platform);
			LoginPage lp = new LoginPage(driver);
			String email =Lib.getPropertyValue("WrongEmail");
			lp.setEmail(email);
			extentTest.log(LogStatus.PASS, "Wrong Email Entered");
			extentTest.log(LogStatus.INFO, "Checking if Next Button is Disabled By Only Entering Email");
			if(elementDisplayed(lp.verifyEnableLoginButton())){
				extentTest.log(LogStatus.FAIL, "Next Button is Enabled");
			}
			else{
				extentTest.log(LogStatus.PASS, "Next Button is Disabled");
			}
			//			elementDisplayed(lp.verifyDisableLoginButton());
			//			extentTest.log(LogStatus.PASS, "Next Button is Disabled");
			//			if(platform.equalsIgnoreCase("ios")){
			//				lp.clickFirstNameTB();
			//				extentTest.log(LogStatus.INFO, "Checking if Correct Email Address Error Message is dispalyed");
			//				if(elementDisplayed(lp.verifyEmailErrorMessage())){
			//					extentTest.log(LogStatus.PASS, "Correct Email Error Message is Displayed");
			//				}
			//				else{
			//					extentTest.log(LogStatus.FAIL, "Wrong Email Error Message is  Displayed");
			//				}
			//				lp.clickLastNameTB();
			//				extentTest.log(LogStatus.INFO, "Checking if Correct First Name  Error Message is dispalyed");
			//				if(elementDisplayed(lp.verifyFirstNameErrorMessage())){
			//					extentTest.log(LogStatus.PASS, "Correct First Name Error Message is Displayed");
			//				}
			//				else{
			//					extentTest.log(LogStatus.FAIL, "Wrong First Name Error Message is Displayed");
			//				}
			//				extentTest.log(LogStatus.PASS, "Correct First Error Message is Displayed");
			//				lp.clickFirstNameTB();
			//				extentTest.log(LogStatus.INFO, "Checking if Correct Last Name Error Message is dispalyed");
			//				if(elementDisplayed(lp.verifyLastNameErrorMessage())){
			//					extentTest.log(LogStatus.PASS, "Correct Last Name Error Message is Displayed");
			//				}
			//				else{
			//					extentTest.log(LogStatus.FAIL, "Wrong Last Name Error Message is Displayed");
			//				}
			//			}
			//			String firstName =Lib.getPropertyValue("FirstName");
			//			lp.setFirstName(firstName);
			//			extentTest.log(LogStatus.PASS, "FirstName Entered");
			//			extentTest.log(LogStatus.INFO, "Checking if Login Button is Disabled By Only Entering Email And FirstName");
			//			if(elementDisplayed(lp.verifyDisableLoginButton())){
			//				extentTest.log(LogStatus.PASS, "Login Button is Disabled");
			//			}
			//			else{
			//				extentTest.log(LogStatus.FAIL, "Login Button is Enabled");
			//			}
			//
			//			String lastName =Lib.getPropertyValue("LastName");
			//			lp.setLastname(lastName);
			//			extentTest.log(LogStatus.PASS, "Last Name Entered");
			//			extentTest.log(LogStatus.INFO, "Checking if Login Button is Disabled By Entering All Field But With Wrong Email Format");
			//			if(elementDisplayed(lp.verifyDisableLoginButton())){
			//				extentTest.log(LogStatus.PASS, "Login Button is Disabled");
			//			}
			//			else{
			//				extentTest.log(LogStatus.FAIL, "Login Button is Enabled");
			//			}
			lp.clearWrongEmail();
			extentTest.log(LogStatus.PASS, "Wrong Email is Cleared");
			String email1 =Lib.getPropertyValue("Email");
			lp.setEmail(email1);
			extentTest.log(LogStatus.PASS, "Correct Email Entered");
			extentTest.log(LogStatus.INFO, "Checking if Next Button is Disabled By Entering Correct Email Format");
			if(elementDisplayed(lp.verifyEnableLoginButton())){
				extentTest.log(LogStatus.PASS, "Next Button is Enabled");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Next Button is Disabled");
			}
			//lp.clickLogin();
			lp.clickNextButton();
			extentTest.log(LogStatus.PASS, "Clicked On Next Button");
			//	String firstName =Lib.getPropertyValue("FirstName");
			extentTest.log(LogStatus.INFO, "Checking if Login Button is Disabled By Not Entering Password");
			if(elementDisplayed(lp.verifyDisableLoginButton())){
				extentTest.log(LogStatus.PASS, "Login Button is Disabled");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Login Button is Enabled");

			}
			clickElement(lp.verifyEnableLoginButton());
			extentTest.log(LogStatus.PASS, "User is able to navigate back to login screen after clicking back button");
			Thread.sleep(2000);
			lp.clickNextButton();
			String password =Lib.getPropertyValue("Password");
			lp.setPassword(password);
			extentTest.log(LogStatus.PASS, "Password entered successfully");
			extentTest.log(LogStatus.INFO, "Checking if Login  Button is Enabled By Entering Password");
			if(elementDisplayed(lp.verifyEnableLoginButton())){
				extentTest.log(LogStatus.PASS, "Login Button is Enabled");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Login Button is Disabled");
			}
			//	lp2.setFirstName(firstName);
			//	String lastName =Lib.getPropertyValue("LastName");
			Thread.sleep(2000);
			//	lp2.setLastname(lastName);
			lp.clickNextButton();
			extentTest.log(LogStatus.PASS, "Login Button is Clicked");
			LandingPage ld = new LandingPage(driver);
			if(platform.equalsIgnoreCase("android")){
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
			}
			else{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
			}
			if(elementDisplayed(ld.verifyHelloText())){
				extentTest.log(LogStatus.PASS, "Login Happened Successfully");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Failed to Login");
			}
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Login Page Test Failed");
		}
	}
}

