package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */

public class IA_819_SC_Chat extends BaseTest{
	@Test
	@Parameters({"platform"})
	public void supportCornerChatTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 240);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-819, Support Corner Chat Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-819, Support Corner Chat Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			click(fm.clickCoreSupportButton());
			extentTest.log(LogStatus.PASS, "Clicked  on Core Support Buton");
			click(fm.chatButton());
			extentTest.log(LogStatus.PASS, "Clicked  on chat Buton");
			if(platform.equalsIgnoreCase("ios")){
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//XCUIElementTypeOther[@name='toggle menu button']"))).click();
			}
			if(platform.equalsIgnoreCase("android")){
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.View[contains(@text,'menu toggle button.')]"))).click();
			}
			if(elementIsDisplayed(fm.toggleButton())){
				extentTest.log(LogStatus.PASS, "Navigated to IVA Screen");
			}
			else{
				extentTest.log(LogStatus.FAIL, "Failed to Navigate to IVA Screen");
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Support Corner Email Test Failed");
		}
	}
}
