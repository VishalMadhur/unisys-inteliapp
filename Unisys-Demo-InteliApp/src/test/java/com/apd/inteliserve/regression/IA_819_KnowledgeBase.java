package com.apd.inteliserve.regression;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.relevantcodes.extentreports.LogStatus;


/**
 * @author  VishalMadhur
 *
 */

public class IA_819_KnowledgeBase extends BaseTest{
	@Test
	@Parameters({"platform"})
	public void knowledgeBase(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-819, Favrouite Module-Knowledge Base Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-819, Favrouite Module--Knowledge Base Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			click(fm.clickKnowledgeBaseButton());
			extentTest.log(LogStatus.PASS, "Clicked on Knowledge Base Button");
				String expectedUrl =getPropertyValue("KnowledgeBaseUrl");
				if(platform.equalsIgnoreCase("ios")){
					clickElement(fm.getURL());
					}
					String actualUrl= getText(fm.getCurrentURL());
					Reporter.log(actualUrl,true);
					if(actualUrl.contains(expectedUrl)) {
						extentTest.log(LogStatus.PASS, "Navigated to Corerect URL -- " + actualUrl);
					}
					else {
						extentTest.log(LogStatus.FAIL, "Navigated to Wrong URL -- " + actualUrl);
					}
					s.assertTrue(actualUrl.contains(expectedUrl));
					//}
					s.assertAll();
		}catch(Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Favrouite Module-Knowledge Base Test Failed");
		}
	}
}