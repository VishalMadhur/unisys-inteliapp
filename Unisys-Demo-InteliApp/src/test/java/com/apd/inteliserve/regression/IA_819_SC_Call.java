package com.apd.inteliserve.regression;


import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.pompages.FavouriteModulesPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author  VishalMadhur
 *
 */



public class IA_819_SC_Call extends BaseTest{
	@Test
	@Parameters({"platform"})
	public void supportCornerEmailTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("IA-819, Support Corner Call Test-iOS");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
		}else {
			extentTest = extent.startTest("IA-819, Support Corner Call Test-Android");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
		}
		try {
			extentTest.log(LogStatus.PASS, "Test Started");
			FavouriteModulesPage fm = new FavouriteModulesPage(driver);
			click(fm.clickCoreSupportButton());
			extentTest.log(LogStatus.PASS, "Clicked  on Core Support Buton");
			click(fm.callButton());
			extentTest.log(LogStatus.PASS, "Clicked  on Call Buton");
			if(platform.equalsIgnoreCase("android")){
				String expectedSupportNumberAndroid =getPropertyValue("SupportNumber");
				String actualSupportNumber = fm.getSupportNumber().getText();
				Reporter.log(actualSupportNumber,true);
				if(actualSupportNumber.equals(expectedSupportNumberAndroid)) {
					extentTest.log(LogStatus.PASS, "Support Number is Verified -- " +actualSupportNumber);
				}
				else {
					extentTest.log(LogStatus.FAIL, "Support Number is Not Verified -- " +actualSupportNumber);
				}
				s.assertEquals(actualSupportNumber, expectedSupportNumberAndroid);

			}
			//			else {
			//String expectedSupportNumberIos =getPropertyValue("SupportNumberIos");
			//			String actualSupportNumber = fm.getSupportNumber().getText();
			//			Reporter.log(actualSupportNumber,true);
			//				if(actualSupportNumber.contains(expectedSupportNumberIos)) {
			//					extentTest.log(LogStatus.PASS, "Support Number is Verified");
			//				}
			//				else {
			//					extentTest.log(LogStatus.FAIL, "Support Number is Not Verified");
			//				}
			//			}
		}catch(Exception ex) {
			ex.printStackTrace();
			extentTest.log(LogStatus.FAIL, ex.getMessage());
			Assert.fail("Support Corner Call Test Failed");
		}
	}
}
