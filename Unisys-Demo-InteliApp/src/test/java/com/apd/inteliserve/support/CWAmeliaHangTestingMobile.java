package com.apd.inteliserve.support;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.apd.inteliserve.general.BaseTest;
import com.apd.inteliserve.general.Lib;
import com.apd.inteliserve.pompages.ProfileScreenLegalPage;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class CWAmeliaHangTestingMobile extends BaseTest{
	String screenshotPath ="";
	@Test
	@Parameters({"platform"})
	public void profileLegalPageTest(@Optional String platform) throws InterruptedException, MalformedURLException {
		WebDriverWait wait = new WebDriverWait(driver, 240);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		SoftAssert s = new SoftAssert();
		if(platform.equalsIgnoreCase("ios")){
			extentTest = extent.startTest("Amelia Hang Test-iOS Simulator -14.4");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Profile")));
			extentTest.log(LogStatus.PASS, "Test Started");
			ProfileScreenLegalPage pl= new ProfileScreenLegalPage(driver);
			clickOnElement("Amelia Icon", clickElement(pl.ivaIcon()));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//XCUIElementTypeOther[@name='Help with Domain Password']"))).click();
			extentTest.log(LogStatus.PASS,"Help with Domain Password Password Selected Successfully");
			Thread.sleep(5000);
			screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//XCUIElementTypeOther[@name='Reset Password']"))).click();
			extentTest.log(LogStatus.PASS,"Reset Password Selected Successfully");
			Thread.sleep(5000);
			screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.INFO,"Waiting For 10 Minutes before clicking Yes Button");
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			driver.findElement(By.xpath("//XCUIElementTypeOther[@name='Yes']")).click();
			Thread.sleep(5000);
			extentTest.log(LogStatus.PASS,"Click on Yes Button Successfully");
			screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			try{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//XCUIElementTypeOther[@name='Yes']"))).click();
				extentTest.log(LogStatus.PASS,"Click on Yes Button Successfully");
				Thread.sleep(5000);
				screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
				extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			}catch(Exception e){
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL,"Failed to Click on Yes Button as Amelia Hanged");
				Thread.sleep(5000);
				screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
				extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			}
		}else {
			extentTest = extent.startTest("Amelia Hang Test-Android Emulator 9.0");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc='Profile']")));
			extentTest.log(LogStatus.PASS, "Test Started");
			ProfileScreenLegalPage pl= new ProfileScreenLegalPage(driver);
			clickOnElement("Amelia Icon", clickElement(pl.ivaIcon()));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.View[@text='Help with Domain Password']"))).click();
			extentTest.log(LogStatus.PASS,"Help with Domain Password Password Selected Successfully");
			Thread.sleep(5000);
			screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.View[@text='Reset Password']"))).click();
			extentTest.log(LogStatus.PASS,"Reset Password Selected Successfully");
			Thread.sleep(5000);
			screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.INFO,"Waiting For 10 Minutes before clicking Yes Button");
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			Thread.sleep(50000);
			pl.profileIcon().click();
			pl.ivaIcon().click();
			clickOnElement("Profile Icon", clickElement(pl.profileIcon()));
			clickOnElement("Amelia Icon", clickElement(pl.ivaIcon()));
			driver.findElement(By.xpath("//android.view.View[@text='Yes']")).click();
			Thread.sleep(5000);
			extentTest.log(LogStatus.PASS,"Click on Yes Button Successfully");
			screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			try{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.View[@text='Yes']"))).click();
				extentTest.log(LogStatus.PASS,"Click on Yes Button Successfully");
				Thread.sleep(5000);
				screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
				extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			}catch(Exception e){
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL,"Failed to Click on Yes Button as Amelia Hanged");
				Thread.sleep(5000);
				screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
				extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			}
		}
	}
}







