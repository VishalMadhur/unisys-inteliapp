package com.apd.inteliserve.support;


import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.apd.inteliserve.general.Lib;

public class LatestCushmanAPKDownload{

	@Test
	public void apkDownloadAndRenameApk() throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver","/Users/iosbuilduser/Desktop/jenkins/workspace/InteliServe/lambweston-inteliapp-android-automation/LambWeston-InteliApp/src/test/resources/chromedriver");
		HashMap<String, Object> chromePrefs = new HashMap<String,Object>();
		chromePrefs.put("profile.default_content_settins.popups", 0);
		chromePrefs.put("download.default_directory", "/Users/iosbuilduser/Library/Android/sdk/platform-tools/");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("window-size=1920,1260");
		options.setExperimentalOption("prefs",chromePrefs);
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		@SuppressWarnings("deprecation")
		WebDriver driver = new ChromeDriver(cap);
		WebDriverWait wait = new WebDriverWait(driver, 240);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		String nexusURL=Lib.getPropertyValue("NexusWelcomeURL");
		driver.get(nexusURL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Lib.getElementPropertyValue("NexusUsernameField"))));
		driver.findElement(By.xpath(Lib.getElementPropertyValue("NexusUsernameField"))).sendKeys(Lib.getPropertyValue("NexusUsername"));
		driver.findElement(By.xpath(Lib.getElementPropertyValue("NexusPasswordField"))).sendKeys(Lib.getPropertyValue("NexusPassword"));
		Thread.sleep(2000);
		driver.findElement(By.xpath(Lib.getElementPropertyValue("NexusLoginButton"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Lib.getElementPropertyValue("NexusRepositoriesFolder"))));
		driver.get(Lib.getPropertyValue("NexusCushmanURL"));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,1500)");
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.xpath(Lib.getElementPropertyValue("APKDownloadFile")));
		String apkFileName=element.getText();
		System.out.println(apkFileName);
		element.click();
		Thread.sleep(25000);
		driver.quit();
		String pathFile="/Users/iosbuilduser/Library/Android/sdk/platform-tools/" + apkFileName;
		System.out.println(pathFile);

		Path yourFile = Paths.get(pathFile);
		try{
			Files.move(yourFile, yourFile.resolveSibling("CushmanLatestBuild.apk"),REPLACE_EXISTING);
			System.out.println("Filed Rename Successfully");
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("Failed to rename File");

		}
	}
}

