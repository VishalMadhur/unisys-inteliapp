package com.apd.inteliserve.support;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.apd.inteliserve.general.Lib;

public class LatestLambwestonAPKUpload{

	@Test
	public void latestLambwestonAPKUpload() throws InterruptedException, IOException {
		WebDriver driver = null;
		try {
			LatestLambwestonAPKDownlaod apk = new LatestLambwestonAPKDownlaod();
			apk.apkDownloadAndRenameApk();
			Thread.sleep(5000);
			System.setProperty("webdriver.chrome.driver","/Users/iosbuilduser/Desktop/jenkins/workspace/InteliServe/lambweston-inteliapp-android-automation/LambWeston-InteliApp/src/test/resources/chromedriver");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("window-size=1920,1260");
			driver = new ChromeDriver(options);
			WebDriverWait wait = new WebDriverWait(driver, 240);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			String apkUploadURL= Lib.getPropertyValue("APKUploadURL");
			driver.get(apkUploadURL);
			Thread.sleep(5000);
			WebElement azureEmailField=driver.findElement(By.xpath(Lib.getElementPropertyValue("MicrosoftEmailField")));
			wait.until(ExpectedConditions.visibilityOf(azureEmailField)).sendKeys(Lib.getPropertyValue("AzureEmail"));
			driver.findElement(By.xpath(Lib.getElementPropertyValue("NextButton"))).click();
			Thread.sleep(5000);
			String currentAuthRL = driver.getCurrentUrl();
			String[]strr= currentAuthRL.split("//");
			String  url1 = strr[0];
			String  url2 = strr[1];
			System.out.println(url1);
			System.out.println(url2);
			String userName=Lib.getPropertyValue("NexusUsername");
			String password=Lib.getPropertyValue("NexusPassword");
			String actualAuthURL=url1+"//"+userName+":"+password+"@"+url2;
			System.out.println(actualAuthURL);
			driver.get(actualAuthURL);
			try {
				Actions actions = new Actions(driver);
				WebElement lambwestonAPK = driver.findElement(By.xpath(Lib.getElementPropertyValue("LambwestonAPK")));
				//actions.contextClick(cushmanAPK).perform();
				lambwestonAPK.click();
				Thread.sleep(5000);
				WebElement deleteAPK = driver.findElement(By.xpath(Lib.getElementPropertyValue("SelectDelete")));
				actions.moveToElement(deleteAPK).click().perform();
				//deleteAPK.click();
				//Keyword k = new Keyword();
				//k.retryingFindClick(By.xpath(Lib.getElementPropertyValue("SelectDelete")));
				Thread.sleep(5000);
				WebElement deleteAPK1= driver.findElement(By.xpath(Lib.getElementPropertyValue("ConfirmDelete")));
				actions.moveToElement(deleteAPK1).perform();
				deleteAPK1.click();
				Thread.sleep(5000);
				driver.get(apkUploadURL);
				//driver.navigate().back();
			}catch(Exception e) {
				e.printStackTrace();
				System.out.println("Lambweston apk is not available to delete");
			}
			WebElement uploadOption=driver.findElement(By.xpath(Lib.getElementPropertyValue("UploadOption")));
			wait.until(ExpectedConditions.visibilityOf(uploadOption)).click();
			driver.findElement(By.xpath(Lib.getElementPropertyValue("FileUpload"))).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath(Lib.getElementPropertyValue("FileType"))).sendKeys("/Users/iosbuilduser/Library/Android/sdk/platform-tools/LambWestonLatestBuild.apk");
			Thread.sleep(50000);
			driver.quit();
			System.out.println("Latest Lambweston APK Uploaded Successfully");
		}catch(Exception e) {
			e.printStackTrace();
			driver.quit();
		}
	}
}

