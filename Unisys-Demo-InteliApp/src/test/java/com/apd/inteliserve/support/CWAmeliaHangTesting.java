package com.apd.inteliserve.support;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import com.apd.inteliserve.general.EmailExtentReport;
import com.apd.inteliserve.general.Keyword;
import com.apd.inteliserve.general.Lib;
import com.relevantcodes.extentreports.LogStatus;

public class CWAmeliaHangTesting extends Keyword {
	WebDriver driver = null;
	public String concatenate=".";
	String screenshotPath ="";

	@BeforeSuite(alwaysRun = true)
	public void setReport() {
		deleteOldScreenshots();
		extent=Lib.generateExtentReport();
	}
	
	@Test
	public void ameliaHangTesting() throws InterruptedException, IOException {
		extentTest = extent.startTest("CW Amelia Hang Up Testing - Web");
				System.setProperty("webdriver.chrome.driver","/Users/iosbuilduser/Desktop/jenkins/workspace/InteliServe/lambweston-inteliapp-android-automation/LambWeston-InteliApp/src/test/resources/chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("window-size=1920,1260");
		driver = new ChromeDriver(options);
		WebDriverWait wait = new WebDriverWait(driver, 240);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://ivauat.unisys.com/Amelia/ui/CWKMUI/chat?bpn_processName=01_Greeting&domainCode=cwk_english&bpn_cwkuseremail=archit.raj@cushwake.com&bpn_cwkuserid=&bpn_cwkusertoken=$idToken");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("name-Help with Domain Password"))).click();
		extentTest.log(LogStatus.PASS,"Help with Domain Password Password Selected Successfully");
		Thread.sleep(5000);
		screenshotPath = concatenate+Lib.captureWebElementScreenshots(driver);
		extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("name-Reset Password"))).click();
		extentTest.log(LogStatus.PASS,"Reset Password Selected Successfully");
		Thread.sleep(5000);
		screenshotPath = concatenate+Lib.captureWebElementScreenshots(driver);
		extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
		extentTest.log(LogStatus.INFO,"Waiting For 10 Minutes before clicking Yes Button");
		Thread.sleep(600000);
		driver.findElement(By.id("name-Yes")).click();
		Thread.sleep(5000);
		extentTest.log(LogStatus.PASS,"Click on Yes Button Successfully");
		screenshotPath = concatenate+Lib.captureWebElementScreenshots(driver);
		extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
		try{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("name-Yes"))).click();
			extentTest.log(LogStatus.PASS,"Click on Yes Button Successfully");
			Thread.sleep(5000);
			screenshotPath = concatenate+Lib.captureWebElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
		}catch(Exception e){
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL,"Failed to Click on Yes Button as Amelia Hanged");
			Thread.sleep(5000);
			screenshotPath = concatenate+Lib.captureWebElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
		}
	}
	
	@AfterMethod(alwaysRun = true)
	public void getResultStatus(ITestResult result) throws InterruptedException
	{
		try {
			Reporter.log("---------------------------------",true);
			Reporter.log("Test Completed",true);
			if(result.getStatus()==ITestResult.FAILURE){
				extentTest.log(LogStatus.FAIL, "TEST CASE FAILED IS "+result.getName()); 
				extentTest.log(LogStatus.ERROR, result.getThrowable());
				String screenshotPath = concatenate+Lib.captureScreenshots(driver, result.getName());
				extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
				extent.endTest(extentTest);
			}
			else if(result.getStatus()==ITestResult.SKIP){
				extentTest.log(LogStatus.SKIP, "Test Case SKIPPED IS " + result.getName());
				extent.endTest(extentTest);
			}
			else if(result.getStatus()==ITestResult.SUCCESS){
				extentTest.log(LogStatus.PASS, "Test Case PASSED IS " + result.getName());
				extent.endTest(extentTest);
			}
		}
		finally{
			driver.quit();
		}
	}

	@AfterSuite(alwaysRun=true)
	public void closeApplicationAndEndReport() {

		try{
			Reporter.log("Flushing Report" ,true);
		}
		finally{
			extent.flush();
			Reporter.log("Report Generated" ,true);
			extentReportZip();
		}
		try{
			EmailExtentReport e = new EmailExtentReport();
			e.generateEmailExtentReport();
		}
		catch(Exception e){
			e.printStackTrace();
			Reporter.log("Failed to Generate Email Extent Report", true);
		}
	}
}