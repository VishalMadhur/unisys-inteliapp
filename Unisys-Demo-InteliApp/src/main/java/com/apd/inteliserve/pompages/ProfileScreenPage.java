package com.apd.inteliserve.pompages;

import org.openqa.selenium.By;
import com.apd.inteliserve.general.BaseTest;
import io.appium.java_client.MobileElement;

public class ProfileScreenPage extends BaseTest{
	String initialsText=getPropertyValue("InitialsText");
	String nameText=getPropertyValue("NameText");
	String name=getPropertyValue("ProfileName");
	String emailtext=getPropertyValue("EmailText");
	String phoneNumber=getPropertyValue("PhoneNumber");
	String location=getPropertyValue("Location");
	String language =getPropertyValue("Language");


	public MobileElement clickProfileIconAnd() {
		return driver.findElement(By.xpath("//android.view.ViewGroup[@content-desc='Profile']"));
	}
	public MobileElement verifyProfileHeaderAnd() {
		return driver.findElement(By.xpath("//android.widget.TextView[@text='Profile']"));
	}
	public MobileElement verifyInitialsAnd() {
		return driver.findElement(By.xpath("//android.widget.TextView[@text='"+initialsText+"']"));
	}
	public MobileElement verifyFullNameAnd() {
		return driver.findElement(By.xpath("//android.widget.TextView[@text='"+nameText+"']"));
	}
	public MobileElement verifyEmailAnd() {
		return driver.findElement(By.xpath("//android.widget.TextView[@text='"+emailtext+"']"));
	}
	public MobileElement verifyPhoneLabelAnd() {
		return driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Phone')]"));
	}
	public MobileElement verifyLocationLabelAnd() {
		return driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Location')]"));
	}
	public MobileElement verifyLanguageLabelAnd() {
		return driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Language')]"));
	}
	public MobileElement verifyPhoneAnd() {
		return driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+phoneNumber+"')]"));
	}
	public MobileElement verifylocationAnd() {
		return driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+location+"')]"));
	}
	public MobileElement verifyLanguageAnd() {
		return driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+language+"')]"));
	}
	public MobileElement clickProfileIconIos() {
		return driver.findElement(By.id("Profile"));
	}
	public MobileElement verifyProfileHeaderIos() {
		return driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='Profile']"));
	}
	public MobileElement verifyInitialsIos() {
		return driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"+initialsText+"']"));
		//XCUIElementTypeStaticText[@name="IS"]

	}
	public MobileElement verifyFullNameIos() {
		return driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"+name+"']"));
	}
	public MobileElement verifyEmailIos() {
		return driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"+emailtext+"']"));
	}
	public MobileElement verifyPhoneLabelIos() {
		return driver.findElement(By.xpath("//XCUIElementTypeOther[contains(@name,'Phone')]"));
	}
	public MobileElement verifyLocationLabelIos() {
		return driver.findElement(By.xpath("//XCUIElementTypeOther[contains(@name,'Location')]"));
	}
	public MobileElement verifyLanguageLabelIos() {
		return driver.findElement(By.xpath("//XCUIElementTypeOther[contains(@name,'Language')]"));
	}
	public MobileElement verifyPhoneIos() {
		return driver.findElement(By.xpath("//XCUIElementTypeStaticText[contains(@name,'"+phoneNumber+"')]"));
	}
	public MobileElement verifylocationIos() {
		return driver.findElement(By.xpath("//XCUIElementTypeStaticText[contains(@name,'"+location+"')]"));
	}
	public MobileElement verifyLanguageIos() {
		return driver.findElement(By.xpath("//XCUIElementTypeStaticText[contains(@name,'"+language+"')]"));
	}
}

//		@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Dismiss All']")
//		MobileElement dismissBTN;
//		
//		@AndroidFindBy(className  = "android.widget.ProgressBar")
//		@iOSXCUITFindBy(xpath = "//XCUIElementTypeActivityIndicator[@name='In progress']")
//		MobileElement profileICON;
//		
//		@AndroidFindBy(xpath  = "//android.widget.TextView[@text=' Powered By ']")
//		@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=' Powered By ']")
//		MobileElement profileHeaderTEXT;
//		
//		@AndroidFindBy(xpath= "//android.widget.TextView[@text=\" © Copyright 2020 Unisys Corporation. \"]")
//		@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\" © Copyright 2020 Unisys Corporation. \"]")
//		MobileElement initialsTEXT;
//		
//		@AndroidFindBy(xpath= "//android.widget.TextView[@text=\" © Copyright 2020 Unisys Corporation. \"]")
//		@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\" © Copyright 2020 Unisys Corporation. \"]")
//		MobileElement nameTEXT;
//		
//		@AndroidFindBy(className  = "android.widget.ImageView")
//		@iOSXCUITFindBy(className = "XCUIElementTypeImage")
//		List<MobileElement> navigationBar;
//		
//
//		public ProfileScreenPage(AppiumDriver<MobileElement> driver) {
//			this.driver = driver;
//			PageFactory.initElements(new AppiumFieldDecorator(driver), this);
//		}
//
//		public void clickDismiss() {
//			dismissBTN.click();
//		}
//		public MobileElement checkLoadingIcon() {
//			return profileICON;
//		}
//		public MobileElement checkPoweredByText() {
//			return profileHeaderTEXT;
//		}
//		public MobileElement checkCopyRightText(){
//			return initialsTEXT;
//		}
//		public  List<MobileElement> getlogoImgCount(){
//			return navigationBar;
//		}
//	}
