package com.apd.inteliserve.pompages;


import java.util.List;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class EditProfilePage {

	AppiumDriver<MobileElement> driver;


	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='options']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='options']")
	MobileElement editProfileICON;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='update_profile_phoneHeader']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='update_profile_phoneHeader']")
	MobileElement phoneTitleTEXT;


	@AndroidFindBy(xpath  = "//android.widget.EditText[@content-desc='update_profile_phoneSubHeader']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@label='update_profile_phoneSubHeader']")
	MobileElement phoneSubTitleTEXT;


	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='enable_save_text']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='enable_save_text']")
	MobileElement enableSaveBTN;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='disable_save_text']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='disable_save_text']")
	MobileElement disableSaveBTN;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='cancel_text']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='cancel_text']")
	MobileElement cancelBTN;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='update_profile_title']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='update_profile_title']")
	MobileElement editProfileHeaderText;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='phoneNumberInfo']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='phoneNumberInfo']")
	MobileElement phoneNumberInfoTEXT;

	//@AndroidFindBy(xpath  = "//android.widget.TextView[@id='android:id/alertTitle']")
	@AndroidFindBy(id  = "android:id/alertTitle")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='Invalid Number']")
	MobileElement invalidNumberTEXT;

	//@AndroidFindBy(xpath  = "//android.widget.Button[@id='android:id/button1']")
	@AndroidFindBy(id  = "android:id/button1")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@label='OK']")
	MobileElement okBTN;


	//@AndroidFindBy(xpath  = "//android.widget.TextView[@id='android:id/message']")
	@AndroidFindBy(id  = "android:id/message")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='Enter a valid phone number.']")
	MobileElement validPhoneNumberTEXT;

	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='clear_icon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='clear_icon']")
	MobileElement clearICON;

	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='alert_icon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='alert_icon']")
	MobileElement aleartICON;

	//@AndroidFindBy(xpath  = "//android.widget.TextView[@id='android:id/alertTitle']")
	@AndroidFindBy(id  = "android:id/alertTitle")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='You have unsaved changes.']")
	MobileElement unsavedChangesTEXT;

	//@AndroidFindBy(xpath  = "//android.widget.TextView[@id='android:id/message']")
	@AndroidFindBy(id  = "android:id/message")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Do you want to discard them?']")
	MobileElement discardTEXT;

	//(xpath  = "//android.widget.Button[@id='android:id/button2']")
	@AndroidFindBy(id  = "android:id/button2")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Discard']")
	MobileElement discardBTN;

	//@AndroidFindBy(xpath  = "//android.widget.Button[@id='android:id/button1']")
	@AndroidFindBy(id  = "android:id/button1")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Cancel']")
	MobileElement cancelPopupBTN;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='profile_item']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='profile_item']")
	MobileElement phoneNumberTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='emailHeaderText']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='emailHeaderText']")
	MobileElement emailHeaderTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='profileEmail']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='profileEmail']")
	MobileElement profileEmailTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='titleHeaderText']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='titleHeaderText profileTile forwardIconTitle']")
	MobileElement titleHeaderTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='profileTile']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='titleHeaderText profileTile forwardIconTitle']")
	MobileElement profileTitleTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='locationHeaderText']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='locationHeaderText profileTile forwardIconLocation']")
	MobileElement locationHeaderTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='profileTile'])[2]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='locationHeaderText profileTile forwardIconLocation']")
	MobileElement profileLocationTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='languageHeaderText']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Language French (France)' or @name='Language English (UK)' or @name='Language Danish' or @name='Language English (US)' or @name='Language Dutch (Netherlands)' or @name='Language French (Canada)' or @name='Language German (Germany)' or @name='Language Italian (Italy)' or @name='Language Japanese' or @name='Language Portuguese (Brazil)' or @name='Language Spanish (Colombia)' or @name='Language Spanish (Mexico)']")
	//@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='languageHeaderText profileLanguage forwardIconLanguage']")
	MobileElement languageHeaderTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='profileLanguage']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Language French (France)' or @name='Language English (UK)' or @name='Language Danish' or @name='Language English (US)' or @name='Language Dutch (Netherlands)' or @name='Language French (Canada)' or @name='Language German (Germany)' or @name='Language Italian (Italy)' or @name='Language Japanese' or @name='Language Portuguese (Brazil)' or @name='Language Spanish (Colombia)' or @name='Language Spanish (Mexico)']")
	//@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='languageHeaderText profileLanguage forwardIconLanguage']")
	MobileElement profileLanguageTEXT;

	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='cameraIcon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='cameraIcon']")
	MobileElement cameraICON;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='update_profile_name']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='update_profile_name']")
	MobileElement profileNameTEXT;

	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='forwardIconTitle']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='titleHeaderText profileTile forwardIconTitle']")
	MobileElement titleForwardICON;

	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='forwardIconLocation']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='locationHeaderText profileTile forwardIconLocation']")
	MobileElement locationForwardICON;

	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='forwardIconLanguage']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='languageHeaderText profileLanguage forwardIconLanguage']")
	MobileElement languageForwardICON;


	//@AndroidFindBy(xpath  = "//android.widget.TextView[@id='android:id/alertTitle']")
	@AndroidFindBy(id  = "android:id/alertTitle")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Unable to Edit Profile']")
	MobileElement unableToEditTEXT;

	//@AndroidFindBy(xpath  = "//android.widget.TextView[@id='android:id/message']")
	@AndroidFindBy(id  = "android:id/message")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='ServcieNow account is needed to edit the profile.']")
	MobileElement serviceAccountNeededTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='basicLanguage']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='language basicLanguage']")
	MobileElement basicLanguageTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='language']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='language basicLanguage']")
	List<MobileElement> allLanguageTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='update_language_title']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='update_language_title']")
	MobileElement LanguageHeaderTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='edit_profile_header']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='edit_profile_header']")
	MobileElement allLangaugesTEXT;


	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='language'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage tickIcon'])[1]")
	MobileElement firstLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='language'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[1]")
	MobileElement secondLanguageTEXT;


	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='language'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[2]")
	MobileElement thirdLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='language'])[4]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[3]")
	MobileElement fourthLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='language'])[5]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[4]")
	MobileElement fifthLanguageTEXT;;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='language'])[6]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[5]")
	MobileElement sixthLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='language'])[7]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[6]")
	MobileElement seventhLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='language'])[8]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[7]")
	MobileElement eigthLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='language'])[9]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[8]")
	MobileElement ninthLanguageTEXT;


	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='language'])[10]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[9]")
	MobileElement tenthLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='language'])[10]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[10]")
	MobileElement eleventhLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='language'])[11]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[11]")
	MobileElement twelvethLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage tickIcon'])[1]")
	MobileElement firstBasicLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[1]")
	MobileElement secondBasicLanguageTEXT;


	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[2]")
	MobileElement thirdBasicLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[4]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[3]")
	MobileElement fourthBasicLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[5]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[4]")
	MobileElement fifthBasicLanguageTEXT;;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[6]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[5]")
	MobileElement sixthBasicLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[7]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[6]")
	MobileElement seventhBasicLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[8]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[7]")
	MobileElement eigthBasicLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[9]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[8]")
	MobileElement ninthBasicLanguageTEXT;


	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[10]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[9]")
	MobileElement tenthBasicLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[10]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[10]")
	MobileElement eleventhBasicLanguageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[11]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage'])[11]")
	MobileElement twelvethBasicLanguageTEXT;

	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='tickIcon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='policyitem tickIcon']")
	MobileElement tickICON;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[1]/..//android.widget.ImageView[@content-desc='tickIcon']")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage tickIcon'])[1]")
	MobileElement firstLanguageTickIcon;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[2]/..//android.widget.ImageView[@content-desc='tickIcon']")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage tickIcon'])[1]")
	MobileElement secondLanguageTickIcon;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='basicLanguage'])[3]/..//android.widget.ImageView[@content-desc='tickIcon']")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='language basicLanguage tickIcon'])[3]")
	MobileElement thirdLanguageTickIcon;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='profile_item'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='profile_item'])[3]")
	MobileElement profilePageLanguageTEXT;


	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles tickIcon'])[1]")
	MobileElement firstTitleTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles'])[1]")
	MobileElement secondTitleTEXT;


	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles'])[2]")
	MobileElement thirdTitleTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[4]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles'])[3]")
	MobileElement fourthTitleTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[5]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles'])[4]")
	MobileElement fifthTitleTEXT;;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[6]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles'])[5]")
	MobileElement sixthTitleTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[7]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles'])[6]")
	MobileElement seventhTitleTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[8]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles'])[7]")
	MobileElement eigthTitleTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[9]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles'])[8]")
	MobileElement ninthTitleTEXT;


	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[10]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles'])[9]")
	MobileElement tenthTitleTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[11]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles'])[10]")
	MobileElement eleventhTitleTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[12]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles'])[11]")
	MobileElement twelvethTitleTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='update_title_title']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='update_title_title']")
	MobileElement tittleHeaderTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='edit_profile_header']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='edit_profile_header']")
	MobileElement allTitlesTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='titles']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='titles']")
	List<MobileElement> allTitleTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[1]/..//android.widget.ImageView[@content-desc='tickIcon']")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles tickIcon'])[1]")
	MobileElement firstTitleTickIcon;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[2]/..//android.widget.ImageView[@content-desc='tickIcon']")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles tickIcon'])[1]")
	MobileElement secondTitleTickIcon;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='titles'])[3]/..//android.widget.ImageView[@content-desc='tickIcon']")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='titles tickIcon'])[3]")
	MobileElement thirdTitleTickIcon;


	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='user_title']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='user_title']")
	MobileElement profilePageTitleTEXT;


	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Add Profile Photo']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Add Profile Photo']")
	MobileElement addProfilePhotoTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Take a Photo']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Take a Photo']")
	MobileElement takePhotoTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Cancel']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Cancel']")
	MobileElement cancelTEXT;

	@AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='user_title']")
	MobileElement allowBTN;

	@AndroidFindBy(id  = "com.android.camera2:id/shutter_button")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='user_title']")
	MobileElement capturPhotoBTN;

	@AndroidFindBy(id  = "com.android.camera2:id/done_button")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='user_title']")
	MobileElement selectPhotoBTN;

	@AndroidFindBy(id = "com.android.camera2:id/cancel_button")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='user_title']")
	MobileElement cancelPhotoBTN;

	@AndroidFindBy(id  = "com.android.camera2:id/retake_button")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='user_title']")
	MobileElement retakeBTN;


	@AndroidFindBy(id  = "com.unisys.inteliapp:id/menu_crop")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='user_title']")
	MobileElement cropBTN;

	@AndroidFindBy(xpath  = "//android.widget.ImageButton[@content-desc='Navigate up']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='Navigate up']")
	MobileElement navigateUpBTN;

	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='editprofile_image']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='editprofile_image']")
	MobileElement editProfileIMG;
	
	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='profile_image']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='profile_image']")
	MobileElement profileIMG;
	
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Remove Current Photo']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='Remove Current Photo']")
	MobileElement removeCurrentPhotoTEXT;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='update_profile_initials']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='update_profile_initials']")
	MobileElement updateProfileInitialsTEXT;
	
	@AndroidFindBy(id  = "com.unisys.inteliapp:id/state_rotate")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='user_title']")
	MobileElement rotateBTN;
	
	@AndroidFindBy(id  = "com.unisys.inteliapp:id/wrapper_rotate_by_angle")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='user_title']")
	MobileElement rotateByAngleBTN;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Change Profile Photo']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='Remove Current Photo']")
	MobileElement changeProfilePhotoTEXT;
	



	//titleHeaderText profileTile forwardIconTitle




	public EditProfilePage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	//	public void clickDismiss() {
	//		dismissBTN.click();
	//	}
	public MobileElement verifyEditProfileIcon() {
		return editProfileICON;
	}
	public MobileElement verifyPhoneTitle() {
		return phoneTitleTEXT;
	}
	public MobileElement verifyPhoneSubTitle(){
		return phoneSubTitleTEXT;
	}
	public MobileElement verifyEnableSaveButton(){
		return enableSaveBTN;
	}
	public MobileElement verifyDisableSaveButton(){
		return disableSaveBTN;
	}
	public MobileElement verifyCancelButton(){
		return cancelBTN;
	}
	public MobileElement verifyEditProfileHeader(){
		return editProfileHeaderText;
	}
	public MobileElement verifyPhoneNumberInfo(){
		return phoneNumberInfoTEXT;
	}
	public MobileElement verifyInvalidText(){
		return invalidNumberTEXT;
	}
	public MobileElement verifyOkButton(){
		return okBTN;
	}
	public MobileElement verifyValidPhoneNumberText(){
		return validPhoneNumberTEXT;
	}
	public MobileElement verifyClearIcon(){
		return clearICON ;
	}
	public MobileElement verifyAlertIcon(){
		return aleartICON ;
	}

	public MobileElement verifyUnsavedChangesText(){
		return unsavedChangesTEXT;
	}
	public MobileElement verifyDiscardText(){
		return discardTEXT;
	}
	public MobileElement verifyDiscardButton(){
		return discardBTN;
	}
	public MobileElement verifyPopCancelButton(){
		return cancelPopupBTN;
	}
	public MobileElement verifyCameraIcon(){
		return cameraICON;
	}
	public MobileElement verifyProfileName(){
		return profileNameTEXT;
	}
	public MobileElement verifyEmailLabel(){
		return emailHeaderTEXT;
	}
	public MobileElement verifyProfileEmail(){
		return profileEmailTEXT;
	}
	public MobileElement verifyTitleLabel(){
		return titleHeaderTEXT;
	}
	public MobileElement verifyLocationLabel(){
		return locationHeaderTEXT;
	}
	public MobileElement verifyProfileTitle(){
		return profileTitleTEXT;
	}
	public MobileElement verifyProfileLocation(){
		return profileLocationTEXT;
	}
	public MobileElement verifyLangauageLabel(){
		return languageHeaderTEXT;
	}
	public MobileElement verifyProfileLanguage(){
		return profileLanguageTEXT;
	}
	public MobileElement verifyPhoneNumber(){
		return phoneNumberTEXT;
	}
	public MobileElement verifyTitleForwardIcon(){
		return titleForwardICON;
	}
	public MobileElement verifyLanguageForwardIcon(){
		return languageForwardICON;
	}
	public MobileElement verifyLocationForwardIcon(){
		return locationForwardICON;
	}
	public MobileElement verifyUnableToEditText(){
		return unableToEditTEXT;
	}
	public MobileElement verifyServiceAccountNeedText(){
		return serviceAccountNeededTEXT;
	}
	public MobileElement verifyBasicLanguage(){
		return basicLanguageTEXT;
	}
	public List<MobileElement> getAllLanguage(){
		return allLanguageTEXT;
	}

	public MobileElement verifyLanguageSubHeader(){
		return allLangaugesTEXT;
	}

	public MobileElement verifyLanguageHeader(){
		return LanguageHeaderTEXT;
	}
	public MobileElement verifyFirstLanguage(){
		return firstLanguageTEXT;
	}
	public MobileElement verifySecondLanguage(){
		return secondLanguageTEXT;
	}
	public MobileElement verifyThirdLanguage(){
		return thirdLanguageTEXT;
	}
	public MobileElement verifyFourthLanguage(){
		return fourthLanguageTEXT;
	}
	public MobileElement verifyFifthLanguage(){
		return fifthLanguageTEXT;
	}
	public MobileElement verifySixthLanguage(){
		return sixthLanguageTEXT;
	}
	public MobileElement verifySeventhLanguage(){
		return seventhLanguageTEXT;
	}
	public MobileElement verifyEighthLanguage(){
		return eigthLanguageTEXT;
	}
	public MobileElement verifyNinthLanguage(){
		return ninthLanguageTEXT;
	}
	public MobileElement verifyTenthLanguage(){
		return tenthLanguageTEXT;
	}
	public MobileElement verifyEleventhLanguage(){
		return eleventhLanguageTEXT;
	}
	public MobileElement verifyTwelvethLanguage(){
		return twelvethLanguageTEXT;
	}
	public MobileElement verifyFirstBasicLanguage(){
		return firstBasicLanguageTEXT;
	}
	public MobileElement verifySecondBasicLanguage(){
		return secondBasicLanguageTEXT;
	}
	public MobileElement verifyThirdBasicLanguage(){
		return thirdBasicLanguageTEXT;
	}
	public MobileElement verifyFourthBasicLanguage(){
		return fourthBasicLanguageTEXT;
	}
	public MobileElement verifyFifthBasicLanguage(){
		return fifthBasicLanguageTEXT;
	}
	public MobileElement verifySixthBasicLanguage(){
		return sixthBasicLanguageTEXT;
	}
	public MobileElement verifySeventhBasicLanguage(){
		return seventhBasicLanguageTEXT;
	}
	public MobileElement verifyEighthBasicLanguage(){
		return eigthBasicLanguageTEXT;
	}
	public MobileElement verifyNinthBasicLanguage(){
		return ninthBasicLanguageTEXT;
	}
	public MobileElement verifyTenthBasicLanguage(){
		return tenthBasicLanguageTEXT;
	}
	public MobileElement verifyEleventhBasicLanguage(){
		return eleventhBasicLanguageTEXT;
	}
	public MobileElement verifyTwelvethBasicLanguage(){
		return twelvethBasicLanguageTEXT;
	}
	public MobileElement verifyTickIcon(){
		return tickICON ;
	}

	public MobileElement verifyFirstLangaugeTickItem(){
		return firstLanguageTickIcon;
	}
	public MobileElement verifySecondLanguageTickItem(){
		return secondLanguageTickIcon;
	}
	public MobileElement verifyThirdLanguageTickItem(){
		return thirdLanguageTickIcon;
	}
	public MobileElement verifyProfilePageLanguage(){
		return profilePageLanguageTEXT;
	}
	public MobileElement verifyTitleSubHeader(){
		return allTitlesTEXT;
	}

	public MobileElement verifyTitleHeader(){
		return tittleHeaderTEXT;
	}
	public MobileElement verifyFirstTitle(){
		return firstTitleTEXT;
	}
	public MobileElement verifySecondTitle(){
		return secondTitleTEXT;
	}
	public MobileElement verifyThirdTitle(){
		return thirdTitleTEXT;
	}
	public MobileElement verifyFourthTitle(){
		return fourthTitleTEXT;
	}
	public MobileElement verifyFifthTitle(){
		return fifthTitleTEXT;
	}
	public MobileElement verifySixthTitle(){
		return sixthTitleTEXT;
	}
	public MobileElement verifySeventhTitle(){
		return seventhTitleTEXT;
	}
	public MobileElement verifyEighthTitle(){
		return eigthTitleTEXT;
	}
	public MobileElement verifyNinthTitle(){
		return ninthTitleTEXT;
	}
	public MobileElement verifyTenthTitle(){
		return tenthTitleTEXT;
	}
	public MobileElement verifyEleventhTitle(){
		return eleventhTitleTEXT;
	}
	public MobileElement verifyTwelvethTitle(){
		return twelvethTitleTEXT;
	}
	public List<MobileElement> getAllTitle(){
		return allTitleTEXT;
	}
	public MobileElement verifyFirstTitleTickItem(){
		return firstTitleTickIcon;
	}
	public MobileElement verifySecondTitleTickItem(){
		return secondTitleTickIcon;
	}
	public MobileElement verifyThirdTitkeTickItem(){
		return thirdTitleTickIcon;
	}
	public MobileElement verifyProfilePageTitle(){
		return profilePageTitleTEXT;
	}
	public MobileElement verifyAddProfilePhotoLabel(){
		return addProfilePhotoTEXT;
	}
	public MobileElement verifyTakePhotoLabel(){
		return takePhotoTEXT;
	}
	public MobileElement verifyCancelLabel(){
		return cancelTEXT;
	}
	public MobileElement verifyAllowButton(){
		return allowBTN;
	}
	public MobileElement verifyCapturePhotoButton(){
		return capturPhotoBTN;
	}
	public MobileElement verifySelectPhotoButton(){
		return selectPhotoBTN;
	}
	public MobileElement verifyCancelPhotoButton(){
		return cancelPhotoBTN;
	}
	public MobileElement verifyRetakePhotoButton(){
		return retakeBTN;
	}
	public MobileElement verifyCropButton(){
		return cropBTN;
	}
	public MobileElement verifynavigateUpButton(){
		return navigateUpBTN;
	}
	public MobileElement verifyEditProfileImage(){
		return editProfileIMG;
	}
	public MobileElement verifyProfileImage(){
		return profileIMG;
	}
	public MobileElement verifyRemoveCurrentPhotoLabel(){
		return removeCurrentPhotoTEXT;
	}
	public MobileElement verifyUpdateProfileInitials(){
		return updateProfileInitialsTEXT;
	}
	public MobileElement clickRotateButton(){
		return rotateBTN;
	}
	public MobileElement clickRotateByAngleButton(){
		return rotateByAngleBTN;
	}
	public MobileElement verifyChangeProfilePhotoLabel(){
		return changeProfilePhotoTEXT;
	}
}
