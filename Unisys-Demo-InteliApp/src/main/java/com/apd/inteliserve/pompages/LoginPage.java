package com.apd.inteliserve.pompages;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class LoginPage {
	AppiumDriver<MobileElement> driver;

	@AndroidFindBy(xpath  = "//android.widget.EditText[@text='Enter Email Address']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='Enter Email Address']")
	MobileElement emailTB;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Login']")
	@iOSXCUITFindBy(id = "enable")
	MobileElement loginBTN;

	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@name='Enter your email, phone, or Skype.']")
	MobileElement azureEmailTB;


	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='enable']")
	@AndroidFindBy(xpath  ="//android.view.ViewGroup[@content-desc='enable']")
	MobileElement nextBTN;

	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='User Name']")
	MobileElement userNameTB;

	@AndroidFindBy(xpath  = "//android.widget.EditText[@content-desc='pwd']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeSecureTextField[@name='pwd']")
	MobileElement passwordTB;

	@iOSXCUITFindBy(xpath="(//XCUIElementTypeStaticText[@name='Log In'])[1]")
	MobileElement loginLink;

	@iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name='Continue']")
	MobileElement continueBTN;

	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Dismiss All']")
	MobileElement dismissBTN;

	@AndroidFindBy(xpath  = "//android.widget.EditText[@text='madhurvishal5@gmail.com']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='madhurvishal5@gmail.com']")
	MobileElement emailTb;


	@AndroidFindBy(xpath  = "//android.widget.EditText[@text='madhurvishal5.gmail.com']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='madhurvishal5.gmail.com']")
	//@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@name=\"email\"]")
	MobileElement emailTb1;

	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Hello')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Hello')]")
	MobileElement helloTEXT;

	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'This email address is not supported')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'This email address is not supported')]")
	MobileElement errorMSG;

	@AndroidFindBy(xpath  = "//android.widget.EditText[@text='Enter First Name']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='Enter First Name']")
	MobileElement firstNameTB;

	@AndroidFindBy(xpath  = "//android.widget.EditText[@text='Enter Last Name']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='Enter Last Name']")
	MobileElement lastNameTB;

	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='disable']")
	@iOSXCUITFindBy(id = "disable")
	MobileElement disableLoginBTN;

	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='enable']")
	@iOSXCUITFindBy(id = "enable")
	MobileElement enableLoginBTN;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Enter correct email address']")
	@iOSXCUITFindBy(xpath= "//XCUIElementTypeStaticText[@name='Enter correct email address']")
	MobileElement emailErrorMSG;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Enter First Name']")
	@iOSXCUITFindBy(xpath= "//XCUIElementTypeStaticText[@name='Enter First Name']")
	MobileElement firstNameErrorMSG;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Enter Last Name']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Enter Last Name']")
	MobileElement lastNameErrorMSG;

	@AndroidFindBy(xpath  = "//android.widget.EditText[@text='Iva']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='Iva']")
	MobileElement firstNameTb;

	@AndroidFindBy(xpath  = "//android.widget.EditText[@text='Scuser']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='Scuser']")
	MobileElement lastNameTb;

	@AndroidFindBy(xpath  = "//android.widget.Button[@text='ALLOW']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='Scuser']")
	MobileElement allowBTN;

	@AndroidFindBy(xpath= "//android.widget.TextView[@content-desc='support_text']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='support_text']")
	MobileElement phoneHelpTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@content-desc='loginPhoneSupport']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='loginPhoneIcon loginPhoneSupport']")
	MobileElement supportNumberTEXT;


	@AndroidFindBy(xpath= "//android.widget.ImageView[@content-desc='loginPhoneIcon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='loginPhoneIcon loginPhoneSupport']")
	MobileElement callICON;



	public LoginPage(AppiumDriver<MobileElement> driver2) {
		this.driver = driver2;
		PageFactory.initElements(new AppiumFieldDecorator(driver2), this);
	}

	public void setEmail(String emailAdress) {
		emailTB.sendKeys(emailAdress);
	}

	public void clickLogin() {
		loginBTN.click();
	}

	public void setAzureEmail(String email) {
		azureEmailTB.sendKeys(email);
	}
	public void clickNextButton() {
		nextBTN.click();
	}
	public void setUserName(String username) {
		userNameTB.sendKeys(username);
	}
	public void setPassword(String password) {
		passwordTB.sendKeys(password);
	}
	public void clickLoginLink() {
		loginLink.click();
	}

	public void clickContinueButton() {
		continueBTN.click();
	}

	public void clickDismiss() {
		dismissBTN.click();
	}

	public void clearEmail() {
		emailTb.clear();
	}
	public void clearWrongEmail() {
		emailTb1.clear();
	}
	public MobileElement verifyHelloText() {
		return helloTEXT;
	}
	public MobileElement verifyErrorMessgae() {
		return errorMSG;
	}
	public void setFirstName(String firstName) {
		firstNameTB.sendKeys(firstName);
	}
	public void setLastname(String lastName) {
		lastNameTB.sendKeys(lastName);
	}
	public MobileElement verifyDisableLoginButton() {
		return disableLoginBTN;
	}
	public MobileElement verifyEnableLoginButton() {
		return enableLoginBTN;
	}
	public MobileElement verifyEmailErrorMessage() {
		return emailErrorMSG;
	}

	public MobileElement verifyFirstNameErrorMessage() {
		return firstNameErrorMSG;
	}

	public MobileElement verifyLastNameErrorMessage() {
		return lastNameErrorMSG;

	}
	public void clickEmailTB() {
		emailTB.click();
	}
	public void clickFirstNameTB() {
		firstNameTB.click();
	}
	public void clickLastNameTB(){
		lastNameTB.click();
	}
	public void clearFirstName() {
		firstNameTb.clear();
	}
	public void clearLastName() {
		lastNameTb.clear();
	}
	public void clickAllowButton() {
		allowBTN.click();
	}
	public MobileElement verifyPhoneHelpText() {
		return phoneHelpTEXT;
	}
	public MobileElement verifySupportNumber() {
		return supportNumberTEXT;
	}
	public MobileElement verifyCallIcon() {
		return callICON;
	}

}



