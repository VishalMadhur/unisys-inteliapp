package com.apd.inteliserve.pompages;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class ConsentCapturePage {

	AppiumDriver<MobileElement> driver;


	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='header_title']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='header_title']")
	MobileElement policyAndNoticesHeaderTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='midscreentext']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='midscreentext']")
	MobileElement policesAndNoticesHelpTEXT;


	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='policyitem'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='policyitem' or @label='policyitem tick_icon'])[1]")
	MobileElement policyLINK;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='policyitem'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='policyitem' or @label='policyitem tick_icon'])[2]")
	MobileElement termsOfUseLink;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='policyitem'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='policyitem' or @label='policyitem tick_icon'])[3]")
	MobileElement endUserLisenceLink;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='confirmDescription']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='confirmDescription']")
	MobileElement policyConfirmTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='proceed']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='disable']")
	MobileElement confimAndProceedTEXT;


	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='disable']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='disable']")
	MobileElement disableConfirmAndProceedBTN;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='thanksno' or @content-desc='noThanks']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='thanksno' or @label='noThanks']")
	MobileElement noThanksBTN;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='modalHeader']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Privacy Policy' or @name='Terms of Use' or @name='End User License']")
	MobileElement policiyHeaderTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='Agree']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='enable_agree']")
	MobileElement agreeTEXT;
	
	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='enable_agree']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='enable_agree']")
	MobileElement enableAgreeBTN;
	
	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='enable_agree']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='enable_agree']")
	MobileElement enabledDisagreeBTN;
	

	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='tick_icon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='policyitem tick_icon']")
	MobileElement tickICON;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='disagree']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='enable_agree']")
	MobileElement disAgreeTEXT;

	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='enable']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='enable']")
	MobileElement enableConfirmAndProceedBTN;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='policyitem'])[1]/..//android.widget.ImageView[@content-desc='tick_icon']")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='policyitem tick_icon'])[1]")
	MobileElement privacyPolicyTickIcon;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='policyitem'])[2]/..//android.widget.ImageView[@content-desc='tick_icon']")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='policyitem tick_icon'])[position()<=3]")
	MobileElement TermsOfUseTickIcon;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='policyitem'])[3]/..//android.widget.ImageView[@content-desc='tick_icon']")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='policyitem tick_icon'])[position()<=3]")
	MobileElement EulaTickIcon;
	
	




	public ConsentCapturePage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}


	public MobileElement verifyPolicyAndNoticesHeader() {
		return policyAndNoticesHeaderTEXT;
	}
	public MobileElement verifyPolicyHelpText() {
		return policesAndNoticesHelpTEXT;
	}
	public MobileElement verifyPolicyLink(){
		return policyLINK;
	}
	public MobileElement verifyTermsOfUseLink(){
		return termsOfUseLink;
	}
	public MobileElement verifyEndUserLiscenceLink(){
		return endUserLisenceLink;
	}
	public MobileElement verifyPolicyConfimText(){
		return policyConfirmTEXT;
	}
	public MobileElement verifyConfirmAndProceedText(){
		return confimAndProceedTEXT;
	}
	public MobileElement verifyDisableConfirmButton(){
		return disableConfirmAndProceedBTN;
	}
	public MobileElement verifyNoThanksButton(){
		return noThanksBTN;
	}
	public MobileElement verifyPolicyHeader(){
		return policiyHeaderTEXT;
	}
	public MobileElement verifyAgreeText(){
		return agreeTEXT;
	}
	public MobileElement verifyDisagreeText(){
		return disAgreeTEXT ;
	}
	public MobileElement verifyTickIcon(){
		return tickICON ;
	}

	public MobileElement verifyEnableConfirmButton(){
		return enableConfirmAndProceedBTN;
	}
	public MobileElement verifyEnableAgreeButton(){
		return enableAgreeBTN;
	}

	public MobileElement verifyEnableDisAgreeButton(){
		return enabledDisagreeBTN;
	}
	public MobileElement verifyPrivacyPolicyTickItem(){
		return privacyPolicyTickIcon;
	}
	public MobileElement verifyTermsOfUseTickItem(){
		return TermsOfUseTickIcon;
	}
	public MobileElement verifyEulaTickItem(){
		return EulaTickIcon;
	}


}
