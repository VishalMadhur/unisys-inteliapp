package com.apd.inteliserve.pompages;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class LandingPage {

	AppiumDriver<MobileElement> driver;
	




	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Hello')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Hello')]")
	MobileElement helloTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Chat with Amelia']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Chat with Amelia']")
	MobileElement ivaTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Amelia, your Enterprise Personal Assistant')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Amelia, your Enterprise Personal Assistant')]")
	MobileElement epaTEXT;



//	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Chat')]")
//	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='chat'])[2]")
//	MobileElement letsChatBTN;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[@content-desc='chat']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='chat']")
	MobileElement letsChatBTN;
	
			
	
	//@AndroidFindBy(xpath= "//android.view.ViewGroup[@bounds='[332,811][748,931]']")
	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='chat']")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='chat'])[2]")
	MobileElement ChatBTN;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Lets Talk']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Lets Talk']")
	MobileElement letsTalkBTN;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='My Favorites']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='My Favorites']")
	MobileElement favoritesTEXT;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Knowledge')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='01']")
	MobileElement knowledgeBaseBTN;

	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Company')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='02']")
	MobileElement companyNewsBTN;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Human')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='03']")
	MobileElement coreSupportButton;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Payroll')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='06']")
	MobileElement payrollButton;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Online')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='07']")
	MobileElement onlineTrainigButton;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Human')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='04']")
	MobileElement HumanButton;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[@content-desc='01']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Knowledge Base']")
	MobileElement firstTILE;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[@content-desc='02']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Company News']")
	MobileElement secondTILE;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[@content-desc='03']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Support Corner']")
	MobileElement thirdTILE;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[@content-desc='04']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Human Resources']")
	MobileElement fourthTILE;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[@content-desc='05']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='University']")
	MobileElement fifthTILE;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[@content-desc='06']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Payroll']")
	MobileElement sixthTILE;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[@content-desc='07']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Online Training']")
	MobileElement seventhTILE;
	

	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'How can I help you today')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'How can I help you today')]")
	MobileElement noOutagesTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'having trouble retrieving current information')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'having trouble retrieving current information')]")
	MobileElement outagesErrorMessage;

	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'have any open tickets at this time')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'have any open tickets at this time')]")
	MobileElement noTicketsTEXT;


	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'INC')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'INC')]")
	MobileElement ticketNUM;

	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='Home']")
	@iOSXCUITFindBy(id = "Home")
	MobileElement homeICON;

	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='Profile']")
	@iOSXCUITFindBy(id = "Profile")
	MobileElement profileICON;

	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='IVA']")
	@iOSXCUITFindBy(id = "IVA")
	MobileElement ivaICON;
	
	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='Modules']")
	@iOSXCUITFindBy(id = "Modules")
	MobileElement launcherICON;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[@text='My Places']")
	@iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name='My Places']")
	MobileElement myPlacesTEXT;


	@AndroidFindBy(xpath= "//android.view.View[contains(@text,'menu toggle button.')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='toggle menu button']")
	MobileElement toggleBTN;

	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'working to restore your information shortly')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'working to restore your information shortly')]")
	MobileElement ticketErrorMessage;
	
	
	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'[Your Company Content]')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'[Your Company Content]')]")
	MobileElement companyContentTEXT;
	
	



	public LandingPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	//	public void clickDismiss() {
	//		dismissBTN.click();
	//	}
	public MobileElement verifyHelloText() {
		return helloTEXT;
	}
	public MobileElement verifyEpaText() {
		return epaTEXT;
	}
	public MobileElement verifyIvaText(){
		return ivaTEXT;
	}
	public MobileElement verifyLetsChatButton(){
		return letsChatBTN;
	}
	public MobileElement chatButton(){
		return ChatBTN;
	}
	public MobileElement verifyLetsTalkButton(){
		return letsTalkBTN;
	}
	public MobileElement verifyFavoritesText(){
		return favoritesTEXT;
	}
	public MobileElement verifyKnowledgeBaseButton(){
		return knowledgeBaseBTN;
	}
	public MobileElement verifyCompanyNewsButton(){
		return companyNewsBTN;
	}
	public MobileElement verifyCoreSupportButton(){
		return coreSupportButton;
	}
	public MobileElement verifyOnlineTrainingButton(){
		return onlineTrainigButton;
	}
	public MobileElement verifyHumanButton(){
		return HumanButton;
	}
	
	public MobileElement verifypayrollButton(){
		return payrollButton;
	}
	public MobileElement verifyNoOutagesText(){
		return noOutagesTEXT;
	}
	public MobileElement verifyTicketNumText(){
		return ticketNUM;
	}
	public MobileElement homeIcon(){
		return homeICON;
	}
	public MobileElement profileIcon(){
		return profileICON;
	}

	public MobileElement ivaIcon(){
		return ivaICON;
	}
	

	public MobileElement launcherIcon(){
		return launcherICON;
	}

	public MobileElement verifyNoTicketText(){
		return noTicketsTEXT;
	}
	public MobileElement toggleButton(){
		return toggleBTN;
	}
	public MobileElement verifyOutageErrorMessage(){
		return outagesErrorMessage;
	}
	public MobileElement verifyTicketErrorMessage(){
		return ticketErrorMessage;
	}
	public MobileElement verifyMyPlacesHeader(){
		return myPlacesTEXT;
	}
	public MobileElement verifyFirstTile(){
		return firstTILE;
	}
	public MobileElement verifySecondTile(){
		return secondTILE;
	}
	public MobileElement verifyThirdTile(){
		return thirdTILE;
	}
	public MobileElement verifyFourthTile(){
		return fourthTILE;
	}
	public MobileElement verifyFifthTile(){
		return fifthTILE;
	}
	public MobileElement verifySixthTile(){
		return sixthTILE;
	}
	public MobileElement verifySeventhTile(){
		return seventhTILE;
	}
	
	public MobileElement verifyCompanyContentLabel(){
		return companyContentTEXT;
	}

}
