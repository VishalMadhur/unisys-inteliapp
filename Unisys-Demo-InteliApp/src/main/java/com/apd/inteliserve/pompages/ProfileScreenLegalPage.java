package com.apd.inteliserve.pompages;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class ProfileScreenLegalPage {
	AppiumDriver<MobileElement> driver;


	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='Profile']")
	@iOSXCUITFindBy(id = "Profile")
	MobileElement profileICON;

	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'COMPANY')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'COMPANY')]")
	MobileElement companyTEXT;
	
	

	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Latitude')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Address')]")
	MobileElement latitudeTEXT;

	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Longitude')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Address')]")
	MobileElement longitudeTEXT;

	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Address')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Address')]")
	MobileElement addressTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Privacy Policy']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Privacy Policy']")
	MobileElement privacyPolicyTEXT;
	
	//@AndroidFindBy(xpath= "//android.widget.TextView[@bounds='[40,887][1000,937]']")
	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='about_item'])[1]")
	MobileElement privacyPolicyLINK;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Terms of Use']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Terms of Use']")
	MobileElement termsOfUseTEXT;

	//@AndroidFindBy(xpath= "//android.widget.TextView[@bounds='[40,1004][1000,1054]']")
	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='about_item'])[2]")
	MobileElement termsOfUseLINK;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='UNISYS']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='UNISYS']")
	MobileElement unisysTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='About']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='About']")
	MobileElement aboutTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='About']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='About']")
	MobileElement aboutHeaderTEXT;

	@AndroidFindBy(xpath= "//android.widget.EditText[contains(@text,'.com')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='TopBrowserBar']")
	//@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Address']")
	MobileElement currentURL;


	@AndroidFindBy(xpath= "//android.widget.EditText[contains(@text,'.com')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@name='URL']")
	MobileElement getCurrentURL;

	//@AndroidFindBy(xpath= "//android.widget.TextView[@bounds='[40,1122][1000,1172]']")
	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='about_item'])[3]")
	MobileElement endUserLicenseLINK;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='End User License']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='End User License']")
	MobileElement endUserLicenseTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Third-Party Notices']")
	@iOSXCUITFindBy(xpath = "	//XCUIElementTypeOther[@name='Third-Party Notices']")
	MobileElement thirdPartyPolicyTEXT;

	//@AndroidFindBy(xpath= "//android.widget.TextView[@bounds='[40,1239][1000,1289]']")
	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='about_item'])[4]")
	MobileElement thirdPartyPolicyLINK;
	
	
	


	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Copyright 2021 Unisys Corporation')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Copyright 2021 Unisys Corporation')]")
	MobileElement copyRightTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Version')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Version')]")
	MobileElement versionNumberTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Unisys provides users with an omnichannel point of engagement')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Unisys provides users with an omnichannel point of engagement')]")
	MobileElement tagLineTEXT;

	@AndroidFindBy(className  = "android.widget.ImageView")
	@iOSXCUITFindBy(className = "XCUIElementTypeImage")
	List<MobileElement> logoImg;

	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='back']")
	@iOSXCUITFindBy(id = "back")
	MobileElement backButton;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Profile']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Profile']")
	MobileElement profileHeaderTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Phone: +1 (516) 581-5686']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Lets Talk']")
	MobileElement supportNumber;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='End User License']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='End User License']")
	MobileElement endUserLicenseHeaderTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Third-Party Notices']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Third-Party Notices']")
	MobileElement thirdPartyNoticesHeaderTEXT;

	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='Home']")
	@iOSXCUITFindBy(id = "Home")
	MobileElement homeICON;

	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='IVA']")
	@iOSXCUITFindBy(id = "IVA")
	MobileElement ivaICON;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='End User License']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Lets Talk']")
	MobileElement endUserLiscenceTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Sign Out']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Sign Out']")
	MobileElement signOutLINK;

	@AndroidFindBy(xpath= "//android.widget.Button[@text='CANCEL']")
	//@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Cancel']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Cancel']")
	MobileElement cancelButton;

	@AndroidFindBy(xpath= "//android.widget.Button[@text='CONFIRM']")
	//@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Confirm']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Confirm']")
	MobileElement confirmButton;

	@AndroidFindBy(xpath  = "//android.widget.EditText[@text='Enter Email Address']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='Enter Email Address']")
	MobileElement emailTB;

	@AndroidFindBy(xpath  = "//android.widget.EditText[@text='Enter First Name']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='Enter First Name']")
	MobileElement firstNameTB;

	@AndroidFindBy(xpath  = "//android.widget.EditText[@text='Enter Last Name']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='Enter Last Name']")
	MobileElement lastNameTB;

	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Hello')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Hello')]")
	MobileElement helloTEXT;


	public ProfileScreenLegalPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}


	public MobileElement profileIcon(){
		return profileICON;
	}
	public MobileElement companyText() {
		return companyTEXT;
	}
	public MobileElement latitudeLabel() {
		return latitudeTEXT;
	}
	public MobileElement longitudeLabel() {
		return longitudeTEXT;
	}
	public MobileElement addressLabel() {
		return addressTEXT;
	}
	public MobileElement privacyPolicyText() {
		return privacyPolicyTEXT;
	}
	public MobileElement termsOfUseText(){
		return termsOfUseTEXT;
	}
	public MobileElement verifyUnisysText(){
		return unisysTEXT;
	}
	public MobileElement aboutText(){
		return aboutTEXT;
	}
	public MobileElement getURL(){
		return currentURL;
	}
	public MobileElement getCurrentURL(){
		return getCurrentURL;
	}
	public MobileElement endUserText(){
		return endUserLicenseTEXT;
	}
	public MobileElement thirdPartyPolicyText(){
		return thirdPartyPolicyTEXT;
	}
	public MobileElement VersionNumberText(){
		return versionNumberTEXT;
	}
	public MobileElement taglineText(){
		return tagLineTEXT;
	}
	public MobileElement copyrightText(){
		return copyRightTEXT;
	}
	public  List<MobileElement> getlogoImgCount(){
		return logoImg;
	}
	public MobileElement clickBackButton(){
		return backButton;
	}
	public MobileElement profileHeaderText(){
		return profileHeaderTEXT;
	}
	public MobileElement verifyAboutHeader(){
		return aboutHeaderTEXT;
	}
	public void clickSupportNumber(){
		supportNumber.click();
	}
	public MobileElement verifyThirdPartyNoticesHeader(){
		return thirdPartyNoticesHeaderTEXT;
	}
	public MobileElement verifyEndUserLiscenceHeader(){
		return endUserLicenseHeaderTEXT;
	}
	public MobileElement homeIcon(){
		return homeICON;
	}
	public MobileElement ivaIcon(){
		return ivaICON;
	}
	public MobileElement signOutLink(){
		return signOutLINK;
	}
	public MobileElement cancelButton(){
		return cancelButton;
	}
	public MobileElement confirmButton(){
		return confirmButton;
	}
	public MobileElement emailTextBox(){
		return emailTB;
	}
	public MobileElement verifyHelloText() {
		return helloTEXT;
	}
	public MobileElement firstNameTextBox() {
		return firstNameTB;
	}
	public MobileElement lastNameTextBox(){
		return lastNameTB;
	}
	public MobileElement privacyPloicyLink(){
		return privacyPolicyLINK;
	}
	public MobileElement termsOfUseLink(){
		return termsOfUseLINK;
	}
	public MobileElement endUserLicenseLink(){
		return endUserLicenseLINK;
	}
	public MobileElement thirdPartyPloicyLink(){
		return thirdPartyPolicyLINK;
	}
}
