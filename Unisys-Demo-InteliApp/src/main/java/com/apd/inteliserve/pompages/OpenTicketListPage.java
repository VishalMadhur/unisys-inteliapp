package com.apd.inteliserve.pompages;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

/**
 * @author  VishalMadhur
 *
 */

public class OpenTicketListPage {

	AppiumDriver<MobileElement> driver;


	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'INC')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@label,'ticket_no')]")
	MobileElement ticketNUM;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='On Hold' or @text='In Progress' or @text='New']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'On Hold') or contains(@name,'In Progress') or contains(@name,'New')]")
	MobileElement statusTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Low' or @text='High' or @text='Critical' or @text='Moderate']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'1') or contains(@name,'2') or contains(@name,'3') or contains(@name,'4')]")
	MobileElement priorityTEXT;

	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='handle']")
	@iOSXCUITFindBy(id = "handle")
	MobileElement swipeBar;

	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Hello')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Hello')]")
	MobileElement helloTEXT;

	
	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'have any open tickets at this time')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'have any open tickets at this time')]")
	MobileElement noTicketsTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'AM') or contains(@text,'PM')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'AM') or contains(@name,'PM')]")
	MobileElement tickedDate;

	@AndroidFindBy(xpath= "(//android.widget.TextView[contains(@text,'INC')])[2]")
	//	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='ticket']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@label,'ticket_no')]")
	MobileElement randomTicket;

	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='back']")
	@iOSXCUITFindBy(id = "back")
	MobileElement backButton;

	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'working to restore your information shortly')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'working to restore your information shortly')]")
	MobileElement ticketErrorMessage;

	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='My Ticket Details']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='My Ticket Details']")
	MobileElement myTicketsHeaderTxt;




	public OpenTicketListPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	public MobileElement verifyTicketNumber(){
		return ticketNUM;
	}
	public MobileElement verifyPriorityText(){
		return priorityTEXT;
	}
	public MobileElement verifyStatusText(){
		return statusTEXT;
	}
	public MobileElement upSwipeBar(){
		return swipeBar;
	}
	public MobileElement swipeTillHelloText(){
		return helloTEXT;
	}

	public MobileElement verifyNoTicketText(){
		return noTicketsTEXT;
	}
	public MobileElement verifyTicketDate(){
		return tickedDate;
	}
	public MobileElement randomTicket(){
		return randomTicket;
	}
	public MobileElement clickBackButton(){
		return backButton;
	}
	public MobileElement verifyTicketErrorMessage(){
		return ticketErrorMessage;
	}
	public MobileElement verifyTicketHeaderText() {
		return myTicketsHeaderTxt;
	}
}
