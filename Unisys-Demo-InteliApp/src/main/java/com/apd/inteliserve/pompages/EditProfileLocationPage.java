package com.apd.inteliserve.pompages;


import java.util.List;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class EditProfileLocationPage {

	AppiumDriver<MobileElement> driver;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='update_location_title']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='update_location_title']")
	MobileElement locationHeaderTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='phone_header_text'])[3]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='phone_header_text']")
	MobileElement locationSubHeaderTEXT;


	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[2]")
	MobileElement firstCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[4]")
	MobileElement secondCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[6]")
	MobileElement thirdCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[4]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[8]")
	MobileElement fourthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[5]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[10]")
	MobileElement fifthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[6]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[12]")
	MobileElement sixthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[7]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[14]")
	MobileElement seventhCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[8]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[16]")
	MobileElement eighthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[9]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[18]")
	MobileElement ninethCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[10]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[20]")
	MobileElement tenthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[11]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[22]")
	MobileElement eleventhCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[12]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[24]")
	MobileElement twelvethCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[13]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[26]")
	MobileElement thirteenthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@text='India'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[28]")
	MobileElement fourteenthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@text='Japan'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[30]")
	MobileElement fifteenthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[32]")
	MobileElement sixteentthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[34]")
	MobileElement seventeenthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[36]")
	MobileElement eighteenthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[4]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[38]")
	MobileElement nineteenthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[5]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[40]")
	MobileElement twentythCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[6]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[42]")
	MobileElement twentyFirstCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[7]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[44]")
	MobileElement twentySecondCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[8]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[46]")
	MobileElement twentyThirdCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[9]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[48]")
	MobileElement twentyFourthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[10]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[50]")
	MobileElement twentyFifthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[11]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[52]")
	MobileElement twentySixthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[12]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[54]")
	MobileElement twentySeventhCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[13]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[56]")
	MobileElement twentyEighthCountryNameTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName rightIcon'])[1]")
	MobileElement twentyNinethCountryNameTEXT;
	
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='changeButtonText']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='countryIcon selectedCountry changeButtonText']")
	MobileElement changeBTN;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='update_city_title']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='update_city_title']")
	MobileElement locationTitleTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='phone_header_text'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@label='phone_header_text'])[1]")
	MobileElement countryTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='phone_header_text'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@label='phone_header_text'])[2]")
	MobileElement allLocationsTEXT;
	
	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='city_disable']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='city_disable']")
	MobileElement disableDoneBTN;
	
	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='city_enable']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='city_enable']")
	MobileElement enableDoneBTN;
	
	
	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='tickIcon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='title subTitle tickIcon']")
	MobileElement tickICON;
	
	@AndroidFindBy(xpath  = "(//android.view.ViewGroup[@content-desc='back'])[2]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='back']")
	MobileElement countryPageBackICON;
	
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='selectedCountry']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='countryIcon selectedCountry changeButtonText']")
	MobileElement selectedCountryTEXT;
	
	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='countryIcon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='countryIcon selectedCountry changeButtonText']")
	MobileElement countryFlagICON;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[2]")
	MobileElement firstOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[4]")
	MobileElement secondOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[6]")
	MobileElement thirdOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[3]")
	MobileElement officeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[4]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[8]")
	MobileElement fourthOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[5]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[10]")
	MobileElement fifthOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[6]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[12]")
	MobileElement sixthOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[7]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[14]")
	MobileElement seventhOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[8]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[16]")
	MobileElement eighthOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[4]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[18]")
	MobileElement ninethOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[5]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[20]")
	MobileElement tenthOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[6]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[22]")
	MobileElement eleventhOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[7]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[24]")
	MobileElement twelevethOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[8]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[26]")
	MobileElement thirteenthOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='title'])[9]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[28]")
	MobileElement fourthteenthOfficeTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[2]")
	MobileElement firstCityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[4]")
	MobileElement secondCityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[6]")
	MobileElement thirdCityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[3]")
	MobileElement cityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[4]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[8]")
	MobileElement fourthCityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[5]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[10]")
	MobileElement fifthCityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[6]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[12]")
	MobileElement sixthCityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[7]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[14]")
	MobileElement seventhCityTEXT;
	
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[8]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[16]")
	MobileElement eighthCityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[4]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[18]")
	MobileElement ninethCityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[5]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[20]")
	MobileElement tenthCityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[6]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[22]")
	MobileElement eleventhCityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[7]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[24]")
	MobileElement twelevethCityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[8]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[26]")
	MobileElement thirteenthCityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='subTitle'])[9]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='title subTitle'])[28]")
	MobileElement fourthteenthCityTEXT;
	
	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='profile_item'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='profile_item'])[2]")
	MobileElement profileLocationTEXT;
	
	
	
	

	

	public EditProfileLocationPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	public MobileElement verifyFirstCountry() {
		return firstCountryNameTEXT;
	}
	public MobileElement verifySecondCountry() {
		return secondCountryNameTEXT;
	}
	public MobileElement verifyThirdCountry() {
		return thirdCountryNameTEXT;
	}
	public MobileElement verifyFourthCountry() {
		return fourthCountryNameTEXT;
	}
	public MobileElement verifyFifthCountry() {
		return fifthCountryNameTEXT;
	}
	public MobileElement verifySixthCountry() {
		return sixthCountryNameTEXT;
	}
	public MobileElement verifySeventhCountry() {
		return seventhCountryNameTEXT;
	}
	public MobileElement verifyEighthCountry() {
		return eighthCountryNameTEXT;
	}
	public MobileElement verifyNinethCountry() {
		return ninethCountryNameTEXT;
	}
	public MobileElement verifyTenthCountry() {
		return tenthCountryNameTEXT;
	}
	public MobileElement verifyEleventhCountry() {
		return eleventhCountryNameTEXT;
	}
	public MobileElement verifyTwelevethCountry() {
		return twelvethCountryNameTEXT;
	}
	public MobileElement verifyThirteenthCountry() {
		return thirteenthCountryNameTEXT;
	}
	public MobileElement verifyFourteenthCountry() {
		return fourteenthCountryNameTEXT;
	}
	public MobileElement verifyFifteenthCountry() {
		return fifteenthCountryNameTEXT;
	}
	public MobileElement verifySixteenthCountry() {
		return sixteentthCountryNameTEXT;
	}
	public MobileElement verifySeventeenthCountry() {
		return seventeenthCountryNameTEXT;
	}
	public MobileElement verifyEighteenthCountry() {
		return eighteenthCountryNameTEXT;
	}
	public MobileElement verifyNineteenthCountry() {
		return nineteenthCountryNameTEXT;
	}
	public MobileElement verifyTwenteithCountry() {
		return twentythCountryNameTEXT;
	}
	public MobileElement verifyTwentyFirstCountry() {
		return twentyFirstCountryNameTEXT;
	}
	public MobileElement verifyTwentySecondCountry() {
		return twentySecondCountryNameTEXT;
	}
	public MobileElement verifyTwentyThirdCountry() {
		return twentyThirdCountryNameTEXT;
	}
	public MobileElement verifyTwentyFourthCountry() {
		return twentyFourthCountryNameTEXT;
	}
	public MobileElement verifyTwentyFifthCountry() {
		return twentyFifthCountryNameTEXT;
	}
	public MobileElement verifyTwentySixthCountry() {
		return twentySixthCountryNameTEXT;
	}
	public MobileElement verifyTwentySeventhCountry() {
		return twentySeventhCountryNameTEXT;
	}
	public MobileElement verifyTwentyEighthCountry() {
		return twentyEighthCountryNameTEXT;
	}
	public MobileElement verifyTwentyNinethCountry() {
		return twentyNinethCountryNameTEXT;
	}
	public MobileElement verifyChangeButton() {
		return changeBTN;
	}
	public MobileElement verifyLocationHeader() {
		return locationHeaderTEXT;
	}
	public MobileElement verifyLocationSubHeader() {
		return locationSubHeaderTEXT;
	}
	public MobileElement verifyLocationTitle() {
		return locationTitleTEXT;
	}
	public MobileElement verifyCountryLabel() {
		return countryTEXT;
	}
	public MobileElement verifyAllLocationsLabel() {
		return allLocationsTEXT;
	}
	public MobileElement verifyDisabledDoneButton() {
		return disableDoneBTN;
	}
	public MobileElement verifyEnabledDoneButton() {
		return enableDoneBTN;
	}
	public MobileElement verifyTickIcon() {
		return tickICON;
	}
	public MobileElement getFirstOffice() {
		return officeTEXT;
	}
	public MobileElement getFirstCity() {
		return cityTEXT;
	}
	public MobileElement verifyFirstOffice() {
		return firstOfficeTEXT;
	}
	public MobileElement verifySecondOffice() {
		return secondOfficeTEXT;
	}
	public MobileElement verifyThirdOffice() {
		return thirdOfficeTEXT;
	}
	public MobileElement verifyFourthOffice() {
		return fourthOfficeTEXT;
	}
	public MobileElement verifyFifthOffice() {
		return fifthOfficeTEXT;
	}
	public MobileElement verifySixthOffice() {
		return sixthOfficeTEXT;
	}
	public MobileElement verifySeventhOffice() {
		return seventhOfficeTEXT;
	}
	public MobileElement verifyEighthOffice() {
		return eighthOfficeTEXT;
	}
	public MobileElement verifyNinethOffice() {
		return ninethOfficeTEXT;
	}
	public MobileElement verifyTenthOffice() {
		return tenthOfficeTEXT;
	}
	public MobileElement verifyEleventhOffice() {
		return eleventhOfficeTEXT;
	}
	public MobileElement verifyTwelvethOffice() {
		return twelevethOfficeTEXT;
	}
	public MobileElement verifyThirteenthOffice() {
		return thirteenthOfficeTEXT;
	}
	public MobileElement verifyFourthteenthOffice() {
		return fourthteenthOfficeTEXT;
	}
	public MobileElement verifyFirstCity() {
		return firstCityTEXT;
	}
	public MobileElement verifySecondCity() {
		return secondCityTEXT;
	}
	public MobileElement verifyThirdCity() {
		return thirdCityTEXT;
	}
	public MobileElement verifyFourthCity() {
		return fourthCityTEXT;
	}
	public MobileElement verifyFifthCity() {
		return fifthCityTEXT;
	}
	public MobileElement verifySixthCity() {
		return sixthCityTEXT;
	}
	public MobileElement verifySeventhCity() {
		return seventhCityTEXT;
	}
	public MobileElement verifyEighthCity() {
		return eighthCityTEXT;
	}
	public MobileElement verifyNinethCity() {
		return ninethCityTEXT;
	}
	public MobileElement verifyTenthCity() {
		return tenthCityTEXT;
	}
	public MobileElement verifyEleventhCity() {
		return eleventhCityTEXT;
	}
	public MobileElement verifyTwelvethCity() {
		return twelevethCityTEXT;
	}
	public MobileElement verifyThirteenthCity() {
		return thirteenthCityTEXT;
	}
	public MobileElement verifyFourthteenthCity() {
		return fourthteenthCityTEXT;
	}
	public MobileElement verifySelectedCountry() {
		return selectedCountryTEXT;
	}
	public MobileElement verifyCountryPageBackIcon() {
		return countryPageBackICON;
	}
	public MobileElement verifyProfilePageLocation() {
		return profileLocationTEXT;
	}
	
	
	
}
