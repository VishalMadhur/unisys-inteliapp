package com.apd.inteliserve.pompages;


import java.util.List;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class KnowledgeBasePage {

	AppiumDriver<MobileElement> driver;




	//@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='title']")
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='header_title']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='header_title']")
	MobileElement knowledgeBaseHeaderTEXT;
	
	
	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='options']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='options']")
	MobileElement contextualMENU;

	@AndroidFindBy(xpath  = "//android.widget.EditText[@content-desc='knowledgeBase_searchKeyword' or @content-desc='article_searchKeyword' or @bounds='[40,293][1040,393]' or @bounds='[40,100][883,200]' or @bounds='[48,250][1552,338]' or @bounds='[48,96][1394,184]']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@label='knowledgeBase_searchKeyword' or @label='article_searchKeyword']")
	MobileElement SearchTEXTBOX;
	
	//@AndroidFindBy(xpath  = "//android.widget.EditText[@content-desc='knowledgeBase_searchKeyword']")
	@AndroidFindBy(xpath  = "//android.widget.EditText[@content-desc='article_searchKeyword' or @bounds='[40,293][1040,393]' or @bounds='[40,100][883,200]' or @bounds='[48,250][1552,338]' or @bounds='[48,96][1394,184]']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@label='knowledgeBase_searchKeyword' or @label='article_searchKeyword']")
	MobileElement articlePageSearchTEXTBOX;
	
	
	@AndroidFindBy(xpath  = "//android.widget.EditText[@text='VPN']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@name='VPN']")
	MobileElement SearchTEXT;



	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='kb_landing_categories']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='kb_landing_categories']")
	MobileElement categoriesTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='kb_switch_header']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='kb_switch_header']")
	MobileElement SeeMoreTEXT;
	

	@AndroidFindBy(xpath  = "//android.widget.TextView[contains(@text,'find the answer')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@label,'find the answer')]")
	MobileElement canNotFindAnswerTEXT;


	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Chat with Amelia']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@label='Chat with Amelia']")
	MobileElement chatWithAmeliaTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Write to Us']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@label='Write to Us']")
	MobileElement writeToUsTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Cancel']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@label='Cancel']")
	MobileElement cancelTEXT;

	
	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='back']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='back']")
	MobileElement backICON;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='category_item']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='category_item forward_icon']")
	MobileElement firstCategoryITEM;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='article_item']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='article_item forward_icon']")
	MobileElement firstArticleITEM;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='category_item']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='category_item forward_icon']")
	List<MobileElement> categoriesITEMS;
	
	//@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='knowledgeBase_cancel']")
	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Cancel']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='knowledgeBase_cancel']")
	MobileElement cancelBTN;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='search_item']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='search_item forward_icon']")
	List<MobileElement> searchITEMS;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='search_item']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='search_item forward_icon']")
	MobileElement firstSearchITEM;
	
	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='clear_icon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='search_item']")
	MobileElement clearICON;
	
	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='search_icon']")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='search_icon'])[2]/XCUIElementTypeImage")
	MobileElement searchSYMBOL;
	
	
	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'VPN') or contains(@text,'Vpn') or contains(@text,'vpn')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'VPN') or contains(@name,'Vpn') or contains(@name,'vpn')]")
	List<MobileElement> searchTEXTS;
	
	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='search_icon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='search_icon']/XCUIElementTypeImage")
	MobileElement searchICON;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='kb_search_noresult']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='kb_search_noresult']")
	MobileElement noResultsTEXT;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='kb_search_result']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='kb_search_result']")
	MobileElement searchResultsTEXT;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='kb_search_spell']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='kb_search_spell']")
	MobileElement noResultsHelpTEXT;
	
	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='search_icon']")
	@iOSXCUITFindBy(xpath = "///XCUIElementTypeOther[@name='eye_icon']/XCUIElementTypeImage")
	MobileElement eyeICON;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='kb_search_article']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='kb_search_article']")
	MobileElement searchHelpTEXT;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='article_dateCreated']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='article_dateCreated']")
	MobileElement articleDateTEXT;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='article_headerText']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='article_headerText']")
	MobileElement articleTitleTEXT;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='attachments_title']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='attachments_title']")
	MobileElement attachmentsHeaderTEXT;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='attachments_fileName']")
	@iOSXCUITFindBy(xpath = "////XCUIElementTypeOther[@label='attachments_fileName']")
	MobileElement attachmentNAME;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='An Error Occurred']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='An Error Occurred']")
	MobileElement attachmentErrorTEXT;
	
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='File Not Supported']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='File Not Supported']")
	MobileElement fileNotSupportedTEXT;
	
	
	@AndroidFindBy(xpath  = "//android.widget.Button[@text='DISMISS']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Dismiss']")
	MobileElement dismissBTN;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Problem Description:']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Problem Description:']")
	MobileElement articleConentTEXT;
	
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='Printer name: ufbp0035 on ufbspsr ']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='redbox-dismiss']")
	MobileElement dismissEscapeBTN;
	
	
	
	@AndroidFindBy(xpath  = "//android.widget.ImageView[@bounds='[40,1345][615,1765]' or @bounds='[48,1095][508,1431]']")
	@iOSXCUITFindBy(className = "XCUIElementTypeImage")
	MobileElement embededIMG;
	
	
	@AndroidFindBy(xpath  = "//android.widget.ImageButton[@content-desc='Navigate up']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@label='Done']")
	MobileElement closeAttachmentBTN;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='kb_article_pagetiltle']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='kb_article_pagetiltle']")
	MobileElement articlePageTitleTEXT;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='coresupportcenter_askit']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='coresupportcenter_askit']")
	MobileElement askItTEXT;
	
	
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='article_item']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@label,'article_item forward_icon')]")
	MobileElement forwardICON;
	
	
	
	
	
	
	
	public KnowledgeBasePage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	

	public MobileElement verifyKnowledgeHeaderText(){
		return knowledgeBaseHeaderTEXT;
	}
	public MobileElement verifyContextualMenu(){
		return contextualMENU;
	}
	public MobileElement verifySearchTextBox(){
		return SearchTEXTBOX;
	}
	
	public MobileElement verifyArticlePageSearchTextBox(){
		return articlePageSearchTEXTBOX;
	}
	
	public MobileElement verifySearchText(){
		return SearchTEXT;
	}
	public MobileElement verifyCategoriesText(){
		return categoriesTEXT;
	}
	public MobileElement verifySeeMoreText(){
		return SeeMoreTEXT;
	}
	public MobileElement verifyCanNotFindAnswerText(){
		return  canNotFindAnswerTEXT;
	}
	public MobileElement verifyChatWithAmeliaText(){
		return chatWithAmeliaTEXT;
	}
	public MobileElement verifyWriteToUsText(){
		return writeToUsTEXT;
	}
	public MobileElement verifyCancelText(){
		return cancelTEXT;
	}
	
	public MobileElement clickBackIcon(){
		return backICON;
	}
	
	public MobileElement clickFirstCategory(){
		return firstCategoryITEM;
	}
	
	public MobileElement clickFirstArticle(){
		return firstArticleITEM;
	}
	
	public  List<MobileElement> getcategoriesCount(){
		return categoriesITEMS;
	}
	
	public  List<MobileElement> getsearchItemsCount(){
		return searchITEMS;
	}
	
	public  List<MobileElement> getsearchTextsCount(){
		return searchTEXTS;
	}
	public MobileElement clickCancelButton(){
		return cancelBTN;
	}
	
	public MobileElement clickFirstSearchItem(){
		return firstSearchITEM ;
	}
	
	public MobileElement verifySearchIcon(){
		return searchICON;
	}
	public MobileElement verifySearchIconInSearchBox(){
		return searchSYMBOL;
	}
	
	public MobileElement verifyNoResultsText(){
		return noResultsTEXT;
	}
	public MobileElement verifyNoResultsHelpText(){
		return noResultsHelpTEXT;
	}
	
	public MobileElement verifyEyeIcon(){
		return eyeICON;
	}
	
	public MobileElement verifySearchHelpText(){
		return searchHelpTEXT;
	}
	
	public MobileElement verifyClearIcon(){
		return clearICON;
	}
	
	public MobileElement verifySearchResultsText(){
		return searchResultsTEXT;
	}
	
	public MobileElement verifyArticleDate(){
		return articleDateTEXT;
	}
	
	public MobileElement verifyArticleTitle(){
		return articleTitleTEXT;
	}
	
	public MobileElement verifyAttachmentsTitle(){
		return attachmentsHeaderTEXT;
	}
	
	public MobileElement verifyAttachments(){
		return attachmentNAME;
	}
	
	public MobileElement verifyAttachmentsErrorMessage(){
		return attachmentErrorTEXT;
	}
	
	public MobileElement verifyFileNotSupportedText(){
		return fileNotSupportedTEXT;
	}
	
	public MobileElement clickDismissButton(){
		return dismissBTN;
	}
	public MobileElement clickDismissEscapeButton(){
		return dismissEscapeBTN;
	}
	public MobileElement verifyArticleContent(){
		return articleConentTEXT;
	}
	public MobileElement verifyEmbededImage(){
		return embededIMG;
	}
	public MobileElement clickCloseAttachmentButton(){
		return closeAttachmentBTN;
	}
	public MobileElement verifyArticlePageTitle(){
		return articlePageTitleTEXT;
	}
	public MobileElement verifyAskITText(){
		return askItTEXT;
	}
	public MobileElement verifyForwardIcon(){
		return forwardICON;
	}
	
	
}

