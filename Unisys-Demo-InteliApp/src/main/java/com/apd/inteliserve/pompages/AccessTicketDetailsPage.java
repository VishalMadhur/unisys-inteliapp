package com.apd.inteliserve.pompages;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class AccessTicketDetailsPage {
	AppiumDriver<MobileElement> driver;

	@AndroidFindBy(xpath= "(//android.widget.TextView[contains(@text,'INC')])[1]")
	////XCUIElementTypeOther[@name="In Progress Feb 26, 9:30 PM INC0111346 Low [IVA]Set up outlook on phone"]
	//@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'INC') and contains(@name,'New') and contains(@name,'Low')]")
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'INC')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"Status Identified 2 Apr 28, 12:22 PM INC0345650 Critical laptop screen issue 2\"]")
	MobileElement incidentNUM;


	@AndroidFindBy(xpath  = "//android.widget.TextView[@text='My Ticket Details']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='My Ticket Details']")
	MobileElement myTicketsHeaderTxt;


	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'INC')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'INC')]")
	MobileElement ticketNUM;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Incident Summary']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Incident Summary']")
	MobileElement incidentSummaryTEXT;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Opened by']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Opened by']")
	MobileElement openedByTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Created on']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Created on']")
	MobileElement createdOnTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Assigned to']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Assigned to']")
	MobileElement assignedToTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Description']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Description']")
	MobileElement descriptionTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='On Hold' or @text='In Progress' or @text='New']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='On Hold' or @name='In Progress' or @name='New']")
	MobileElement statusTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Low' or @text='High' or @text='Critical' or @text='Moderate']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Low' or @name='High' or @name='Critical' or @name='Moderate']")
	MobileElement priorityTEXT;
	
	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='back']")
	@iOSXCUITFindBy(id = "back")
	MobileElement backButton;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Hello')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Hello')]")
	MobileElement helloTEXT;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[@text='You don't have any open tickets at this time.']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='You don't have any open tickets at this time.']")
	MobileElement noTicketsTEXT;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'AM') or contains(@text,'PM')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'AM') or contains(@name,'PM')]")
	MobileElement tickedDate;


	public AccessTicketDetailsPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	public MobileElement clickFirstTicket(){
		 return incidentNUM;
	}

	public MobileElement verifyTicketHeaderText() {
		return myTicketsHeaderTxt;
	}
	public MobileElement verifyTicketNumber() {
		return ticketNUM;
	}
	public MobileElement verifyIncidentSummaryText(){
		return incidentSummaryTEXT;
	}
	public MobileElement verifyOpenedByText(){
		return openedByTEXT;
	}
	public MobileElement verifyCreatedOnText(){
		return createdOnTEXT;
	}
	public MobileElement verifyAssignedToText(){
		return assignedToTEXT;
	}
	public MobileElement verifyDescriptionText(){
		return descriptionTEXT;
	}
	public MobileElement verifyPriorityText(){
		return priorityTEXT;
	}
	public MobileElement verifyStatusText(){
		return statusTEXT;
	}
	public MobileElement clickBackButton(){
		return backButton;
	}
	public MobileElement verifyHelloText(){
		return helloTEXT;
	}
	public MobileElement verifyNoTicketText(){
		return noTicketsTEXT;
	}
	public MobileElement verifyTicketDate(){
		return tickedDate;
	}
}
