package com.apd.inteliserve.pompages;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class SplashScreenPage {
	AppiumDriver<MobileElement> driver;

	
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Dismiss All']")
	MobileElement dismissBTN;
	
	@AndroidFindBy(className  = "android.widget.ProgressBar")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeActivityIndicator[@name='In progress']")
	MobileElement loadingICON;
	
	@AndroidFindBy(xpath  = "//android.widget.TextView[@text=' Powered By ']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=' Powered By ']")
	MobileElement poweredByTEXT;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[@text=' © Copyright 2020 Unisys Corporation. ']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=' © Copyright 2020 Unisys Corporation. ']")
	MobileElement copyRightTEXT;
	
	@AndroidFindBy(className  = "android.widget.ImageView")
	@iOSXCUITFindBy(className = "XCUIElementTypeImage")
	List<MobileElement> logoImg;
	

	public SplashScreenPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	public void clickDismiss() {
		dismissBTN.click();
	}
	public MobileElement verifyLoadingIcon() {
		return loadingICON;
	}
	public MobileElement verifyPoweredByText() {
		return poweredByTEXT;
	}
	public MobileElement verifyCopyRightText(){
		return copyRightTEXT;
	}
	public  List<MobileElement> getlogoImgCount(){
		return logoImg;
	}
}



