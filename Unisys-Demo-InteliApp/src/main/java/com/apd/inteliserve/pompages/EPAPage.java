package com.apd.inteliserve.pompages;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class EPAPage {
	AppiumDriver<MobileElement> driver;


	//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Dismiss All']")
	//	MobileElement dismissBTN;




	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='IVA']")
	@iOSXCUITFindBy(id = "IVA")
	MobileElement ivaICON;


	@AndroidFindBy(xpath= "//android.view.View[contains(@text,'menu toggle button.')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='nav/menu']/XCUIElementTypeImage")
	MobileElement toggleBTN;

	@AndroidFindBy(className= "android.widget.EditText")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Your message'])[2]")
	MobileElement editBox;
	
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='main']/XCUIElementTypeOther[4]")
	MobileElement messageBox;
	
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Return']")
	MobileElement returnBTN;
	
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name='delete']")
	MobileElement deleteBTN;

	@AndroidFindBy(xpath= "//android.widget.Button[@text='Send message']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Send message']")
	MobileElement sendMessage;

	@AndroidFindBy(xpath= "//android.view.View[contains(@text,'INC')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'INC') or contains(@value,'INC')]")
	MobileElement ticketNumEPA;
	
	@AndroidFindBy(xpath= "//android.view.View[contains(@text,'INC')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'INC')]")
	List<MobileElement> openTicketsList;
	
	@AndroidFindBy(xpath= "(//android.view.View[contains(@text,'INC')])[last()]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[contains(@name,'INC')])[last()]")
	MobileElement lastTicketNumEPA;
	
	@AndroidFindBy(xpath= "(//android.view.View[contains(@text,'INC')])[last()-1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[contains(@name,'INC')])[last()-1]")
	MobileElement secondLastticketNumEPA;

	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'INC')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'INC')]")
	MobileElement ticketNUM;

	@AndroidFindBy(xpath= "//android.widget.RadioButton[@text='Escalate ticket']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Escalate ticket']")
	MobileElement escalateTicketButton;

	@AndroidFindBy(xpath= "//android.widget.RadioButton[@text='Create ticket']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Create ticket']")
	MobileElement createTicketButton;

	@AndroidFindBy(xpath= "//android.widget.Button[@text='Reset Conversation']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Reset Conversation']")
	MobileElement resetConversationButton;


	@AndroidFindBy(xpath= "//android.widget.RadioButton[@text='Add comments to ticket']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Add comments to ticket']")
	MobileElement addCommentButton;

	@AndroidFindBy(xpath= "//android.widget.RadioButton[@text='No']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='No']")
	MobileElement noButton;


	@AndroidFindBy(xpath= "//android.widget.RadioButton[@text='Yes']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Yes']")
	MobileElement yesButton;

	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='ticket']")
	@iOSXCUITFindBy(id = "IVA")
	MobileElement ticketDetails;

	@AndroidFindBy(className= "android.view.View")
	@iOSXCUITFindBy(id = "IVA")
	List<MobileElement> ivaResponseOne;

	@AndroidFindBy(className= "android.view.View")
	@iOSXCUITFindBy(id = "IVA")
	MobileElement ivaResponse;

	@AndroidFindBy(xpath= "//android.widget.EditText[@bounds='[40,1465][1040,1590]']")
	@iOSXCUITFindBy(xpath ="//XCUIElementTypeStaticText[@name='Title']")
	//@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='main']/XCUIElementTypeOther[4]/XCUIElementTypeOther[1]/XCUIElementTypeTextField")
	MobileElement titleEditBox;


	@AndroidFindBy(xpath= "//android.widget.EditText[@bounds='[40,1592][1040,1717]']")
	@iOSXCUITFindBy(xpath ="//XCUIElementTypeStaticText[@name='Description']")
	//@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='main']/XCUIElementTypeOther[4]/XCUIElementTypeOther[2]/XCUIElementTypeTextField")
	MobileElement descriptioinEditBox;


	@AndroidFindBy(xpath= "//android.view.View[contains(@text,'chat note toggle button')]")
	@iOSXCUITFindBy(xpath ="//XCUIElementTypeOther[contains(@name,'chat note toggle button')]")
	MobileElement chattoggleBTN;

	@AndroidFindBy(xpath= "(//android.view.View[contains(@text,'INC')])[1]")
	//@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[contains(@name,'INC')])[last()]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'INC')]")
	MobileElement createdTicketNUM;

	@AndroidFindBy(xpath= "//android.widget.Image[@text='close Chat Notes']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeImage[@name='close Chat Notes']")
	MobileElement closeChatNotesIcon;




	public EPAPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	//	public void clickDismiss() {
	//		dismissBTN.click();
	//	}


	public MobileElement ivaIcon(){
		return ivaICON;
	}
	public MobileElement toggleButton(){
		return toggleBTN;
	}
	public MobileElement editBox(){
		return editBox;
	}
	public MobileElement messageBox(){
		return messageBox;
	}
	public MobileElement sendMessage(){
		return sendMessage;
	}
	public MobileElement verifyTicketNumText(){
		return ticketNUM;
	}
	public MobileElement escalateTicketButton(){
		return escalateTicketButton;
	}
	public MobileElement createTicketButton(){
		return createTicketButton;
	}
	public MobileElement addCommentButton(){
		return addCommentButton;
	}
	public MobileElement noRadioButton(){
		return noButton;
	}
	public MobileElement yesRadioButton(){
		return yesButton;
	}
	public MobileElement verifyTicketDetails(){
		return ticketDetails;
	}
	public MobileElement verifyTicketNumEPA(){
		return ticketNumEPA;
	}
	public MobileElement verifyLastTicketNumEPA(){
		return lastTicketNumEPA;
	}
	public MobileElement verifySecondLastTicketNumEPA(){
		return secondLastticketNumEPA;
	}
	public List<MobileElement> verifyIVAResponseOne(){
		return ivaResponseOne;
	}
	public List<MobileElement> verifyOpenTicketsList(){
		return openTicketsList;
	}
	public MobileElement verifyIVAResponse(){
		return ivaResponse;
	}

	public MobileElement titleEditBox(){
		return titleEditBox;
	}

	public MobileElement descriptionEditBox(){
		return descriptioinEditBox;
	}
	public MobileElement chatToggleButton(){
		return chattoggleBTN;
	}
	public MobileElement getCreatedTicketNumber(){
		return createdTicketNUM;
	}
	public MobileElement closeChatIcon(){
		return closeChatNotesIcon;
	}
	public MobileElement resetConversationButton(){
		return resetConversationButton;
	}
	public MobileElement clickReturnButton(){
		return returnBTN;
	}
	public MobileElement clickDeleteButton(){
		return deleteBTN;
	}

}

