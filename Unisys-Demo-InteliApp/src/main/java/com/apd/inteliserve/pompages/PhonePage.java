package com.apd.inteliserve.pompages;


import java.util.List;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class PhonePage {

	AppiumDriver<MobileElement> driver;



	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='phone_title']")
	//@AndroidFindBy(xpath= "//android.widget.TextView[@bounds='[100,60][980,250]']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='phone_title']")
	MobileElement phoneHeaderTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='phone_header_call_text'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@label='phone_header_call_text'])[1]")
	MobileElement locationTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='countryName']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='countryIcon countryName forwardIcon']")
	MobileElement selectCountryTEXT;



	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='countryName']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='countryIcon countryName changeButtonText']")
	MobileElement displayedCountryTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='selectLocation']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='selectLocation']")
	MobileElement selectLocationHelpTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='phone_header_call_text'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@label='phone_header_call_text'])[2]")
	MobileElement callAnAgentTEXT;


	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName'])[1]")
	MobileElement firstCountryTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName'])[2]")
	MobileElement secondCountryTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[4]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName'])[3]")
	MobileElement thirdCountryTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[5]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName'])[4]")
	MobileElement fourthCountryTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[6]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName'])[5]")
	MobileElement fifthCountryTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[7]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName'])[6]")
	MobileElement sixthCountryTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='countryName'])[8]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='countryIcon countryName'])[7]")
	MobileElement seventhCountryTEXT;

	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='tickIcon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='countryIcon countryName tickIcon']")
	MobileElement tickICON;

	@AndroidFindBy(xpath  = "//android.view.ViewGroup[@content-desc='back']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='back']")
	MobileElement backICON;

	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='countryIcon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@label,'countryIcon')]")
	MobileElement countryIcon;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='phoneNumber']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='phoneNumber phoneNumberLangText phoneNumberCovText phoneCallIcon']")
	MobileElement phoneNumberTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='phoneNumberLangText']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='phoneNumber phoneNumberLangText phoneNumberCovText phoneCallIcon']")
	MobileElement phoneNumberLangaugeTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='phoneNumberCovText']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='phoneNumber phoneNumberLangText phoneNumberCovText phoneCallIcon']")
	MobileElement phoneNumberCoverageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='phoneNumber'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='phoneNumber phoneNumberLangText phoneNumberCovText phoneCallIcon'])[2]")
	MobileElement secondPhoneNumberTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='phoneNumberLangText'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='phoneNumber phoneNumberLangText phoneNumberCovText phoneCallIcon'])[2]")
	MobileElement secondPhoneNumberLangaugeTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='phoneNumberCovText'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='phoneNumber phoneNumberLangText phoneNumberCovText phoneCallIcon'])[2]")
	MobileElement secondPhoneNumberCoverageTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='phoneNumber'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='phoneNumber phoneNumberLangText phoneNumberCovText phoneCallIcon'])[3]")
	MobileElement thirdPhoneNumberTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='phoneNumberLangText'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='phoneNumber phoneNumberLangText phoneNumberCovText phoneCallIcon'])[3]")
	MobileElement thirdPhoneNumberLangaugeTEXT;

	@AndroidFindBy(xpath  = "(//android.widget.TextView[@content-desc='phoneNumberCovText'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='phoneNumber phoneNumberLangText phoneNumberCovText phoneCallIcon'])[3]")
	MobileElement thirdPhoneNumberCoverageTEXT;

	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='phoneCallIcon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@label,'phoneCallIcon')]")
	MobileElement phoneCallICON;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='changeButtonText']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='countryIcon countryName changeButtonText']")
	MobileElement changeCountryBTN;


	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='location_title']")
	//@AndroidFindBy(xpath= "//android.widget.TextView[@bounds='[100,60][980,250]']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='location_title']")
	MobileElement locationHeaderTEXT;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='phone_header_text']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='phone_header_text']")
	MobileElement countriesLabelTEXT;

	@AndroidFindBy(xpath  = "//android.widget.ImageView[@content-desc='countryIcon']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='countryIcon countryName']")
	List<MobileElement> countryIcons;

	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='countryName']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='countryIcon countryName']")
	List<MobileElement> countryNames;










	public PhonePage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}


	public MobileElement verifyPhoneHeaderText(){
		return phoneHeaderTEXT;
	}
	public MobileElement verifylocationText(){
		return locationTEXT;
	}
	public MobileElement verifySelectCountryText(){
		return selectCountryTEXT;
	}
	public MobileElement verifyDisplayedCountryText(){
		return displayedCountryTEXT;
	}
	public MobileElement verifyLocationHelpText(){
		return selectLocationHelpTEXT;
	}
	public MobileElement verifyCallAnAgentText(){
		return  callAnAgentTEXT;
	}
	public MobileElement verifyFirstCountry(){
		return firstCountryTEXT;
	}
	public MobileElement verifySecondCountry(){
		return secondCountryTEXT;
	}
	public MobileElement verifyThirdCountry(){
		return thirdCountryTEXT;
	}
	public MobileElement verifyFourthCountry(){
		return fourthCountryTEXT;
	}
	public MobileElement verifyFifthCountry(){
		return fifthCountryTEXT;
	}
	public MobileElement verifySixthCountry(){
		return sixthCountryTEXT;
	}
	public MobileElement verifySeventhCountry(){
		return seventhCountryTEXT;
	}
	public MobileElement verifyTickIcon(){
		return tickICON;
	}
	public MobileElement clickBackIcon(){
		return backICON;
	}
	public MobileElement verifyCountryIcon(){
		return countryIcon;
	}
	public MobileElement verifyPhoneNumber(){
		return phoneNumberTEXT;
	}
	public MobileElement verifyPhoneNumberLanguage(){
		return phoneNumberLangaugeTEXT;
	}
	public MobileElement verifyPhoneNumberCoverage(){
		return phoneNumberCoverageTEXT;
	}
	public MobileElement verifySecondPhoneNumber(){
		return secondPhoneNumberTEXT;
	}
	public MobileElement verifySecondPhoneNumberLanguage(){
		return secondPhoneNumberLangaugeTEXT;
	}
	public MobileElement verifySecondPhoneNumberCoverage(){
		return secondPhoneNumberCoverageTEXT;
	}
	public MobileElement verifyThirdPhoneNumber(){
		return thirdPhoneNumberTEXT;
	}
	public MobileElement verifyThirdPhoneNumberLanguage(){
		return thirdPhoneNumberLangaugeTEXT;
	}
	public MobileElement verifyThirdPhoneNumberCoverage(){
		return thirdPhoneNumberCoverageTEXT;
	}
	public MobileElement verifylocationHeader(){
		return locationHeaderTEXT;
	}
	public MobileElement verifycountriesHeader(){
		return countriesLabelTEXT;
	}
	public MobileElement verifyPhoneCallIcon(){
		return phoneCallICON;
	}
	public MobileElement clickChangeCountryButton(){
		return changeCountryBTN;
	}
	public  List<MobileElement> getcountryIconsCount(){
		return countryIcons;
	}
	public  List<MobileElement> getcountryNamesCount(){
		return countryNames;
	}
}

