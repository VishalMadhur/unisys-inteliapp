package com.apd.inteliserve.pompages;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class MyPlacesPage {

	AppiumDriver<MobileElement> driver;


	@AndroidFindBy(xpath  = "//android.widget.TextView[@content-desc='header_title']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='header_title']")
	MobileElement MyPlacesHeaderTEXT;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='places_category_desc'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@label='places_category_desc'])[1]")
	MobileElement supportDescriptionTEXT;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='places_category_desc'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@label='places_category_desc'])[2]")
	MobileElement learningDescriptionTEXT;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='places_category_desc'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@label='places_category_desc'])[3]")
	MobileElement companyDescriptionTEXT;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='places_category_desc'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@label='places_category_desc'])[4]")
	MobileElement servicesDescriptionTEXT;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='places_category_title'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@label='places_category_title'])[1]")
	MobileElement supportTitleTEXT;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='places_category_title'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@label='places_category_title'])[2]")
	MobileElement learningTitleTEXT;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='places_category_title'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@label='places_category_title'])[3]")
	MobileElement companyTitleTEXT;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='places_category_title'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@label='places_category_title'])[4]")
	MobileElement servicesTitleTEXT;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[1]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Support Corner']")
	MobileElement supportCornerTILE;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[2]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Knowledge Base']")
	MobileElement knowledgeBaseTILE;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[2]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='tile_name'])[4]")
	MobileElement knowledgeBaseButton;
	
	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[1]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='tile_name'])[2]")
	MobileElement supportCornerButton;
	
	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[3]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='tile_name'])[6]")
	MobileElement crisisManagementButton;
	
	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[5]")
	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@label='tile_name'])[22]")
	MobileElement companyNewsButton;




	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[3]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Crisis Management']")
	MobileElement crisisManagementTILE;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[3]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Emergency Contacts']")
	MobileElement emerergencyContactsTILE;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[4]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='CSR']")
	MobileElement CSRTILE;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[5]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Feedback']")
	MobileElement feedbackTILE;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[5]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='University']")
	MobileElement universityTILE;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[6]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Blog']")
	MobileElement blockTILE;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[7]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Tech Talk']")
	MobileElement techTalkTILE;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[8]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Online Training']")
	MobileElement onlineTrainingTILE;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[5]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Company News']")
	MobileElement companyNewsTILE;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[6]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Human Resources']")
	MobileElement humanResourcesTILE;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[7]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Organizational Chart']")
	MobileElement organizationChartTILE;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[8]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Payroll']")
	MobileElement payrollTILE;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[8]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Holiday List']")
	MobileElement holidayListTILE;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[9]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Medical Insurance']")
	MobileElement medicalInsuranceTILE;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[10]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Timesheet']")
	MobileElement timesheetTILE;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[11]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Expense Reporting']")
	MobileElement expenseReportingTILE;


	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[12]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Travel Desk']")
	MobileElement travelDeskTILE;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[13]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Meetings']")
	MobileElement meetingsTILE;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[13]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Performance Review']")
	MobileElement performanceReviewTILE;

	@AndroidFindBy(xpath= "(//android.widget.TextView[@content-desc='tile_name'])[14]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Attendance']")
	MobileElement attadanceTILE;



	@AndroidFindBy(xpath= "//android.widget.TextView[@text='My Places']")
	@iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name='My Places']")
	MobileElement myPlacesTEXT;



	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'[Your Company Content]')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Stay tuned')]")
	MobileElement companyContentTEXT;





	public MyPlacesPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	//	public void clickDismiss() {
	//		dismissBTN.click();
	//	}
	public MobileElement verifyMyPlacesHeader() {
		return MyPlacesHeaderTEXT;
	}
	public MobileElement verifySupportHeader() {
		return supportTitleTEXT;
	}
	public MobileElement verifyLearningHeader(){
		return learningTitleTEXT;
	}
	public MobileElement verifyCompanyHeader(){
		return companyTitleTEXT;
	}
	public MobileElement verifyServicesHeader(){
		return servicesTitleTEXT;
	}
	public MobileElement verifySupportDescription(){
		return supportDescriptionTEXT;
	}
	public MobileElement verifyLearningDescription(){
		return learningDescriptionTEXT;
	}
	public MobileElement verifyCompanyDescription(){
		return companyDescriptionTEXT;
	}
	public MobileElement verifyServicesDescription(){
		return servicesDescriptionTEXT;
	}
	public MobileElement verifyFirstSupportTile(){
		return supportCornerTILE;
	}
	public MobileElement verifySecondSupportTile(){
		return knowledgeBaseTILE;
	}
	public MobileElement verifyThirdSupportTile(){
		return crisisManagementTILE ;
	}

	public MobileElement verifyFourthSupportTile(){
		return emerergencyContactsTILE;
	}
	public MobileElement verifyFifthSupportTile(){
		return CSRTILE;
	}
	public MobileElement verifySixthSupportTile(){
		return feedbackTILE;
	}
	public MobileElement verifyFirstLearningTile(){
		return universityTILE;
	}
	public MobileElement verifySecondLearningTile(){
		return blockTILE;
	}
	public MobileElement verifyThirdLearningTile(){
		return techTalkTILE;
	}
	public MobileElement verifyFourthLearningTile(){
		return onlineTrainingTILE;
	}
	public MobileElement verifyFirstCompanyTile(){
		return companyNewsTILE;
	}
	public MobileElement verifySecondCompanyTile(){
		return humanResourcesTILE;
	}
	public MobileElement verifyThirdCompanyTile(){
		return organizationChartTILE;
	}
	public MobileElement verifyFourthCompanyTile(){
		return payrollTILE;
	}
	public MobileElement verifyFifthCompanyTile(){
		return holidayListTILE;
	}
	public MobileElement verifySixthCompanyTile(){
		return medicalInsuranceTILE;
	}
	public MobileElement verifyFirstServicesTile(){
		return timesheetTILE;
	}
	public MobileElement verifySecondServicesTile(){
		return expenseReportingTILE;
	}
	public MobileElement verifyThirdServicesTile(){
		return travelDeskTILE;
	}
	public MobileElement verifyFourthServicesTile(){
		return meetingsTILE;
	}
	public MobileElement verifyFifthServicesTile(){
		return performanceReviewTILE;
	}
	public MobileElement verifySixthServicesTile(){
		return attadanceTILE;
	}
	public MobileElement verifyCompanyContentLabel(){
		return companyContentTEXT;
	}
	public MobileElement clickKknowledgeBaseButton(){
		return knowledgeBaseButton;
	}
	public MobileElement clickSupportCornerButton(){
		return supportCornerButton;
	}
	public MobileElement clickCrisisManagemnetButton(){
		return crisisManagementButton;
	}
	public MobileElement clickKCompanyNewsButtonButton(){
		return companyNewsButton;
	}

}
