package com.apd.inteliserve.pompages;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class FavouriteModulesPage {

	AppiumDriver<MobileElement> driver;



	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Knowledge')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='01']")
	MobileElement knowledgeBaseBTN;

	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Company')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='02']")
	MobileElement companyNewsBTN;
	
	@AndroidFindBy(xpath= "//android.widget.TextView[contains(@text,'Support')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='03']")
	MobileElement coreSupportButton;
	

	
	@AndroidFindBy(xpath= "//android.widget.EditText[contains(@text,'.com')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='TopBrowserBar']")
	//@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Address']")
	MobileElement currentURL;


	@AndroidFindBy(xpath= "//android.widget.EditText[contains(@text,'.com')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@name='URL']")
	MobileElement getCurrentURL;

	@AndroidFindBy(id= "com.ksmobile.cb:id/bl")
	MobileElement currentTitle;



	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Support Corner']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Support Corner']")
	MobileElement supportHeaderTEXT;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Ask I.T.']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Ask I.T.']")
	MobileElement askITTEXT;

	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Multi-channel support')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Multi-channel support')]")
	MobileElement askItContentTEXT;


	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Email']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='01']")
	

	MobileElement emailBTN;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Chat']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='02']")
	MobileElement chatButton;

	@AndroidFindBy(xpath= "//android.widget.TextView[@text='Phone']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@label='03']")
	MobileElement callButton;

	@AndroidFindBy(xpath= "//android.view.View[contains(@text,'menu toggle button.')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='toggle menu button']")
	MobileElement toggleBTN;

	//@AndroidFindBy(id= "com.google.android.gm:id/to")
	@AndroidFindBy(xpath= "//android.widget.MultiAutoCompleteTextView[contains(@text,'.com')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'.com')]") 
	MobileElement toEmailTEXT;

	
	//@AndroidFindBy(id= "com.google.android.dialer:id/digits")
	@AndroidFindBy(id= "com.google.android.dialer:id/digits")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name,'+18559361210')]")
	MobileElement supportNumber;

	@AndroidFindBy(xpath= "//android.view.ViewGroup[@content-desc='back']")
	@iOSXCUITFindBy(id = "back")
	MobileElement backButton;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Hello')]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Hello')]")
	MobileElement helloTEXT;






	public FavouriteModulesPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	public MobileElement clickKnowledgeBaseButton(){
		return knowledgeBaseBTN;
	}
	public MobileElement clickCompanyNewsButton(){
		return companyNewsBTN;
	}
	public MobileElement clickCoreSupportButton(){
		return coreSupportButton;
	}

	public void clickWebTitle(){
		currentTitle.click();
	}
	public MobileElement getURL(){
		return currentURL;
	}
	public MobileElement getCurrentURL(){
		return getCurrentURL;
	}
	public MobileElement verifyHeaderText(){
		return supportHeaderTEXT;
	}
	public MobileElement verifyItText(){
		return askITTEXT;
	}
	public MobileElement verifyItContentText(){
		return askItContentTEXT;
	}
	public MobileElement emailButton(){
		return emailBTN;
	}
	public MobileElement chatButton(){
		return  chatButton;
	}
	public MobileElement callButton(){
		return callButton;
	}
	public MobileElement toggleButton(){
		return toggleBTN;
	}
	public MobileElement getToEmailText(){
		return toEmailTEXT;
	}
	public MobileElement getSupportNumber(){
		return supportNumber;
	}
	public void clickBackButton(){
		backButton.click();
	}
	public MobileElement verifyHelloText(){
		return helloTEXT;
	}
}

