package com.apd.inteliserve.general;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

public class EmailExtentReport {
	//@Parameters({"platform"})
	public void generateEmailExtentReport() {                                                
		WebDriver driver=null;
		WebElement elementdashboard;
		String reportPath ="";
		String destinationPath="";
		String phantomJSExePath="";
		// WebElement elementPassPercentage;
		// WebElement elementTests;
		// WebElement elementSteps;
		// WebElement elementTotalTests;
		// WebElement elementTotalTime;
		// WebElement elementTotalSteps;
		// WebElement elementOverallTime;
		//        if(platform.equalsIgnoreCase("ios")){
		//            reportPath="/Users/iosbuilduser/Desktop/jenkins/workspace/InteliServe/unisys-inteliapp-ios-automation/Unisys-Demo-InteliApp/ExtentReports/InteliServe Mobile Test Automation Report.html";
		//            destinationPath="/Users/iosbuilduser/Desktop/jenkins/workspace/InteliServe/unisys-inteliapp-ios-automation/Unisys-Demo-InteliApp/ExtentReports/";
		//            phantomJSExePath = "/Users/iosbuilduser/Desktop/jenkins/workspace/InteliServe/unisys-inteliapp-ios-automation/Unisys-Demo-InteliApp/src/test/resources/phantomjs";
		//        }
		//        else{
		reportPath="/Users/iosbuilduser/Desktop/jenkins/workspace/InteliServe/unisys-inteliapp-android-automation/Unisys-Demo-InteliApp/ExtentReports/InteliServe Mobile Test Automation Report.html";
		destinationPath="/Users/iosbuilduser/Desktop/jenkins/workspace/InteliServe/unisys-inteliapp-android-automation/Unisys-Demo-InteliApp/ExtentReports/";
		phantomJSExePath = "/Users/iosbuilduser/Desktop/jenkins/workspace/InteliServe/unisys-inteliapp-android-automation/Unisys-Demo-InteliApp/src/test/resources/phantomjs";

		//  }
		try {
			if (!new File(reportPath).exists()) {
				throw new Exception("Report not found at " + reportPath);
			}
			if (!new File(destinationPath).exists()) {
				throw new Exception("Destination folder not found at " + destinationPath);
			}
			if (!new File(phantomJSExePath).exists()) {
				throw new Exception("PhantomJS Exe not found on the given path - " + phantomJSExePath);
			}

			System.out.println("Report Path - " + reportPath);
			System.out.println("Destination Folder - " + destinationPath);
			System.out.println("PhantomJS Path - " + phantomJSExePath);

			System.setProperty("phantomjs.binary.path",phantomJSExePath);

			System.out.println("Creating Phantom Instance...");
			driver = new PhantomJSDriver();                                                                          
			String url="file:///" + reportPath;
			System.out.println(url);
			driver.get(url);                                                                       
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			System.out.println("Navigating to Dashboard...");
			driver.findElement(By.xpath("//a[@class='dashboard-view']")).click();                                

			System.out.println("Getting snapshot of Report sections...");
			elementdashboard= driver.findElement(By.xpath("//div[@id='dashboard-view']"));
			// elementTotalTests= driver.findElement(By.xpath("//div[@class='card suite-total-tests']")); 
			// elementTotalSteps= driver.findElement(By.xpath("//div[@class='card suite-total-steps']")); 
			// elementTotalTime= driver.findElement(By.xpath("//div[@class='card suite-total-time-current']"));
			// elementOverallTime= driver.findElement(By.xpath("//div[@class='card suite-total-time-overall']"));
			// elementTests= driver.findElement(By.xpath("(//div[@class='card-panel'])[1]"));            
			// elementSteps= driver.findElement(By.xpath("(//div[@class='card-panel'])[2]"));
			// elementPassPercentage= driver.findElement(By.xpath("(//div[@class='card-panel'])[3]")); 

			takeSnapShot(driver, destinationPath + "dashboard.png", elementdashboard);
			// takeSnapShot(driver, destinationPath + "totalsteps.png", elementTotalSteps);
			// takeSnapShot(driver, destinationPath + "totaltestsimage.png", elementTotalTests);
			// takeSnapShot(driver, destinationPath + "totaltimeimage.png", elementTotalTime);
			// takeSnapShot(driver, destinationPath + "overalltimeimage.png", elementOverallTime);
			// takeSnapShot(driver, destinationPath + "passpercentageimage.png", elementPassPercentage);
			// takeSnapShot(driver, destinationPath + "testsimage.png", elementTests);
			// takeSnapShot(driver, destinationPath + "stepsimage.png", elementSteps);

			System.out.println("Creating HTML file...");
			File resultFile = new File(destinationPath + "emailreport.html");
			PrintWriter writer = new PrintWriter(resultFile);
			writer.write("<html>\n");
			writer.append("<body>\n");
			writer.append("<img src=\"dashboard.png\"/>\n");
			// writer.append("<div style='width:1340px;background-color:black;'>");
			// writer.append("<div style='padding: 20px';>");
			// writer.append("<img src=\"totaltestsimage.png\"/>&nbsp&nbsp&nbsp<img src=\"totalsteps.png\"/>&nbsp&nbsp&nbsp<img src=\"finalreportname.png\"/>&nbsp&nbsp&nbsp<img src=\"totaltimeimage.png\"/>&nbsp&nbsp&nbsp<img src=\"overalltimeimage.png\"/>\n");
			// writer.append("<img src=\"testsimage.png\"/>&nbsp&nbsp&nbsp<img src=\"stepsimage.png\"/>&nbsp&nbsp&nbsp<img src=\"passpercentageimage.png\"/>\n");
			// writer.append("</div>\n");
			// writer.append("</div>\n");
			writer.append("</body>\n");
			writer.append("</html>\n");
			writer.close();
			System.out.println("Email Report Generation Complete!");
		}
		catch (NoSuchElementException e)
		{
			System.out.println("Error with XPath. Please check the XPath values");
			e.printStackTrace();
		}
		catch (Exception e) {
			System.out.println("Error Message = " + e.getMessage());
			e.printStackTrace();
		}finally{
			driver.quit();
		}
	}

	public static void takeSnapShot(WebDriver webdriver,String destinationPath,WebElement webElement) {

		try {
			TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
			File srcFile = scrShot.getScreenshotAs(OutputType.FILE);                                      //storing screenshot in srcfile
			BufferedImage fullImg = ImageIO.read(srcFile);

			Point point = webElement.getLocation();                                                      // getting upper left corner of particular section
			int width = webElement.getSize().getWidth();                                                 //find width from that corner
			int height = webElement.getSize().getHeight();                                              // find height from that corner
			BufferedImage ElementScreenshot = fullImg.getSubimage(point.getX(), point.getY(), width, height);//getting subimage from complete webpage
			ImageIO.write(ElementScreenshot, "png", srcFile);
			File DestFile = new File(destinationPath);                                                       //create the png file in particular folder
			FileUtils.copyFile(srcFile, DestFile);
		} catch (WebDriverException e) {
			System.out.println("webdriver Exception");
		} catch (IOException e){
			System.out.println("InputOutput Exception");
		}

	}
}
