package com.apd.inteliserve.general;
import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import com.relevantcodes.extentreports.ExtentReports;

/**
 * @author  VishalMadhur
 *
 */
public class Lib implements IAutoConstant {
	public static Workbook wb;
	static String propertyValue ="";
	static Properties prop=new Properties();


	//@Parameters({"platform"})
	public static ExtentReports generateExtentReport() {
		ExtentReports extent =null;
		try {
			//Date d = new Date();
			//String currentDate = d.toString().replaceAll(":","_");
			//extent=new ExtentReports(System.getProperty("user.dir")+"/LambWeston-ExtentReport/"+"InteliServe Test Automation Report" +"_"+ currentDate + ".html", false);
			//if(platform.equalsIgnoreCase("ios")){
//				extent=new ExtentReports(System.getProperty("user.dir")+"/ExtentReports/"+"InteliServe Mobile Test Automation Report.html");
//				extent.loadConfig(new File(System.getProperty("user.dir")+""+REPORTS_CONFIG_PATH+"")); 
//				extent.addSystemInfo("Client, Release Version", "Unisys, 3.1.0").addSystemInfo("Platform - Name, Version", "iOS, 13.6").addSystemInfo("Device - Type, Name, Screen Size", "Simulator, iPhone 11, 6.1").addSystemInfo("User Name", "Vishal Madhur");
			//}
			//else{
				extent=new ExtentReports(System.getProperty("user.dir")+"/ExtentReports/"+"InteliServe Mobile Test Automation Report.html");
				extent.loadConfig(new File(System.getProperty("user.dir")+""+REPORTS_CONFIG_PATH+"")); 
				extent.addSystemInfo("Client, Release Version", "Unisys, 3.2.0").addSystemInfo("Platform - Name&Version", "Android-9, iOS-13.5").addSystemInfo("Device - Type&Screen Size", "Emulator-6.0, Simulator-6.1").addSystemInfo("User Name", "Vishal Madhur");
			//}
			//extent.addSystemInfo("Appium Version", "1.20.0").addSystemInfo("Platforms", "Android & iOS").addSystemInfo("Android Version", "9-Pie").addSystemInfo("iOS Version", "13.4").addSystemInfo("Android-Screen-Size", "5.93").addSystemInfo("iOS-Screen-Size", "5.80").addSystemInfo("Build", "34").addSystemInfo("Version", "1.0.0").addSystemInfo("User Name", "Vishal Madhur");
		}catch(Exception e) {
		}
		return extent;
	}

	public static String getCellValue(String sheet,int row,int colomn){
		String cellValue ="";
		try {
			wb = WorkbookFactory.create(new FileInputStream(EXCEL_PATH));
			cellValue =wb.getSheet(sheet).getRow(row).getCell(colomn).toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cellValue;
	}
	public static int getRowCount(String sheet){
		int rowCount =0;
		try {
			wb = WorkbookFactory.create(new FileInputStream(EXCEL_PATH));
			rowCount =wb.getSheet(sheet).getLastRowNum();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rowCount;
	}


	public static String captureScreenshots(WebDriver driver,String testCaseName){
		Date d = new Date();
		String currentDate = d.toString().replaceAll(":","_");
		String screenShotPath="./ExtentReports/screenshots/" +testCaseName+"_"+currentDate+".png";
		//		String screenShotPath = System.getProperty("user.dir") + "/screenshots/" +testCaseName+"_"+currentDate+".png";
		try{
			TakesScreenshot ts = (TakesScreenshot)driver;
			File srcFile=ts.getScreenshotAs(OutputType.FILE);
			File destFile = new File(screenShotPath);
			FileUtils.copyFile(srcFile, destFile);
		}catch(Exception e){
			e.printStackTrace();
		}
		return screenShotPath;
	}

	public static String captureElementScreenshots(AppiumDriver<MobileElement> driver){
		Date d = new Date();
		String currentDate = d.toString().replaceAll(":","_");
		String screenShotPath="./ExtentReports/screenshots/"+currentDate+".png";
		try{
			TakesScreenshot ts = (TakesScreenshot)driver;
			File srcFile=ts.getScreenshotAs(OutputType.FILE);
			File destFile = new File(screenShotPath);
			FileUtils.copyFile(srcFile, destFile);
		}catch(Exception e){
			e.printStackTrace();
		}
		return screenShotPath;
	}

	public static String captureWebElementScreenshots(WebDriver driver){
		Date d = new Date();
		String currentDate = d.toString().replaceAll(":","_");
		String screenShotPath="./ExtentReports/screenshots/"+currentDate+".png";
		try{
			TakesScreenshot ts = (TakesScreenshot)driver;
			File srcFile=ts.getScreenshotAs(OutputType.FILE);
			File destFile = new File(screenShotPath);
			FileUtils.copyFile(srcFile, destFile);
		}catch(Exception e){
			e.printStackTrace();
		}
		return screenShotPath;
	}

	public static String getPropertyValue(String key){
		try{
			prop.load(new FileInputStream(CONFIG_PATH));
			propertyValue=prop.getProperty(key);
		}catch(Exception e){
			e.printStackTrace();
		}
		return propertyValue;
	}

	public static String getElementPropertyValue(String key){
		try{
			prop.load(new FileInputStream(LOCATOR_PROPERTIES_PATH));
			propertyValue=prop.getProperty(key);
		}catch(Exception e){
		}
		return propertyValue;
	}
}

