package com.apd.inteliserve.general;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.service.local.AppiumDriverLocalService;


/**
 * @author  VishalMadhur
 *
 */
public class BaseTest extends Keyword{
	//	public  static ExtentReports extent;
	//	public  static ExtentTest extentTest;
	public  AppiumDriverLocalService service;
	public String concatenate=".";

	@BeforeSuite(alwaysRun = true)
	//@Parameters({"platform"})
	public void setReport() {
		deleteOldScreenshots();
		extent=Lib.generateExtentReport();
	}


	@BeforeMethod(alwaysRun =true)
	@Parameters({"platform"})
	public  void setEnvironmentAndOpenApplication(@Optional String platform) throws Exception {
		try{
			//			service = AppiumDriverLocalService.buildDefaultService();
			//			service.start();


			switch (platform.toLowerCase()) {
			case "ios":
				try {
					Thread.sleep(3000);
					iosLaunch();
					Thread.sleep(2000);
					login(platform);
				}catch(Exception e) {
					e.printStackTrace();
					Reporter.log("Failed to start Test in iOS",true);
				}
				break;
			case "android":
				try {
				//	Thread.sleep(5000);
					Thread.sleep(3000);
					androidLaunch();
					Thread.sleep(5000);
					login(platform);
				}catch(Exception e) {
					e.printStackTrace();
					Reporter.log("Failed to start Test in Android",true);
				}
				break;
			}

		}catch(Exception e){
			e.printStackTrace();
			Reporter.log("Failed to start Test Parallely",true);
			Assert.fail();
		}
	}

	@AfterMethod(alwaysRun = true)
	public void getResultStatus(ITestResult result) throws InterruptedException
	{
		try {
			Reporter.log("---------------------------------",true);
			Reporter.log("Test Completed",true);
			if(result.getStatus()==ITestResult.FAILURE){
				extentTest.log(LogStatus.FAIL, "TEST CASE FAILED IS "+result.getName()); 
				extentTest.log(LogStatus.ERROR, result.getThrowable());
				String screenshotPath = concatenate+Lib.captureScreenshots(driver, result.getName());
				extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
				extent.endTest(extentTest);
			}
			else if(result.getStatus()==ITestResult.SKIP){
				extentTest.log(LogStatus.SKIP, "Test Case SKIPPED IS " + result.getName());
				extent.endTest(extentTest);
			}
			else if(result.getStatus()==ITestResult.SUCCESS){
				extentTest.log(LogStatus.PASS, "Test Case PASSED IS " + result.getName());
				extent.endTest(extentTest);
			}
		}
		finally{
			driver.quit();
		}
	}

	@AfterSuite(alwaysRun=true)
	//@Parameters({"platform"})
	public void closeApplicationAndEndReport() {

		try{
			Reporter.log("Flushing Report" ,true);
		}
		finally{
			//for (String s : Reporter.getOutput()) { 
			//	extent.setTestRunnerOutput(s); 
			//}
			extent.flush();
			Reporter.log("Report Generated" ,true);
			extentReportZip();
			//extent.close();
			//service.stop();
		}
		try{
			EmailExtentReport e = new EmailExtentReport();
			e.generateEmailExtentReport();
		}
		catch(Exception e){
			e.printStackTrace();
			Reporter.log("Failed to Generate Email Extent Report", true);
		}


	}
}