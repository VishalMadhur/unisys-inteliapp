package com.apd.inteliserve.general;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.zeroturnaround.zip.ZipUtil;

import com.apd.inteliserve.pompages.LoginPage;
import com.apd.inteliserve.pompages.LandingPage;
import com.apd.inteliserve.pompages.ProfileScreenLegalPage;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.offset.ElementOption;

public class Keyword  extends Lib{
	public  static ExtentReports extent;
	public  ExtentTest extentTest;
	public  AppiumDriver<MobileElement> driver;
	DesiredCapabilities capabilities = new DesiredCapabilities();
	String url ="http://127.0.0.1:4723/wd/hub";
	public String concatenate=".";

	public boolean elementIsDisplayed(MobileElement element)  {
		try {
			element.isDisplayed();
		} catch (Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.ERROR, e.getMessage());
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			return false;
		}
		return true;
	}  

	public boolean elementDisplayed(MobileElement element)  {
		try {
			element.isDisplayed();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void click(MobileElement element)  {
		try {
			element.click();
		} catch (Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.FAIL, e.getMessage());
			Assert.fail("Failed to Perform Click Operation");
		}
	} 

	public boolean clickElement(MobileElement element)  {
		try {
			element.click();
		} catch (Exception e) {
			e.printStackTrace();
			extentTest.log(LogStatus.ERROR, e.getMessage());
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.FAIL, "Failed to click on element");
			return false;
		}
		return true;
	} 

	public  void sendExcelTestData(MobileElement element,String sheet,int row,int colomn) throws Exception{
		try{
			element.sendKeys(Lib.getCellValue(sheet, row, colomn));
		}catch(Exception e){
			e.printStackTrace();
			extentTest.log(LogStatus.ERROR, e.getMessage());
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
		}
	}

	public  void sendTestData(MobileElement element,String data) throws Exception{
		try{
			element.sendKeys(data);
		}catch(Exception e){
			e.printStackTrace();
			extentTest.log(LogStatus.ERROR, e.getMessage());
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
		}
	}

	public  List<MobileElement> getElements(List<MobileElement> element){
		try{
			return element;
		}catch(Exception e){
			e.printStackTrace();
			extentTest.log(LogStatus.ERROR, e.getMessage());
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.FAIL, "Failed to Click on Element");

		}
		return null;
	}

	public String getText(MobileElement element){
		try{
			return element.getText();
		}catch(Exception e){
			e.printStackTrace();
			extentTest.log(LogStatus.ERROR, e.getMessage());
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.FAIL, "Failed to Get Text of Element");
		}
		return null;
	}

	public String getText(MobileElement element,@Optional String platform){
		try{
			if(platform.equals("andriod")){
				return element.getText();
			}
			else {
				return element.getAttribute("name");
			}
		}catch(Exception e){
			e.printStackTrace();
			extentTest.log(LogStatus.ERROR, e.getMessage());
			String screenshotPath = Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.FAIL, "Failed to Get Text of Element");
		}
		return null;
	}


	public String getAttribute(MobileElement element, String value){
		try{
			return element.getAttribute(value);
		}catch(Exception e){
			e.printStackTrace();
			extentTest.log(LogStatus.ERROR, e.getMessage());
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.FAIL, "Failed to Get Text of Element");
		}
		return null;
	}
	public boolean matchData(String expectedLabel, String actualLabel){
		try{
			Assert.assertEquals(expectedLabel,actualLabel);
			//Assert.assertTrue(expectedLabel.contains(actualLabel));
			return true;
		}catch(AssertionError e){
			e.printStackTrace();
			extentTest.log(LogStatus.ERROR, e.getMessage());
		}
		return false;
	}

	public boolean containsData(String actualLabel, String expectedLabel){
		try{
			Assert.assertTrue(actualLabel.contains(expectedLabel));
			return true;
		}catch(AssertionError e){
			e.printStackTrace();
			extentTest.log(LogStatus.ERROR, "Expected " +expectedLabel +" but found " + actualLabel);
		}
		return false;
	}

	public void verifyLabel(String component, boolean expression){
		if(expression){
			extentTest.log(LogStatus.PASS, component +" Label is  Dispalyed Correctly");	
		}
		else{
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.FAIL, component +" Label is Not  Dispalyed Correctly");	
		}
	}

	public void verifyData(String actualData,String component, boolean expression){
		if(expression){
			extentTest.log(LogStatus.PASS, component +"  is  Dispalyed Correctly ---------" + actualData);	
		}
		else{
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.FAIL, component +"is Not  Dispalyed Correctly -----------" + actualData);	
		}
	}

	public void verifyElement(String component, boolean expression){
		if(expression){
			extentTest.log(LogStatus.PASS, component +"  is  Dispalyed");	
		}
		else{
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.FAIL, component +"is Not  Dispalyed");	
		}
	}

	public void verifyElementNotDisplayed(String component, boolean expression){
		if(!expression){
			extentTest.log(LogStatus.PASS, component +"  is Not Dispalyed");	
		}
		else{
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.FAIL, component +"is Dispalyed");	
		}
	}


	public void clickOnElement(String component, boolean expression){
		if(expression){
			extentTest.log(LogStatus.PASS,"Clicked on " + component );	
		}
		else{
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.FAIL, "Failed to click on " + component );	
		}
	}

	public void waitForVisibityOfSpecificElement(String component,MobileElement element){
		try{
			WebDriverWait wait = new WebDriverWait(driver, 240);
			wait.until(ExpectedConditions.visibilityOf(element));
			extentTest.log(LogStatus.PASS, component +" Found Successfully");
		}catch(Exception e){
			e.printStackTrace();
			String screenshotPath = Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.FAIL, component +" Not Found");
		}
	}

	public void iosLaunch(){

		String appPath="/Users/iosbuilduser/Library/Developer/Xcode/DerivedData/InteliMobileapp-alspjxikxwnbpsavkbhkeyznmyun/Build/Products/Debug-iphonesimulator/InteliApp.app";
		//String appPath="Users/iosbuilduser/Library/Developer/Xcode/DerivedData/InteliMobileapp-dqtjkusftefalkeqonccbaebjxth/Build/Products/Debug-iphonesimulator/InteliApp.app";
		capabilities.setCapability("platformName", "iOS");
		capabilities.setCapability("platformVersion", "13.5");
		capabilities.setCapability("deviceName", "iPhone 11");
		//capabilities.setCapability("language", "en");
		//capabilities.setCapability("locationServicesEnabled", true);
		//capabilities.setCapability("locationServicesAuthorized", true);
		capabilities.setCapability("app", appPath);
		try{
			driver = new IOSDriver<MobileElement>(new URL(url), capabilities);
		}catch(Exception e) {
			e.printStackTrace();
			Reporter.log("Failed to Launch App on iOS",true);
			Assert.fail("Failed to Launch App on iOS");
		}
	}


	public void androidLaunch() {
		try {
			String device=Lib.getPropertyValue("DEVICENAME");
			//String udidNumber=Lib.getPropertyValue("UDID");
			String version=Lib.getPropertyValue("PLATFORMVERSION");
			capabilities.setCapability("deviceName",device);
			//capabilities.setCapability("udid", udidNumber);
			capabilities.setCapability("platformName","Android");
			capabilities.setCapability("platformVersion", version); 
			capabilities.setCapability("appPackage", "com.unisys.inteliapp");
			capabilities.setCapability("appActivity","com.unisys.inteliapp.MainActivity");
			//capabilities.setCapability("appPackage", "com.unisys.cushmanwakefield.inteliapp");
			//capabilities.setCapability("appActivity","com.unisys.cushmanwakefield.inteliapp.MainActivity");
			//capabilities.setCapability("appWaitPackage","com.unisys.inteliapp.MainActivity");
			//capabilities.setCapability("appWaitDuration","60000");
			//capabilities.setCapability("systemPort","8201");
			driver = new AndroidDriver<MobileElement>(new URL(url), capabilities);
		}catch(Exception e) {
			e.printStackTrace();
			Reporter.log("Failed to Launch App on Android",true);
			Assert.fail("Failed to Launch App on Android");
		}
	}

	
	@Parameters({"platform"})
	public void login(@Optional String platform) throws Exception  {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			LoginPage lp2 = new LoginPage(driver);
			LandingPage ld = new LandingPage(driver);
			//lp2.clickAllowButton();
			String email =Lib.getPropertyValue("Email");
			if(platform.equalsIgnoreCase("ios")){
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//XCUIElementTypeTextField[@value='Enter Email Address']")));
			}
			else{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.EditText[@text='Enter Email Address']")));
			}
			lp2.setEmail(email);
			lp2.clickNextButton();
			//	String firstName =Lib.getPropertyValue("FirstName");
			String password =Lib.getPropertyValue("Password");
			lp2.setPassword(password);
			//	lp2.setFirstName(firstName);
			//	String lastName =Lib.getPropertyValue("LastName");
			Thread.sleep(2000);
			//	lp2.setLastname(lastName);
			lp2.clickNextButton();
			//extentTest.log(LogStatus.PASS, "Login Happend Successfully");
			//lp2.clickLogin();
			Thread.sleep(10000);
			int checkLogin = 20;    
			for (int i = 0; i < checkLogin; i++) {    
				if (elementDisplayed(ld.homeIcon())) {
					Reporter.log("Login Successfull",true);
					break;
				} else { 
					Reporter.log("Login Failed",true);
					Thread.sleep(5000);
					lp2.clickNextButton();
					password =Lib.getPropertyValue("Password");
					lp2.setPassword(password);
					Thread.sleep(2000);
					lp2.clickNextButton();   
				}     
			}
		}catch(Exception e) {
			e.printStackTrace();
			Reporter.log("Failed to Login",true);
		}
	}


	public void loginWithOutEnablingLocation() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			LoginPage lp2 = new LoginPage(driver);
			String email =Lib.getPropertyValue("Email");
			lp2.setEmail(email);
			String firstName =Lib.getPropertyValue("FirstName");
			lp2.setFirstName(firstName);
			String lastName =Lib.getPropertyValue("LastName");
			lp2.setLastname(lastName);
			lp2.clickLogin();
		}catch(Exception e) {
			e.printStackTrace();
			Reporter.log("Failed to Login",true);
		}
	}

	@Parameters({"platform"})
	public void logOut(@Optional String platform){
		try{
			ProfileScreenLegalPage pl= new ProfileScreenLegalPage(driver);
			click(pl.profileIcon());
			Thread.sleep(2000);
			scroll(platform,"Sign Out");


			//			RemoteWebElement element = (RemoteWebElement)driver. findElement(By.name("Sign Out"));
			//			String elementID = element.getId();
			//			HashMap<String, String> scrollObject = new HashMap<String, String>();
			//			scrollObject.put("element", elementID);
			//			scrollObject.put("toVisible", "not an empty string");
			//			driver.executeScript("mobile:scroll", scrollObject);

			if(elementIsDisplayed(pl.signOutLink())){
				click(pl.signOutLink());
			}
			click(pl.confirmButton());
			extentTest.log(LogStatus.PASS, "Logout Happend Successfully");
		}catch(Exception e) {
			e.printStackTrace();
			Reporter.log("Failed to logout",true);
		}
	}


	@Parameters({"platform"})
	public void scroll(@Optional String platform,@Optional String visibleText){
		switch (platform.toLowerCase()) {
		case "android":
			try{
				driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+visibleText+"\").instance(0))"));
				extentTest.log(LogStatus.PASS, "Scroll Function is Working Properly");
			}catch(Exception e){
				e.printStackTrace();
				Reporter.log("Failed to scroll in android",true);
			}
			break;
		case "ios":
			try{
				RemoteWebElement element = (RemoteWebElement)driver.findElement(By.className("XCUIElementTypeOther"));
				String elementID = element.getId();
				HashMap<String, String> scrollObject = new HashMap<String, String>();
				scrollObject.put("element", elementID); // Only for ‘scroll in element’
				scrollObject.put("direction", "down");
				driver.executeScript("mobile:scroll", scrollObject);
				extentTest.log(LogStatus.PASS, "Scroll Function is Working Properly");

				//				RemoteWebElement parent = (RemoteWebElement)driver.findElement(By.className("XCUIElementTypeOther"));
				//				String parentID = parent.getId();
				//				HashMap<String, String> scrollObject = new HashMap<String, String>();
				//				scrollObject.put("Sign Out", parentID);
				//				scrollObject.put("name",visibleText);
				//				scrollObject.put("direction", "down");
				//				driver.executeScript("mobile:scroll", scrollObject);

			}catch(Exception e){
				e.printStackTrace();
				Reporter.log("Failed to scroll in ios",true);
			}
			break;
		}	
	}

	//	public void iOSScroll(String elementName){
	//		try{
	//			RemoteWebElement parent = (RemoteWebElement)driver.findElement(By.className("XCUIElementTypeOther"));
	//			String parentID = parent.getId();
	//			HashMap<String, String> scrollObject = new HashMap<String, String>();
	//			scrollObject.put("element", parentID);
	//			scrollObject.put("name",elementName );
	//			driver.executeScript("mobile:scroll", scrollObject);
	//			extentTest.log(LogStatus.PASS, "Scroll Function is Working Properly");
	//		}catch(Exception e){
	//			e.printStackTrace();
	//		}
	//	}

	@SuppressWarnings("rawtypes")
	public void swipe(MobileElement element1, MobileElement element2){
		try{
			TouchAction action=new TouchAction((MobileDriver)driver);
			action.longPress(ElementOption.element(element1)).moveTo(ElementOption.element(element2)).release().perform();
			extentTest.log(LogStatus.PASS, "Swipe Bar is Working Properly");
		}catch(Exception e){
			e.printStackTrace();
			extentTest.log(LogStatus.ERROR, e.getMessage());
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.FAIL, "Failed to Perform Swipe Action");
		}
	}

	@SuppressWarnings("rawtypes")
	public void swipe1(MobileElement element1){
		try{
			TouchAction action=new TouchAction((MobileDriver)driver);
			action.longPress(ElementOption.element(element1)).moveTo(ElementOption.element(element1,1250,1250)).release().perform();
			extentTest.log(LogStatus.PASS, "Swipe Bar is Working Properly");
		}catch(Exception e){
			e.printStackTrace();
			extentTest.log(LogStatus.ERROR, e.getMessage());
			String screenshotPath = concatenate+Lib.captureElementScreenshots(driver);
			extentTest.log(LogStatus.INFO,extentTest.addScreenCapture(screenshotPath));
			extentTest.log(LogStatus.FAIL, "Failed to Perform Swipe Action");
		}
	}

	public void extentReportZip() {
		try{
			ZipUtil.pack(new File("./ExtentReports"), new File("./ExtentReports.zip"));
			System.out.println("Folder zipped successfully");
			System.out.println("=======================");
		}catch(Exception e){
			System.err.println("Failed to Unzip");
			System.out.println("=======================");
		}
	}

	public void deleteOldScreenshots(){
		File folder = new File("./ExtentReports/screenshots");
		File fList[] = folder.listFiles();

		for (File f : fList) {
			if (f.getName().endsWith(".png") || f.getName().endsWith(".html")) {
				f.delete(); 
			}
		}
	}
}
