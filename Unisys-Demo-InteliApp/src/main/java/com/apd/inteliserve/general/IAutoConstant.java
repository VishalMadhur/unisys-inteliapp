package com.apd.inteliserve.general;

/**
 * @author  VishalMadhur
 *
 */
public interface IAutoConstant {
	String EXCEL_PATH="./src/test/resources/inputdata.xlsx";
	String CONFIG_PATH="./src/test/resources/config.properties";
	String LOCATOR_PROPERTIES_PATH="./src/test/resources/elementLocator.properties";
	String REPORTS_CONFIG_PATH="/src/test/resources/ReportsConfig.xml";
	String TESTNG_FILE_PATH="./testng.xml";
	String TESTSUITE_EXCEL_PATH="./src/test/resources/testsuite.xlsx";
}